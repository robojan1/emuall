
#include <GBAemu/gba.h>
#include <GBAemu/defines.h>
#include <GBAemu/cpu/cpu.h>
#include <GBAemu/cpu/armException.h>
#include <GBAemu/cpu/hostInstructions.h>

#include <emuall/util/log.h>
#include <emuall/util/savedata.h>

Cpu::Cpu(Gba &gba) :
	_system(gba), _cpsr(0), _spsr(0), _hostFlags(0), _activeMode(Mode::Supervisor)
{
}

Cpu::~Cpu()
{

}

void Cpu::Init() {
	Reset();

	if(_system.GetOptionBool(OPTIONID_DETECTABLE)) {
		RegisterSWI(0x30, std::bind(&Cpu::SWI_write, this, std::placeholders::_1, std::placeholders::_2));
		RegisterSWI(0x31, std::bind(&Cpu::SWI_exitEmulator, this, std::placeholders::_1));
	}
}

void Cpu::LoadState(emuall::SaveData data)
{
	data >> _hostFlags;
	for (int i = 0; i << 16; i++) {
		data >> _registers[i];
	}
	data >> _cpsr;
	data >> _spsr;
	data >> _pipelineInstruction;
	_activeMode = static_cast<Mode>(data.Get<uint32_t>());
	for (int i = 0; i < 7; i++)
	{
		data >> _registersUser[i];
	}
	for (int i = 0; i < 7; i++)
	{
		data >> _registersFIQ[i];
	}
	data >> _registersIRQ[0] >> _registersIRQ[1];
	data >> _registersABT[0] >> _registersFIQ[1];
	data >> _registersUND[0] >> _registersUND[1];
	data >> _registersSVC[0] >> _registersSVC[1];
	data >> _spsrSVC;
	data >> _spsrABT;
	data >> _spsrUND;
	data >> _spsrIRQ;
	data >> _spsrFIQ;
}

emuall::SaveData Cpu::SaveState() const
{
	emuall::SaveData data(1);
	data << _hostFlags;
	for (int i = 0; i << 16; i++) {
		data << _registers[i];
	}
	data << _cpsr;
	data << _spsr;
	data << _pipelineInstruction;
	data << static_cast<uint32_t>(_activeMode);
	for(int i = 0; i < 7; i++)
	{
		data << _registersUser[i];
	}
	for (int i = 0; i < 7; i++)
	{
		data << _registersFIQ[i];
	}
	data << _registersIRQ[0] << _registersIRQ[1];
	data << _registersABT[0] << _registersFIQ[1];
	data << _registersUND[0] << _registersUND[1];
	data << _registersSVC[0] << _registersSVC[1];
	data << _spsrSVC;
	data << _spsrABT;
	data << _spsrUND;
	data << _spsrIRQ;
	data << _spsrFIQ;
	return data;
}

void Cpu::RegisterSWI(int id, std::function<bool(uint32_t, uint32_t, uint32_t, uint32_t)> func)
{
	assert(id >= 0 & id < 256);
	_swiLookup[id] = func;
}

void Cpu::Reset()
{
	for (int i = 0; i < 16; i++) {
		_registers[i] = 0;
	}
	for (int i = 0; i < 7; i++) {
		_registersUser[i] = 0;
		_registersFIQ[i] = 0;
	}
	for (int i = 0; i < 2; i++) {
		_registersIRQ[i] = 0;
		_registersABT[i] = 0;
		_registersUND[i] = 0;
		_registersSVC[i] = 0;
	}
	_spsrSVC = 0;
	_spsrABT = 0;
	_spsrUND = 0;
	_spsrIRQ = 0;
	_spsrFIQ = 0;
	SetMode(Mode::Supervisor);
	SetThumbMode(false);
	EnableIRQs(false);
	EnableFIQs(false);
	_pipelineInstruction = ARM_NOP;
	_hostFlags = 0;

	_system.GetMemory().SetBiosProtected(0xE129f000);
}

void Cpu::Tick(bool step)
{
	// Read instruction
	try {
		if (AreIRQsEnabled()) {
			uint8_t *registers = _system.GetMemory().GetRegisters();
			if ((IOREG32(registers, IME) & 1) != 0 && (IOREG16(registers, IE) & IOREG16(registers, IF) & IRQ_MASK) != 0) {
				IRQ();
			}
		}

		if ((_registers[REGPC] & ~0xF) != 0x08061410) {
			volatile int nothing = 0;
		}

		if (IsInThumbMode()) {
			TickThumb(step);
		}
		else {
			TickARM(step);
		}
	}
	catch (BreakPointARMException &e) {
		_registers[REGPC] = e.addr;
		_pipelineInstruction = ARM_NOP;
		throw;
	}
	catch (DataAbortARMException &e) {
		logger::Log(logger::Error, "Data Abort ARM Exception at 0x%08x, while accessing address 0x%08x", _registers[REGPC], e._address);
		throw;
	}
}

bool Cpu::IsInBios() const {
	return _registers[REGPC] >> 24 == 0;
}

bool Cpu::InAPrivilegedMode() const
{
	return (_cpsr & 0x1F) != 0x10000;
}

bool Cpu::InABankedUserRegistersMode() const
{
	return _activeMode == Mode::FIQ;
}

Cpu::Mode Cpu::GetMode() const
{
	switch ((_cpsr & 0x1F)) {
	case 0x10: return Mode::User;
	case 0x11: return Mode::FIQ;
	case 0x12: return Mode::IRQ;
	case 0x13: return Mode::Supervisor;
	case 0x17: return Mode::Abort;
	case 0x1B: return Mode::Undefined;
	case 0x1F: return Mode::System;
	default: return Mode::Invalid;
	}
}

void Cpu::SetMode(Mode mode)
{
	_cpsr &= ~0x1F;
	switch (mode) {
	case Mode::User:
		_cpsr |= 0x10;
		break;
	case Mode::FIQ:
		_cpsr |= 0x11;
		break;
	case Mode::IRQ:
		_cpsr |= 0x12;
		break;
	case Mode::Supervisor:
		_cpsr |= 0x13;
		break;
	case Mode::Abort:
		_cpsr |= 0x17;
		break;
	case Mode::Undefined:
		_cpsr |= 0x1B;
		break;
	case Mode::System:
		_cpsr |= 0x1F;
		break;
	default:
	case Mode::Invalid:
		logger::Log(logger::Error, "Tried setting the cpu to an invallid mode");
		_cpsr |= 0x10;
		break;	
	}
	UpdateMode();
}

bool Cpu::IsInThumbMode() const
{
	return (_cpsr & 0x20) != 0;
}

bool Cpu::IsInArmMode() const
{
	return (_cpsr & 0x20) == 0;
}

void Cpu::SetThumbMode(bool thumb)
{
	_cpsr = (_cpsr & ~0x20) | (thumb ? 0x20 : 0x00);
}

bool Cpu::AreFIQsEnabled() const
{
	return (_cpsr & 0x40) == 0;
}

void Cpu::EnableFIQs(bool enabled)
{
	_cpsr = (_cpsr & ~0x40) | (enabled ? 0x00 : 0x40);
}

bool Cpu::AreIRQsEnabled() const
{
	return (_cpsr & 0x80) == 0;
}

void Cpu::EnableIRQs(bool enabled)
{
	_cpsr = (_cpsr & ~0x80) | (enabled ? 0x00 : 0x80);
}

uint32_t Cpu::GetRegisterValue(int id)
{
	if (id >= 0 && id < 16) {
		return _registers[id];
	}
	else if (id == 16) {
		return _cpsr;
	}
	else if (id == 17) {
		return _spsr;
	}
	return 0;
}

void Cpu::SetRegisterValue(int id, uint32_t value) {
	if(id >= 0 && id < 16) {
		_registers[id] = value;
	}
}

void Cpu::UpdateMode()
{
	Mode newMode = GetMode();
	if (newMode == _activeMode) {
		return;
	}
	switch (_activeMode) {
	default:
	case Mode::Invalid:
	case Mode::User:
	case Mode::System:
		_registersUser[5] = _registers[13];
		_registersUser[6] = _registers[14];
		break;
	case Mode::FIQ:
		_registersFIQ[0] = _registers[8];
		_registersFIQ[1] = _registers[9];
		_registersFIQ[2] = _registers[10];
		_registersFIQ[3] = _registers[11];
		_registersFIQ[4] = _registers[12];
		_registersFIQ[5] = _registers[13];
		_registersFIQ[6] = _registers[14];
		for (int i = 8; i <= 12; i++) {
			_registers[i] = _registersUser[i - 8];
		}
		_spsrFIQ = _spsr;
		break;
	case Mode::IRQ:
		_registersIRQ[0] = _registers[13];
		_registersIRQ[1] = _registers[14];
		_spsrIRQ = _spsr;
		break;
	case Mode::Supervisor:
		_registersSVC[0] = _registers[13];
		_registersSVC[1] = _registers[14];
		_spsrSVC = _spsr;
		break;
	case Mode::Abort:
		_registersABT[0] = _registers[13];
		_registersABT[1] = _registers[14];
		_spsrABT = _spsr;
		break;
	case Mode::Undefined:
		_registersUND[0] = _registers[13];
		_registersUND[1] = _registers[14];
		_spsrUND = _spsr;
		break;
	}
	switch (newMode) {
	default:
	case Mode::Invalid:
	case Mode::User:
	case Mode::System:
		_registers[13] = _registersUser[5];
		_registers[14] = _registersUser[6];
		break;
	case Mode::FIQ:
		_registersUser[0] = _registers[8];
		_registersUser[1] = _registers[9];
		_registersUser[2] = _registers[10];
		_registersUser[3] = _registers[11];
		_registersUser[4] = _registers[12];
		for (int i = 8; i <= 14; i++) {
			_registers[i] = _registersFIQ[i - 8];
		}
		_spsr = _spsrFIQ;
		break;
	case Mode::IRQ:
		_registers[13] = _registersIRQ[0];
		_registers[14] = _registersIRQ[1];
		_spsr = _spsrIRQ;
		break;
	case Mode::Supervisor:
		_registers[13] = _registersSVC[0];
		_registers[14] = _registersSVC[1];
		_spsr = _spsrSVC;
		break;
	case Mode::Abort:
		_registers[13] = _registersABT[0];
		_registers[14] = _registersABT[1];
		_spsr = _spsrABT;
		break;
	case Mode::Undefined:
		_registers[13] = _registersUND[0];
		_registers[14] = _registersUND[1];
		_spsr = _spsrUND;
		break;
	}
	_activeMode = newMode;
}

void Cpu::SoftwareInterrupt(uint8_t value)
{
	bool passAlong = true;
	logger::Log(logger::Debug, "Software interrupt %d", value);
	_system.GetMemory().SetBiosProtected(0xE3A02004);
	if(_swiLookup[value]) {
		passAlong = _swiLookup[value](_registers[REGA1], _registers[REGA2], _registers[REGA3], _registers[REGA4]);
	}
	if (passAlong) {
		SaveHostFlagsToCPSR();
		_spsrSVC = _cpsr;
		uint32_t returnAddress = IsInThumbMode() ? _registers[REGPC] - 2 : _registers[REGPC] - 4;
		SetThumbMode(false);
		EnableIRQs(false);
		SetMode(Mode::Supervisor);
		_registers[REGLR] = returnAddress;
		_registers[REGPC] = 0x00000008;
		_pipelineInstruction = ARM_NOP;
	}
}

void Cpu::IRQ()
{
	SaveHostFlagsToCPSR();
	_spsrIRQ = _cpsr;
	uint32_t returnAddress = _registers[REGPC] + 4 + (IsInThumbMode() ? -2 : -4);
	SetThumbMode(false);
	EnableIRQs(false);
	SetMode(Mode::IRQ);
	_registers[REGLR] = returnAddress;
	_registers[REGPC] = 0x00000018;
	_pipelineInstruction = ARM_NOP;
	_system.GetMemory().SetBiosProtected(0xE55EC002);
}

uint32_t Cpu::GetBreakpointInstruction(uint32_t address)
{
	auto it = _breakpoints.find(address & ~3);
	if (it != _breakpoints.end()) return it->second;
	return _system.GetMemory().ManagedRead32(address & ~3);
}

uint16_t Cpu::GetThumbBreakpointInstruction(uint32_t address)
{
	uint32_t instr = GetBreakpointInstruction(address);
	if((address & 2) != 0)
	{
		return instr >> 16;
	} else
	{
		return instr & 0xFFFF;
	}
}

bool Cpu::IsStalled()
{
	if (IsInThumbMode()) {
		return (_pipelineInstruction & 0xFFFF) == THUMB_NOP;
	}
	else {
		return _pipelineInstruction == ARM_NOP;
	}
}

void Cpu::SaveHostFlagsToCPSR()
{
	_cpsr &= ~(CPSR_C_MASK | CPSR_N_MASK | CPSR_V_MASK | CPSR_Z_MASK);
	if ((_hostFlags & (1 << 0)) != 0) _cpsr |= CPSR_C_MASK;
	if ((_hostFlags & (1 << 6)) != 0) _cpsr |= CPSR_Z_MASK;
	if ((_hostFlags & (1 << 7)) != 0) _cpsr |= CPSR_N_MASK;
	if ((_hostFlags & (1 << 11)) != 0) _cpsr |= CPSR_V_MASK;
}

void Cpu::LoadHostFlagsFromCPSR()
{
	_hostFlags &= ~((1 << 0) | (1 << 6) | (1 << 7) | (1 << 11));
	if ((_cpsr & CPSR_V_MASK) != 0) _hostFlags |= (1 << 11);
	if ((_cpsr & CPSR_C_MASK) != 0) _hostFlags |= (1 << 0);
	if ((_cpsr & CPSR_Z_MASK) != 0) _hostFlags |= (1 << 6);
	if ((_cpsr & CPSR_N_MASK) != 0) _hostFlags |= (1 << 7);
}

void Cpu::AddBreakpoint(uint32_t uniqueAddress)
{
	if((uniqueAddress & 0x1) != 0)
	{
		logger::Log(logger::Warn, "Tried setting breakpoint to odd address. This is not valid");
		uniqueAddress &= ~1;
	}
	if (IsBreakpoint(uniqueAddress)) return;
	auto &memory = _system.GetMemory();
	uint32_t wordAddress = uniqueAddress & ~3;
	uint32_t instr = memory.ManagedRead32(wordAddress);
	if((uniqueAddress & 0x2) == 0)
	{
		// Tries to set breakpoint in the middle of the ARM instruction. This is possible for thumb
		if((instr & 0xFFFF0000) == 0xEFFF0000)
		{
			uint32_t newInstr = 0xEFFFEFFF;
			memory.ManagedWrite32(wordAddress, newInstr);
		} else
		{
			uint32_t newInstr = 0xEFFDEFFF;
			logger::Log(logger::Debug, "Adding breakpoint at %08x: %08x", wordAddress, instr);
			_breakpoints[wordAddress] = instr;
			memory.ManagedWrite32(wordAddress, newInstr);
		}
	} else
	{
		if ((instr & 0xFFFF) == 0xEFFF)
		{
			// There is already a breakpoint in the middle of the word
			uint32_t newInstr = 0xEFFFEFFF;
			memory.ManagedWrite32(wordAddress, newInstr);
		} else
		{
			uint32_t newInstr = (instr & 0xFFFF) | 0xEFFF0000;
			logger::Log(logger::Debug, "Adding breakpoint at %08x: %08x", wordAddress, instr);
			_breakpoints[wordAddress] = instr;
			memory.ManagedWrite32(wordAddress, newInstr);
		}
	}
}

void Cpu::RemoveBreakpoint(uint32_t address)
{
	uint32_t wordAddress = address & ~3;
	if (!IsBreakpoint(address)) {
		logger::Log(logger::Warn, "Tried to remove unknown breakpoint at address 0x%016x", address);
		return;
	}
	auto &memory = _system.GetMemory();
	uint32_t instr = memory.ManagedRead32(wordAddress);
	uint32_t oldInstr = _breakpoints[wordAddress];
	if((address & 0x2) == 0)
	{
		if((instr & 0xFFFF) != 0xEFFF)
		{
			logger::Log(logger::Warn, "Tried to remove unknown breakpoint at address 0x%016x", address);
			return;
		}
		if ((instr & 0xFFFF0000) == 0xEFFD0000)
		{
			uint32_t newInstr = oldInstr;
			memory.ManagedWrite32(wordAddress, newInstr);
			logger::Log(logger::Debug, "Removing breakpoint at %08x: %08x", wordAddress, oldInstr);
			_breakpoints.erase(wordAddress);
		} else
		{
			uint32_t newInstr = (instr & 0xFFFF0000) | (oldInstr & 0xFFFF);
			memory.ManagedWrite32(wordAddress, newInstr);
		}
	} else
	{
		if ((instr & 0xFFFF0000) != 0xEFFF0000)
		{
			logger::Log(logger::Warn, "Tried to remove unknown breakpoint at address 0x%016x", address);
			return;
		}
		if((instr & 0xFFFF) == 0xEFFF)
		{
			uint32_t newInstr = 0xEFFDEFFF;
			memory.ManagedWrite32(wordAddress, newInstr);
		} else
		{
			uint32_t newInstr = oldInstr;
			memory.ManagedWrite32(wordAddress, newInstr);
			logger::Log(logger::Debug, "Removing breakpoint at %08x: %08x", wordAddress, oldInstr);
			_breakpoints.erase(wordAddress);
		}
	}
}

bool Cpu::IsBreakpoint(uint32_t address)
{
	uint32_t wordAddress = address & ~3;
	auto instrIt= _breakpoints.find(wordAddress);
	if (instrIt == _breakpoints.end()) return false;
	auto instr = _system.GetMemory().ManagedRead32(wordAddress);
	if((address & 0x2) != 0)
	{
		return (instr & 0xFFFF0000) == 0xEFFF0000;
	} else
	{
		return (instr & 0xFFFF) == 0xEFFF;
	}
}

uint32_t Cpu::GetPrefetchInstruction() const {
	return _pipelineInstruction;
}

bool Cpu::SWI_write(uint32_t ptr, uint32_t size) {
	logger::Log(logger::Debug, "SWI_write 0x%08x %u", ptr, size);
	auto &memory = _system.GetMemory();
	
	_outputStr.reserve(size);
	for(uint32_t i = 0; i < size; i++) {
		char c = memory.Read8(ptr + i);
		if (c == '\n') {
			logger::Log(logger::Message, "[ROM]: %s", _outputStr.c_str());
			_outputStr.clear();
		}
		else {
			_outputStr += c;
		}
	}
	return false;
}

bool Cpu::SWI_exitEmulator(uint32_t exitCode) {
	logger::Log(logger::Debug, "SWI_exitEmulator %u", exitCode);

	if (!_outputStr.empty()) {
		logger::Log(logger::Message, "[ROM]: %s", _outputStr.c_str());
		_outputStr.clear();
	}

	_system.ExitEmulator(exitCode);
	return false;
}
