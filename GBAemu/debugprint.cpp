#include <GBAemu/debugprint.h>
#include <GBAemu/gba.h>
#include <emuall/util/log.h>
#include <cstring>
#include <stdlib.h>

DebugPrint::DebugPrint(Gba & system) : _system(system), _isVisible(false), _addr(0), _get(0), _put(0)
{
}

DebugPrint::~DebugPrint()
{
}

void DebugPrint::Init()
{
	RegisterEvents();
}

void DebugPrint::HandleEvent(uint32_t address, int size)
{
	if (address < AGBPRINT_PROTECT + 1 && address + size >= AGBPRINT_PROTECT) {
		bool protect = _system.GetMemory().Read16(AGBPRINT_PROTECT) != 0;
		if (protect == _isVisible) return;
		_isVisible = protect;
		if (protect) {
			EnableAGBPrint();
		}
		else {
			DisableAGBPrint();
		}
	}
}

void DebugPrint::RegisterEvents()
{
	Memory &mem = _system.GetMemory();
	mem.RegisterEvent(AGBPRINT_PROTECT - 2, this);
	_system.GetCpu().RegisterSWI(SWI_AGBPRINT, std::bind(&DebugPrint::HandleSWI, this));
}

void DebugPrint::InitBackups()
{
	Memory &mem = _system.GetMemory();
	for (int i = 0; i < 4; i++) {
		_controlBackup[i] = mem.Read16(AGBPRINT_ADDR + i * 2);
	}
	for (int i = 0; i < 2; i++) {
		_functionBackup[i] = mem.Read16(AGBPRINT_FUNCTION + i * 2);
	}
	memcpy(_bufferBackup, mem.GetROM() + _addr, AGBPRINT_BUFFERSIZE);
	memcpy(_msgBuffer, mem.GetROM() + _addr, AGBPRINT_BUFFERSIZE);
	_protectBackup = mem.Read16(AGBPRINT_PROTECT);
}

bool DebugPrint::HandleSWI()
{
	auto &memory = _system.GetMemory();
	static std::string printBuffer;
	uint16_t protect = memory.Read16(AGBPRINT_PROTECT);
	uint16_t get = memory.Read16(AGBPRINT_GET);
	uint16_t put = memory.Read16(AGBPRINT_PUT);
	uint32_t addr = memory.Read32(AGBPRINT_ADDR);
	logger::Log(logger::Debug, "SWI_AGBPrint protect %d get %d put %d addr %08x", protect, get, put, addr);
	const char *ptr = reinterpret_cast<char *>(memory.GetROM()) + addr;
	while (get != put) {
		char c = ptr[get];
		if (c == '\n') {
			logger::Log(logger::Message, "[AGBPrint] %s", printBuffer.c_str());
			printBuffer.clear();
		}
		else {
			printBuffer += c;
		}
		get++;
	}
	memory.Write16(AGBPRINT_GET, get, false);
	return false;
}

void DebugPrint::WritePrintCode()
{
	auto &mem = _system.GetMemory();
	mem.Write16(AGBPRINT_FUNCTION, 0xDF00 | SWI_AGBPRINT, false); // swi 
	mem.Write16(AGBPRINT_FUNCTION+2, 0x4770, false); // bx lr
}

void DebugPrint::EnableAGBPrint()
{
	logger::Log(logger::Debug, "Enabling AGB print memory");
	auto &mem = _system.GetMemory();
	WritePrintCode();
	mem.Write32(AGBPRINT_ADDR, _addr);
	mem.Write16(AGBPRINT_GET, _get);
	mem.Write16(AGBPRINT_PUT, _put);
	//memcpy(mem.GetROM() + _addr, _msgBuffer, AGBPRINT_BUFFERSIZE);
}

void DebugPrint::DisableAGBPrint()
{
	logger::Log(logger::Debug, "Disabling AGB print memory");
	auto &mem = _system.GetMemory();
	uint32_t newAddr = mem.Read32(AGBPRINT_ADDR);
	if (newAddr != _addr) {
		_addr = newAddr & 0x1FFFFFF;
		memcpy(_bufferBackup, mem.GetROM() + _addr, AGBPRINT_BUFFERSIZE);
	}
	//memcpy(_msgBuffer, mem.GetROM() + _addr, AGBPRINT_BUFFERSIZE);
	//memcpy(mem.GetROM() + _addr, _bufferBackup, AGBPRINT_BUFFERSIZE);
	_get = mem.Read16(AGBPRINT_GET);
	_put = mem.Read16(AGBPRINT_PUT);
	for (int i = 0; i < 4; i++) {
		mem.Write16(AGBPRINT_ADDR + i * 2, _controlBackup[i], false);
	}
	for (int i = 0; i < 2; i++) {
		mem.Write16(AGBPRINT_FUNCTION + i * 2, _functionBackup[i], false);
	}
	mem.Write16(AGBPRINT_PROTECT, _protectBackup, false);
}
