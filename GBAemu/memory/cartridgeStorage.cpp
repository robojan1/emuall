
#include <stdint.h>

#include <GBAemu/memory/cartridgeStorage.h>
#include <emu.h>
#include <emuall/util/log.h>

CartridgeStorage::CartridgeStorage(Memory &memory) :
	_memory(memory)
{
	
}

CartridgeStorage::~CartridgeStorage()
{

}

bool CartridgeStorage::Load(const SaveData_t * data)
{
	return true;
}

bool CartridgeStorage::Save(SaveData_t * data)
{
	data->ramData = nullptr;
	data->ramDataLen = 0;
	return true;
}

emuall::SaveData CartridgeStorage::SaveState()
{
	return emuall::SaveData();
}

void CartridgeStorage::LoadState(emuall::SaveData data)
{
}

uint32_t CartridgeStorage::GetSize()
{
	return 0;
}

uint8_t CartridgeStorage::ReadMemory(uint32_t address)
{
	return 0;
}

uint32_t CartridgeStorage::Read32(uint32_t address)
{
	logger::Log(logger::Warn, "32 Bit read from unimplemented cartridge at address 0x%08x", address);
	return 0;
}

uint32_t CartridgeStorage::Read16(uint32_t address)
{
	logger::Log(logger::Warn, "16 Bit read from unimplemented cartridge at address 0x%08x", address);
	return 0;
}

uint32_t CartridgeStorage::Read8(uint32_t address)
{
	logger::Log(logger::Warn, "8 Bit read from unimplemented cartridge at address 0x%08x", address);
	return 0;
}

void CartridgeStorage::Write32(uint32_t address, uint32_t val)
{
	logger::Log(logger::Warn, "32 Bit write to unimplemented cartridge at address 0x%08x with data 0x%08x", address, val);
}

void CartridgeStorage::Write16(uint32_t address, uint16_t val)
{
	logger::Log(logger::Warn, "16 Bit write to unimplemented cartridge at address 0x%08x with data 0x%04x", address, val);
}

void CartridgeStorage::Write8(uint32_t address, uint8_t val)
{
	logger::Log(logger::Warn, "8 Bit write to unimplemented cartridge at address 0x%08x with data 0x%02x", address, val);
}

int CartridgeStorage::GetMemoryPointer(uint32_t address, void **data, uint32_t *size) {
	*data = nullptr;
	*size = 0;
	return 0;
}
