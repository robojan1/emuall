
#include <GBAemu/memory/sram.h>
#include <GBAemu/cpu/armException.h>
#include <emuall/util/log.h>
#include <string.h>

Sram::Sram(Memory &memory) : 
	CartridgeStorage(memory)
{
	memset(_dataStorage, 0xFF, 0x8000);
}

Sram::~Sram()
{

}

bool Sram::Load(const SaveData_t * data)
{
	if (data->ramData == nullptr) return false;
	if (data->ramDataLen != GetSize()) {
		logger::Log(logger::Error, "Save data size mismatch");
		return false;
	}
	memcpy(_dataStorage, data->ramData, data->ramDataLen);
	return true;
}

bool Sram::Save(SaveData_t * data)
{
	data->ramDataLen = GetSize();
	data->ramData = new uint8_t[data->ramDataLen];
	memcpy(data->ramData, _dataStorage, data->ramDataLen);
	return true;
}

uint32_t Sram::GetSize()
{
	return 0x8000;
}

uint8_t Sram::ReadMemory(uint32_t address)
{
	if (address < 0x8000) {
		return _dataStorage[address];
	}
	throw DataAbortARMException(address);
}

uint32_t Sram::Read32(uint32_t address)
{
	int index = (address >> 24) & 0xF;
	if (index == 0xe) {
		logger::Log(logger::Warn, "32 bit read from SRAM");
		return Read(address);
	}
	return CartridgeStorage::Read32(address);
}

uint32_t Sram::Read16(uint32_t address)
{
	int index = (address >> 24) & 0xF;
	if (index == 0xe) {
		logger::Log(logger::Warn, "16 bit read from SRAM");
		return Read(address);
	}
	return CartridgeStorage::Read16(address);
}

uint32_t Sram::Read8(uint32_t address)
{
	int index = (address >> 24) & 0xF;
	if (index == 0xe) {
		return Read(address);
	}
	return CartridgeStorage::Read8(address);
}

void Sram::Write32(uint32_t address, uint32_t val)
{
	int index = (address >> 24) & 0xF;
	if (index == 0xe) {
		logger::Log(logger::Warn, "32 bit write to SRAM");
		Write(address, val & 0xFF);
	}
	else {
		CartridgeStorage::Write32(address, val);
	}
}

void Sram::Write16(uint32_t address, uint16_t val)
{
	int index = (address >> 24) & 0xF;
	if (index == 0xe) {
		logger::Log(logger::Warn, "16 bit write to SRAM");
		Write(address, val & 0xFF);
	}
	else {
		CartridgeStorage::Write16(address, val);
	}
}

void Sram::Write8(uint32_t address, uint8_t val)
{
	int index = (address >> 24) & 0xF;
	if (index == 0xe) {
		Write(address, val);
	}
	else {
		CartridgeStorage::Write8(address, val);
	}
}

uint8_t Sram::Read(uint_least16_t address)
{
	return _dataStorage[address & 0x7FFF];
}

void Sram::Write(uint_least16_t address, uint8_t val)
{
	_dataStorage[address & 0x7FFF] = val;
}

int Sram::GetMemoryPointer(uint32_t address, void **data, uint32_t *size) {
	int index = (address >> 24) & 0xF;
	if (index == 0xe) {
		uint32_t maxSize = 0x8000 - (address & 0x7FFF);
		if(*size > maxSize) *size = maxSize;
		*data = &_dataStorage[address & 0x7FFF];
		return 1;
	}
	return CartridgeStorage::GetMemoryPointer(address, data, size);
}
