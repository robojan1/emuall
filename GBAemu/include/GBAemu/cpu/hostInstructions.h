#ifndef GBAEMU_CPU_HOSTINSTRUCTIONS_H
#define GBAEMU_CPU_HOSTINSTRUCTIONS_H

#include <cstdint>

// The flags functions modify the last four bits in the flags variable,
// NZCV, Negative, Zero, Cary, overflow

#ifdef _MSC_VER
#include <intrin.h>
#if defined(_M_AMD64) || defined(_M_IX86)
inline bool IsConditionMet(uint8_t cond, uint32_t flags) {
	switch (cond) {
	case 0: return (flags & 0x0040) != 0; // Equal
	case 1: return (flags & 0x0040) == 0; // Not equal
	case 2: return (flags & 0x0001) != 0; // Carry set
	case 3: return (flags & 0x0001) == 0; // Carry clean
	case 4: return (flags & 0x0080) != 0; // Negative
	case 5: return (flags & 0x0080) == 0; // Postive
	case 6: return (flags & 0x0800) != 0; // Overflow
	case 7: return (flags & 0x0800) == 0; // Not overflow
	case 8: return (flags & 0x0041) == 1; // Unsigned G
	case 9: return (flags & 0x0041) != 1; // Unsigned LE
	case 10: return (flags & 0x0880) == 0 || (flags & 0x0880) == 0x880; // GE
	case 11: return (flags & 0x0880) == 0x800 || (flags & 0x0880) == 0x80; // LT
	case 12: return ((flags & 0x08c0) == 0 || (flags & 0x08c0) == 0x880); // GT
	case 13: return (flags & 0x40) != 0 || (((flags ^ (flags >> 4)) & 0x80) != 0x00); // LE
	default:
	case 14: return true; // Always
	case 15: return false; // Never
	}
}

inline void AND(uint32_t x, uint32_t y, uint32_t &result) {
	result = x & y;
}

inline void EOR(uint32_t x, uint32_t y, uint32_t &result) {
	result = x ^ y;
}

inline void SUB(uint32_t x, uint32_t y, uint32_t &result) {
	result = x - y;
}

inline void ADD(uint32_t x, uint32_t y, uint32_t &result) {
	result = x + y;
}

inline void ADC(uint32_t x, uint32_t y, uint32_t &result, uint32_t &flags) {
	_addcarry_u32(flags & 0x1, x, y, &result);
}

inline void SBC(uint32_t x, uint32_t y, uint32_t &result, uint32_t &flags) {
	_subborrow_u32((flags & 0x1) ^ 0x1, x, y, &result);
}

inline void ORR(uint32_t x, uint32_t y, uint32_t &result) {
	result = x | y;
}

inline void BIC(uint32_t x, uint32_t y, uint32_t &result) {
	result = x & ~y;
}

inline void LSL(uint32_t x, uint8_t amount, uint32_t &result) {
	if (amount >= 32)
		result = 0;
	else
		result = x << amount;
}

inline void LSL_CFLAG(uint32_t x, uint8_t amount, uint32_t &result, uint32_t &flags) {
	if (amount == 0) {
		result = x;
	}
	else if (amount == 32) {
		result = 0;
		flags = (flags & ~0x1) | (x & 1);
	}
	else if (amount > 32) {
		result = 0;
	}
	else {
		_ReadWriteBarrier();
		result = x << amount;
		_ReadWriteBarrier();
		uint64_t f = __readeflags();
		_ReadWriteBarrier();
		flags = (flags & ~0x1) | (f & 0x1);
	}
}

inline void LSR(uint32_t x, uint8_t amount, uint32_t &result) {
	if (amount >= 32)
		result = 0;
	else
		result = x >> amount;
}

inline void LSR_CFLAG(uint32_t x, uint8_t amount, uint32_t &result, uint32_t &flags) {
	if (amount == 0) {
		result = x;
	}
	else if (amount == 32) {
		result = 0;
		flags = (flags & ~0x1) | ((x >> 31) & 1);
	}
	else if (amount > 32) {
		result = 0;
	}
	else {
		_ReadWriteBarrier();
		result = x >> amount;
		_ReadWriteBarrier();
		uint64_t f = __readeflags();
		_ReadWriteBarrier();
		flags = (flags & ~0x1) | (f & 0x1);
	}
}

inline void ASR_CFLAG(uint32_t x, uint8_t amount, uint32_t &result, uint32_t &flags) {
	if (amount == 0) {
		result = x;
	}
	else if (amount >= 32) {
		if ((x & (1 << 31)) != 0) {
			result = 0xFFFFFFFF;
			flags = (flags & ~0x1) | 0x81;
		}
		else {
			result = 0;
			flags = (flags & ~0x1) | 0x40;
		}
	}
	else {
		_ReadWriteBarrier();
		result = static_cast<uint32_t>(static_cast<int32_t>(x) >> amount);
		_ReadWriteBarrier();
		uint64_t f = __readeflags();
		_ReadWriteBarrier();
		flags = (flags & ~0x1) | (f & 0x1);
	}
}

inline void ASR(uint32_t x, uint8_t amount, uint32_t &result) {
	if (amount >= 32)
		result = (x & 0x80000000) != 0 ? 0xFFFFFFFF : 0x0;
	else
		result = static_cast<uint32_t>(static_cast<int32_t>(x) >> amount);
}

inline void ADC_FLAGS(uint32_t x, uint32_t y, uint32_t &result, uint32_t &flags) {
	_addcarry_u32(flags & 0x1, x, y, &result);
	_ReadWriteBarrier();
	uint64_t f = __readeflags();
	_ReadWriteBarrier();
	flags = (flags & ~0x8c1) | (f & 0x8c1);
}

inline void SBC_FLAGS(uint32_t x, uint32_t y, uint32_t &result, uint32_t &flags) {
	_subborrow_u32((flags & 0x1) ^ 0x1, x, y, &result);
	_ReadWriteBarrier();
	uint64_t f = __readeflags();
	_ReadWriteBarrier();
	flags = (flags & ~0x8c1) | ((f ^ 0x1) & 0x8c1);
}

inline void LSL_FLAGS(uint32_t x, uint8_t amount, uint32_t &result, uint32_t &flags) {
	if (amount == 0) {
		volatile uint32_t t = x;
		_ReadWriteBarrier();
		result = t | x;
		_ReadWriteBarrier();
		uint64_t f = __readeflags();
		_ReadWriteBarrier();
		flags = (flags & ~0xc0) | (f & 0xc0);
	}
	else if (amount == 32) {
		result = 0;
		flags = (flags & ~0xc1) | 0x40 | (x & 1);
	}
	else if (amount > 32) {
		result = 0;
		flags = (flags & ~0xc1) | 0x40;
	}
	else {
		result = x << amount;
		_ReadWriteBarrier();
		uint64_t f = __readeflags();
		_ReadWriteBarrier();
		flags = (flags & ~0xc1) | (f & 0xc1);
	}
}

inline void LSR_FLAGS(uint32_t x, uint8_t amount, uint32_t &result, uint32_t &flags) {
	if (amount == 0) {
		uint32_t t = x;
		_ReadWriteBarrier();
		result = t | x;
		_ReadWriteBarrier();
		uint64_t f = __readeflags();
		_ReadWriteBarrier();
		flags = (flags & ~0xc0) | (f & 0xc0);
	}
	else if (amount == 32) {
		result = 0;
		flags = (flags & ~0xc1) | 0x40 | ((x >> 31) & 1);
	}
	else if (amount > 32) {
		result = 0;
		flags = (flags & ~0xc1) | 0x40;
	}
	else {
		_ReadWriteBarrier();
		result = x >> amount;
		_ReadWriteBarrier();
		uint64_t f = __readeflags();
		_ReadWriteBarrier();
		flags = (flags & ~0xc1) | (f & 0xc1);
	}
}

inline void ASR_FLAGS(uint32_t x, uint8_t amount, uint32_t &result, uint32_t &flags) {
	if (amount == 0) {
		uint32_t t = 0;
		_ReadWriteBarrier();
		result = t | x;
		_ReadWriteBarrier();
		uint64_t f = __readeflags();
		_ReadWriteBarrier();
		flags = (flags & ~0xc0) | (f & 0xc0);
	}
	else if (amount >= 32) {
		if ((x & 0x80000000) != 0) {
			result = 0xFFFFFFFF;
			flags = (flags & ~0xc1) | 0x81;
		}
		else {
			result = 0;
			flags = (flags & ~0xc1) | 0x40;
		}
	}
	else {
		_ReadWriteBarrier();
		result = static_cast<uint32_t>(static_cast<int32_t>(x) >> amount);
		_ReadWriteBarrier();
		uint64_t f = __readeflags();
		_ReadWriteBarrier();
		flags = (flags & ~0xc1) | (f & 0xc1);
	}
}

inline void ROR(uint32_t x, uint8_t amount, uint32_t &result) {
	result = _rotr(x, amount);
}

inline void ROR_FLAGS(uint32_t x, uint8_t amount, uint32_t &result, uint32_t &flags) {
	result = _rotr(x, amount);
	volatile uint32_t t = 0;
	_ReadWriteBarrier();
	result = t | result;
	_ReadWriteBarrier();
	uint64_t f = __readeflags();
	_ReadWriteBarrier();
	if (amount == 0) {
		flags = (flags & ~0xc0) | (f & 0xc0);
	}
	else {
		flags = (flags & ~0xc1) | (f & 0xc0) | ((result >> 31) & 1);
	}
}

inline void ROR_CFLAG(uint32_t x, uint8_t amount, uint32_t &result, uint32_t &flags) {
	if (amount == 0) {
		result = x;
	}
	else {
		result = _rotr(x, amount);
		flags = (flags & ~0x1) | ((result >> 31) & 0x1);
	}
}

#endif

#if defined(_M_AMD64) // MSVC doesn't support inline assembly in x64 :(

inline void AND_FLAGS(uint32_t x, uint32_t y, uint32_t &result, uint32_t &flags) {
	result = x & y;
	_ReadWriteBarrier();
	uint64_t f = __readeflags();
	_ReadWriteBarrier();
	flags = (flags & ~0xc0) | (f & 0xc0);
}

inline void EOR_FLAGS(uint32_t x, uint32_t y, uint32_t &result, uint32_t &flags) {
	result = x ^ y;
	_ReadWriteBarrier();
	uint64_t f = __readeflags();
	_ReadWriteBarrier();
	flags = (flags & ~0xc0) | (f & 0xc0);
}

inline void SUB_FLAGS(uint32_t x, uint32_t y, uint32_t &result, uint32_t &flags) {
	result = x - y;
	_ReadWriteBarrier();
	uint64_t f = __readeflags();
	_ReadWriteBarrier();
	flags = (flags & ~0x8c1) | ((f ^ 0x1) & 0x8c1);
}

inline void ADD_FLAGS(uint32_t x, uint32_t y, uint32_t &result, uint32_t &flags) {
	result = x + y;
	_ReadWriteBarrier();
	uint64_t f = __readeflags();
	_ReadWriteBarrier();
	flags = (flags & ~0x8c1) | (f & 0x8c1);
}

inline void TST(uint32_t x, uint32_t y, uint32_t &flags) {
	volatile uint32_t t = x & y;
	_ReadWriteBarrier();
	uint64_t f = __readeflags();
	_ReadWriteBarrier();
	flags = (flags & ~0xc0) | (f & 0xc0);
}

inline void TEQ(uint32_t x, uint32_t y, uint32_t &flags) {
	volatile uint32_t t = x ^ y;
	_ReadWriteBarrier();
	uint64_t f = __readeflags();
	_ReadWriteBarrier();
	flags = (flags & ~0xc0) | (f & 0xc0);
}

inline void CMP(uint32_t x, uint32_t y, uint32_t &flags) {
	volatile uint32_t t = x - y;
	_ReadWriteBarrier();
	uint64_t f = __readeflags();
	_ReadWriteBarrier();
	flags = (flags & ~0x8c1) | ((f ^ 0x1) & 0x8c1);
}

inline void CMN(uint32_t x, uint32_t y, uint32_t &flags) {
	volatile uint32_t t = x + y;
	_ReadWriteBarrier();
	uint64_t f = __readeflags();
	_ReadWriteBarrier();
	flags = (flags & ~0x8c1) | (f & 0x8c1);
}

inline void ORR_FLAGS(uint32_t x, uint32_t y, uint32_t &result, uint32_t &flags) {
	result = x | y;
	_ReadWriteBarrier();
	uint64_t f = __readeflags();
	_ReadWriteBarrier();
	flags = (flags & ~0xc0) | (f & 0xc0);
}

inline void MOV_FLAGS(uint32_t x, uint32_t &flags) {
	uint32_t t = x;
	_ReadWriteBarrier();
	t |= x;
	_ReadWriteBarrier();
	uint64_t f = __readeflags();
	_ReadWriteBarrier();
	flags = (flags & ~0xc0) | (f & 0xc0);
}

inline void BIC_FLAGS(uint32_t x, uint32_t y, uint32_t &result, uint32_t &flags) {
	result = x & ~y;
	_ReadWriteBarrier();
	uint64_t f = __readeflags();
	_ReadWriteBarrier();
	flags = (flags & ~0xc0) | (f & 0xc0);
}

inline void MVN_FLAGS(uint32_t x, uint32_t &flags) {
	volatile uint32_t t = 0xFFFFFFFF;
	t ^= x;
	_ReadWriteBarrier();
	uint64_t f = __readeflags();
	_ReadWriteBarrier();
	flags = (flags & ~0xc0) | (f & 0xc0);
}

inline int32_t SAR(int32_t x, uint8_t amount) {
	return x >> amount;
}

inline void RRX(uint32_t x, uint32_t &result, uint32_t &flags) {
	result = (x >> 1) | (flags << 31);
}

inline void RRX_CFLAG(uint32_t x, uint32_t &result, uint32_t &flags) {
	result = (x >> 1) | (flags << 31);
	flags = (flags & ~1) | (x & 1);
}

inline void MUL_FLAGS(uint32_t x, uint32_t y, uint32_t &result, uint32_t &flags) {
	uint32_t t = 0;
	_ReadWriteBarrier();
	result = (x * y) | t;
	_ReadWriteBarrier();
	uint64_t f = __readeflags();
	_ReadWriteBarrier();
	flags = (flags & ~0xc0) | (f & 0xc0);
}

inline void SMLAL(uint32_t x, uint32_t y, uint32_t &resultLo, uint32_t &resultHi) {
	int64_t result = (static_cast<int64_t>(static_cast<int32_t>(resultHi)) << 32) |
		static_cast<uint32_t>(resultLo);
	result += static_cast<uint64_t>(static_cast<int32_t>(x)) *
		static_cast<uint64_t>(static_cast<int32_t>(y));
	resultLo = static_cast<uint32_t>(result & 0xFFFFFFFF);
	resultHi = static_cast<uint32_t>(result >> 32);
}

inline void SMLAL_FLAGS(uint32_t x, uint32_t y, uint32_t &resultLo, uint32_t &resultHi, uint32_t &flags) {
	int64_t result = (static_cast<int64_t>(static_cast<int32_t>(resultHi)) << 32) |
		static_cast<uint32_t>(resultLo);
	_ReadWriteBarrier();
	result += static_cast<uint64_t>(static_cast<int32_t>(x)) *
		static_cast<uint64_t>(static_cast<int32_t>(y));
	_ReadWriteBarrier();
	uint64_t f = __readeflags();
	_ReadWriteBarrier();
	flags = (flags & ~0xc0) | (f & 0xc0);
	resultLo = static_cast<uint32_t>(result & 0xFFFFFFFF);
	resultHi = static_cast<uint32_t>(result >> 32);
}

inline void UMLAL(uint32_t x, uint32_t y, uint32_t &resultLo, uint32_t &resultHi) {
	uint64_t result = (static_cast<uint64_t>(resultHi) << 32) | resultLo;
	result += static_cast<uint64_t>(x) * static_cast<uint64_t>(y);
	resultLo = static_cast<uint32_t>(result & 0xFFFFFFFF);
	resultHi = static_cast<uint32_t>(result >> 32);
}

inline void UMLAL_FLAGS(uint32_t x, uint32_t y, uint32_t &resultLo, uint32_t &resultHi, uint32_t &flags) {
	uint64_t result = (static_cast<uint64_t>(resultHi) << 32) | resultLo;
	_ReadWriteBarrier();
	result += static_cast<uint64_t>(x) * static_cast<uint64_t>(y);
	_ReadWriteBarrier();
	uint64_t f = __readeflags();
	_ReadWriteBarrier();
	flags = (flags & ~0xc0) | (f & 0xc0);
	resultLo = static_cast<uint32_t>(result & 0xFFFFFFFF);
	resultHi = static_cast<uint32_t>(result >> 32);
}

inline void SMULL(uint32_t x, uint32_t y, uint32_t &resultLo, uint32_t &resultHi) {
	int64_t result = static_cast<int64_t>(static_cast<int32_t>(x)) * static_cast<int64_t>(static_cast<int32_t>(y));
	resultLo = static_cast<uint32_t>(result & 0xFFFFFFFF);
	resultHi = static_cast<uint32_t>(result >> 32);
}

inline void SMULL_FLAGS(uint32_t x, uint32_t y, uint32_t &resultLo, uint32_t &resultHi, uint32_t &flags) {
	uint32_t t = 0;
	int64_t result = static_cast<int64_t>(static_cast<int32_t>(x)) * static_cast<int64_t>(static_cast<int32_t>(y));
	_ReadWriteBarrier();
	result = result | t;
	_ReadWriteBarrier();
	uint64_t f = __readeflags();
	_ReadWriteBarrier();
	flags = (flags & ~0xc0) | (f & 0xc0);
	resultLo = static_cast<uint32_t>(result & 0xFFFFFFFF);
	resultHi = static_cast<uint32_t>(result >> 32);
}

inline void UMULL(uint32_t x, uint32_t y, uint32_t &resultLo, uint32_t &resultHi) {
	uint64_t result = static_cast<uint64_t>(x) * static_cast<uint64_t>(y);
	resultLo = static_cast<uint32_t>(result & 0xFFFFFFFF);
	resultHi = static_cast<uint32_t>(result >> 32);
}

inline void UMULL_FLAGS(uint32_t x, uint32_t y, uint32_t &resultLo, uint32_t &resultHi, uint32_t &flags) {
	uint32_t t = 0;
	uint64_t result = static_cast<uint64_t>(x) * static_cast<uint64_t>(y);
	_ReadWriteBarrier();
	result = result | t;
	_ReadWriteBarrier();
	uint64_t f = __readeflags();
	_ReadWriteBarrier();
	flags = (flags & ~0xc0) | (f & 0xc0);
	_ReadWriteBarrier();
	resultLo = static_cast<uint32_t>(result & 0xFFFFFFFF);
	resultHi = static_cast<uint32_t>(result >> 32);
}

inline void MLA(uint32_t a, uint32_t b, uint32_t c, uint32_t &result) {
	result = static_cast<uint32_t>(a * b + c);
}

inline void MLA_FLAGS(uint32_t a, uint32_t b, uint32_t c, uint32_t &result, uint32_t &flags) {
	_ReadWriteBarrier();
	result = static_cast<uint32_t>(a * b + c);
	_ReadWriteBarrier();
	uint64_t f = __readeflags();
	_ReadWriteBarrier();
	flags = (flags & ~0xc0) | (f & 0xc0);
}



#elif defined(_M_IX86)

inline void AND_FLAGS(uint32_t x, uint32_t y, uint32_t &result, uint32_t &flags) {
	__asm {
		mov eax, x;
		mov ecx, y;
		and eax, ecx;
		pushf;
		mov ecx, dword ptr[result];
		mov dword ptr[ecx], eax;
		mov ecx, dword ptr[flags];
		mov eax, dword ptr[ecx];
		and eax, 0xFFFFFF3F;
		pop dx;
		and edx, 0x000000C0;
		or eax, edx;
		mov dword ptr[ecx], eax;
	}
}

inline void EOR_FLAGS(uint32_t x, uint32_t y, uint32_t &result, uint32_t &flags) {
	__asm {
		mov eax, x;
		mov ecx, y;
		xor eax, ecx;
		pushf;
		mov ecx, dword ptr[result];
		mov dword ptr[ecx], eax;
		mov ecx, dword ptr[flags];
		mov eax, dword ptr[ecx];
		and eax, 0xFFFFFF3F;
		pop dx;
		and edx, 0x000000C0;
		or eax, edx;
		mov dword ptr[ecx], eax;
	}
}

inline void SUB_FLAGS(uint32_t x, uint32_t y, uint32_t &result, uint32_t &flags) {
	__asm {
		mov eax, x;
		mov ecx, y;
		sub eax, ecx;
		pushf;
		mov ecx, dword ptr[result];
		mov dword ptr[ecx], eax;
		mov ecx, dword ptr[flags];
		mov eax, dword ptr[ecx];
		and eax, 0xFFFFF73E;
		pop dx;
		and edx, 0x000008C1;
		or eax, edx;
		xor eax, 0x1;
		mov dword ptr[ecx], eax;
	}
}

inline void ADD_FLAGS(uint32_t x, uint32_t y, uint32_t &result, uint32_t &flags) {
	__asm {
		mov eax, x;
		mov ecx, y;
		add eax, ecx;
		pushf;
		mov ecx, dword ptr[result];
		mov dword ptr[ecx], eax;
		mov ecx, dword ptr[flags];
		mov eax, dword ptr[ecx];
		and eax, 0xFFFFF73E;
		pop dx;
		and edx, 0x000008C1;
		or eax, edx;
		mov dword ptr[ecx], eax;
	}
}

inline void TST(uint32_t x, uint32_t y, uint32_t &flags) {
	__asm {
		mov eax, x;
		mov ecx, y;
		and eax, ecx;
		pushf;
		mov ecx, dword ptr[flags];
		mov eax, dword ptr[ecx];
		and eax, 0xFFFFFF3F;
		pop dx;
		and edx, 0x000000C0;
		or eax, edx;
		mov dword ptr[ecx], eax;
	}
}

inline void TEQ(uint32_t x, uint32_t y, uint32_t &flags) {
	__asm {
		mov eax, x;
		mov ecx, y;
		xor eax, ecx;
		pushf;
		mov ecx, dword ptr[flags];
		mov eax, dword ptr[ecx];
		and eax, 0xFFFFFF3F;
		pop dx;
		and edx, 0x000000C0;
		or eax, edx;
		mov dword ptr[ecx], eax;
	}
}

inline void CMP(uint32_t x, uint32_t y, uint32_t &flags) {
	__asm {
		mov eax, x;
		mov ecx, y;
		cmp eax, ecx;
		pushf;
		mov ecx, dword ptr[flags];
		mov eax, dword ptr[ecx];
		and eax, 0xFFFFF73E;
		pop dx;
		and edx, 0x000008C1;
		or eax, edx;
		xor eax, 0x1;
		mov dword ptr[ecx], eax;
	}
}

inline void CMN(uint32_t x, uint32_t y, uint32_t &flags) {
	__asm {
		mov eax, x;
		mov ecx, y;
		add eax, ecx;
		pushf;
		mov ecx, dword ptr[flags];
		mov eax, dword ptr[ecx];
		and eax, 0xFFFFF73E;
		pop dx;
		and edx, 0x000008C1;
		or eax, edx;
		mov dword ptr[ecx], eax;
	}
}

inline void ORR_FLAGS(uint32_t x, uint32_t y, uint32_t &result, uint32_t &flags) {
	__asm {
		mov eax, x;
		mov ecx, y;
		or eax, ecx;
		pushf;
		mov ecx, dword ptr[result];
		mov dword ptr[ecx], eax;
		mov ecx, dword ptr[flags];
		mov eax, dword ptr[ecx];
		and eax, 0xFFFFFF3F;
		pop dx;
		and edx, 0x000000C0;
		or eax, edx;
		mov dword ptr[ecx], eax;
	}
}

inline void MOV_FLAGS(uint32_t x, uint32_t &flags) {
	__asm {
		mov eax, x;
		or eax, eax;
		pushf;
		mov ecx, dword ptr[flags];
		mov eax, dword ptr[ecx];
		and eax, 0xFFFFFF3F;
		pop dx;
		and edx, 0x000000C0;
		or eax, edx;
		mov dword ptr[ecx], eax;
	}
}

inline void BIC_FLAGS(uint32_t x, uint32_t y, uint32_t &result, uint32_t &flags) {
	__asm {
		mov eax, x;
		mov ecx, y;
		not ecx;
		and eax, ecx;
		pushf;
		mov ecx, dword ptr[result];
		mov dword ptr[ecx], eax;
		mov ecx, dword ptr[flags];
		mov eax, dword ptr[ecx];
		and eax, 0xFFFFFF3F;
		pop dx;
		and edx, 0x000000C0;
		or eax, edx;
		mov dword ptr[ecx], eax;
	}
}

inline void MVN_FLAGS(uint32_t x, uint32_t &flags) {
	__asm {
		mov eax, x;
		not eax;
		or eax, eax;
		pushf;
		mov ecx, dword ptr[flags];
		mov eax, dword ptr[ecx];
		and eax, 0xFFFFFF3F;
		pop dx;
		and edx, 0x000000C0;
		or eax, edx;
		mov dword ptr[ecx], eax;
	}
}

inline int32_t SAR(int32_t x, uint8_t amount) {
	__asm {
		mov CL, amount;
		mov eax, x;
		sar eax, CL;
		mov x, eax;
	}
	return x;
}

inline void RRX(uint32_t x, uint32_t &result, uint32_t &flags) {
	__asm {
		mov ecx, dword ptr[flags];
		mov eax, dword ptr[ecx];
		push ax;
		mov eax, x;
		mov cl, 1;
		popf;
		rcr eax, cl;
		MOV ecx, dword ptr[result];
		MOV dword ptr[ecx], eax;
	}
}

inline void RRX_CFLAG(uint32_t x, uint32_t &result, uint32_t &flags) {
	__asm {
		mov ecx, dword ptr[flags];
		mov eax, dword ptr[ecx];
		push ax;
		mov eax, x;
		mov cl, 1;
		popf;
		rcr eax, cl;
		pushf;
		MOV ecx, dword ptr[result];
		MOV dword ptr[ecx], eax;
		mov ecx, dword ptr[flags];
		mov eax, dword ptr[ecx];
		and eax, 0xFFFFFFFE;
		pop dx;
		and edx, 0x00000001;
		or eax, edx;
		mov dword ptr[ecx], eax;
	}
}

inline void MUL_FLAGS(uint32_t x, uint32_t y, uint32_t &result, uint32_t &flags) {
	uint32_t t = 0;
	_ReadWriteBarrier();
	result = (x * y) | t;
	_ReadWriteBarrier();
	uint64_t f = __readeflags();
	_ReadWriteBarrier();
	flags = (flags & ~0xc0) | (f & 0xc0);
}

inline void SMLAL(uint32_t x, uint32_t y, uint32_t &resultLo, uint32_t &resultHi) {
	int64_t result = (static_cast<int64_t>(static_cast<int32_t>(resultHi)) << 32) |
		static_cast<uint32_t>(resultLo);
	result += static_cast<uint64_t>(static_cast<int32_t>(x)) *
		static_cast<uint64_t>(static_cast<int32_t>(y));
	resultLo = static_cast<uint32_t>(result & 0xFFFFFFFF);
	resultHi = static_cast<uint32_t>(result >> 32);
}

inline void SMLAL_FLAGS(uint32_t x, uint32_t y, uint32_t &resultLo, uint32_t &resultHi, uint32_t &flags) {
	int64_t result = (static_cast<int64_t>(static_cast<int32_t>(resultHi)) << 32) |
		static_cast<uint32_t>(resultLo);
	_ReadWriteBarrier();
	result += static_cast<uint64_t>(static_cast<int32_t>(x)) *
		static_cast<uint64_t>(static_cast<int32_t>(y));
	_ReadWriteBarrier();
	uint64_t f = __readeflags();
	_ReadWriteBarrier();
	flags = (flags & ~0xc0) | (f & 0xc0);
	resultLo = static_cast<uint32_t>(result & 0xFFFFFFFF);
	resultHi = static_cast<uint32_t>(result >> 32);
}

inline void UMLAL(uint32_t x, uint32_t y, uint32_t &resultLo, uint32_t &resultHi) {
	uint64_t result = (static_cast<uint64_t>(resultHi) << 32) | resultLo;
	result += static_cast<uint64_t>(x) * static_cast<uint64_t>(y);
	resultLo = static_cast<uint32_t>(result & 0xFFFFFFFF);
	resultHi = static_cast<uint32_t>(result >> 32);
}

inline void UMLAL_FLAGS(uint32_t x, uint32_t y, uint32_t &resultLo, uint32_t &resultHi, uint32_t &flags) {
	uint64_t result = (static_cast<uint64_t>(resultHi) << 32) | resultLo;
	_ReadWriteBarrier();
	result += static_cast<uint64_t>(x) * static_cast<uint64_t>(y);
	_ReadWriteBarrier();
	uint64_t f = __readeflags();
	_ReadWriteBarrier();
	flags = (flags & ~0xc0) | (f & 0xc0);
	resultLo = static_cast<uint32_t>(result & 0xFFFFFFFF);
	resultHi = static_cast<uint32_t>(result >> 32);
}

inline void SMULL(uint32_t x, uint32_t y, uint32_t &resultLo, uint32_t &resultHi) {
	int64_t result = static_cast<int64_t>(static_cast<int32_t>(x)) * static_cast<int64_t>(static_cast<int32_t>(y));
	resultLo = static_cast<uint32_t>(result & 0xFFFFFFFF);
	resultHi = static_cast<uint32_t>(result >> 32);
}

inline void SMULL_FLAGS(uint32_t x, uint32_t y, uint32_t &resultLo, uint32_t &resultHi, uint32_t &flags) {
	flags = (flags & ~0xC0);
	int64_t result = static_cast<int64_t>(static_cast<int32_t>(x)) * static_cast<int64_t>(static_cast<int32_t>(y));
	if (result == 0)
		flags |= 0x40;
	if (result < 0)
		flags |= 0x80;
	resultLo = static_cast<uint32_t>(result & 0xFFFFFFFF);
	resultHi = static_cast<uint32_t>(result >> 32);
}

inline void UMULL(uint32_t x, uint32_t y, uint32_t &resultLo, uint32_t &resultHi) {
	uint64_t result = static_cast<uint64_t>(x) * static_cast<uint64_t>(y);
	resultLo = static_cast<uint32_t>(result & 0xFFFFFFFF);
	resultHi = static_cast<uint32_t>(result >> 32);
}

inline void UMULL_FLAGS(uint32_t x, uint32_t y, uint32_t &resultLo, uint32_t &resultHi, uint32_t &flags) {
	flags = (flags & ~0xC0);
	int64_t result = static_cast<int64_t>(static_cast<uint64_t>(x) * static_cast<uint64_t>(y));
	if (result == 0)
		flags |= 0x40;
	if (result < 0)
		flags |= 0x80;
	resultLo = static_cast<uint32_t>(result & 0xFFFFFFFF);
	resultHi = static_cast<uint32_t>(result >> 32);
}

inline void MLA(uint32_t a, uint32_t b, uint32_t c, uint32_t &result) {
	result = static_cast<uint32_t>(a * b + c);
}

inline void MLA_FLAGS(uint32_t a, uint32_t b, uint32_t c, uint32_t &result, uint32_t &flags) {
	_ReadWriteBarrier();
	result = static_cast<uint32_t>(a * b + c);
	_ReadWriteBarrier();
	uint64_t f = __readeflags();
	_ReadWriteBarrier();
	flags = (flags & ~0xc0) | (f & 0xc0);
}


#else 
#error Implement these instructions for your architecture
#endif

#elif defined(__GNUC__)

#ifdef __x86_64__

inline bool IsConditionMet(uint8_t cond, uint32_t flags) {
	switch(cond) {
	case 0: return (flags & 0x0040) != 0; // Equal
	case 1: return (flags & 0x0040) == 0; // Not equal
	case 2: return (flags & 0x0001) != 0; // Carry set
	case 3: return (flags & 0x0001) == 0; // Carry clean
	case 4: return (flags & 0x0080) != 0; // Negative
	case 5: return (flags & 0x0080) == 0; // Postive
	case 6: return (flags & 0x0800) != 0; // Overflow
	case 7: return (flags & 0x0800) == 0; // Not overflow
	case 8: return (flags & 0x0041) == 1; // Unsigned G
	case 9: return (flags & 0x0041) != 1; // Unsigned LE
	case 10: return (flags & 0x0880) == 0 || (flags & 0x0880) == 0x880; // GE
	case 11: return (flags & 0x0880) == 0x800 || (flags & 0x0880) == 0x80; // LT
	case 12: return ((flags & 0x08c0) == 0 || (flags & 0x08c0) == 0x880); // GT
	case 13: return (flags & 0x40) != 0 || (((flags ^ (flags >> 4)) & 0x80) != 0x00); // LE
	default:
	case 14: return true; // Always
	case 15: return false; // Never
	}
}

inline void AND(uint32_t x, uint32_t y, uint32_t &result) {
	result = x & y;
}

inline void AND_FLAGS(uint32_t x, uint32_t y, uint32_t &result, uint32_t &flags) {
	uint64_t f = flags;
	asm (
		"andl %3, %0;\n"
		"pushfq;\n"
		"andq $0xffffffffffffff3f, %1;\n"
		"popq %%rbx;\n"
		"andq $0xC0, %%rbx;\n"
		"orq %%rbx, %1;\n"
		: "=r" (result), "+&r"(f)
		: "0"(x), "r"(y)
		: "rbx");
	flags = static_cast<uint32_t>(f);
}

inline void EOR(uint32_t x, uint32_t y, uint32_t &result) {
	result = x ^ y;
}

inline void EOR_FLAGS(uint32_t x, uint32_t y, uint32_t &result, uint32_t &flags) {
	uint64_t f = flags;
	asm (
		"xorl %3, %0;\n"
		"pushfq;\n"
		"andq $0xffffffffffffff3f, %1;\n"
		"popq %%rbx;\n"
		"andq $0xC0, %%rbx;\n"
		"orq %%rbx, %1;\n"
		: "=r" (result), "+&r"(f)
		: "0"(x), "r"(y)
		: "rbx");
	flags = static_cast<uint32_t>(f);
}

inline void SUB(uint32_t x, uint32_t y, uint32_t &result) {
	result = x - y;
}

inline void SUB_FLAGS(uint32_t x, uint32_t y, uint32_t &result, uint32_t &flags) {
	uint64_t f = flags;
	asm (
		"subl %3, %0;\n"
		"pushfq;\n"
		"andq $0xfffffffffffff73e, %1;\n"
		"popq %%rbx;\n"
		"andq $0x8C1, %%rbx;\n"
		"orq %%rbx, %1;\n"
		"xorq $0x1, %1;\n"
		: "=r" (result), "+&r"(f)
		: "0"(x), "r"(y)
		: "rbx");
	flags = static_cast<uint32_t>(f);
}

inline void ADD(uint32_t x, uint32_t y, uint32_t &result) {
	result = x + y;
}

inline void ADD_FLAGS(uint32_t x, uint32_t y, uint32_t &result, uint32_t &flags) {
	uint64_t f = flags;
	asm (
		"addl %3, %0;\n"
		"pushfq;\n"
		"andq $0xfffffffffffff73e, %1;\n"
		"popq %%rbx;\n"
		"andq $0x8C1, %%rbx;\n"
		"orq %%rbx, %1;\n"
		: "=r" (result), "+&r"(f)
		: "0"(x), "r"(y)
		: "rbx");
	flags = static_cast<uint32_t>(f);
}

inline void ADC(uint32_t x, uint32_t y, uint32_t &result, uint32_t &flags) {
	asm (
		"pushq %3;\n"
		"popfq;\n"
		"adcl %2, %0;\n"
		: "=r" (result)
		: "0" (x), "r"(y), "r"((uint64_t)flags)
	:);
}

inline void ADC_FLAGS(uint32_t x, uint32_t y, uint32_t &result, uint32_t &flags) {
	uint64_t f = flags;
	asm (
		"pushq %1;\n"
		"popfq;\n"
		"adcl %3, %0;\n"
		"pushfq;\n"
		"andq $0xfffffffffffff73e, %1;\n"
		"popq %%rbx;\n"
		"andq $0x8C1, %%rbx;\n"
		"orq %%rbx, %1;\n"
		: "=r" (result), "+&r"(f)
		: "0"(x), "r"(y)
		: "rbx");
	flags = static_cast<uint32_t>(f);
}

inline void SBC(uint32_t x, uint32_t y, uint32_t &result, uint32_t &flags) {
	asm (
		"xorq $1, %3;\n"
		"pushq %3;\n"
		"popfq;\n"
		"sbbl %2, %0;\n"
		: "=r" (result)
		: "0" (x), "r"(y), "r"((uint64_t)flags)
		:);
}

inline void SBC_FLAGS(uint32_t x, uint32_t y, uint32_t &result, uint32_t &flags) {
	uint64_t f = flags;
	asm (
		"xorq $1, %1;\n"
		"pushq %1;\n"
		"popfq;\n"
		"sbbl  %3, %0;\n"
		"pushfq;\n"
		"andq $0xfffffffffffff73e, %1;\n"
		"popq %%rbx;\n"
		"andq $0x8C1, %%rbx;\n"
		"orq %%rbx, %1;\n"
		"xorq $0x1, %1;\n"
		: "=r" (result), "+&r"(f)
		: "0"(x), "r"(y)
		: "rbx");
	flags = static_cast<uint32_t>(f);
}

inline void TST(uint32_t x, uint32_t y, uint32_t &flags) {
	uint64_t f = flags;
	asm (
		"andl %2, %1;\n"
		"pushfq;\n"
		"andq $0xFFFFFFFFFFFFFF3F, %0;\n"
		"popq %%rax;\n"
		"andq $0x000000C0, %%rax;\n"
		"orq %%rax, %0;\n"
		: "+&r"(f)
		: "r" (x), "r"(y)
		: "rax");
	flags = static_cast<uint32_t>(f);
}

inline void TEQ(uint32_t x, uint32_t y, uint32_t &flags) {
	uint64_t f = flags;
	asm (
		"xorl %2, %1;\n"
		"pushfq;\n"
		"andq $0xFFFFFFFFFFFFFF3F, %0;\n"
		"popq %%rax;\n"
		"andq $0x000000C0, %%rax;\n"
		"orq %%rax, %0;\n"
		: "+&r"(f)
		: "r" (x), "r"(y)
		: "rax");
	flags = static_cast<uint32_t>(f);
}

inline void CMP(uint32_t x, uint32_t y, uint32_t &flags) {
	uint64_t f = flags;
	asm (
		"cmpl %2, %1;\n"
		"pushfq;\n"
		"andq $0xFFFFFFFFFFFFF73E, %0;\n"
		"popq %%rax;\n"
		"andq $0x000008C1, %%rax;\n"
		"orq %%rax, %0;\n"
        "xorq $0x1, %0;\n"
		: "+&r"(f)
		: "r" (x), "r"(y)
		: "rax");
	flags = static_cast<uint32_t>(f);
}

inline void CMN(uint32_t x, uint32_t y, uint32_t &flags) {
	uint64_t f = flags;
	asm (
		"addl %2, %1;\n"
		"pushfq;\n"
		"andq $0xFFFFFFFFFFFFF73E, %0;\n"
		"popq %%rax;\n"
		"andq $0x000008C1, %%rax;\n"
		"orq %%rax, %0;\n"
		: "+&r"(f)
		: "r" (x), "r"(y)
		: "rax");
	flags = static_cast<uint32_t>(f);
}

inline void ORR(uint32_t x, uint32_t y, uint32_t &result) {
	result = x | y;
}

inline void ORR_FLAGS(uint32_t x, uint32_t y, uint32_t &result, uint32_t &flags) {
	uint64_t f = flags;
	asm (
		"orl %3, %0;\n"
		"pushfq;\n"
		"andq $0xfffffffffffffF3F, %1;\n"
		"popq %%rbx;\n"
		"andq $0x0C0, %%rbx;\n"
		"orq %%rbx, %1;\n"
		: "=r" (result), "+&r"(f)
		: "0"(x), "r"(y)
		: "rbx");
	flags = static_cast<uint32_t>(f);
}

inline void MOV_FLAGS(uint32_t x, uint32_t &flags) {
	uint64_t f = flags;
	asm (
		"orl %1, %1;\n"
		"pushfq;\n"
		"andq $0xFFFFFFFFFFFFFF3F, %0;\n"
		"popq %%rax;\n"
		"andq $0x000000C0, %%rax;\n"
		"orq %%rax, %0;\n"
		: "+r"(f)
		: "r" (x)
		: "rax");
	flags = static_cast<uint32_t>(f);
}

inline void BIC(uint32_t x, uint32_t y, uint32_t &result) {
	result = x & ~y;
}

inline void BIC_FLAGS(uint32_t x, uint32_t y, uint32_t &result, uint32_t &flags) {
	uint64_t f = flags;
	asm (
		"notl %3;\n"
		"andl %3, %0;\n"
		"pushfq;\n"
		"andq $0xfffffffffffffF3F, %1;\n"
		"popq %%rbx;\n"
		"andq $0x0C0, %%rbx;\n"
		"orq %%rbx, %1;\n"
		: "=r" (result), "+&r"(f)
		: "0"(x), "r"(y)
		: "rbx");
	flags = static_cast<uint32_t>(f);
}

inline void MVN_FLAGS(uint32_t x, uint32_t &flags) {
	uint64_t f = flags;
	asm (
		"xorl $0xFFFFFFFF, %1;\n"
		"pushfq;\n"
		"andq $0xFFFFFFFFFFFFFF3F, %0;\n"
		"popq %%rax;\n"
		"andq $0x000000C0, %%rax;\n"
		"orq %%rax, %0;\n"
		: "+r"(f)
		: "r" (x)
		: "rax");
	flags = static_cast<uint32_t>(f);
}

inline void LSL_FLAGS(uint32_t x, uint8_t amount, uint32_t &result, uint32_t &flags) {
	uint64_t f = flags;
	if(__builtin_expect(amount == 0, 0)) {
		asm (
			"orl %1, %1;\n"
			"pushfq;\n"
			"andq $0xFFFFFFFFFFFFFF3F, %0;\n"
			"popq %%rax;\n"
			"andq $0x000000C0, %%rax;\n"
			"orq %%rax, %0;\n"
			: "+r"(f), "=r" (x)
			: "1"(x)
			: "rax");
		result = x;
	} else if(__builtin_expect(amount == 32, 0)) {
		result = 0;
		f = (f & ~0xc1) | 0x40 | (x & 1);
	} else if(__builtin_expect(amount > 32, 0)) {
		result = 0;
		f = (f & ~0xc1) | 0x40;
	} else {
		asm (
			"shll %%cl, %0;\n"
			"pushfq;\n"
			"andq $0xfffffffffffffF3e, %1;\n"
			"popq %%rcx;\n"
			"andq $0x0C1, %%rcx;\n"
			"orq %%rcx, %1;\n"
			: "=r" (result), "+&r"(f)
			: "0"(x), "c"(amount)
			: );
	}
	flags = static_cast<uint32_t>(f);
}

inline void LSR_FLAGS(uint32_t x, uint8_t amount, uint32_t &result, uint32_t &flags) {
	uint64_t f = flags;
	if(__builtin_expect(amount == 0, 0)) {
		asm (
		"orl %1, %1;\n"
		"pushfq;\n"
		"andq $0xFFFFFFFFFFFFFF3F, %0;\n"
		"popq %%rax;\n"
		"andq $0x000000C0, %%rax;\n"
		"orq %%rax, %0;\n"
		: "+r"(f), "=r"(x)
		: "1" (x)
		: "rax");
		result = x;
	} else if(__builtin_expect(amount == 32, 0)) {
		result = 0;
		f = (f & ~0xc1) | 0x40 | ((x >> 31) & 1);
	} else if(__builtin_expect(amount > 32, 0)) {
		result = 0;
		f = (f & ~0xc1) | 0x40;
	} else {
		asm (
		"shrl %%cl, %0;\n"
		"pushfq;\n"
		"andq $0xfffffffffffffF3e, %1;\n"
		"popq %%rcx;\n"
		"andq $0x0C1, %%rcx;\n"
		"orq %%rcx, %1;\n"
		: "=r" (result), "+&r"(f)
		: "0"(x), "c"(amount)
		: );
	}
	flags = static_cast<uint32_t>(f);
}

inline void ASR_FLAGS(uint32_t x, uint8_t amount, uint32_t &result, uint32_t &flags) {
	uint64_t f = flags;
	if(__builtin_expect(amount == 0, 0)) {
		asm (
		"orl %1, %1;\n"
		"pushfq;\n"
		"andq $0xFFFFFFFFFFFFFF3F, %0;\n"
		"popq %%rax;\n"
		"andq $0x000000C0, %%rax;\n"
		"orq %%rax, %0;\n"
		: "+r"(f), "=r"(x)
		: "1" (x)
		: "rax");
		result = x;
	} else if(__builtin_expect(amount >= 32, 0)) {
		if((x & (1 << 31)) != 0) {
			result = 0xFFFFFFFF;
			f = (f & ~0xc1) | 0x81;
		} else {
			result = 0;
			f = (f & ~0xc1) | 0x40;
		}
	} else {
		asm (
		"sarl %%cl, %0;\n"
		"pushfq;\n"
		"andq $0xfffffffffffffF3e, %1;\n"
		"popq %%rcx;\n"
		"andq $0x0C1, %%rcx;\n"
		"orq %%rcx, %1;\n"
		: "=r" (result), "+&r"(f)
		: "0"(x), "c"(amount)
		: );
	}
	flags = static_cast<uint32_t>(f);
}

inline void MUL_FLAGS(uint32_t x, uint32_t y, uint32_t &result, uint32_t &flags) {
	uint64_t f = flags;
	asm (
		"imul %3, %2;\n"
		"orl %0, %0;\n"
		"pushfq;\n"
		"andq $0xfffffffffffffF3F, %1;\n"
		"popq %%rcx;\n"
		"andq $0x0C0, %%rcx;\n"
		"orq %%rcx, %1;\n"
		: "=r" (result), "+&r"(f)
		: "0" (x), "r"(y)
		: "rcx");
	flags = static_cast<uint32_t>(f);
}

inline void SMLAL(uint32_t x, uint32_t y, uint32_t &resultLo, uint32_t &resultHi) {
	int64_t result = (static_cast<int64_t>(static_cast<int32_t>(resultHi)) << 32) |
	                 static_cast<uint32_t>(resultLo);
	result += static_cast<uint64_t>(static_cast<int32_t>(x)) *
	          static_cast<uint64_t>(static_cast<int32_t>(y));
	resultLo = static_cast<uint32_t>(result & 0xFFFFFFFF);
	resultHi = static_cast<uint32_t>(result >> 32);
}

inline void SMLAL_FLAGS(uint32_t x, uint32_t y, uint32_t &resultLo, uint32_t &resultHi, uint32_t &flags) {
	uint64_t f = flags;
	int64_t result = (static_cast<int64_t>(static_cast<int32_t>(resultHi)) << 32) |
	                 static_cast<uint32_t>(resultLo);
	asm (
		"imul %3, %2;\n"
		"add %2, %0;\n"
		"pushfq;\n"
		"andq $0xfffffffffffffF3F, %1;\n"
		"popq %%rcx;\n"
		"andq $0x0C0, %%rcx;\n"
		"orq %%rcx, %1;\n"
		: "+r" (result), "+r"(f)
		: "r" (static_cast<int64_t>(static_cast<int32_t>(x))), "r"(static_cast<int64_t>(static_cast<int32_t>(y)))
		: "rcx");
	flags = static_cast<uint32_t>(f);
	resultLo = static_cast<uint32_t>(result & 0xFFFFFFFF);
	resultHi = static_cast<uint32_t>(result >> 32);
}

inline void UMLAL(uint32_t x, uint32_t y, uint32_t &resultLo, uint32_t &resultHi) {
	uint64_t result = (static_cast<uint64_t>(resultHi) << 32) | resultLo;
	result += static_cast<uint64_t>(x) * static_cast<uint64_t>(y);
	resultLo = static_cast<uint32_t>(result & 0xFFFFFFFF);
	resultHi = static_cast<uint32_t>(result >> 32);
}

inline void UMLAL_FLAGS(uint32_t x, uint32_t y, uint32_t &resultLo, uint32_t &resultHi, uint32_t &flags) {
	uint64_t f = flags;
	uint64_t result = (static_cast<uint64_t>(resultHi) << 32) | resultLo;
	asm (
		"imul %3, %2;\n"
		"add %2, %0;\n"
		"pushfq;\n"
		"andq $0xfffffffffffffF3F, %1;\n"
		"popq %%rcx;\n"
		"andq $0x0C0, %%rcx;\n"
		"orq %%rcx, %1;\n"
		: "+r" (result), "+r"(f)
		: "r" (static_cast<uint64_t>(x)), "r"(static_cast<uint64_t>(y))
		: "rcx");
	flags = static_cast<uint32_t>(f);
	resultLo = static_cast<uint32_t>(result & 0xFFFFFFFF);
	resultHi = static_cast<uint32_t>(result >> 32);
}

inline void SMULL(uint32_t x, uint32_t y, uint32_t &resultLo, uint32_t &resultHi) {
	int64_t result = static_cast<int64_t>(static_cast<int32_t>(x)) * static_cast<int64_t>(static_cast<int32_t>(y));
	resultLo = static_cast<uint32_t>(result & 0xFFFFFFFF);
	resultHi = static_cast<uint32_t>(result >> 32);
}

inline void SMULL_FLAGS(uint32_t x, uint32_t y, uint32_t &resultLo, uint32_t &resultHi, uint32_t &flags) {
	uint64_t f = flags;
	int64_t result;
	asm (
		"imul %3, %2;\n"
		"or %0, %0;\n"
		"pushfq;\n"
		"andq $0xfffffffffffffF3F, %1;\n"
		"popq %%rcx;\n"
		"andq $0x0C0, %%rcx;\n"
		"orq %%rcx, %1;\n"
		: "=r" (result), "+&r"(f)
		: "0" (static_cast<uint64_t>(static_cast<int32_t>(x))), "r"(static_cast<uint64_t>(static_cast<int32_t>(y)))
		: "rcx");
	flags = static_cast<uint32_t>(f);
	resultLo = static_cast<uint32_t>(result & 0xFFFFFFFF);
	resultHi = static_cast<uint32_t>(result >> 32);
}

inline void UMULL(uint32_t x, uint32_t y, uint32_t &resultLo, uint32_t &resultHi) {
	uint64_t result = static_cast<uint64_t>(x) * static_cast<uint64_t>(y);
	resultLo = static_cast<uint32_t>(result & 0xFFFFFFFF);
	resultHi = static_cast<uint32_t>(result >> 32);
}

inline void UMULL_FLAGS(uint32_t x, uint32_t y, uint32_t &resultLo, uint32_t &resultHi, uint32_t &flags) {
	uint64_t f = flags;
	uint64_t result;
	asm (
		"imul %3, %2;\n"
		"or %0, %0;\n"
		"pushfq;\n"
		"andq $0xfffffffffffffF3F, %1;\n"
		"popq %%rcx;\n"
		"andq $0x0C0, %%rcx;\n"
		"orq %%rcx, %1;\n"
		: "=r" (result), "+&r"(f)
		: "0" (static_cast<uint64_t>(x)), "r"(static_cast<uint64_t>(y))
		: "rcx");
	flags = static_cast<uint32_t>(f);
	resultLo = static_cast<uint32_t>(result & 0xFFFFFFFF);
	resultHi = static_cast<uint32_t>(result >> 32);
}

inline void MLA(uint32_t a, uint32_t b, uint32_t c, uint32_t &result) {
	result = static_cast<uint32_t>(a * b + c);
}

inline void MLA_FLAGS(uint32_t a, uint32_t b, uint32_t c, uint32_t &result, uint32_t &flags) {
	uint64_t f = flags;
	asm (
		"imul %3, %2;\n"
		"addl %4, %0;\n"
		"pushfq;\n"
		"andq $0xfffffffffffffF3F, %1;\n"
		"popq %%rcx;\n"
		"andq $0x0C0, %%rcx;\n"
		"orq %%rcx, %1;\n"
		: "=r" (result), "+&r"(f)
		: "0" (a), "r"(b), "r"(c)
		: "rcx");
	flags = static_cast<uint32_t>(f);
}

inline int32_t SAR(int32_t x, uint8_t amount) {
	return x >> amount;
}

inline void ROR(uint32_t x, uint8_t amount, uint32_t &result) {
	result = (x >> amount) | (x << (32 - amount));
}

inline void ROR_FLAGS(uint32_t x, uint8_t amount, uint32_t &result, uint32_t &flags) {
	uint64_t f = flags;
	if(__builtin_expect(amount == 0, 0)) {
		asm (
			"orl %1, %1;\n"
			"pushfq;\n"
			"andq $0xFFFFFFFFFFFFFF3F, %0;\n"
			"popq %%rax;\n"
			"andq $0x000000C0, %%rax;\n"
			"orq %%rax, %0;\n"
			: "+r"(f), "=r"(x)
			: "1" (x)
			: "rax");
		result = x;
	} else {
		asm (
			"rorl %%cl, %0;\n"
			"pushfq;\n"
	        "orl %0, %0;\n"
			"pushfq;\n"
			"andq $0xfffffffffffffF3e, %1;\n"
			"popq %%rcx;\n"
			"andq $0x0C0, %%rcx;\n"
	        "orq %%rcx, %1;\n"
			"popq %%rcx;\n"
			"andq $0x001, %%rcx;\n"
			"orq %%rcx, %1;\n"
			: "=r" (result), "+&r"(f)
			: "0"(x), "c"(amount)
			: );
	}
	flags = static_cast<uint32_t>(f);
}

inline void ROR_CFLAG(uint32_t x, uint8_t amount, uint32_t &result, uint32_t &flags) {
	uint64_t f = flags;
	if(__builtin_expect(amount == 0, 0)) {
		result = x;
	} else {
		asm (
			"rorl %%cl, %0;\n"
			"pushfq;\n"
			"andq $0xfffffffffffffffe, %1;\n"
			"popq %%rcx;\n"
			"andq $0x001, %%rcx;\n"
			"orq %%rcx, %1;\n"
			: "=r" (result), "+&r"(f)
			: "0"(x), "c"(amount)
			: );
	}
	flags = static_cast<uint32_t>(f);
}

inline void RRX(uint32_t x, uint32_t &result, uint32_t &flags) {
	result = (x >> 1) | (flags << 31);
}

inline void RRX_CFLAG(uint32_t x, uint32_t &result, uint32_t &flags) {
	result = (x >> 1) | (flags << 31);
	flags = (flags & ~1) | (x & 1);
}

inline void LSL(uint32_t x, uint8_t amount, uint32_t &result) {
	if(amount >= 32)
		result = 0;
	else
		result = x << amount;
}

inline void LSL_CFLAG(uint32_t x, uint8_t amount, uint32_t &result, uint32_t &flags) {
	uint64_t f = flags;
	if(__builtin_expect(amount == 0, 0)) {
		result = x;
	} else if(__builtin_expect(amount == 32, 0)) {
		result = 0;
		f = (f & ~0x1) | (x & 1);
	} else if(__builtin_expect(amount > 32, 0)) {
		result = 0;
	} else {
		asm (
			"shll %%cl, %0;\n"
			"pushfq;\n"
			"andq $0xfffffffffffffffe, %1;\n"
			"popq %%rcx;\n"
			"andq $0x001, %%rcx;\n"
			"orq %%rcx, %1;\n"
			: "=r" (result), "+&r"(f)
			: "0"(x), "c"(amount)
			: );
	}
	flags = static_cast<uint32_t>(f);
}

inline void LSR(uint32_t x, uint8_t amount, uint32_t &result) {
	if(amount >= 32)
		result = 0;
	else
		result = x >> amount;
}

inline void LSR_CFLAG(uint32_t x, uint8_t amount, uint32_t &result, uint32_t &flags) {
	uint64_t f = flags;
	if(__builtin_expect(amount == 0, 0)) {
		result = x;
	} else if(__builtin_expect(amount == 32, 0)) {
		result = 0;
		f = (f & ~0x1) | ((x >> 31) & 1);
	} else if(__builtin_expect(amount > 32, 0)) {
		result = 0;
	} else {
		asm (
			"shrl %%cl, %0;\n"
			"pushfq;\n"
			"andq $0xfffffffffffffffe, %1;\n"
			"popq %%rcx;\n"
			"andq $0x001, %%rcx;\n"
			"orq %%rcx, %1;\n"
			: "=r" (result), "+&r"(f)
			: "0"(x), "c"(amount)
			: );
	}
	flags = static_cast<uint32_t>(f);
}

inline void ASR_CFLAG(uint32_t x, uint8_t amount, uint32_t &result, uint32_t &flags) {
	uint64_t f = flags;
	if(__builtin_expect(amount == 0, 0)) {
		result = x;
	} else if(__builtin_expect(amount >= 32, 0)) {
		if((x & (1 << 31)) != 0) {
			result = 0xFFFFFFFF;
			f = (f & ~0x1) | 0x81;
		} else {
			result = 0;
			f = (f & ~0x1) | 0x40;
		}
	} else {
		asm (
			"sarl %%cl, %0;\n"
			"pushfq;\n"
			"andq $0xfffffffffffffffe, %1;\n"
			"popq %%rcx;\n"
			"andq $0x001, %%rcx;\n"
			"orq %%rcx, %1;\n"
			: "=r" (result), "+&r"(f)
			: "0"(x), "c"(amount)
			: );
	}
	flags = static_cast<uint32_t>(f);
}

inline void ASR(uint32_t x, uint8_t amount, uint32_t &result) {
	if(amount >= 32)
		result = (x & 0x80000000) != 0 ? 0xFFFFFFFF : 0x0;
	else
		result = static_cast<uint32_t>(static_cast<int32_t>(x) >> amount);
}


#else 
#error Implement these instructions for your architecture
#endif
#else
#error Define these instructions for your compiler
#endif

#endif