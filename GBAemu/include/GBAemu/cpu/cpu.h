#pragma once

#include <stdint.h>
#include <unordered_map>
#include <array>
#include <functional>
#include <string>

class Gba;
namespace emuall {
	class SaveData;
}

class Cpu {
public:
	enum class Mode {
		User,
		FIQ, 
		IRQ, 
		Supervisor,
		Abort,
		Undefined, 
		System,
		Invalid,
	};

	Cpu(Gba &gba);
	~Cpu();

	void Init();
	void LoadState(emuall::SaveData data);
	emuall::SaveData SaveState() const;

	void RegisterSWI(int id, std::function<bool(uint32_t, uint32_t, uint32_t, uint32_t)> func);

	void Reset();

	void Tick(bool step = false);

	bool IsInBios() const;
	bool InAPrivilegedMode() const;
	bool InABankedUserRegistersMode() const;
	Mode GetMode() const;
	void SetMode(Mode mode);
	bool IsInThumbMode() const;
	bool IsInArmMode() const;
	void SetThumbMode(bool thumb);
	bool AreFIQsEnabled() const;
	void EnableFIQs(bool enabled);
	bool AreIRQsEnabled() const;
	void EnableIRQs(bool enabled);
	uint32_t GetRegisterValue(int id);
	void SetRegisterValue(int id, uint32_t value);

	void UpdateMode();
	void SaveHostFlagsToCPSR();
	void LoadHostFlagsFromCPSR();

	void AddBreakpoint(uint32_t address);
	void RemoveBreakpoint(uint32_t address);
	bool IsBreakpoint(uint32_t address);
	uint32_t GetBreakpointInstruction(uint32_t address);
	uint16_t GetThumbBreakpointInstruction(uint32_t address);
	bool IsStalled();

	uint32_t GetPrefetchInstruction() const;

	void ExecuteARM(uint32_t instruction);
	void ExecuteThumb(uint16_t instruction);
private:
	void TickARM(bool step);
	void TickThumb(bool step);
	uint32_t GetShifterOperandLSL(uint32_t instruction);
	uint32_t GetShifterOperandLSLReg(uint32_t instruction);
	uint32_t GetShifterOperandLSR(uint32_t instruction);
	uint32_t GetShifterOperandLSRReg(uint32_t instruction);
	uint32_t GetShifterOperandASR(uint32_t instruction);
	uint32_t GetShifterOperandASRReg(uint32_t instruction);
	uint32_t GetShifterOperandROR(uint32_t instruction);
	uint32_t GetShifterOperandRORReg(uint32_t instruction);
	uint32_t GetShifterOperandImm(uint32_t instruction);
	uint32_t GetShifterOperandLSLFlags(uint32_t instruction);
	uint32_t GetShifterOperandLSLRegFlags(uint32_t instruction);
	uint32_t GetShifterOperandLSRFlags(uint32_t instruction);
	uint32_t GetShifterOperandLSRRegFlags(uint32_t instruction);
	uint32_t GetShifterOperandASRFlags(uint32_t instruction);
	uint32_t GetShifterOperandASRRegFlags(uint32_t instruction);
	uint32_t GetShifterOperandRORFlags(uint32_t instruction);
	uint32_t GetShifterOperandRORRegFlags(uint32_t instruction);
	uint32_t GetShifterOperandImmFlags(uint32_t instruction);

	// SWI implementations
	bool SWI_write(uint32_t ptr, uint32_t size);
	bool SWI_exitEmulator(uint32_t exitcode);

	void SoftwareInterrupt(uint8_t value);
	void IRQ();
	Gba &_system;
	uint32_t _hostFlags;
	uint32_t _registers[16];
	uint32_t _cpsr;
	uint32_t _spsr;
	uint32_t _pipelineInstruction;

	Mode _activeMode;
	uint32_t _registersUser[7];
	uint32_t _registersFIQ[7];
	uint32_t _registersIRQ[2];
	uint32_t _registersABT[2];
	uint32_t _registersUND[2];
	uint32_t _registersSVC[2];
	uint32_t _spsrSVC;
	uint32_t _spsrABT;
	uint32_t _spsrUND;
	uint32_t _spsrIRQ;
	uint32_t _spsrFIQ;

	std::unordered_map<uint32_t, uint32_t> _breakpoints;
	std::array<std::function<bool(uint32_t,uint32_t,uint32_t,uint32_t)>, 256> _swiLookup;
	std::string _outputStr;
};