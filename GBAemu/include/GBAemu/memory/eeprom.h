#ifndef EEPROM_H_
#define EEPROM_H_

#include <GBAemu/memory/cartridgeStorage.h>
#include <GBAemu/memory/memory.h>

class Eeprom : public CartridgeStorage {
public:
	explicit Eeprom(Memory &memory);
	virtual ~Eeprom();

	bool Load(const SaveData_t *data) override;
	bool Save(SaveData_t *data) override;
	emuall::SaveData SaveState() override;
	void LoadState(emuall::SaveData data) override;

	uint32_t GetSize() override;
	uint8_t ReadMemory(uint32_t address) override;
	uint32_t Read32(uint32_t address) override;
	uint32_t Read16(uint32_t address) override;
	uint32_t Read8(uint32_t address) override;
	void Write32(uint32_t address, uint32_t val) override;
	void Write16(uint32_t address, uint16_t val) override;
	void Write8(uint32_t address, uint8_t val) override;

	int GetMemoryPointer(uint32_t address, void **data, uint32_t *size) override;
private:
	enum State {
		Idle,
		ReadAddress,
		ReadData,
		WriteData,
		Writing,
		Reading,
	};

	void Write(int bit);
	int Read();
	int GetDMAAccessSize();

	State _state;
	uint16_t _address;
	int32_t _counter;
	int32_t _dmaAccessSize;
	int32_t _addressSize;
	uint8_t _dataBuffer[8];
	uint32_t _actionTimeStart;

	uint8_t _dataStorage[0x2000];

};

#endif