#ifndef SRAM_H_
#define SRAM_H_

#include <GBAemu/memory/cartridgeStorage.h>

class Memory;

class Sram : public CartridgeStorage {
public:
	explicit Sram(Memory &memory);
	virtual ~Sram();

	bool Load(const SaveData_t *data) override;
	bool Save(SaveData_t *data) override;

	uint32_t GetSize() override;
	uint8_t ReadMemory(uint32_t address) override;
	uint32_t Read32(uint32_t address) override;
	uint32_t Read16(uint32_t address) override;
	uint32_t Read8(uint32_t address) override;
	void Write32(uint32_t address, uint32_t val) override;
	void Write16(uint32_t address, uint16_t val) override;
	void Write8(uint32_t address, uint8_t val) override;

	int GetMemoryPointer(uint32_t address, void **data, uint32_t *size) override;

private:
	uint8_t Read(uint_least16_t address);
	void Write(uint_least16_t address, uint8_t val);

	uint8_t _dataStorage[0x8000];

};

#endif