#ifndef GBAEMU_DEBUGPRINT_H
#define GBAEMU_DEBUGPRINT_H

#include <GBAemu/memory/memory.h>
#include <GBAemu/defines.h>

class DebugPrint : public MemoryEventHandler
{
public:
	DebugPrint(Gba &system);
	~DebugPrint();

	void Init();
	void InitBackups();

	void HandleEvent(uint32_t address, int size) override;

private:
	void RegisterEvents();
	bool HandleSWI();
	void WritePrintCode();
	void EnableAGBPrint();
	void DisableAGBPrint();

	Gba &_system;
	uint16_t _controlBackup[4];
	uint16_t _functionBackup[2];
	uint16_t _protectBackup;
	uint8_t _bufferBackup[AGBPRINT_BUFFERSIZE];
	uint8_t _msgBuffer[AGBPRINT_BUFFERSIZE];
	bool _isVisible;
	uint16_t _get;
	uint16_t _put;
	uint32_t _addr;
};

#endif