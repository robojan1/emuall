#include <GBAemu/gba.h>
#include <GBAemu/defines.h>
#include <GBAemu/cpu/armException.h>

#include <emuall/util/log.h>
#include <emuall/util/savedata.h>

#include <cstring>

Gba::Gba() :
	_memory(*this), _cpu(*this), _gpu(*this), _input(*this), _dbgPrint(*this),
	_if(0), _halted(false), _stopped(false), _running(false), _disassembler(*this),
	_timerInfo{ {0}, {0}, {0}, {0} }, _callbackFuncs{0,}, _detectable(false)
{
}

Gba::~Gba()
{

}

int Gba::Init()
{
	try {
		_memory.Init();
		_cpu.Init();
		_gpu.Init();
		_input.Init();
		_dbgPrint.Init();

		_memory.RegisterEvent(IE, this);
		_memory.RegisterEvent(IME, this);
		_memory.RegisterEvent(POSTFLG, this);
		_memory.RegisterEvent(TM0CNT_L, this);
		_memory.RegisterEvent(TM1CNT_L, this);
		_memory.RegisterEvent(TM2CNT_L, this);
		_memory.RegisterEvent(TM3CNT_L, this);

		_detectable = GetOptionBool(OPTIONID_DETECTABLE);
		if(_detectable) {
			InitEmulatorInterface();
		}

		InitRegisters();
		return 1;
	} catch(BaseException &ex) {
		logger::Log(logger::Error, "Failed to initialize the gba emulator: %s", ex.GetMsg());
		return 0;
	}
}

int Gba::Load(const SaveData_t *data)
{
	int result = _memory.Load(data);
	if (result != 0) {
		_dbgPrint.InitBackups();
	}
	return result;
}

int Gba::Save(SaveData_t * data)
{
	return _memory.Save(data);
}

int Gba::LoadState(const SaveData_t * data)
{
	if (!Load(data)) return 0;
	if(data->miscData != nullptr)
	{
		emuall::SaveData state(data->miscData, data->miscDataLen);
		state >> _if >> _halted >> _stopped;
		for(int i = 0; i < 4; i++)
		{
			_timerInfo[i].prescaler = state.Get<uint16_t>();
			_timerInfo[i].cnt = state.Get<uint16_t>();
			_timerInfo[i].prescalerMatch = state.Get<uint16_t>();
			_timerInfo[i].val = state.Get<uint16_t>();
			_timerInfo[i].reload = state.Get<uint16_t>();
		}
		_memory.LoadState(state.GetChild());
		_cpu.LoadState(state.GetChild());
		_gpu.LoadState(state.GetChild());
		_input.LoadState(state.GetChild());
	}
	return 0;
}

int Gba::SaveState(SaveData_t * data)
{
	if (!Save(data)) return 0;
	emuall::SaveData state(1);
	state << _if << _halted << _stopped;
	for (int i = 0; i < 4; i++)
	{
		state << static_cast<uint16_t>(_timerInfo[i].prescaler);
		state << static_cast<uint16_t>(_timerInfo[i].cnt);
		state << static_cast<uint16_t>(_timerInfo[i].prescalerMatch);
		state << static_cast<uint16_t>(_timerInfo[i].val);
		state << static_cast<uint16_t>(_timerInfo[i].reload);
	}
	auto memData = _memory.SaveState();
	auto cpuData = _cpu.SaveState();
	auto gpuData = _gpu.SaveState();
	auto inputData = _input.SaveState();
	state << memData << cpuData << gpuData << inputData;
	auto &resultData = state.GetData();
	data->miscDataLen = resultData.size();
	data->miscData = new uint8_t[data->miscDataLen];
	memcpy(data->miscData, resultData.data(), data->miscDataLen);
	return 1;
}

void Gba::Step()
{
	if (!_stopped) {
		while (_memory.IsDMAActive()) {
			_memory.Tick();
			_gpu.Tick();
			TimerTick();
		}
		TimerTick();
		_memory.Tick();
		if (!_halted) {
			try {
				_cpu.Tick(true);
			}
			catch (BreakPointARMException &e) {
				logger::Log(logger::Message, "Breakpoint hit %d", e.addr);
			}
			catch (DataAbortARMException &e) {
				logger::Log(logger::Error, "Data Abort ARM Exception");
			}
		}
		else {
			uint8_t *registers = _memory.GetRegisters();
			if ((IOREG8(registers, IF) & IOREG8(registers, IE) & IRQ_MASK) != 0) {
				_halted = false;
				_stopped = false;
			}
		}
		_gpu.Tick();
	}
	else {
		uint8_t *registers = _memory.GetRegisters();
		if ((IOREG8(registers, IF) & IOREG8(registers, IE) & IRQ_MASK) != 0) {
			_halted = false;
			_stopped = false;
		}
	}
}

int Gba::Tick(unsigned int time)
{
	if (!IsRunning())
		return 0;
	int execute = ((uint64_t)time * FCPU + 500000) / 1000000;
	// Clear breakpoint
	Step(); Step();
	for (int i = execute; i != 0; --i)
	{
		if (!_stopped) {
			TimerTick();
			_memory.Tick();
			if (!_halted && !_memory.IsDMAActive()) {
				try {
					_cpu.Tick();
				}
				catch (BreakPointARMException &e) {
					logger::Log(logger::Message, "Breakpoint hit %d", e.addr);
					Run(false);
					_callbackFuncs.OnBreakpointHit(_callbackFuncs.user, e.addr);
					break;
				}
				catch (DataAbortARMException &e) {
					Run(false);
					logger::Log(logger::Error, "Data Abort ARM Exception");
					_callbackFuncs.OnBreakpointHit(_callbackFuncs.user, e._address);
					break;
				}
			}
			else {
				uint8_t *registers = _memory.GetRegisters();
				if ((IOREG8(registers, IF) & IOREG8(registers, IE) & IRQ_MASK) != 0) {
					_halted = false;
					_stopped = false;
				}
			}
			_gpu.Tick();
		}
		else {
			uint8_t *registers = _memory.GetRegisters();
			if ((IOREG8(registers, IF) & IOREG8(registers, IE) & IRQ_MASK) != 0) {
				_halted = false;
				_stopped = false;
			}
		}
	}
	return 1;
}

void Gba::Run(bool run /*= true*/)
{
	_running = run;
	if (run)
	{
		logger::Log(logger::Message, "Start running the cpu");
	}
	else {
		logger::Log(logger::Message, "Stop running the cpu");
	}
}

bool Gba::IsRunning() const
{
	return _running;
}

void Gba::RequestIRQ(int mask)
{
	uint8_t *registers = _memory.GetRegisters();
	_if |= mask;
	IOREG16(registers, IF) = _if;
}

void Gba::HandleEvent(uint32_t address, int size)
{
	uint8_t *registers = _memory.GetRegisters();
	if (address <= IE + 1 && IE < address + size) {
		IOREG16(registers, IE) |= 0xC000;
	}
	if (address <= IF + 1 && IF < address + size) {
		_if &= (~IOREG16(registers, IF) | 0xC000);
		IOREG16(registers, IF) = _if;
	}
	if (address <= IME + 3 && IME < address + size) {
		IOREG32(registers, IME) |= 0xFFFFFFFE;
	}
	if (address <= HALTCNT && HALTCNT < address + size) {
		if ((IOREG8(registers, HALTCNT) & 0x80) != 0) {
			PowerModeStop();
		}
		else {
			PowerModeHalt();
		}
		IOREG8(registers, HALTCNT) = 0;
	}
	if (address <= TM0CNT_L + 1 && TM0CNT_L < address + size) {
		_timerInfo[0].reload = IOREG16(registers, TM0CNT_L);
		IOREG16(registers, TM0CNT_L) = _timerInfo[0].val;
	}
	if (address <= TM1CNT_L + 1 && TM1CNT_L < address + size) {
		_timerInfo[1].reload = IOREG16(registers, TM1CNT_L);
		IOREG16(registers, TM1CNT_L) = _timerInfo[1].val;
	}
	if (address <= TM2CNT_L + 1 && TM2CNT_L < address + size) {
		_timerInfo[2].reload = IOREG16(registers, TM2CNT_L);
		IOREG16(registers, TM2CNT_L) = _timerInfo[2].val;
	}
	if (address <= TM3CNT_L + 1 && TM3CNT_L < address + size) {
		_timerInfo[3].reload = IOREG16(registers, TM3CNT_L);
		IOREG16(registers, TM3CNT_L) = _timerInfo[3].val;
	}
	if (address <= TM0CNT_H + 1 && TM0CNT_H < address + size) {
		uint16_t newVal = IOREG16(registers, TM0CNT_H);
		if ((_timerInfo[0].cnt ^ newVal) & (1<<7)) {
			_timerInfo[0].val = _timerInfo[0].reload;
			IOREG16(registers, TM0CNT_L) = _timerInfo[0].val;
		}
		switch (newVal & 3) {
		case 0: _timerInfo[0].prescalerMatch = 0; break;
		case 1: _timerInfo[0].prescalerMatch = 63; break;
		case 2: _timerInfo[0].prescalerMatch = 255; break;
		case 3: _timerInfo[0].prescalerMatch = 1023; break;
		}
		_timerInfo[0].cnt = newVal;
	}
	if (address <= TM1CNT_H + 1 && TM1CNT_H < address + size) {
		uint16_t newVal = IOREG16(registers, TM1CNT_H);
		if ((_timerInfo[1].cnt ^ newVal) & (1 << 7)) {
			_timerInfo[1].val = _timerInfo[1].reload;
			IOREG16(registers, TM1CNT_L) = _timerInfo[1].val;
		}
		switch (newVal & 3) {
		case 0: _timerInfo[1].prescalerMatch = 0; break;
		case 1: _timerInfo[1].prescalerMatch = 63; break;
		case 2: _timerInfo[1].prescalerMatch = 255; break;
		case 3: _timerInfo[1].prescalerMatch = 1023; break;
		}
		_timerInfo[1].cnt = newVal;
	}
	if (address <= TM2CNT_H + 1 && TM2CNT_H < address + size) {
		uint16_t newVal = IOREG16(registers, TM2CNT_H);
		if ((_timerInfo[2].cnt ^ newVal) & (1 << 7)) {
			_timerInfo[2].val = _timerInfo[2].reload;
			IOREG16(registers, TM2CNT_L) = _timerInfo[2].val;
		}
		switch (newVal & 3) {
		case 0: _timerInfo[2].prescalerMatch = 0; break;
		case 1: _timerInfo[2].prescalerMatch = 63; break;
		case 2: _timerInfo[2].prescalerMatch = 255; break;
		case 3: _timerInfo[2].prescalerMatch = 1023; break;
		}
		_timerInfo[2].cnt = newVal;
	}
	if (address <= TM3CNT_H + 1 && TM3CNT_H < address + size) {
		uint16_t newVal = IOREG16(registers, TM3CNT_H);
		if ((_timerInfo[3].cnt ^ newVal) & (1 << 7)) {
			_timerInfo[3].val = _timerInfo[3].reload;
			IOREG16(registers, TM3CNT_L) = _timerInfo[3].val;
		}
		switch (newVal & 3) {
		case 0: _timerInfo[3].prescalerMatch = 0; break;
		case 1: _timerInfo[3].prescalerMatch = 63; break;
		case 2: _timerInfo[3].prescalerMatch = 255; break;
		case 3: _timerInfo[3].prescalerMatch = 1023; break;
		}
		_timerInfo[3].cnt = newVal;
	}
	if(address <= EMULATORID + 3 && EMULATORID <= address + size) {
		IOREG32(registers, EMULATORID) = EMULATORID_VAL;
	}
}


void Gba::OnKey(int id, bool pressed)
{
	_input.OnKeyPressed(id, pressed);
}

void Gba::InitRegisters()
{
	uint8_t *registers = _memory.GetRegisters();
	IOREG16(registers, IE) = 0xC000;
	_if = 0xC000;
	IOREG16(registers, IF) = _if;
	IOREG32(registers, IME) = 0xFFFFFFFE;
}

void Gba::PowerModeStop()
{
	_halted = true;
	_stopped = true;
}

void Gba::PowerModeHalt()
{
	_halted = true;
	_stopped = false;
}

void Gba::TimerTick()
{
	bool lastOverflow = false;
	for (int i = 0; i < 4; i++) {
		if (_timerInfo[i].cnt & (1 << 7)) {
			// enabled
			if (_timerInfo[i].cnt & (1 << 2) && lastOverflow) {
				// Count up
				if (_timerInfo[i].val == 0xFFFF) {
					_timerInfo[i].val = _timerInfo[i].reload;
					lastOverflow = true;
					if (_timerInfo[i].cnt & (1 << 6)) {
						RequestIRQ(IRQ_TIM0 << i);
					}
				}
				else {
					_timerInfo[i].val++;
					lastOverflow = false;
				}
				IOREG16(_memory.GetRegisters(), TM0CNT_L + 4 * i) = _timerInfo[i].val;
				_timerInfo[i].prescaler = 0;
			}
			else if(_timerInfo[i].prescaler == _timerInfo[i].prescalerMatch){
				_timerInfo[i].prescaler = 0;
				if (_timerInfo[i].val == 0xFFFF) {
					_timerInfo[i].val = _timerInfo[i].reload;
					lastOverflow = true;
					if (_timerInfo[i].cnt & (1 << 6)) {
						RequestIRQ(IRQ_TIM0 << i);
					}
				}
				else {
					_timerInfo[i].val++;
					lastOverflow = false;
				}
				IOREG16(_memory.GetRegisters(), TM0CNT_L + 4 * i) = _timerInfo[i].val;
			}
			else {
				lastOverflow = false;
				_timerInfo[i].prescaler++;
			}
		}
	}

}

void Gba::SetCallbackFunctions(const callBackfunctions_t &funcs) {
	_callbackFuncs = funcs;
}

void Gba::DrawFrame(int id) {
	if(_callbackFuncs.DrawFrame != nullptr) {
		_callbackFuncs.DrawFrame(_callbackFuncs.user, id);
	}
}

float Gba::GetOptionFloat(int id) {
	if(_callbackFuncs.GetOptionFloat) {
		return _callbackFuncs.GetOptionFloat(_callbackFuncs.user, id);
	}
	return 0;
}

std::string Gba::GetOptionString(int id) {
	if(_callbackFuncs.GetOptionString) {
		const char *resultCstr = _callbackFuncs.GetOptionString(_callbackFuncs.user, id);
		std::string result = resultCstr;
		_callbackFuncs.ReleaseString(_callbackFuncs.user, resultCstr);
		return result;
	}
	return {};
}

int Gba::GetOptionInt(int id) {
	if(_callbackFuncs.GetOptionInt) {
		return _callbackFuncs.GetOptionInt(_callbackFuncs.user, id);
	}
	return 0;
}

void Gba::InitEmulatorInterface() {
	_memory.RegisterEvent(EMULATORID, this);
	IOREG32(_memory.GetRegisters(), EMULATORID) = EMULATORID_VAL;
}

void Gba::ExitEmulator(uint32_t exitCode) {
	if(_callbackFuncs.ExitEmulator) {
		return _callbackFuncs.ExitEmulator(_callbackFuncs.user, exitCode);
	}
}
