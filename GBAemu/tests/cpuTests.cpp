//
// Created by Robbert-Jan de Jager on 29-9-18.
//

#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <GBAemu/cpu/cpu.h>
#include <GBAemu/gba.h>
#include <GBAemu/cpu/hostInstructions.h>
/*
namespace logger {
	void Log(enum loglevel level, const char *fmt, ...)
	{
		va_list args;
		va_start(args, fmt);
		vprintf(fmt, args);
		va_end(args);
	}
}*/

std::function<uint32_t(uint32_t)> _mem_read32;
std::function<void(uint32_t, uint32_t, bool)> _mem_write32;

Memory::Memory(Gba &gba) : _system(gba) {}
Memory::~Memory() {}
uint8_t Memory::Read8(uint32_t address) { return 0; }
uint16_t Memory::Read16(uint32_t address) { return 0; }
uint32_t Memory::Read32(uint32_t address) { return _mem_read32 ? _mem_read32(address) : 0; }
void Memory::Write8(uint32_t address, uint8_t value, bool event){}
void Memory::Write16(uint32_t address, uint16_t value, bool event){}
void Memory::Write32(uint32_t address, uint32_t value, bool event) { if (_mem_write32) _mem_write32(address, value, event); }
void Memory::ManagedWrite32(uint32_t address, uint32_t value) {}
uint32_t Memory::ManagedRead32(uint32_t address) { return address; }
void Memory::SetBiosProtected(uint32_t word) {}
uint8_t *Memory::GetRegisters() {return nullptr; }
void Memory::HandleEvent(uint32_t address, int size) {}
Gba::Gba() : _memory(*this), _cpu(*this), _gpu(*this), _input(*this), _disassembler(*this), _dbgPrint(*this) {}
int Gba::Init() {
	_cpu.Init();
	return 1;
};
Gba::~Gba() {}
void Gba::HandleEvent(uint32_t address, int size) {}
void Gba::ExitEmulator(uint32_t exitCode) {}
Gpu::Gpu(Gba &gba) : _system(gba) {}
Gpu::~Gpu() {}
void Gpu::HandleEvent(uint32_t address, int size) {}
KeyInput::KeyInput(Gba &gba) : _system(gba) {}
KeyInput::~KeyInput() {}
void KeyInput::HandleEvent(uint32_t address, int size) {}
Disassembler::Disassembler(Gba &system) : _system(system) {}
Disassembler::~Disassembler() {}
DebugPrint::DebugPrint(Gba &system) : _system(system) {}
DebugPrint::~DebugPrint() {}
void DebugPrint::HandleEvent(uint32_t, int) {}

int Gba::GetOptionInt(int id ) {
	switch(id) {
		case OPTIONID_DETECTABLE: return 1;
		default: return 0;
	}
}

TEST(GBACpuTest, InitialRegisterState)
{
	Gba gba;
	gba.Init();
	for(int i = 0; i < 16; i++) {
		EXPECT_EQ(gba.GetCpu().GetRegisterValue(i), 0);
	}
	EXPECT_EQ(gba.GetCpu().GetRegisterValue(16), 0x000000D3);
	gba.GetCpu().SetMode(Cpu::Mode::User);
	for(int i = 0; i < 16; i++) {
		EXPECT_EQ(gba.GetCpu().GetRegisterValue(i), 0);
	}
	gba.GetCpu().SetMode(Cpu::Mode::FIQ);
	for(int i = 0; i < 16; i++) {
		EXPECT_EQ(gba.GetCpu().GetRegisterValue(i), 0);
	}
	gba.GetCpu().SetMode(Cpu::Mode::IRQ);
	for(int i = 0; i < 16; i++) {
		EXPECT_EQ(gba.GetCpu().GetRegisterValue(i), 0);
	}
	gba.GetCpu().SetMode(Cpu::Mode::Supervisor);
	for(int i = 0; i < 16; i++) {
		EXPECT_EQ(gba.GetCpu().GetRegisterValue(i), 0);
	}
	gba.GetCpu().SetMode(Cpu::Mode::Abort);
	for(int i = 0; i < 16; i++) {
		EXPECT_EQ(gba.GetCpu().GetRegisterValue(i), 0);
	}
	gba.GetCpu().SetMode(Cpu::Mode::Undefined);
	for(int i = 0; i < 16; i++) {
		EXPECT_EQ(gba.GetCpu().GetRegisterValue(i), 0);
	}
	gba.GetCpu().SetMode(Cpu::Mode::System);
	for(int i = 0; i < 16; i++) {
		EXPECT_EQ(gba.GetCpu().GetRegisterValue(i), 0);
	}
}

struct GBAInstructionState {
	uint32_t inputRegisters[17];
	uint32_t outputRegisters[17];
	uint32_t instruction;
};

class GBAInstructionTest : public ::testing::TestWithParam<GBAInstructionState> {

protected:
	virtual void SetUp() {
		gba.Init();
		Cpu &cpu = gba.GetCpu();
		cpu.EnableFIQs(false);
		cpu.EnableIRQs(false);
		cpu.SetMode(Cpu::Mode::Supervisor);
	}

	Gba gba;
};

TEST_P(GBAInstructionTest, InstructionARMTest)
{
	auto &param = GetParam();
	Cpu &cpu = gba.GetCpu();
	for(int i = 0; i < 17; i++) {
		cpu.SetRegisterValue(i, param.inputRegisters[i]);
	}
	cpu.ExecuteARM(param.instruction);
	for(int i = 0; i < 17; i++) {
		EXPECT_EQ(cpu.GetRegisterValue(i), param.outputRegisters[i]) <<
			"Instruction " << std::hex << std::setfill('0') << std::setw(8) <<  " failed: Register " <<
			std::dec << std::setw(1) << i << " was " << std::hex << std::setw(8) << cpu.GetRegisterValue(i) <<
			" Expected " << param.outputRegisters[i] << std::endl;
	}
}

INSTANTIATE_TEST_CASE_P(ArmTest, GBAInstructionTest, testing::Values(
		GBAInstructionState{{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0xD3}, {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,4,0xD3}, 0},
		GBAInstructionState{{0x630,0xE0,0xE0,0,0xE0,0x01,0x4000200,0x3003B2C,0,0,0,0,0,0x3007EA0,0x1731,0x3Dc,0xD3}, {0x630,0xE0,0xE0,0,0xE0,0x01,0x4000200,0x3003B2C,0,0,0,0,0,0x3007EA0,0x1731,0x3E0,0xD3}, 0xE0A33003}
		));

struct HostInstructionState {
	const char *name;
	void (*func)(uint32_t a, uint32_t b, uint32_t &result);
	uint32_t a, b;
	uint32_t expected_result;
};

struct HostInstructionFlagState {
	const char *name;
	void (*func)(uint32_t a, uint32_t b, uint32_t &result, uint32_t &flags);
	uint32_t a, b;
	uint32_t expected_result;
	uint32_t initial_flags;
	uint32_t used_flags;
	uint32_t expected_flags;
};

struct HostInstructionMovState {
	const char *name;
	void (*func)(uint32_t x, uint32_t &flags);
	uint32_t x;
	uint32_t initial_flags;
	uint32_t used_flags;
	uint32_t expected_flags;
};

struct HostTestInstructionState {
	const char *name;
	void (*func)(uint32_t a, uint32_t b, uint32_t &flags);
	uint32_t a, b;
	uint32_t used_flags;
	uint32_t expected_flags;
};

struct HostInstructionShiftState {
	const char *name;
	void (*func)(uint32_t x, uint8_t amount, uint32_t &result);
	uint32_t x;
	uint8_t amount;
	uint32_t expected_result;
};

struct HostInstructionShiftFlagsState {
	const char *name;
	void (*func)(uint32_t x, uint8_t amount, uint32_t &result, uint32_t &flags);
	uint32_t x;
	uint8_t amount;
	uint32_t expected_result;
	uint32_t initial_flags;
	uint32_t used_flags;
	uint32_t expected_flags;
};

class HostInstructionTest : public ::testing::TestWithParam<HostInstructionState> {
};

class HostInstructionFlagTest : public ::testing::TestWithParam<HostInstructionFlagState> {
};

class HostInstructionMovTest : public ::testing::TestWithParam<HostInstructionMovState> {
};

class HostTestInstructionTest : public ::testing::TestWithParam<HostTestInstructionState> {
};

class HostInstructionShiftTest : public ::testing::TestWithParam<HostInstructionShiftState> {
};

class HostInstructionShiftFlagsTest : public ::testing::TestWithParam<HostInstructionShiftFlagsState> {
};

class HostInstructionShiftCFlagsTest : public ::testing::TestWithParam<HostInstructionShiftFlagsState> {
};

TEST_P(HostInstructionTest, HostInstructionTest)
{
	uint32_t result = 0x01234567;
	auto param = GetParam();
	param.func(param.a, param.b, result);
	EXPECT_EQ(result, param.expected_result) << param.name << "(" << std::hex << std::setfill('0') << std::setw(8)
		<< param.a << "," << param.b << ")<" << result << "> != " << param.expected_result << std::endl;
}

TEST_P(HostTestInstructionTest, HostInstructionTest)
{
	uint32_t flags = 0;
	auto param = GetParam();
	param.func(param.a, param.b, flags);
	EXPECT_EQ(flags & param.used_flags, param.expected_flags) << param.name << "(" << std::hex << std::setfill('0') << std::setw(8)
		<< param.a << "," << param.b << ")<"<< (flags & param.used_flags) << "> != " << param.expected_flags << std::endl;
	EXPECT_EQ(flags & ~param.used_flags & 0xCD5, 0) << param.name << "(" << std::hex << std::setfill('0') << std::setw(8)
        << param.a << "," << param.b << ")<"<< (flags & ~param.used_flags) << "> != " << (0x000 & ~param.used_flags) << std::endl;
	flags = 0xCD5;
	param.func(param.a, param.b, flags);
	EXPECT_EQ(flags & param.used_flags, param.expected_flags) << param.name << "(" << std::hex << std::setfill('0') << std::setw(8)
		<< param.a << "," << param.b << ")<"<< (flags & param.used_flags) << "> != " << param.expected_flags << std::endl;
	EXPECT_EQ(flags & ~param.used_flags & 0xCD5, 0xCD5 & ~param.used_flags) << param.name << "(" << std::hex << std::setfill('0') << std::setw(8)
        << param.a << "," << param.b << ")<"<< (flags & ~param.used_flags) << "> != " << (0xCD5 & ~param.used_flags) << std::endl;
}

TEST_P(HostInstructionFlagTest, HostInstructionFlagTest)
{
	auto param = GetParam();
	uint32_t flags = param.initial_flags;
	uint32_t result = 0x01234567;
	param.func(param.a, param.b, result, flags);
	EXPECT_EQ(result, param.expected_result) << "Result" << param.name << "(" << std::hex << std::setfill('0') << std::setw(8)
	                                         << param.a << "," << param.b << "," << param.initial_flags << ")<" << result << "> != " << param.expected_result << std::endl;
	EXPECT_EQ(flags & param.used_flags, param.expected_flags) << "Flags:" << param.name << "(" << std::hex << std::setfill('0') << std::setw(8)
        << param.a << "," << param.b << "," << param.initial_flags << ")<"<< (flags & param.used_flags) << "> != " << param.expected_flags << std::endl;

	EXPECT_EQ(flags & ~param.used_flags & 0xCD5, param.initial_flags & ~param.used_flags) << param.name << "(" << std::hex << std::setfill('0') << std::setw(8)
	    << param.a << "," << param.b << ")<"<< (flags & ~param.used_flags) << "> != " << (param.initial_flags & ~param.used_flags) << std::endl;
}


TEST_P(HostInstructionMovTest, HostInstructionMovTest)
{
	auto param = GetParam();
	uint32_t flags = param.initial_flags;
	param.func(param.x, flags);
	EXPECT_EQ(flags & param.used_flags, param.expected_flags) << param.name << "(" << std::hex << std::setfill('0') << std::setw(8)
      << param.x << "," << param.initial_flags << ")<"<< (flags & param.used_flags) << "> != " << param.expected_flags << std::endl;
	EXPECT_EQ(flags & ~param.used_flags & 0xCD5, param.initial_flags & ~param.used_flags) << param.name << "(" << std::hex << std::setfill('0') << std::setw(8)
	  << param.x << "," << param.initial_flags << ")<"<< (flags & ~param.used_flags) << "> != " << param.expected_flags << std::endl;
}

TEST_P(HostInstructionShiftTest, HostInstructionShiftTest)
{
	uint32_t result = 0x01234567;
	auto param = GetParam();
	param.func(param.x, param.amount, result);
	EXPECT_EQ(result, param.expected_result) << param.name << "(" << std::hex << std::setfill('0') << std::setw(8)
	                                         << param.x << "," << static_cast<uint32_t>(param.amount) << ")<" << result << "> != " << param.expected_result << std::endl;
}

TEST_P(HostInstructionShiftFlagsTest, HostInstructionShiftFlagsTest)
{
	uint32_t result = 0x01234567;
	auto param = GetParam();
	uint32_t flags = param.initial_flags;

	param.func(param.x, param.amount, result, flags);
	EXPECT_EQ(result, param.expected_result) << param.name << "(" << std::hex << std::setfill('0') << std::setw(8)
	                                         << param.x << "," << static_cast<uint32_t>(param.amount) << ")<"
	                                         << result << "> != " << param.expected_result << std::endl;
	EXPECT_EQ(flags & param.used_flags, param.expected_flags) << param.name << "(" << std::hex << std::setfill('0') << std::setw(8)
                                             << param.x << "," << static_cast<uint32_t>(param.amount) << ")<"
                                             << (flags & param.used_flags) << "> != " << param.expected_flags << std::endl;
	EXPECT_EQ(flags & ~param.used_flags & 0xCD5, param.initial_flags & ~param.used_flags) << param.name << "(" << std::hex << std::setfill('0') << std::setw(8)
                                          << param.x << "," << static_cast<uint32_t>(param.amount) << ")<"
                                          << (flags & ~param.used_flags & 0xCD5) << "> != " << (param.initial_flags & ~param.used_flags ) << std::endl;
}

TEST_P(HostInstructionShiftCFlagsTest, HostInstructionShiftCFlagsTest)
{
	uint32_t result = 0x01234567;
	auto param = GetParam();
	uint32_t flags = param.initial_flags;

	param.func(param.x, param.amount, result, flags);
	EXPECT_EQ(result, param.expected_result) << param.name << "(" << std::hex << std::setfill('0') << std::setw(8)
	                                         << param.x << "," << static_cast<uint32_t>(param.amount) << ")<"
	                                         << result << "> != " << param.expected_result << std::endl;
	EXPECT_EQ(flags & param.used_flags, param.expected_flags) << param.name << "(" << std::hex << std::setfill('0') << std::setw(8)
		<< param.x << "," << static_cast<uint32_t>(param.amount) << ")<"
		<< (flags & param.used_flags) << "> != " << param.expected_flags << std::endl;
	EXPECT_EQ(flags & ~param.used_flags & 0xCD5, param.initial_flags & ~param.used_flags) << param.name << "(" << std::hex << std::setfill('0') << std::setw(8)
          << param.x << "," << static_cast<uint32_t>(param.amount) << ")<"
          << (flags & ~param.used_flags & 0xCD5) << "> != " << (param.initial_flags & ~param.used_flags ) << std::endl;
}

INSTANTIATE_TEST_CASE_P(HostInstructionTest, HostInstructionTest, testing::Values(
		HostInstructionState{"AND", AND, 0x55555555, 0xaaaaaaaa, 0x00000000},
		HostInstructionState{"AND", AND, 0x55555555, 0x00FFFF00, 0x00555500},
		HostInstructionState{"AND", AND, 0x55555555, 0xaaaaFFFF, 0x00005555},
		HostInstructionState{"AND", AND, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF},
		HostInstructionState{"AND", AND, 0xFFFFFFFF, 0x00000000, 0x00000000},
		HostInstructionState{"EOR", EOR, 0x55555555, 0xaaaaaaaa, 0xFFFFFFFF},
		HostInstructionState{"EOR", EOR, 0x55555555, 0x00FFFF00, 0x55aaaa55},
		HostInstructionState{"EOR", EOR, 0x55555555, 0xaaaaFFFF, 0xFFFFaaaa},
		HostInstructionState{"EOR", EOR, 0xFFFFFFFF, 0xFFFFFFFF, 0x00000000},
		HostInstructionState{"EOR", EOR, 0xFFFFFFFF, 0x00000000, 0xFFFFFFFF},
		HostInstructionState{"SUB", SUB, 0x55555555, 0xaaaaaaaa, 0xAAAAAAAB},
		HostInstructionState{"SUB", SUB, 0x55555555, 0x00FFFF00, 0x54555655},
		HostInstructionState{"SUB", SUB, 0x55555555, 0xaaaaFFFF, 0xaaaa5556},
		HostInstructionState{"SUB", SUB, 0xFFFFFFFF, 0xFFFFFFFF, 0x00000000},
		HostInstructionState{"SUB", SUB, 0xFFFFFFFF, 0x00000000, 0xFFFFFFFF},
		HostInstructionState{"ADD", ADD, 0x55555555, 0xaaaaaaaa, 0xFFFFFFFF},
		HostInstructionState{"ADD", ADD, 0x55555555, 0x00FFFF00, 0x56555455},
		HostInstructionState{"ADD", ADD, 0x55555555, 0xaaaaFFFF, 0x00005554},
		HostInstructionState{"ADD", ADD, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFE},
		HostInstructionState{"ADD", ADD, 0xFFFFFFFF, 0x00000000, 0xFFFFFFFF},
		HostInstructionState{"ORR", ORR, 0x55555555, 0xaaaaaaaa, 0xFFFFFFFF},
		HostInstructionState{"ORR", ORR, 0x55555555, 0x00FFFF00, 0x55FFFF55},
		HostInstructionState{"ORR", ORR, 0x55555555, 0xaaaaFFFF, 0xFFFFFFFF},
		HostInstructionState{"ORR", ORR, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF},
		HostInstructionState{"ORR", ORR, 0xFFFFFFFF, 0x00000000, 0xFFFFFFFF},
		HostInstructionState{"BIC", BIC, 0x55555555, 0xaaaaaaaa, 0x55555555},
		HostInstructionState{"BIC", BIC, 0x55555555, 0x00FFFF00, 0x55000055},
		HostInstructionState{"BIC", BIC, 0x55555555, 0xaaaaFFFF, 0x55550000},
		HostInstructionState{"BIC", BIC, 0xFFFFFFFF, 0xFFFFFFFF, 0x00000000},
		HostInstructionState{"BIC", BIC, 0xFFFFFFFF, 0x00000000, 0xFFFFFFFF}
		));

INSTANTIATE_TEST_CASE_P(HostInstructionFlagTest, HostInstructionFlagTest, testing::Values(
		HostInstructionFlagState{"AND_FLAGS", AND_FLAGS, 0x55555555, 0xaaaaaaaa, 0x00000000, 0xCD5, 0x0C0, 0x040},
		HostInstructionFlagState{"AND_FLAGS", AND_FLAGS, 0x55555555, 0x00FFFF00, 0x00555500, 0xCD5, 0x0C0, 0x000},
		HostInstructionFlagState{"AND_FLAGS", AND_FLAGS, 0x55555555, 0xaaaaFFFF, 0x00005555, 0xCD5, 0x0C0, 0x000},
		HostInstructionFlagState{"AND_FLAGS", AND_FLAGS, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xCD5, 0x0C0, 0x080},
		HostInstructionFlagState{"AND_FLAGS", AND_FLAGS, 0xFFFFFFFF, 0x00000000, 0x00000000, 0xCD5, 0x0C0, 0x040},
		HostInstructionFlagState{"EOR_FLAGS", EOR_FLAGS, 0x55555555, 0xaaaaaaaa, 0xFFFFFFFF, 0xCD5, 0x0C0, 0x080},
		HostInstructionFlagState{"EOR_FLAGS", EOR_FLAGS, 0x55555555, 0x00FFFF00, 0x55aaaa55, 0xCD5, 0x0C0, 0x000},
		HostInstructionFlagState{"EOR_FLAGS", EOR_FLAGS, 0x55555555, 0xaaaaFFFF, 0xFFFFaaaa, 0xCD5, 0x0C0, 0x080},
		HostInstructionFlagState{"EOR_FLAGS", EOR_FLAGS, 0xFFFFFFFF, 0xFFFFFFFF, 0x00000000, 0xCD5, 0x0C0, 0x040},
		HostInstructionFlagState{"EOR_FLAGS", EOR_FLAGS, 0xFFFFFFFF, 0x00000000, 0xFFFFFFFF, 0xCD5, 0x0C0, 0x080},
		HostInstructionFlagState{"SUB_FLAGS", SUB_FLAGS, 0x55555555, 0xaaaaaaaa, 0xAAAAAAAB, 0xCD5, 0x8C1, 0x880},
		HostInstructionFlagState{"SUB_FLAGS", SUB_FLAGS, 0x55555555, 0x00FFFF00, 0x54555655, 0xCD5, 0x8C1, 0x001},
		HostInstructionFlagState{"SUB_FLAGS", SUB_FLAGS, 0x55555555, 0xaaaaFFFF, 0xaaaa5556, 0xCD5, 0x8C1, 0x880},
		HostInstructionFlagState{"SUB_FLAGS", SUB_FLAGS, 0xFFFFFFFF, 0xFFFFFFFF, 0x00000000, 0xCD5, 0x8C1, 0x041},
		HostInstructionFlagState{"SUB_FLAGS", SUB_FLAGS, 0xFFFFFFFF, 0x00000000, 0xFFFFFFFF, 0xCD5, 0x8C1, 0x081},
		HostInstructionFlagState{"ADD_FLAGS", ADD_FLAGS, 0x55555555, 0xaaaaaaaa, 0xFFFFFFFF, 0xCD5, 0x8C1, 0x080},
		HostInstructionFlagState{"ADD_FLAGS", ADD_FLAGS, 0x55555555, 0x00FFFF00, 0x56555455, 0xCD5, 0x8C1, 0x000},
		HostInstructionFlagState{"ADD_FLAGS", ADD_FLAGS, 0x55555555, 0xaaaaFFFF, 0x00005554, 0xCD5, 0x8C1, 0x001},
		HostInstructionFlagState{"ADD_FLAGS", ADD_FLAGS, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFE, 0xCD5, 0x8C1, 0x081},
		HostInstructionFlagState{"ADD_FLAGS", ADD_FLAGS, 0xFFFFFFFF, 0x00000000, 0xFFFFFFFF, 0xCD5, 0x8C1, 0x080},
		HostInstructionFlagState{"ORR_FLAGS", ORR_FLAGS, 0x55555555, 0xaaaaaaaa, 0xFFFFFFFF, 0xCD5, 0x0C0, 0x080},
		HostInstructionFlagState{"ORR_FLAGS", ORR_FLAGS, 0x55555555, 0x00FFFF00, 0x55FFFF55, 0xCD5, 0x0C0, 0x000},
		HostInstructionFlagState{"ORR_FLAGS", ORR_FLAGS, 0x55555555, 0xaaaaFFFF, 0xFFFFFFFF, 0xCD5, 0x0C0, 0x080},
		HostInstructionFlagState{"ORR_FLAGS", ORR_FLAGS, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xCD5, 0x0C0, 0x080},
		HostInstructionFlagState{"ORR_FLAGS", ORR_FLAGS, 0xFFFFFFFF, 0x00000000, 0xFFFFFFFF, 0xCD5, 0x0C0, 0x080},
		HostInstructionFlagState{"BIC_FLAGS", BIC_FLAGS, 0x55555555, 0xaaaaaaaa, 0x55555555, 0xCD5, 0x0C0, 0x000},
		HostInstructionFlagState{"BIC_FLAGS", BIC_FLAGS, 0x55555555, 0x00FFFF00, 0x55000055, 0xCD5, 0x0C0, 0x000},
		HostInstructionFlagState{"BIC_FLAGS", BIC_FLAGS, 0x55555555, 0xaaaaFFFF, 0x55550000, 0xCD5, 0x0C0, 0x000},
		HostInstructionFlagState{"BIC_FLAGS", BIC_FLAGS, 0xFFFFFFFF, 0xFFFFFFFF, 0x00000000, 0xCD5, 0x0C0, 0x040},
		HostInstructionFlagState{"BIC_FLAGS", BIC_FLAGS, 0xFFFFFFFF, 0x00000000, 0xFFFFFFFF, 0xCD5, 0x0C0, 0x080},
		HostInstructionFlagState{"ADC", ADC, 0x55555555, 0xaaaaaaaa, 0xFFFFFFFF, 0x000, 0xCD5, 0x000},
		HostInstructionFlagState{"ADC", ADC, 0x55555555, 0x00FFFF00, 0x56555455, 0x000, 0xCD5, 0x000},
		HostInstructionFlagState{"ADC", ADC, 0x55555555, 0xaaaaFFFF, 0x00005554, 0x000, 0xCD5, 0x000},
		HostInstructionFlagState{"ADC", ADC, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFE, 0x000, 0xCD5, 0x000},
		HostInstructionFlagState{"ADC", ADC, 0xFFFFFFFF, 0x00000000, 0xFFFFFFFF, 0x000, 0xCD5, 0x000},
		HostInstructionFlagState{"ADC", ADC, 0x55555555, 0xaaaaaaaa, 0x00000000, 0xCD5, 0xCD5, 0xCD5},
		HostInstructionFlagState{"ADC", ADC, 0x55555555, 0x00FFFF00, 0x56555456, 0xCD5, 0xCD5, 0xCD5},
		HostInstructionFlagState{"ADC", ADC, 0x55555555, 0xaaaaFFFF, 0x00005555, 0xCD5, 0xCD5, 0xCD5},
		HostInstructionFlagState{"ADC", ADC, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xCD5, 0xCD5, 0xCD5},
		HostInstructionFlagState{"ADC", ADC, 0xFFFFFFFF, 0x00000000, 0x00000000, 0xCD5, 0xCD5, 0xCD5},
		HostInstructionFlagState{ "ADC_FLAGS", ADC_FLAGS, 0x55555555, 0xaaaaaaaa, 0x00000000, 0xCD5, 0x8C1, 0x041 },
		HostInstructionFlagState{ "ADC_FLAGS", ADC_FLAGS, 0x55555555, 0x00FFFF00, 0x56555456, 0xCD5, 0x8C1, 0x000 },
		HostInstructionFlagState{ "ADC_FLAGS", ADC_FLAGS, 0x55555555, 0xaaaaFFFF, 0x00005555, 0xCD5, 0x8C1, 0x001 },
		HostInstructionFlagState{ "ADC_FLAGS", ADC_FLAGS, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xCD5, 0x8C1, 0x081 },
		HostInstructionFlagState{ "ADC_FLAGS", ADC_FLAGS, 0xFFFFFFFF, 0x00000000, 0x00000000, 0xCD5, 0x8C1, 0x041 },
		HostInstructionFlagState{"ADC_FLAGS", ADC_FLAGS, 0x55555555, 0xaaaaaaaa, 0xFFFFFFFF, 0x000, 0x8C1, 0x080},
		HostInstructionFlagState{"ADC_FLAGS", ADC_FLAGS, 0x55555555, 0x00FFFF00, 0x56555455, 0x000, 0x8C1, 0x000},
		HostInstructionFlagState{"ADC_FLAGS", ADC_FLAGS, 0x55555555, 0xaaaaFFFF, 0x00005554, 0x000, 0x8C1, 0x001},
		HostInstructionFlagState{"ADC_FLAGS", ADC_FLAGS, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFE, 0x000, 0x8C1, 0x081},
		HostInstructionFlagState{"ADC_FLAGS", ADC_FLAGS, 0xFFFFFFFF, 0x00000000, 0xFFFFFFFF, 0x000, 0x8C1, 0x080}
));

INSTANTIATE_TEST_CASE_P(HostInstructionFlagTest2, HostInstructionFlagTest, testing::Values(
		HostInstructionFlagState{"SBC", SBC, 0x55555555, 0xaaaaaaaa, 0xAAAAAAAA, 0x000, 0xCD5, 0x000},
		HostInstructionFlagState{"SBC", SBC, 0x55555555, 0x00FFFF00, 0x54555654, 0x000, 0xCD5, 0x000},
		HostInstructionFlagState{"SBC", SBC, 0x55555555, 0xaaaaFFFF, 0xaaaa5555, 0x000, 0xCD5, 0x000},
		HostInstructionFlagState{"SBC", SBC, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0x000, 0xCD5, 0x000},
		HostInstructionFlagState{"SBC", SBC, 0xFFFFFFFF, 0x00000000, 0xFFFFFFFE, 0x000, 0xCD5, 0x000},
		HostInstructionFlagState{"SBC", SBC, 0x55555555, 0xaaaaaaaa, 0xAAAAAAAB, 0xCD5, 0xCD5, 0xCD5},
		HostInstructionFlagState{"SBC", SBC, 0x55555555, 0x00FFFF00, 0x54555655, 0xCD5, 0xCD5, 0xCD5},
		HostInstructionFlagState{"SBC", SBC, 0x55555555, 0xaaaaFFFF, 0xaaaa5556, 0xCD5, 0xCD5, 0xCD5},
		HostInstructionFlagState{"SBC", SBC, 0xFFFFFFFF, 0xFFFFFFFF, 0x00000000, 0xCD5, 0xCD5, 0xCD5},
		HostInstructionFlagState{"SBC", SBC, 0xFFFFFFFF, 0x00000000, 0xFFFFFFFF, 0xCD5, 0xCD5, 0xCD5},
		HostInstructionFlagState{"SBC_FLAGS", SBC_FLAGS, 0x55555555, 0xaaaaaaaa, 0xAAAAAAAA, 0x000, 0x8C1, 0x880},
		HostInstructionFlagState{"SBC_FLAGS", SBC_FLAGS, 0x55555555, 0x00FFFF00, 0x54555654, 0x000, 0x8C1, 0x001},
		HostInstructionFlagState{"SBC_FLAGS", SBC_FLAGS, 0x55555555, 0xaaaaFFFF, 0xaaaa5555, 0x000, 0x8C1, 0x880},
		HostInstructionFlagState{"SBC_FLAGS", SBC_FLAGS, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0x000, 0x8C1, 0x080},
		HostInstructionFlagState{"SBC_FLAGS", SBC_FLAGS, 0xFFFFFFFF, 0x00000000, 0xFFFFFFFE, 0x000, 0x8C1, 0x081},
		HostInstructionFlagState{"SBC_FLAGS", SBC_FLAGS, 0x55555555, 0xaaaaaaaa, 0xAAAAAAAB, 0xCD5, 0x8C1, 0x880},
		HostInstructionFlagState{"SBC_FLAGS", SBC_FLAGS, 0x55555555, 0x00FFFF00, 0x54555655, 0xCD5, 0x8C1, 0x001},
		HostInstructionFlagState{"SBC_FLAGS", SBC_FLAGS, 0x55555555, 0xaaaaFFFF, 0xaaaa5556, 0xCD5, 0x8C1, 0x880},
		HostInstructionFlagState{"SBC_FLAGS", SBC_FLAGS, 0xFFFFFFFF, 0xFFFFFFFF, 0x00000000, 0xCD5, 0x8C1, 0x041},
		HostInstructionFlagState{"SBC_FLAGS", SBC_FLAGS, 0xFFFFFFFF, 0x00000000, 0xFFFFFFFF, 0xCD5, 0x8C1, 0x081}
		));

INSTANTIATE_TEST_CASE_P(HostTestInstructionTest, HostTestInstructionTest, testing::Values(
		HostTestInstructionState{"TST", TST, 0x55555555, 0xaaaaaaaa, 0xC0, 0x40},
		HostTestInstructionState{"TST", TST, 0x55555555, 0x00FFFF00, 0xC0, 0x00},
		HostTestInstructionState{"TST", TST, 0x55555555, 0xaaaaFFFF, 0xC0, 0x00},
		HostTestInstructionState{"TST", TST, 0xFFFFFFFF, 0xFFFFFFFF, 0xC0, 0x80},
		HostTestInstructionState{"TST", TST, 0xFFFFFFFF, 0x00000000, 0xC0, 0x40},
		HostTestInstructionState{"TEQ", TEQ, 0x55555555, 0xaaaaaaaa, 0xC0, 0x80},
		HostTestInstructionState{"TEQ", TEQ, 0x55555555, 0x00FFFF00, 0xC0, 0x00},
		HostTestInstructionState{"TEQ", TEQ, 0x55555555, 0xaaaaFFFF, 0xC0, 0x80},
		HostTestInstructionState{"TEQ", TEQ, 0xFFFFFFFF, 0xFFFFFFFF, 0xC0, 0x40},
		HostTestInstructionState{"TEQ", TEQ, 0xFFFFFFFF, 0x00000000, 0xC0, 0x80},
		HostTestInstructionState{"CMP", CMP, 0x55555555, 0xaaaaaaaa, 0x8C1, 0x880},
		HostTestInstructionState{"CMP", CMP, 0x55555555, 0x00FFFF00, 0x8C1, 0x001},
		HostTestInstructionState{"CMP", CMP, 0x55555555, 0xaaaaFFFF, 0x8C1, 0x880},
		HostTestInstructionState{"CMP", CMP, 0xFFFFFFFF, 0xFFFFFFFF, 0x8C1, 0x041},
		HostTestInstructionState{"CMP", CMP, 0xFFFFFFFF, 0x00000000, 0x8C1, 0x081},
		HostTestInstructionState{"CMN", CMN, 0x55555555, 0xaaaaaaaa, 0x8C1, 0x080},
		HostTestInstructionState{"CMN", CMN, 0x55555555, 0x00FFFF00, 0x8C1, 0x000},
		HostTestInstructionState{"CMN", CMN, 0x55555555, 0xaaaaFFFF, 0x8C1, 0x001},
		HostTestInstructionState{"CMN", CMN, 0xFFFFFFFF, 0xFFFFFFFF, 0x8C1, 0x081},
		HostTestInstructionState{"CMN", CMN, 0xFFFFFFFF, 0x00000000, 0x8C1, 0x080}
		));

INSTANTIATE_TEST_CASE_P(HostInstructionMovTest, HostInstructionMovTest, testing::Values(
		HostInstructionMovState{"MOV_FLAGS", MOV_FLAGS, 0x00000000, 0x000, 0x8C1, 0x040},
		HostInstructionMovState{"MOV_FLAGS", MOV_FLAGS, 0x55555555, 0x000, 0x8C1, 0x000},
		HostInstructionMovState{"MOV_FLAGS", MOV_FLAGS, 0xAAAAAAAA, 0x000, 0x8C1, 0x080},
		HostInstructionMovState{"MOV_FLAGS", MOV_FLAGS, 0xFFFFFFFF, 0x000, 0x8C1, 0x080},
		HostInstructionMovState{"MOV_FLAGS", MOV_FLAGS, 0x00000000, 0x8C1, 0x8C1, 0x841},
		HostInstructionMovState{"MOV_FLAGS", MOV_FLAGS, 0x55555555, 0x8C1, 0x8C1, 0x801},
		HostInstructionMovState{"MOV_FLAGS", MOV_FLAGS, 0xAAAAAAAA, 0x8C1, 0x8C1, 0x881},
		HostInstructionMovState{"MOV_FLAGS", MOV_FLAGS, 0xFFFFFFFF, 0x8C1, 0x8C1, 0x881},
		HostInstructionMovState{"MVN_FLAGS", MVN_FLAGS, 0x00000000, 0x000, 0x8C1, 0x080},
		HostInstructionMovState{"MVN_FLAGS", MVN_FLAGS, 0x55555555, 0x000, 0x8C1, 0x080},
		HostInstructionMovState{"MVN_FLAGS", MVN_FLAGS, 0xAAAAAAAA, 0x000, 0x8C1, 0x000},
		HostInstructionMovState{"MVN_FLAGS", MVN_FLAGS, 0xFFFFFFFF, 0x000, 0x8C1, 0x040},
		HostInstructionMovState{"MVN_FLAGS", MVN_FLAGS, 0x00000000, 0x8C1, 0x8C1, 0x881},
		HostInstructionMovState{"MVN_FLAGS", MVN_FLAGS, 0x55555555, 0x8C1, 0x8C1, 0x881},
		HostInstructionMovState{"MVN_FLAGS", MVN_FLAGS, 0xAAAAAAAA, 0x8C1, 0x8C1, 0x801},
		HostInstructionMovState{"MVN_FLAGS", MVN_FLAGS, 0xFFFFFFFF, 0x8C1, 0x8C1, 0x841}
		));

INSTANTIATE_TEST_CASE_P(HostInstructionShiftTest, HostInstructionShiftTest, testing::Values(
		HostInstructionShiftState{"LSL", LSL, 0x00000000, 0, 0x00000000},
		HostInstructionShiftState{"LSL", LSL, 0x00000001, 0, 0x00000001},
		HostInstructionShiftState{"LSL", LSL, 0xFFFFFFFF, 0, 0xFFFFFFFF},
		HostInstructionShiftState{"LSL", LSL, 0x80000000, 0, 0x80000000},
		HostInstructionShiftState{"LSL", LSL, 0x00000000, 16, 0x00000000},
		HostInstructionShiftState{"LSL", LSL, 0x00000001, 16, 0x00010000},
		HostInstructionShiftState{"LSL", LSL, 0xFFFFFFFF, 16, 0xFFFF0000},
		HostInstructionShiftState{"LSL", LSL, 0x80000000, 16, 0x00000000},
		HostInstructionShiftState{"LSL", LSL, 0xFFFFFFFF, 32, 0x00000000},
		HostInstructionShiftState{"LSL", LSL, 0x00000001, 1, 0x00000002},
		HostInstructionShiftState{"LSL", LSL, 0x00000001, 5, 0x00000020},
		HostInstructionShiftState{"LSL", LSL, 0xFFFFFFFF, 31, 0x80000000},
		HostInstructionShiftState{"LSL", LSL, 0x80000000, 1, 0x00000000},
		HostInstructionShiftState{"LSR", LSR, 0x00000000, 0, 0x00000000},
		HostInstructionShiftState{"LSR", LSR, 0x00000001, 0, 0x00000001},
		HostInstructionShiftState{"LSR", LSR, 0xFFFFFFFF, 0, 0xFFFFFFFF},
		HostInstructionShiftState{"LSR", LSR, 0x80000000, 0, 0x80000000},
		HostInstructionShiftState{"LSR", LSR, 0x00000000, 16, 0x00000000},
		HostInstructionShiftState{"LSR", LSR, 0x00000001, 16, 0x00000000},
		HostInstructionShiftState{"LSR", LSR, 0xFFFFFFFF, 16, 0x0000FFFF},
		HostInstructionShiftState{"LSR", LSR, 0x80000000, 16, 0x00008000},
		HostInstructionShiftState{"LSR", LSR, 0xFFFFFFFF, 32, 0x00000000},
		HostInstructionShiftState{"LSR", LSR, 0x00010001, 1, 0x00008000},
		HostInstructionShiftState{"LSR", LSR, 0x00010001, 5, 0x00000800},
		HostInstructionShiftState{"LSR", LSR, 0xFFFFFFFF, 31, 0x00000001},
		HostInstructionShiftState{"LSR", LSR, 0x00000001, 1, 0x00000000},
		HostInstructionShiftState{"ASR", ASR, 0x00000001, 0, 0x00000001},
		HostInstructionShiftState{"ASR", ASR, 0xFFFFFFFF, 0, 0xFFFFFFFF},
		HostInstructionShiftState{"ASR", ASR, 0x80000000, 0, 0x80000000},
		HostInstructionShiftState{"ASR", ASR, 0x00000000, 16, 0x00000000},
		HostInstructionShiftState{"ASR", ASR, 0x00000001, 16, 0x00000000},
		HostInstructionShiftState{"ASR", ASR, 0xFFFFFFFF, 16, 0xFFFFFFFF},
		HostInstructionShiftState{"ASR", ASR, 0x80000000, 16, 0xFFFF8000},
		HostInstructionShiftState{"ASR", ASR, 0xFFFFFFFF, 32, 0xFFFFFFFF},
		HostInstructionShiftState{"ASR", ASR, 0x80000000, 32, 0xFFFFFFFF},
		HostInstructionShiftState{"ASR", ASR, 0x10000000, 32, 0x00000000},
		HostInstructionShiftState{"ASR", ASR, 0x00010001, 1, 0x00008000},
		HostInstructionShiftState{"ASR", ASR, 0x00010001, 5, 0x00000800},
		HostInstructionShiftState{"ASR", ASR, 0xFFFFFFFF, 31, 0xFFFFFFFF},
		HostInstructionShiftState{"ROR", ROR, 0x00000001, 1, 0x80000000},
		HostInstructionShiftState{"ROR", ROR, 0x00000000, 0, 0x00000000},
		HostInstructionShiftState{"ROR", ROR, 0x80000000, 0, 0x80000000},
		HostInstructionShiftState{"ROR", ROR, 0x00000000, 16, 0x00000000},
		HostInstructionShiftState{"ROR", ROR, 0x00000001, 16, 0x00010000},
		HostInstructionShiftState{"ROR", ROR, 0x80000000, 16, 0x00008000},
		HostInstructionShiftState{"ROR", ROR, 0x10000000, 32, 0x10000000},
		HostInstructionShiftState{"ROR", ROR, 0x00010001, 1, 0x80008000},
		HostInstructionShiftState{"ROR", ROR, 0x00010001, 5, 0x08000800},
		HostInstructionShiftState{"ROR", ROR, 0xFFFFFFFF, 31, 0xFFFFFFFF},
		HostInstructionShiftState{"ROR", ROR, 0x00000001, 1, 0x80000000}
));

INSTANTIATE_TEST_CASE_P(HostInstructionShiftFlagsTest, HostInstructionShiftFlagsTest, testing::Values(
		HostInstructionShiftFlagsState{"LSL_FLAGS", LSL_FLAGS, 0x00000000, 0, 0x00000000, 0x000, 0x0C1, 0x040},
		HostInstructionShiftFlagsState{"LSL_FLAGS", LSL_FLAGS, 0x00000001, 0, 0x00000001, 0x000, 0x0C1, 0x000},
		HostInstructionShiftFlagsState{"LSL_FLAGS", LSL_FLAGS, 0xFFFFFFFF, 0, 0xFFFFFFFF, 0x000, 0x0C1, 0x080},
		HostInstructionShiftFlagsState{"LSL_FLAGS", LSL_FLAGS, 0x80000000, 0, 0x80000000, 0x000, 0x0C1, 0x080},
		HostInstructionShiftFlagsState{"LSL_FLAGS", LSL_FLAGS, 0x00000000, 16, 0x00000000, 0x000, 0x0C1, 0x040},
		HostInstructionShiftFlagsState{"LSL_FLAGS", LSL_FLAGS, 0x00000001, 16, 0x00010000, 0x000, 0x0C1, 0x000},
		HostInstructionShiftFlagsState{"LSL_FLAGS", LSL_FLAGS, 0xFFFFFFFF, 16, 0xFFFF0000, 0x000, 0x0C1, 0x081},
		HostInstructionShiftFlagsState{"LSL_FLAGS", LSL_FLAGS, 0x80000000, 16, 0x00000000, 0x000, 0x0C1, 0x040},
		HostInstructionShiftFlagsState{"LSL_FLAGS", LSL_FLAGS, 0xFFFFFFFF, 32, 0x00000000, 0x000, 0x0C1, 0x041},
		HostInstructionShiftFlagsState{"LSL_FLAGS", LSL_FLAGS, 0x00000001, 1, 0x00000002, 0x000, 0x0C1, 0x000},
		HostInstructionShiftFlagsState{"LSL_FLAGS", LSL_FLAGS, 0x00000001, 5, 0x00000020, 0x000, 0x0C1, 0x000},
		HostInstructionShiftFlagsState{"LSL_FLAGS", LSL_FLAGS, 0xFFFFFFFF, 31, 0x80000000, 0x000, 0x0C1, 0x081},
		HostInstructionShiftFlagsState{"LSL_FLAGS", LSL_FLAGS, 0x80000000, 1, 0x00000000, 0x000, 0x0C1, 0x041},
		HostInstructionShiftFlagsState{"LSR_FLAGS", LSR_FLAGS, 0x00000000, 0, 0x00000000, 0x000, 0x0C1, 0x040},
		HostInstructionShiftFlagsState{"LSR_FLAGS", LSR_FLAGS, 0x00000001, 0, 0x00000001, 0x000, 0x0C1, 0x000},
		HostInstructionShiftFlagsState{"LSR_FLAGS", LSR_FLAGS, 0xFFFFFFFF, 0, 0xFFFFFFFF, 0x000, 0x0C1, 0x080},
		HostInstructionShiftFlagsState{"LSR_FLAGS", LSR_FLAGS, 0x80000000, 0, 0x80000000, 0x000, 0x0C1, 0x080},
		HostInstructionShiftFlagsState{"LSR_FLAGS", LSR_FLAGS, 0x00000000, 16, 0x00000000, 0x000, 0x0C1, 0x040},
		HostInstructionShiftFlagsState{"LSR_FLAGS", LSR_FLAGS, 0x00000001, 16, 0x00000000, 0x000, 0x0C1, 0x040},
		HostInstructionShiftFlagsState{"LSR_FLAGS", LSR_FLAGS, 0xFFFFFFFF, 16, 0x0000FFFF, 0x000, 0x0C1, 0x001},
		HostInstructionShiftFlagsState{"LSR_FLAGS", LSR_FLAGS, 0x80000000, 16, 0x00008000, 0x000, 0x0C1, 0x000},
		HostInstructionShiftFlagsState{"LSR_FLAGS", LSR_FLAGS, 0xFFFFFFFF, 32, 0x00000000, 0x000, 0x0C1, 0x041},
		HostInstructionShiftFlagsState{"LSR_FLAGS", LSR_FLAGS, 0x00010001, 1, 0x00008000, 0x000, 0x0C1, 0x001},
		HostInstructionShiftFlagsState{"LSR_FLAGS", LSR_FLAGS, 0x00010001, 5, 0x00000800, 0x000, 0x0C1, 0x000},
		HostInstructionShiftFlagsState{"LSR_FLAGS", LSR_FLAGS, 0xFFFFFFFF, 31, 0x00000001, 0x000, 0x0C1, 0x001},
		HostInstructionShiftFlagsState{"LSR_FLAGS", LSR_FLAGS, 0x00000001, 1, 0x00000000, 0x000, 0x0C1, 0x041},
		HostInstructionShiftFlagsState{"ASR_FLAGS", ASR_FLAGS, 0x00000001, 0, 0x00000001, 0x000, 0x0C1, 0x000},
		HostInstructionShiftFlagsState{"ASR_FLAGS", ASR_FLAGS, 0xFFFFFFFF, 0, 0xFFFFFFFF, 0x000, 0x0C1, 0x080},
		HostInstructionShiftFlagsState{"ASR_FLAGS", ASR_FLAGS, 0x80000000, 0, 0x80000000, 0x000, 0x0C1, 0x080},
		HostInstructionShiftFlagsState{"ASR_FLAGS", ASR_FLAGS, 0x00000000, 16, 0x00000000, 0x000, 0x0C1, 0x040},
		HostInstructionShiftFlagsState{"ASR_FLAGS", ASR_FLAGS, 0x00000001, 16, 0x00000000, 0x000, 0x0C1, 0x040},
		HostInstructionShiftFlagsState{"ASR_FLAGS", ASR_FLAGS, 0xFFFFFFFF, 16, 0xFFFFFFFF, 0x000, 0x0C1, 0x081},
		HostInstructionShiftFlagsState{"ASR_FLAGS", ASR_FLAGS, 0x80000000, 16, 0xFFFF8000, 0x000, 0x0C1, 0x080},
		HostInstructionShiftFlagsState{"ASR_FLAGS", ASR_FLAGS, 0xFFFFFFFF, 32, 0xFFFFFFFF, 0x000, 0x0C1, 0x081},
		HostInstructionShiftFlagsState{"ASR_FLAGS", ASR_FLAGS, 0x80000000, 32, 0xFFFFFFFF, 0x000, 0x0C1, 0x081},
		HostInstructionShiftFlagsState{"ASR_FLAGS", ASR_FLAGS, 0x10000000, 32, 0x00000000, 0x000, 0x0C1, 0x040},
		HostInstructionShiftFlagsState{"ASR_FLAGS", ASR_FLAGS, 0x00010001, 1, 0x00008000, 0x000, 0x0C1, 0x001},
		HostInstructionShiftFlagsState{"ASR_FLAGS", ASR_FLAGS, 0x00010001, 5, 0x00000800, 0x000, 0x0C1, 0x000},
		HostInstructionShiftFlagsState{"ASR_FLAGS", ASR_FLAGS, 0xFFFFFFFF, 31, 0xFFFFFFFF, 0x000, 0x0C1, 0x081},
		HostInstructionShiftFlagsState{"ROR_FLAGS", ROR_FLAGS, 0x00000001, 1, 0x80000000, 0x000, 0x0C1, 0x081},
		HostInstructionShiftFlagsState{"ROR_FLAGS", ROR_FLAGS, 0x00000000, 0, 0x00000000, 0x000, 0x0C1, 0x040},
		HostInstructionShiftFlagsState{"ROR_FLAGS", ROR_FLAGS, 0x80000000, 0, 0x80000000, 0x000, 0x0C1, 0x080},
		HostInstructionShiftFlagsState{"ROR_FLAGS", ROR_FLAGS, 0x00000000, 16, 0x00000000, 0x000, 0x0C1, 0x040},
		HostInstructionShiftFlagsState{"ROR_FLAGS", ROR_FLAGS, 0x00000001, 16, 0x00010000, 0x000, 0x0C1, 0x000},
		HostInstructionShiftFlagsState{"ROR_FLAGS", ROR_FLAGS, 0x80000000, 16, 0x00008000, 0x000, 0x0C1, 0x000},
		HostInstructionShiftFlagsState{"ROR_FLAGS", ROR_FLAGS, 0x10000000, 32, 0x10000000, 0x000, 0x0C1, 0x000},
		HostInstructionShiftFlagsState{"ROR_FLAGS", ROR_FLAGS, 0x00010001, 1, 0x80008000, 0x000, 0x0C1, 0x081},
		HostInstructionShiftFlagsState{"ROR_FLAGS", ROR_FLAGS, 0x00010001, 5, 0x08000800, 0x000, 0x0C1, 0x000},
		HostInstructionShiftFlagsState{"ROR_FLAGS", ROR_FLAGS, 0xFFFFFFFF, 31, 0xFFFFFFFF, 0x000, 0x0C1, 0x081},
		HostInstructionShiftFlagsState{"ROR_FLAGS", ROR_FLAGS, 0x00000001, 1, 0x80000000, 0x000, 0x0C1, 0x081}
));

INSTANTIATE_TEST_CASE_P(HostInstructionShiftFlagsTest2, HostInstructionShiftFlagsTest, testing::Values(
		HostInstructionShiftFlagsState{"LSL_FLAGS", LSL_FLAGS, 0x00000000, 0, 0x00000000, 0x8C1, 0x0C1, 0x041},
		HostInstructionShiftFlagsState{"LSL_FLAGS", LSL_FLAGS, 0x00000001, 0, 0x00000001, 0x8C1, 0x0C1, 0x001},
		HostInstructionShiftFlagsState{"LSL_FLAGS", LSL_FLAGS, 0xFFFFFFFF, 0, 0xFFFFFFFF, 0x8C1, 0x0C1, 0x081},
		HostInstructionShiftFlagsState{"LSL_FLAGS", LSL_FLAGS, 0x80000000, 0, 0x80000000, 0x8C1, 0x0C1, 0x081},
		HostInstructionShiftFlagsState{"LSL_FLAGS", LSL_FLAGS, 0x00000000, 16, 0x00000000, 0x8C1, 0x0C1, 0x040},
		HostInstructionShiftFlagsState{"LSL_FLAGS", LSL_FLAGS, 0x00000001, 16, 0x00010000, 0x8C1, 0x0C1, 0x000},
		HostInstructionShiftFlagsState{"LSL_FLAGS", LSL_FLAGS, 0xFFFFFFFF, 16, 0xFFFF0000, 0x8C1, 0x0C1, 0x081},
		HostInstructionShiftFlagsState{"LSL_FLAGS", LSL_FLAGS, 0x80000000, 16, 0x00000000, 0x8C1, 0x0C1, 0x040},
		HostInstructionShiftFlagsState{"LSL_FLAGS", LSL_FLAGS, 0xFFFFFFFF, 32, 0x00000000, 0x8C1, 0x0C1, 0x041},
		HostInstructionShiftFlagsState{"LSL_FLAGS", LSL_FLAGS, 0x00000001, 1, 0x00000002, 0x8C1, 0x0C1, 0x000},
		HostInstructionShiftFlagsState{"LSL_FLAGS", LSL_FLAGS, 0x00000001, 5, 0x00000020, 0x8C1, 0x0C1, 0x000},
		HostInstructionShiftFlagsState{"LSL_FLAGS", LSL_FLAGS, 0xFFFFFFFF, 31, 0x80000000, 0x8C1, 0x0C1, 0x081},
		HostInstructionShiftFlagsState{"LSL_FLAGS", LSL_FLAGS, 0x80000000, 1, 0x00000000, 0x8C1, 0x0C1, 0x041},
		HostInstructionShiftFlagsState{"LSR_FLAGS", LSR_FLAGS, 0x00000000, 0, 0x00000000, 0x8C1, 0x0C1, 0x041},
		HostInstructionShiftFlagsState{"LSR_FLAGS", LSR_FLAGS, 0x00000001, 0, 0x00000001, 0x8C1, 0x0C1, 0x001},
		HostInstructionShiftFlagsState{"LSR_FLAGS", LSR_FLAGS, 0xFFFFFFFF, 0, 0xFFFFFFFF, 0x8C1, 0x0C1, 0x081},
		HostInstructionShiftFlagsState{"LSR_FLAGS", LSR_FLAGS, 0x80000000, 0, 0x80000000, 0x8C1, 0x0C1, 0x081},
		HostInstructionShiftFlagsState{"LSR_FLAGS", LSR_FLAGS, 0x00000000, 16, 0x00000000, 0x8C1, 0x0C1, 0x040},
		HostInstructionShiftFlagsState{"LSR_FLAGS", LSR_FLAGS, 0x00000001, 16, 0x00000000, 0x8C1, 0x0C1, 0x040},
		HostInstructionShiftFlagsState{"LSR_FLAGS", LSR_FLAGS, 0xFFFFFFFF, 16, 0x0000FFFF, 0x8C1, 0x0C1, 0x001},
		HostInstructionShiftFlagsState{"LSR_FLAGS", LSR_FLAGS, 0x80000000, 16, 0x00008000, 0x8C1, 0x0C1, 0x000},
		HostInstructionShiftFlagsState{"LSR_FLAGS", LSR_FLAGS, 0xFFFFFFFF, 32, 0x00000000, 0x8C1, 0x0C1, 0x041},
		HostInstructionShiftFlagsState{"LSR_FLAGS", LSR_FLAGS, 0x00010001, 1, 0x00008000, 0x8C1, 0x0C1, 0x001},
		HostInstructionShiftFlagsState{"LSR_FLAGS", LSR_FLAGS, 0x00010001, 5, 0x00000800, 0x8C1, 0x0C1, 0x000},
		HostInstructionShiftFlagsState{"LSR_FLAGS", LSR_FLAGS, 0xFFFFFFFF, 31, 0x00000001, 0x8C1, 0x0C1, 0x001},
		HostInstructionShiftFlagsState{"LSR_FLAGS", LSR_FLAGS, 0x00000001, 1, 0x00000000, 0x8C1, 0x0C1, 0x041},
		HostInstructionShiftFlagsState{"ASR_FLAGS", ASR_FLAGS, 0x00000001, 0, 0x00000001, 0x8C1, 0x0C1, 0x001},
		HostInstructionShiftFlagsState{"ASR_FLAGS", ASR_FLAGS, 0xFFFFFFFF, 0, 0xFFFFFFFF, 0x8C1, 0x0C1, 0x081},
		HostInstructionShiftFlagsState{"ASR_FLAGS", ASR_FLAGS, 0x80000000, 0, 0x80000000, 0x8C1, 0x0C1, 0x081},
		HostInstructionShiftFlagsState{"ASR_FLAGS", ASR_FLAGS, 0x00000000, 16, 0x00000000, 0x8C1, 0x0C1, 0x040},
		HostInstructionShiftFlagsState{"ASR_FLAGS", ASR_FLAGS, 0x00000001, 16, 0x00000000, 0x8C1, 0x0C1, 0x040},
		HostInstructionShiftFlagsState{"ASR_FLAGS", ASR_FLAGS, 0xFFFFFFFF, 16, 0xFFFFFFFF, 0x8C1, 0x0C1, 0x081},
		HostInstructionShiftFlagsState{"ASR_FLAGS", ASR_FLAGS, 0x80000000, 16, 0xFFFF8000, 0x8C1, 0x0C1, 0x080},
		HostInstructionShiftFlagsState{"ASR_FLAGS", ASR_FLAGS, 0xFFFFFFFF, 32, 0xFFFFFFFF, 0x8C1, 0x0C1, 0x081},
		HostInstructionShiftFlagsState{"ASR_FLAGS", ASR_FLAGS, 0x80000000, 32, 0xFFFFFFFF, 0x8C1, 0x0C1, 0x081},
		HostInstructionShiftFlagsState{"ASR_FLAGS", ASR_FLAGS, 0x10000000, 32, 0x00000000, 0x8C1, 0x0C1, 0x040},
		HostInstructionShiftFlagsState{"ASR_FLAGS", ASR_FLAGS, 0x00010001, 1, 0x00008000, 0x8C1, 0x0C1, 0x001},
		HostInstructionShiftFlagsState{"ASR_FLAGS", ASR_FLAGS, 0x00010001, 5, 0x00000800, 0x8C1, 0x0C1, 0x000},
		HostInstructionShiftFlagsState{"ASR_FLAGS", ASR_FLAGS, 0xFFFFFFFF, 31, 0xFFFFFFFF, 0x8C1, 0x0C1, 0x081},
		HostInstructionShiftFlagsState{"ROR_FLAGS", ROR_FLAGS, 0x00000001, 1, 0x80000000, 0x8C1, 0x0C1, 0x081},
		HostInstructionShiftFlagsState{"ROR_FLAGS", ROR_FLAGS, 0x00000000, 0, 0x00000000, 0x8C1, 0x0C1, 0x041},
		HostInstructionShiftFlagsState{"ROR_FLAGS", ROR_FLAGS, 0x80000000, 0, 0x80000000, 0x8C1, 0x0C1, 0x081},
		HostInstructionShiftFlagsState{"ROR_FLAGS", ROR_FLAGS, 0x00000000, 16, 0x00000000, 0x8C1, 0x0C1, 0x040},
		HostInstructionShiftFlagsState{"ROR_FLAGS", ROR_FLAGS, 0x00000001, 16, 0x00010000, 0x8C1, 0x0C1, 0x000},
		HostInstructionShiftFlagsState{"ROR_FLAGS", ROR_FLAGS, 0x80000000, 16, 0x00008000, 0x8C1, 0x0C1, 0x000},
		HostInstructionShiftFlagsState{"ROR_FLAGS", ROR_FLAGS, 0x10000000, 32, 0x10000000, 0x8C1, 0x0C1, 0x000},
		HostInstructionShiftFlagsState{"ROR_FLAGS", ROR_FLAGS, 0x00010001, 1, 0x80008000, 0x8C1, 0x0C1, 0x081},
		HostInstructionShiftFlagsState{"ROR_FLAGS", ROR_FLAGS, 0x00010001, 5, 0x08000800, 0x8C1, 0x0C1, 0x000},
		HostInstructionShiftFlagsState{"ROR_FLAGS", ROR_FLAGS, 0xFFFFFFFF, 31, 0xFFFFFFFF, 0x8C1, 0x0C1, 0x081},
		HostInstructionShiftFlagsState{"ROR_FLAGS", ROR_FLAGS, 0x00000001, 1, 0x80000000, 0x8C1, 0x0C1, 0x081}
));

INSTANTIATE_TEST_CASE_P(HostInstructionShiftCFlagsTest, HostInstructionShiftCFlagsTest, testing::Values(
		HostInstructionShiftFlagsState{"LSL_CFLAG", LSL_CFLAG, 0x00000000, 0, 0x00000000, 0x8C1, 0x001, 0x001},
		HostInstructionShiftFlagsState{"LSL_CFLAG", LSL_CFLAG, 0x00000001, 0, 0x00000001, 0x8C1, 0x001, 0x001},
		HostInstructionShiftFlagsState{"LSL_CFLAG", LSL_CFLAG, 0xFFFFFFFF, 0, 0xFFFFFFFF, 0x8C1, 0x001, 0x001},
		HostInstructionShiftFlagsState{"LSL_CFLAG", LSL_CFLAG, 0x80000000, 0, 0x80000000, 0x8C1, 0x001, 0x001},
		HostInstructionShiftFlagsState{"LSL_CFLAG", LSL_CFLAG, 0x00000000, 16, 0x00000000, 0x8C1, 0x001, 0x000},
		HostInstructionShiftFlagsState{"LSL_CFLAG", LSL_CFLAG, 0x00000001, 16, 0x00010000, 0x8C1, 0x001, 0x000},
		HostInstructionShiftFlagsState{"LSL_CFLAG", LSL_CFLAG, 0xFFFFFFFF, 16, 0xFFFF0000, 0x8C1, 0x001, 0x001},
		HostInstructionShiftFlagsState{"LSL_CFLAG", LSL_CFLAG, 0x80000000, 16, 0x00000000, 0x8C1, 0x001, 0x000},
		HostInstructionShiftFlagsState{"LSL_CFLAG", LSL_CFLAG, 0xFFFFFFFF, 32, 0x00000000, 0x8C1, 0x001, 0x001},
		HostInstructionShiftFlagsState{"LSL_CFLAG", LSL_CFLAG, 0x00000001, 1, 0x00000002, 0x8C1, 0x001, 0x000},
		HostInstructionShiftFlagsState{"LSL_CFLAG", LSL_CFLAG, 0x00000001, 5, 0x00000020, 0x8C1, 0x001, 0x000},
		HostInstructionShiftFlagsState{"LSL_CFLAG", LSL_CFLAG, 0xFFFFFFFF, 31, 0x80000000, 0x8C1, 0x001, 0x001},
		HostInstructionShiftFlagsState{"LSL_CFLAG", LSL_CFLAG, 0x80000000, 1, 0x00000000, 0x8C1, 0x001, 0x001},
		HostInstructionShiftFlagsState{"LSR_CFLAG", LSR_CFLAG, 0x00000000, 0, 0x00000000, 0x8C1, 0x001, 0x001},
		HostInstructionShiftFlagsState{"LSR_CFLAG", LSR_CFLAG, 0x00000001, 0, 0x00000001, 0x8C1, 0x001, 0x001},
		HostInstructionShiftFlagsState{"LSR_CFLAG", LSR_CFLAG, 0xFFFFFFFF, 0, 0xFFFFFFFF, 0x8C1, 0x001, 0x001},
		HostInstructionShiftFlagsState{"LSR_CFLAG", LSR_CFLAG, 0x80000000, 0, 0x80000000, 0x8C1, 0x001, 0x001},
		HostInstructionShiftFlagsState{"LSR_CFLAG", LSR_CFLAG, 0x00000000, 16, 0x00000000, 0x8C1, 0x001, 0x000},
		HostInstructionShiftFlagsState{"LSR_CFLAG", LSR_CFLAG, 0x00000001, 16, 0x00000000, 0x8C1, 0x001, 0x000},
		HostInstructionShiftFlagsState{"LSR_CFLAG", LSR_CFLAG, 0xFFFFFFFF, 16, 0x0000FFFF, 0x8C1, 0x001, 0x001},
		HostInstructionShiftFlagsState{"LSR_CFLAG", LSR_CFLAG, 0x80000000, 16, 0x00008000, 0x8C1, 0x001, 0x000},
		HostInstructionShiftFlagsState{"LSR_CFLAG", LSR_CFLAG, 0xFFFFFFFF, 32, 0x00000000, 0x8C1, 0x001, 0x001},
		HostInstructionShiftFlagsState{"LSR_CFLAG", LSR_CFLAG, 0x00010001, 1, 0x00008000, 0x8C1, 0x001, 0x001},
		HostInstructionShiftFlagsState{"LSR_CFLAG", LSR_CFLAG, 0x00010001, 5, 0x00000800, 0x8C1, 0x001, 0x000},
		HostInstructionShiftFlagsState{"LSR_CFLAG", LSR_CFLAG, 0xFFFFFFFF, 31, 0x00000001, 0x8C1, 0x001, 0x001},
		HostInstructionShiftFlagsState{"LSR_CFLAG", LSR_CFLAG, 0x00000001, 1, 0x00000000, 0x8C1, 0x001, 0x001},
		HostInstructionShiftFlagsState{"ASR_CFLAG", ASR_CFLAG, 0x00000001, 0, 0x00000001, 0x8C1, 0x001, 0x001},
		HostInstructionShiftFlagsState{"ASR_CFLAG", ASR_CFLAG, 0xFFFFFFFF, 0, 0xFFFFFFFF, 0x8C1, 0x001, 0x001},
		HostInstructionShiftFlagsState{"ASR_CFLAG", ASR_CFLAG, 0x80000000, 0, 0x80000000, 0x8C1, 0x001, 0x001},
		HostInstructionShiftFlagsState{"ASR_CFLAG", ASR_CFLAG, 0x00000000, 16, 0x00000000, 0x8C1, 0x001, 0x000},
		HostInstructionShiftFlagsState{"ASR_CFLAG", ASR_CFLAG, 0x00000001, 16, 0x00000000, 0x8C1, 0x001, 0x000},
		HostInstructionShiftFlagsState{"ASR_CFLAG", ASR_CFLAG, 0xFFFFFFFF, 16, 0xFFFFFFFF, 0x8C1, 0x001, 0x001},
		HostInstructionShiftFlagsState{"ASR_CFLAG", ASR_CFLAG, 0x80000000, 16, 0xFFFF8000, 0x8C1, 0x001, 0x000},
		HostInstructionShiftFlagsState{"ASR_CFLAG", ASR_CFLAG, 0xFFFFFFFF, 32, 0xFFFFFFFF, 0x8C1, 0x001, 0x001},
		HostInstructionShiftFlagsState{"ASR_CFLAG", ASR_CFLAG, 0x80000000, 32, 0xFFFFFFFF, 0x8C1, 0x001, 0x001},
		HostInstructionShiftFlagsState{"ASR_CFLAG", ASR_CFLAG, 0x10000000, 32, 0x00000000, 0x8C1, 0x001, 0x000},
		HostInstructionShiftFlagsState{"ASR_CFLAG", ASR_CFLAG, 0x00010001, 1, 0x00008000, 0x8C1, 0x001, 0x001},
		HostInstructionShiftFlagsState{"ASR_CFLAG", ASR_CFLAG, 0x00010001, 5, 0x00000800, 0x8C1, 0x001, 0x000},
		HostInstructionShiftFlagsState{"ASR_CFLAG", ASR_CFLAG, 0xFFFFFFFF, 31, 0xFFFFFFFF, 0x8C1, 0x001, 0x001},
		HostInstructionShiftFlagsState{"ROR_CFLAG", ROR_CFLAG, 0x00000001, 1, 0x80000000, 0x8C1, 0x001, 0x001},
		HostInstructionShiftFlagsState{"ROR_CFLAG", ROR_CFLAG, 0x00000000, 0, 0x00000000, 0x8C1, 0x001, 0x001},
		HostInstructionShiftFlagsState{"ROR_CFLAG", ROR_CFLAG, 0x80000000, 0, 0x80000000, 0x8C1, 0x001, 0x001},
		HostInstructionShiftFlagsState{"ROR_CFLAG", ROR_CFLAG, 0x00000000, 16, 0x00000000, 0x8C1, 0x001, 0x000},
		HostInstructionShiftFlagsState{"ROR_CFLAG", ROR_CFLAG, 0x00000001, 16, 0x00010000, 0x8C1, 0x001, 0x000},
		HostInstructionShiftFlagsState{"ROR_CFLAG", ROR_CFLAG, 0x80000000, 16, 0x00008000, 0x8C1, 0x001, 0x000},
		HostInstructionShiftFlagsState{"ROR_CFLAG", ROR_CFLAG, 0x10000000, 32, 0x10000000, 0x8C1, 0x001, 0x000},
		HostInstructionShiftFlagsState{"ROR_CFLAG", ROR_CFLAG, 0x00010001, 1, 0x80008000, 0x8C1, 0x001, 0x001},
		HostInstructionShiftFlagsState{"ROR_CFLAG", ROR_CFLAG, 0x00010001, 5, 0x08000800, 0x8C1, 0x001, 0x000},
		HostInstructionShiftFlagsState{"ROR_CFLAG", ROR_CFLAG, 0xFFFFFFFF, 31, 0xFFFFFFFF, 0x8C1, 0x001, 0x001},
		HostInstructionShiftFlagsState{"ROR_CFLAG", ROR_CFLAG, 0x00000001, 1, 0x80000000, 0x8C1, 0x001, 0x001}
));


TEST(HostInstructionMisc, RRX) {
	uint32_t result;
	uint32_t flags = 0x8C1;

	result = 0x01234567;
	RRX(0x01234567, result, flags);
	EXPECT_EQ(flags, 0x8C1);
	EXPECT_EQ(result, 0x8091A2B3);

	RRX(0xFFFFFFFF, result, flags);
	EXPECT_EQ(flags, 0x8C1);
	EXPECT_EQ(result, 0xFFFFFFFF);

	RRX(0x00000000, result, flags);
	EXPECT_EQ(flags, 0x8C1);
	EXPECT_EQ(result, 0x80000000);

	RRX(0x00010000, result, flags);
	EXPECT_EQ(flags, 0x8C1);
	EXPECT_EQ(result, 0x80008000);

	flags = 0x000;
	result = 0x01234567;
	RRX(0x01234567, result, flags);
	EXPECT_EQ(flags, 0x000);
	EXPECT_EQ(result, 0x0091A2B3);

	RRX(0xFFFFFFFF, result, flags);
	EXPECT_EQ(flags, 0x000);
	EXPECT_EQ(result, 0x7FFFFFFF);

	RRX(0x00000000, result, flags);
	EXPECT_EQ(flags, 0x000);
	EXPECT_EQ(result, 0x00000000);

	RRX(0x00010000, result, flags);
	EXPECT_EQ(flags, 0x000);
	EXPECT_EQ(result, 0x00008000);
}

TEST(HostInstructionMisc, RRX_CFLAG) {
	uint32_t result;
	uint32_t flags;

	result = 0x01234567;
	flags = 0x8C1;
	RRX_CFLAG(0x01234567, result, flags);
	EXPECT_EQ(flags, 0x8C1);
	EXPECT_EQ(result, 0x8091A2B3);

	flags = 0x8C1;
	RRX_CFLAG(0xFFFFFFFF, result, flags);
	EXPECT_EQ(flags, 0x8C1);
	EXPECT_EQ(result, 0xFFFFFFFF);

	flags = 0x8C1;
	RRX_CFLAG(0x00000000, result, flags);
	EXPECT_EQ(flags, 0x8C0);
	EXPECT_EQ(result, 0x80000000);

	flags = 0x8C1;
	RRX_CFLAG(0x00010000, result, flags);
	EXPECT_EQ(flags, 0x8C0);
	EXPECT_EQ(result, 0x80008000);

	flags = 0x000;
	result = 0x01234567;
	RRX_CFLAG(0x01234567, result, flags);
	EXPECT_EQ(flags, 0x001);
	EXPECT_EQ(result, 0x0091A2B3);

	flags = 0x000;
	RRX_CFLAG(0xFFFFFFFF, result, flags);
	EXPECT_EQ(flags, 0x001);
	EXPECT_EQ(result, 0x7FFFFFFF);

	flags = 0x000;
	RRX_CFLAG(0x00000000, result, flags);
	EXPECT_EQ(flags, 0x000);
	EXPECT_EQ(result, 0x00000000);

	flags = 0x000;
	RRX_CFLAG(0x00010000, result, flags);
	EXPECT_EQ(flags, 0x000);
	EXPECT_EQ(result, 0x00008000);
}

TEST(HostInstructionMisc, MUL) {
	uint32_t result;
	uint32_t flags;

	flags = 0x8C1;
	result = 0x01234567;
	MUL_FLAGS(0, 0, result, flags);
	EXPECT_EQ(flags, 0x841);
	EXPECT_EQ(result, 0);

	flags = 0x8C1;
	result = 0x01234567;
	MUL_FLAGS(1, 124, result, flags);
	EXPECT_EQ(flags, 0x801);
	EXPECT_EQ(result, 124);

	flags = 0x8C1;
	result = 0x01234567;
	MUL_FLAGS(static_cast<uint32_t>(-1), static_cast<uint32_t>(124), result, flags);
	EXPECT_EQ(flags, 0x881);
	EXPECT_EQ(result, static_cast<uint32_t>(-124));

	flags = 0x8C1;
	result = 0x01234567;
	MUL_FLAGS(static_cast<uint32_t>(-1), static_cast<uint32_t>(-1), result, flags);
	EXPECT_EQ(flags, 0x801);
	EXPECT_EQ(result, static_cast<uint32_t>(1));

	flags = 0x8C1;
	result = 0x01234567;
	MUL_FLAGS(static_cast<uint32_t>(1000000), static_cast<uint32_t>(1000000), result, flags);
	EXPECT_EQ(flags, 0x881);
	EXPECT_EQ(result, static_cast<uint32_t>(0xD4A51000));
}

TEST(HostInstructionMisc, UMULL) {
	uint32_t resultLo, resultHi;

	resultLo = 0x01234567;
	resultHi = 0x01234567;
	UMULL(0, 0, resultLo, resultHi);
	EXPECT_EQ(resultLo, 0);
	EXPECT_EQ(resultHi, 0);

	resultLo = 0x01234567;
	resultHi = 0x01234567;
	UMULL(static_cast<uint32_t>(-1), static_cast<uint32_t>(1), resultLo, resultHi);
	EXPECT_EQ(resultLo, 0xFFFFFFFF);
	EXPECT_EQ(resultHi, 0);

	resultLo = 0x01234567;
	resultHi = 0x01234567;
	UMULL(static_cast<uint32_t>(-1), static_cast<uint32_t>(-1), resultLo, resultHi);
	EXPECT_EQ(resultLo, 0x00000001);
	EXPECT_EQ(resultHi, 0xFFFFFFFE);

	resultLo = 0x01234567;
	resultHi = 0x01234567;
	UMULL(static_cast<uint32_t>(1000000), static_cast<uint32_t>(1000000), resultLo, resultHi);
	EXPECT_EQ(resultLo, 0xD4A51000);
	EXPECT_EQ(resultHi, 0x000000E8);

	resultLo = 0x01234567;
	resultHi = 0x01234567;
	UMULL(static_cast<uint32_t>(1000000), static_cast<uint32_t>(-1000000), resultLo, resultHi);
	EXPECT_EQ(resultLo, 0x2B5AF000);
	EXPECT_EQ(resultHi, 0x000F4157);
}

TEST(HostInstructionMisc, UMULL_FLAGS) {
	uint32_t resultLo, resultHi;
	uint32_t flags;

	flags = 0x8C1;
	resultLo = 0x01234567;
	resultHi = 0x01234567;
	UMULL_FLAGS(0, 0, resultLo, resultHi, flags);
	EXPECT_EQ(flags, 0x841);
	EXPECT_EQ(resultLo, 0);
	EXPECT_EQ(resultHi, 0);

	flags = 0x8C1;
	resultLo = 0x01234567;
	resultHi = 0x01234567;
	UMULL_FLAGS(static_cast<uint32_t>(-1), static_cast<uint32_t>(1), resultLo, resultHi, flags);
	EXPECT_EQ(flags, 0x801);
	EXPECT_EQ(resultLo, 0xFFFFFFFF);
	EXPECT_EQ(resultHi, 0);

	flags = 0x8C1;
	resultLo = 0x01234567;
	resultHi = 0x01234567;
	UMULL_FLAGS(static_cast<uint32_t>(-1), static_cast<uint32_t>(-1), resultLo, resultHi, flags);
	EXPECT_EQ(flags, 0x881);
	EXPECT_EQ(resultLo, 0x00000001);
	EXPECT_EQ(resultHi, 0xFFFFFFFE);

	flags = 0x8C1;
	resultLo = 0x01234567;
	resultHi = 0x01234567;
	UMULL_FLAGS(static_cast<uint32_t>(1000000), static_cast<uint32_t>(1000000), resultLo, resultHi, flags);
	EXPECT_EQ(flags, 0x801);
	EXPECT_EQ(resultLo, 0xD4A51000);
	EXPECT_EQ(resultHi, 0x000000E8);

	flags = 0x8C1;
	resultLo = 0x01234567;
	resultHi = 0x01234567;
	UMULL_FLAGS(static_cast<uint32_t>(1000000), static_cast<uint32_t>(-1000000), resultLo, resultHi, flags);
	EXPECT_EQ(flags, 0x801);
	EXPECT_EQ(resultLo, 0x2B5AF000);
	EXPECT_EQ(resultHi, 0x000F4157);
}

TEST(HostInstructionMisc, SMULL) {
	uint32_t resultLo, resultHi;

	resultLo = 0x01234567;
	resultHi = 0x01234567;
	SMULL(0, 0, resultLo, resultHi);
	EXPECT_EQ(resultLo, 0);
	EXPECT_EQ(resultHi, 0);

	resultLo = 0x01234567;
	resultHi = 0x01234567;
	SMULL(static_cast<uint32_t>(-1), static_cast<uint32_t>(1), resultLo, resultHi);
	EXPECT_EQ(resultLo, 0xFFFFFFFF);
	EXPECT_EQ(resultHi, 0xFFFFFFFF);

	resultLo = 0x01234567;
	resultHi = 0x01234567;
	SMULL(static_cast<uint32_t>(-1), static_cast<uint32_t>(-1), resultLo, resultHi);
	EXPECT_EQ(resultLo, 0x00000001);
	EXPECT_EQ(resultHi, 0x00000000);

	resultLo = 0x01234567;
	resultHi = 0x01234567;
	SMULL(static_cast<uint32_t>(1000000), static_cast<uint32_t>(1000000), resultLo, resultHi);
	EXPECT_EQ(resultLo, 0xD4A51000);
	EXPECT_EQ(resultHi, 0x000000E8);

	resultLo = 0x01234567;
	resultHi = 0x01234567;
	SMULL(static_cast<uint32_t>(1000000), static_cast<uint32_t>(-1000000), resultLo, resultHi);
	EXPECT_EQ(resultLo, 0x2B5AF000);
	EXPECT_EQ(resultHi, 0xFFFFFF17);
}

TEST(HostInstructionMisc, SMULL_FLAGS) {
	uint32_t resultLo, resultHi;
	uint32_t flags;

	flags = 0x8C1;
	resultLo = 0x01234567;
	resultHi = 0x01234567;
	SMULL_FLAGS(0, 0, resultLo, resultHi, flags);
	EXPECT_EQ(flags, 0x841);
	EXPECT_EQ(resultLo, 0);
	EXPECT_EQ(resultHi, 0);

	flags = 0x8C1;
	resultLo = 0x01234567;
	resultHi = 0x01234567;
	SMULL_FLAGS(static_cast<uint32_t>(-1), static_cast<uint32_t>(1), resultLo, resultHi, flags);
	EXPECT_EQ(flags, 0x881);
	EXPECT_EQ(resultLo, 0xFFFFFFFF);
	EXPECT_EQ(resultHi, 0xFFFFFFFF);

	flags = 0x8C1;
	resultLo = 0x01234567;
	resultHi = 0x01234567;
	SMULL_FLAGS(static_cast<uint32_t>(-1), static_cast<uint32_t>(-1), resultLo, resultHi, flags);
	EXPECT_EQ(flags, 0x801);
	EXPECT_EQ(resultLo, 0x00000001);
	EXPECT_EQ(resultHi, 0x00000000);

	flags = 0x8C1;
	resultLo = 0x01234567;
	resultHi = 0x01234567;
	SMULL_FLAGS(static_cast<uint32_t>(1000000), static_cast<uint32_t>(1000000), resultLo, resultHi, flags);
	EXPECT_EQ(flags, 0x801);
	EXPECT_EQ(resultLo, 0xD4A51000);
	EXPECT_EQ(resultHi, 0x000000E8);

	flags = 0x8C1;
	resultLo = 0x01234567;
	resultHi = 0x01234567;
	SMULL_FLAGS(static_cast<uint32_t>(1000000), static_cast<uint32_t>(-1000000), resultLo, resultHi, flags);
	EXPECT_EQ(flags, 0x881);
	EXPECT_EQ(resultLo, 0x2B5AF000);
	EXPECT_EQ(resultHi, 0xFFFFFF17);
}



struct HostInstructionCondState {
	uint8_t id;
	uint32_t flags;
	bool result;
};

class HostInstructionCondTest : public ::testing::TestWithParam<HostInstructionCondState> {
};

TEST_P(HostInstructionCondTest, HostInstructionCondTest) {
	auto &param = GetParam();
	bool result = IsConditionMet(param.id, param.flags);
	EXPECT_EQ(result, param.result) << "IsConditionMet(" << static_cast<int>(param.id) <<
		", " << std::setw(4) << std::hex << param.flags << ")<" << result << "> ==" << param.result << std::endl;
}

INSTANTIATE_TEST_CASE_P(HostInstructionCondTest, HostInstructionCondTest, testing::Values(
	HostInstructionCondState{0, 0xFBF, false},
	HostInstructionCondState{0, 0x040, true},
	HostInstructionCondState{1, 0xFBF, true},
	HostInstructionCondState{1, 0x040, false},
	HostInstructionCondState{2, 0xFFE, false},
	HostInstructionCondState{2, 0x001, true},
	HostInstructionCondState{3, 0xFFE, true},
	HostInstructionCondState{3, 0x001, false},
	HostInstructionCondState{4, 0xF7F, false},
	HostInstructionCondState{4, 0x080, true},
	HostInstructionCondState{5, 0xF7F, true},
	HostInstructionCondState{5, 0x080, false},
	HostInstructionCondState{6, 0x7FF, false},
	HostInstructionCondState{6, 0x800, true},
	HostInstructionCondState{7, 0x7FF, true},
	HostInstructionCondState{7, 0x800, false},
	HostInstructionCondState{8, 0xFFF, false},
	HostInstructionCondState{8, 0x000, false},
	HostInstructionCondState{8, 0xFFE, false},
	HostInstructionCondState{8, 0x001, true},
	HostInstructionCondState{8, 0xFBF, true},
	HostInstructionCondState{8, 0x040, false},
	HostInstructionCondState{8, 0xFBE, false},
	HostInstructionCondState{8, 0x041, false},
	HostInstructionCondState{9, 0xFBE, true},
	HostInstructionCondState{9, 0x041, true},
	HostInstructionCondState{9, 0xFBF, false},
	HostInstructionCondState{9, 0x040, true},
	HostInstructionCondState{9, 0xFFE, true},
	HostInstructionCondState{9, 0x001, false},
	HostInstructionCondState{9, 0xFFF, true},
	HostInstructionCondState{9, 0x000, true},
	HostInstructionCondState{10, 0xFFF, true},
	HostInstructionCondState{10, 0x000, true},
	HostInstructionCondState{10, 0xF7F, false},
	HostInstructionCondState{10, 0x080, false},
	HostInstructionCondState{10, 0x7FF, false},
	HostInstructionCondState{10, 0x800, false},
	HostInstructionCondState{10, 0x77F, true},
	HostInstructionCondState{10, 0x880, true},
	HostInstructionCondState{11, 0xFFF, false},
	HostInstructionCondState{11, 0x000, false},
	HostInstructionCondState{11, 0xF7F, true},
	HostInstructionCondState{11, 0x080, true},
	HostInstructionCondState{11, 0x7FF, true},
	HostInstructionCondState{11, 0x800, true},
	HostInstructionCondState{11, 0x77F, false},
	HostInstructionCondState{11, 0x880, false}
	));

INSTANTIATE_TEST_CASE_P(HostInstructionCondTest2, HostInstructionCondTest, testing::Values(
	HostInstructionCondState{12, 0xFFF, false},
	HostInstructionCondState{12, 0x000, true},
	HostInstructionCondState{12, 0xFBF, true},
	HostInstructionCondState{12, 0x040, false},
	HostInstructionCondState{12, 0xF7F, false},
	HostInstructionCondState{12, 0x080, false},
	HostInstructionCondState{12, 0xF3F, false},
	HostInstructionCondState{12, 0x0C0, false},
	HostInstructionCondState{12, 0x7FF, false},
	HostInstructionCondState{12, 0x800, false},
	HostInstructionCondState{12, 0x7BF, false},
	HostInstructionCondState{12, 0x840, false},
	HostInstructionCondState{12, 0x77F, false},
	HostInstructionCondState{12, 0x880, true},
	HostInstructionCondState{12, 0x73F, true},
	HostInstructionCondState{12, 0x8C0, false},
	HostInstructionCondState{13, 0xFFF, true},
	HostInstructionCondState{13, 0x000, false},
	HostInstructionCondState{13, 0xFBF, false},
	HostInstructionCondState{13, 0x040, true},
	HostInstructionCondState{13, 0xF7F, true},
	HostInstructionCondState{13, 0x080, true},
	HostInstructionCondState{13, 0xF3F, true},
	HostInstructionCondState{13, 0x0C0, true},
	HostInstructionCondState{13, 0x7FF, true},
	HostInstructionCondState{13, 0x800, true},
	HostInstructionCondState{13, 0x7BF, true},
	HostInstructionCondState{13, 0x840, true},
	HostInstructionCondState{13, 0x77F, true},
	HostInstructionCondState{13, 0x880, false},
	HostInstructionCondState{13, 0x73F, false},
	HostInstructionCondState{13, 0x8C0, true},
	HostInstructionCondState{14, 0x000, true},
	HostInstructionCondState{14, 0xFFF, true},
	HostInstructionCondState{15, 0x000, false},
	HostInstructionCondState{15, 0xFFF, false}
));

struct ArmLoadStoreMultipleState {
	bool includeRef;
	bool upwards;
	bool writeback;
	bool store;
	uint16_t register_list;
	uint8_t rn;
};

class ArmLoadStoreMultipleTest : public ::testing::TestWithParam<ArmLoadStoreMultipleState> {
protected:
	ArmLoadStoreMultipleTest() {
		// You can do set-up work for each test here.
	}

	~ArmLoadStoreMultipleTest() override {
		// You can do clean-up work that doesn't throw exceptions here.
	}

	void SetUp() override
	{
		_gba.Init();
		auto &cpu = _gba.GetCpu();
		cpu.SetRegisterValue(0, 0x00000000);
		cpu.SetRegisterValue(1, 0x01010101);
		cpu.SetRegisterValue(2, 0x02020202);
		cpu.SetRegisterValue(3, 0x03030303);
		cpu.SetRegisterValue(4, 0x04040404);
		cpu.SetRegisterValue(5, 0x05050505);
		cpu.SetRegisterValue(6, 0x06060606);
		cpu.SetRegisterValue(7, 0x07070707);
		cpu.SetRegisterValue(8, 0x08080808);
		cpu.SetRegisterValue(9, 0x09090909);
		cpu.SetRegisterValue(0xa, 0x0a0a0a0a);
		cpu.SetRegisterValue(0xb, 0x0b0b0b0b);
		cpu.SetRegisterValue(0xc, 0x0c0c0c0c);
		cpu.SetRegisterValue(0xd, 0x0d0d0d0d);
		cpu.SetRegisterValue(0xe, 0x0e0e0e0e);
		cpu.SetRegisterValue(0xf, 0x0f0f0f00);
		memset(_mem, 0xaa, sizeof(_mem));
		_mem_read32 = [this](uint32_t addr) -> uint32_t
		{
			return addr;
		};
		_mem_write32 = [this](uint32_t addr, uint32_t value, bool event)
		{
			uint32_t maskedAddr = addr & (16 * 4 - 1);
			*reinterpret_cast<uint32_t *>(reinterpret_cast<uint8_t *>(_mem) + maskedAddr) = value;
		};
	}

	void TearDown() override
	{
		_mem_read32 = nullptr;
		_mem_write32 = nullptr;
	}

	Gba _gba;
	uint32_t _mem[17];
};

TEST_P(ArmLoadStoreMultipleTest, ArmLoadStoreMultipleTest) {
	auto &param = GetParam();
	uint32_t instr = 0xE8000000 |
		(param.includeRef ? 0x01000000 : 0) |
		(param.upwards ? 0x00800000 : 0) |
		(param.writeback ? 0x00200000 : 0) |
		(param.store ? 0 : 0x00100000) |
		((param.rn & 0xF) << 16) |
		param.register_list;
	auto &cpu = _gba.GetCpu();
	uint32_t address = cpu.GetRegisterValue(param.rn);
	int numRegisters = 0;
	for(int i = 0; i < 16; i++)
	{
		if ((param.register_list & (1 << i)) != 0) numRegisters++;
	}

	// Do the actual test
	cpu.ExecuteARM(instr);
	if(param.store)
	{
		for(int i = 0; i < 15; i++)
		{
			if(i == param.rn && param.writeback)
			{
				uint32_t regVal = cpu.GetRegisterValue(i);
				if(param.upwards)
				{
					EXPECT_EQ(regVal, address + numRegisters * 4) << "Writen back address is wrong";
				} else
				{
					EXPECT_EQ(regVal, address - numRegisters * 4) << "Writen back address is wrong";
				}
			}
			else {
				EXPECT_EQ(cpu.GetRegisterValue(i), i | (i << 8) | (i << 16) | (i << 24)) << "Register " << i << " should not have been modified";
			}
		}
		EXPECT_EQ(cpu.GetRegisterValue(15), 0x0f0f0f04) << "Register 15 should not have been modified";

		uint32_t start_address;
		uint32_t end_address; 
		if (param.upwards) {
			start_address = address;
			end_address = address + numRegisters * 4 - 4;
		} else
		{
			start_address = address - numRegisters * 4 + 4;
			end_address = address;
		}
		if(param.includeRef)
		{
			start_address += param.upwards ? 4 : -4;
			end_address += param.upwards ? 4 : -4;
		}
		address = start_address;
		for (int i = 0; i < 16; i++)
		{
			if ((param.register_list & (1 << i)) != 0) {
				uint32_t maskedAddr = address & (16 * 4 - 1);
				uint32_t memVal = *reinterpret_cast<uint32_t *>(reinterpret_cast<uint8_t *>(_mem) + maskedAddr);
				address += 4;
				EXPECT_EQ(memVal, i | (i << 8) | (i << 16) | (i << 24)) << "Wrong value stored in memory for register " << i;
			}
		}
	} else
	{
		// Check is registers are unchanged
		for (int i = 0; i < 15; i++)
		{
			if ((param.register_list & (1 << i)) != 0) continue;
			if (i == param.rn && param.writeback)
			{
				uint32_t regVal = cpu.GetRegisterValue(i);
				if (param.upwards)
				{
					EXPECT_EQ(regVal, address + numRegisters * 4) << "Writen back address is wrong";
				}
				else
				{
					EXPECT_EQ(regVal, address - numRegisters * 4) << "Writen back address is wrong";
				}
			}
			else {
				EXPECT_EQ(cpu.GetRegisterValue(i), i | (i << 8) | (i << 16) | (i << 24)) << "Register " << i << " should not have been modified";
			}
		}
		if ((param.register_list & (1 << 15)) == 0)
		{
			EXPECT_EQ(cpu.GetRegisterValue(15), 0x0f0f0f04) << "Register 15 should not have been modified";
		}

		uint32_t start_address;
		uint32_t end_address;
		if (param.upwards) {
			start_address = address;
			end_address = address + numRegisters * 4 - 4;
		}
		else
		{
			start_address = address - numRegisters * 4 + 4;
			end_address = address;
		}
		if (param.includeRef)
		{
			start_address += param.upwards ? 4 : -4;
			end_address += param.upwards ? 4 : -4;
		}
		address = start_address;
		for (int i = 0; i < 16; i++)
		{
			if ((param.register_list & (1 << i)) != 0) {
				EXPECT_EQ(address, cpu.GetRegisterValue(i)) << "Wrong value stored in memory for register " << i;
				address += 4;
			}
		}
	}
}

std::string LoadStoreNameGen(testing::TestParamInfo<ArmLoadStoreMultipleState> test)
{
	std::stringstream ss;
	auto &param = test.param;
	ss << (param.store ? "STM" : "LDM");
	ss << (param.upwards ? "I" : "D");
	ss << (param.includeRef ? "B" : "A");
	ss << "_r" << static_cast<int>(param.rn);
	if (param.writeback) ss << "_w";
	ss << "_";
	bool first = true;
	for (int i = 0; i < 16; i++)
	{
		if ((param.register_list & (1 << i)) != 0)
		{
			if (first)
				first = false;
			else
				ss << "_";
			ss << "r" << i;
		}
	}
	return ss.str();
}

INSTANTIATE_TEST_CASE_P(ArmLDMTest, ArmLoadStoreMultipleTest, testing::Values(
	ArmLoadStoreMultipleState{ false, false, false, false, 0x0001, 0 },
	ArmLoadStoreMultipleState{ false, false, false, false, 0x0100, 0 },
	ArmLoadStoreMultipleState{ false, false, false, false, 0x00f0, 0 },
	ArmLoadStoreMultipleState{ false, false, false, false, 0xfffe, 0 },
	ArmLoadStoreMultipleState{ false, false, true, false, 0x0100, 0 },
	ArmLoadStoreMultipleState{ false, false, true, false, 0x00f0, 0 },
	ArmLoadStoreMultipleState{ false, false, true, false, 0xfffe, 0 },
	ArmLoadStoreMultipleState{ false, true, false, false, 0x0001, 0 },
	ArmLoadStoreMultipleState{ false, true, false, false, 0x0100, 0 },
	ArmLoadStoreMultipleState{ false, true, false, false, 0x00f0, 0 },
	ArmLoadStoreMultipleState{ false, true, false, false, 0xfffe, 0 },
	ArmLoadStoreMultipleState{ false, true, true, false, 0x0100, 0 },
	ArmLoadStoreMultipleState{ false, true, true, false, 0x00f0, 0 },
	ArmLoadStoreMultipleState{ false, true, true, false, 0xfffe, 0 },
	ArmLoadStoreMultipleState{ true, false, false, false, 0x0001, 0 },
	ArmLoadStoreMultipleState{ true, false, false, false, 0x0100, 0 },
	ArmLoadStoreMultipleState{ true, false, false, false, 0x00f0, 0 },
	ArmLoadStoreMultipleState{ true, false, false, false, 0xfffe, 0 },
	ArmLoadStoreMultipleState{ true, false, true, false, 0x0100, 0 },
	ArmLoadStoreMultipleState{ true, false, true, false, 0x00f0, 0 },
	ArmLoadStoreMultipleState{ true, false, true, false, 0xfffe, 0 },
	ArmLoadStoreMultipleState{ true, true, false, false, 0x0001, 0 },
	ArmLoadStoreMultipleState{ true, true, false, false, 0x0100, 0 },
	ArmLoadStoreMultipleState{ true, true, false, false, 0x00f0, 0 },
	ArmLoadStoreMultipleState{ true, true, false, false, 0xfffe, 0 },
	ArmLoadStoreMultipleState{ true, true, true, false, 0x0100, 0 },
	ArmLoadStoreMultipleState{ true, true, true, false, 0x00f0, 0 },
	ArmLoadStoreMultipleState{ true, true, true, false, 0xfffe, 0 }
), LoadStoreNameGen);

INSTANTIATE_TEST_CASE_P(ArmSTMTest, ArmLoadStoreMultipleTest, testing::Values(
	ArmLoadStoreMultipleState{ false, false, false, true, 0x0001, 0 },
	ArmLoadStoreMultipleState{ false, false, false, true, 0x0100, 0 },
	ArmLoadStoreMultipleState{ false, false, false, true, 0x00f0, 0 },
	ArmLoadStoreMultipleState{ false, false, false, true, 0x7fff, 0 },
	ArmLoadStoreMultipleState{ false, false, true, true, 0x0001, 0 },
	ArmLoadStoreMultipleState{ false, false, true, true, 0x0100, 0 },
	ArmLoadStoreMultipleState{ false, false, true, true, 0x00f0, 0 },
	ArmLoadStoreMultipleState{ false, false, true, true, 0x7fff, 0 },
	ArmLoadStoreMultipleState{ false, true, false, true, 0x0001, 0 },
	ArmLoadStoreMultipleState{ false, true, false, true, 0x0100, 0 },
	ArmLoadStoreMultipleState{ false, true, false, true, 0x00f0, 0 },
	ArmLoadStoreMultipleState{ false, true, false, true, 0x7fff, 0 },
	ArmLoadStoreMultipleState{ false, true, true, true, 0x0001, 0 },
	ArmLoadStoreMultipleState{ false, true, true, true, 0x0100, 0 },
	ArmLoadStoreMultipleState{ false, true, true, true, 0x00f0, 0 },
	ArmLoadStoreMultipleState{ false, true, true, true, 0x7fff, 0 },
	ArmLoadStoreMultipleState{ true, false, false, true, 0x0001, 0 },
	ArmLoadStoreMultipleState{ true, false, false, true, 0x0100, 0 },
	ArmLoadStoreMultipleState{ true, false, false, true, 0x00f0, 0 },
	ArmLoadStoreMultipleState{ true, false, false, true, 0x7fff, 0 },
	ArmLoadStoreMultipleState{ true, false, true, true, 0x0001, 0 },
	ArmLoadStoreMultipleState{ true, false, true, true, 0x0100, 0 },
	ArmLoadStoreMultipleState{ true, false, true, true, 0x00f0, 0 },
	ArmLoadStoreMultipleState{ true, false, true, true, 0x7fff, 0 },
	ArmLoadStoreMultipleState{ true, true, false, true, 0x0001, 0 },
	ArmLoadStoreMultipleState{ true, true, false, true, 0x0100, 0 },
	ArmLoadStoreMultipleState{ true, true, false, true, 0x00f0, 0 },
	ArmLoadStoreMultipleState{ true, true, false, true, 0x7fff, 0 },
	ArmLoadStoreMultipleState{ true, true, true, true, 0x0001, 0 },
	ArmLoadStoreMultipleState{ true, true, true, true, 0x0100, 0 },
	ArmLoadStoreMultipleState{ true, true, true, true, 0x00f0, 0 },
	ArmLoadStoreMultipleState{ true, true, true, true, 0x7fff, 0 }
), LoadStoreNameGen);


int main(int argc, char** argv) {
	// The following line must be executed to initialize Google Mock
	// (and Google Test) before running the tests.
	::testing::InitGoogleMock(&argc, argv);

	int result = RUN_ALL_TESTS();

	return result;
}
