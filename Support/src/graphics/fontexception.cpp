
#include <emuall/graphics/font.h>
#include <ft2build.h>
#include FT_FREETYPE_H

using namespace emuall;

FontException::FontException(int errorCode, const char *format, ...) :
	BaseException("")
{
	va_list args;
	va_start(args, format);
	CreateMsg(format, args);
	va_end(args);
	_msg->append("(");
	_msg->append(GetErrorMessage(errorCode));
	_msg->append(")");
}

FontException::FontException(int errorCode, const char *format, va_list args) :
		BaseException(format, args)
{
	_msg->append("(");
	_msg->append(GetErrorMessage(errorCode));
	_msg->append(")");
}

FontException::FontException(int errorCode) : 
	BaseException(GetErrorMessage(errorCode))
{

}

FontException::FontException(const char *format, ...) :
	BaseException("")
{
	va_list args;
	va_start(args, format);
	CreateMsg(format, args);
	va_end(args);
}

FontException::FontException(const char *format, va_list args) :
		BaseException(format, args)
{
}

FontException::~FontException()
{

}

const char * FontException::GetErrorMessage(int errorCode)
{
	#undef __FTERRORS_H__
	#define FT_ERRORDEF( e, v, s )  case e: return s;
	#define FT_ERROR_START_LIST     switch(errorCode) {
	#define FT_ERROR_END_LIST       }
	#include FT_ERRORS_H
	return "Unknown error";
}
