#include <emuall/graphics/graphicsexception.h>
#include <GL/glew.h>
#include <GL/glu.h>
#include <cstdarg>

GraphicsException::GraphicsException(unsigned int error, const char *format, ...) :
	BaseException(""), _error(error)
{
	va_list args;
	va_start(args, format);
	CreateMsg(format, args);
	va_end(args);
}

GraphicsException::GraphicsException(unsigned int error, const char *format, va_list args) :
		BaseException(format, args), _error(error)
{
}

GraphicsException::GraphicsException(unsigned int error) :
	BaseException(""), _error(error)
{
	*_msg = (const char *)gluErrorString(_error);
}

GraphicsException::~GraphicsException()
{

}
