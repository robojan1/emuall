#include <emuall/util/stddir.h>
#include <string>
#include <vector>
#include <cstdlib>
#include <cassert>

#if defined(_WIN32) || defined(WIN32)

#include <tchar.h>
#include <ShObjIdl.h>
#include <objbase.h>
#include <comdef.h>
#include <KnownFolders.h>
#include <PathCch.h>

static std::string WideStringToStdString(LPWSTR in)
{
	int strSize = WideCharToMultiByte(CP_UTF8, 0, in, -1, NULL, 0, NULL, NULL);
	assert(strSize > 0);
	std::vector<char> buffer;
	buffer.resize(strSize);
	WideCharToMultiByte(CP_UTF8, 0, in, -1, buffer.data(), static_cast<int>(buffer.size()), NULL, NULL);
	return std::string(buffer.begin(), buffer.end()-1);
}

std::string emuall::GetExecuatableDir()
{
	static std::string executableDir;
	if (executableDir.empty()) {
		std::vector<wchar_t> exePathBuffer;
		DWORD copied = 0;
		do {
			exePathBuffer.resize(exePathBuffer.size() + 256);
			copied = GetModuleFileNameW(NULL, exePathBuffer.data(), static_cast<DWORD>(exePathBuffer.size()));
		} while (copied >= exePathBuffer.size());

		// Remove executable
		PathCchRemoveFileSpec(exePathBuffer.data(), exePathBuffer.size());

		// Convert to std::string
		executableDir = WideStringToStdString(exePathBuffer.data());
	}
	return executableDir;
}

static std::string GetBaseDir()
{
	static std::string baseDir;
	if (baseDir.empty()) {
		IKnownFolder *folder;
		IKnownFolderManager *manager;
		HRESULT hr = CoCreateInstance(CLSID_KnownFolderManager, NULL, CLSCTX_INPROC_SERVER, IID_PPV_ARGS(&manager));
		if (!SUCCEEDED(hr)) {
			_com_error err(hr);
			_ftprintf(stderr, TEXT("[Error] Could not create KnownFolderManager: %s"), err.ErrorMessage());
			abort();
		}
		hr = manager->GetFolder(FOLDERID_Documents, &folder);
		if (!SUCCEEDED(hr)) {
			_com_error err(hr);
			_ftprintf(stderr, TEXT("[Error] Could not get the documents folder: %s"), err.ErrorMessage());

			manager->Release();
			abort();
		}
		LPWSTR documentsPath;
		folder->GetPath(0, &documentsPath);

		baseDir = WideStringToStdString(documentsPath);

		CoTaskMemFree(documentsPath);

		folder->Release();
		manager->Release();

		baseDir.append("\\My Games\\Emuall\\");
	}
	return baseDir;
}

std::string emuall::GetPluginsDir()
{
	static std::string pluginDir;
	if (pluginDir.empty()) {
		pluginDir = GetExecuatableDir();
		pluginDir += "\\plugins";
	}
	return pluginDir;
}
#elif defined(__APPLE__)

std::string emuall::GetPluginsDir()
{
	return std::string();
}
#elif defined(__unix__)

#include <unistd.h>
#include <sys/types.h>
#include <pwd.h>
#include <libgen.h>

static std::string GetHomeDir()
{
	const char *homedir = std::getenv("HOME");
	if (homedir == nullptr) {
		struct passwd *pw = getpwuid(geteuid());
		homedir = pw->pw_dir;
	}
	return homedir;
}

static std::string GetBaseDir()
{
	static std::string baseDir;
	if (baseDir.empty()) {
		baseDir = GetHomeDir();
		baseDir += "/emuall/";
	}
	return baseDir;
}

std::string emuall::GetExecuatableDir()
{
	static std::string executableDir;
	if (executableDir.empty()) {
		std::vector<char> exePathBuffer;
		ssize_t copied = 0;
		do {
			exePathBuffer.resize(exePathBuffer.size() + 256);
			copied = readlink("/proc/self/exe", exePathBuffer.data(), exePathBuffer.size());
			assert(copied > 0);
		} while (copied >= exePathBuffer.size());

		executableDir = dirname(exePathBuffer.data());
	}
	return executableDir;
}

std::string emuall::GetPluginsDir()
{
	return INSTALL_PREFIX "/lib/emuall";
}
#endif

std::string emuall::GetConfigDir()
{
	return GetBaseDir();
}

std::string emuall::GetSavesDir()
{
	static std::string savesDir = GetBaseDir() + "savedata";
	return savesDir;
}

std::string emuall::GetSaveStatesDir()
{
	static std::string savestate = GetBaseDir() + "savestate";
	return savestate;
}
