#include <emuall/util/savedata.h>
#include <emuall/util/endian.h>
#include <cassert>
#include <cstring>

using namespace emuall;

SaveData::SaveData(int version) : _version(version), _pos(0)
{
	*this << static_cast<uint32_t>(0); // Size
	*this << static_cast<int32_t>(version); // Version
}

SaveData::SaveData(const std::vector<uint8_t> &data) : _data(data), _pos(0) {
	Parse();
}

SaveData::SaveData(const uint8_t *data, size_t dataLen) : _data(data, data + dataLen), _pos(0) {
	Parse();
}

SaveData::~SaveData() {

}


SaveData &SaveData::operator<<(uint8_t x) {
	_data.resize(_data.size() + sizeof(x));
	_data[_pos] = x;
	_pos += sizeof(x);
	return *this;
}

SaveData &SaveData::operator<<(uint16_t x) {
	auto end = Endian::getSingleton(false);
	_data.resize(_data.size() + sizeof(x));
	*reinterpret_cast<uint16_t *>(_data.data() + _pos) = end->convu16(x);
	_pos += sizeof(x);
	return *this;
}

SaveData &SaveData::operator<<(uint32_t x) {
	auto end = Endian::getSingleton(false);
	_data.resize(_data.size() + sizeof(x));
	*reinterpret_cast<uint32_t *>(_data.data() + _pos) = end->convu32(x);
	_pos += sizeof(x);
	return *this;
}

SaveData &SaveData::operator<<(uint64_t x) {
	auto end = Endian::getSingleton(false);
	_data.resize(_data.size() + sizeof(x));
	*reinterpret_cast<uint64_t *>(_data.data() + _pos) = end->convu64(x);
	_pos += sizeof(x);
	return *this;
}

SaveData &SaveData::operator<<(int8_t x) {
	_data.resize(_data.size() + sizeof(x));
	_data[_pos] = static_cast<uint8_t>(x);
	_pos += sizeof(x);
	return *this;
}

SaveData &SaveData::operator<<(int16_t x) {
	auto end = Endian::getSingleton(false);
	_data.resize(_data.size() + sizeof(x));
	*reinterpret_cast<int16_t *>(_data.data() + _pos) = end->convi16(x);
	_pos += sizeof(x);
	return *this;
}

SaveData &SaveData::operator<<(int32_t x) {
	auto end = Endian::getSingleton(false);
	_data.resize(_data.size() + sizeof(x));
	*reinterpret_cast<int32_t *>(_data.data() + _pos) = end->convi32(x);
	_pos += sizeof(x);
	return *this;
}

SaveData &SaveData::operator<<(int64_t x) {
	auto end = Endian::getSingleton(false);
	_data.resize(_data.size() + sizeof(x));
	*reinterpret_cast<int64_t *>(_data.data() + _pos) = end->convi64(x);
	_pos += sizeof(x);
	return *this;
}

SaveData &SaveData::operator<<(float x) {
	auto end = Endian::getSingleton(false);
	_data.resize(_data.size() + sizeof(x));
	*reinterpret_cast<float *>(_data.data() + _pos) = end->convf(x);
	_pos += sizeof(x);
	return *this;
}

SaveData &SaveData::operator<<(double x) {
	auto end = Endian::getSingleton(false);
	_data.resize(_data.size() + sizeof(x));
	*reinterpret_cast<double *>(_data.data() + _pos) = end->convd(x);
	_pos += sizeof(x);
	return *this;
}

SaveData& SaveData::operator<<(bool x)
{
	_data.resize(_data.size() + 1);
	_data[_pos] = x ? 1 : 0;
	_pos += 1;
	return *this;
}

SaveData &SaveData::operator<<(const std::string &x) {
	*this << static_cast<uint32_t>(x.size());
	_data.resize(_data.size() + x.size());
	for (std::size_t i = 0; i < x.size(); i++) {
		_data[_pos + i] = x[i];
	}
	_pos += static_cast<uint32_t>(x.size());
	return *this;
}

SaveData &SaveData::operator<<(SaveData &x) {
	auto &data = x.GetData();
	_data.insert(_data.end(), data.begin(), data.end());
	_pos += static_cast<uint32_t>(data.size());
	return *this;
}

SaveData& SaveData::StoreBuffer(const void* src, size_t size)
{
	*this << static_cast<uint32_t>(size);
	_data.resize(_data.size() + size);
	memcpy(_data.data() + _pos, src, size);
	_pos += size;
	return *this;
}

SaveData &SaveData::operator>>(uint8_t &x) {
	x = _data[_pos];
	_pos += sizeof(x);
	return *this;
}

SaveData &SaveData::operator>>(uint16_t &x) {
	auto end = Endian::getSingleton(false);
	x = end->convu16(*reinterpret_cast<uint16_t *>(_data.data() + _pos));
	_pos += sizeof(x);
	return *this;
}

SaveData &SaveData::operator>>(uint32_t &x) {
	auto end = Endian::getSingleton(false);
	x = end->convu32(*reinterpret_cast<uint32_t *>(_data.data() + _pos));
	_pos += sizeof(x);
	return *this;
}

SaveData &SaveData::operator>>(uint64_t &x) {
	auto end = Endian::getSingleton(false);
	x = end->convu64(*reinterpret_cast<uint64_t *>(_data.data() + _pos));
	_pos += sizeof(x);
	return *this;
}

SaveData &SaveData::operator>>(int8_t &x) {
	x = static_cast<int8_t>(_data[_pos]);
	_pos += sizeof(x);
	return *this;
}

SaveData &SaveData::operator>>(int16_t &x) {
	auto end = Endian::getSingleton(false);
	x = end->convi16(*reinterpret_cast<int16_t *>(_data.data() + _pos));
	_pos += sizeof(x);
	return *this;
}

SaveData &SaveData::operator>>(int32_t &x) {
	auto end = Endian::getSingleton(false);
	x = end->convi32(*reinterpret_cast<int32_t *>(_data.data() + _pos));
	_pos += sizeof(x);
	return *this;
}

SaveData &SaveData::operator>>(int64_t &x) {
	auto end = Endian::getSingleton(false);
	x = end->convi64(*reinterpret_cast<int64_t *>(_data.data() + _pos));
	_pos += sizeof(x);
	return *this;
}

SaveData &SaveData::operator>>(float &x) {
	auto end = Endian::getSingleton(false);
	x = end->convf(*reinterpret_cast<float *>(_data.data() + _pos));
	_pos += sizeof(x);
	return *this;
}

SaveData &SaveData::operator>>(double &x) {
	auto end = Endian::getSingleton(false);
	x = end->convd(*reinterpret_cast<double *>(_data.data() + _pos));
	_pos += sizeof(x);
	return *this;
}

SaveData& SaveData::operator>>(bool& x)
{
	x = _data[_pos] != 0;
	_pos++;
	return *this;
}

SaveData &SaveData::operator>>(std::string &x) {
	uint32_t strLen; *this >> strLen;
	x.resize(strLen);
	for (std::size_t i = 0; i < strLen; i++) {
		x[i] = _data[_pos + i];
	}
	_pos += strLen;
	return *this;
}

SaveData &SaveData::operator>>(SaveData &x) {
	uint32_t dataLen; *this >> dataLen;
	_pos -= sizeof(uint32_t);
	x = SaveData(_data.data() + _pos, dataLen);
	_pos += dataLen;
	return *this;
}

std::vector<uint8_t> SaveData::GetBuffer()
{
	uint32_t bufferSize; *this >> bufferSize;
	std::vector<uint8_t> buffer(_data.data() + _pos, _data.data() + _pos + bufferSize);
	_pos += bufferSize;
	return buffer;
}

SaveData SaveData::GetChild()
{
	SaveData data;
	*this >> data;
	return data;
}

const std::vector<uint8_t>& emuall::SaveData::GetData()
{
	auto end = Endian::getSingleton(false);
	*reinterpret_cast<uint32_t *>(_data.data()) = end->convu32(static_cast<uint32_t>(_data.size()));
	return _data;
}

void SaveData::Parse() 
{
	uint32_t dataSize;
	*this >> dataSize >> static_cast<int32_t&>(_version);
	assert(dataSize == _data.size());
}
