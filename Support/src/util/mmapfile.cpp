#include <emuall/util/mmapfile.h>
#include <string>
#include <emuall/util/unicode.h>
#include <cassert>
#include <stdexcept>

#ifndef _WIN32
#include <sys/mman.h>
#include <unistd.h>
#include <sys/fcntl.h>
#endif


#ifdef _WIN32
static DWORD FileModeToDesiredAccess(enum MemoryMappedFile::FileMode mode)
{
	switch(mode)
	{
	case MemoryMappedFile::ReadOnly:
		return GENERIC_READ;
	case MemoryMappedFile::WriteOnly:
		return GENERIC_WRITE;
	case MemoryMappedFile::ReadWrite:
		return GENERIC_READ | GENERIC_WRITE;
	case MemoryMappedFile::ReadExecute:
		return GENERIC_READ;
	case MemoryMappedFile::ReadWriteExecute:
		return GENERIC_READ;
	default:
		assert(false);
		return GENERIC_READ;
	}
}

static DWORD FileModeToProtection(enum MemoryMappedFile::FileMode mode)
{
	switch(mode)
	{
	case MemoryMappedFile::ReadOnly:
		return PAGE_READONLY;
	case MemoryMappedFile::WriteOnly:
		return PAGE_READWRITE;
	case MemoryMappedFile::ReadWrite:
		return PAGE_READWRITE;
	case MemoryMappedFile::ReadExecute:
		return PAGE_EXECUTE_READ;
	case MemoryMappedFile::ReadWriteExecute:
		return PAGE_EXECUTE_READWRITE;
	default:
		assert(false);
		return PAGE_READONLY;
	}
}

static DWORD FileModeToMapAccess(enum MemoryMappedFile::FileMode mode)
{
	switch (mode)
	{
	case MemoryMappedFile::ReadOnly:
		return FILE_MAP_READ;
	case MemoryMappedFile::WriteOnly:
		return FILE_MAP_WRITE;
	case MemoryMappedFile::ReadWrite:
		return FILE_MAP_READ | FILE_MAP_WRITE;
	case MemoryMappedFile::ReadExecute:
		return FILE_MAP_READ | FILE_MAP_EXECUTE;
	case MemoryMappedFile::ReadWriteExecute:
		return FILE_MAP_READ | FILE_MAP_WRITE | FILE_MAP_EXECUTE;
	default:
		assert(false);
		return FILE_MAP_READ;
	}
}

MemoryMappedFile::MemoryMappedFile(const std::string &path, enum FileMode mode)
{
	std::u16string u16path = utf8_to_utf16(path);
	_fileHdl = CreateFileW(reinterpret_cast<const wchar_t *>(u16path.c_str()), 
		FileModeToDesiredAccess(mode), FILE_SHARE_READ,
		nullptr, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, nullptr);
	if(_fileHdl == INVALID_HANDLE_VALUE)
	{
		DWORD err = GetLastError();
		throw FileException(err, "Could not open file \"%s\": %08x", path.c_str(), err);
	}

	SECURITY_ATTRIBUTES sec{ sizeof(SECURITY_ATTRIBUTES), nullptr, FALSE };

	_mapHdl = CreateFileMapping(_fileHdl, nullptr, FileModeToProtection(mode), 0, 0, NULL);
	if(_mapHdl == INVALID_HANDLE_VALUE)
	{
		DWORD err = GetLastError();
		CloseHandle(_fileHdl);
		throw FileException(err, "Could not create map into file \"%s\": %08x", path.c_str(), err);		
	}

	_data = MapViewOfFile(_mapHdl, FileModeToMapAccess(mode), 0, 0, 0);
	if(_data == nullptr)
	{
		DWORD err = GetLastError();
		CloseHandle(_mapHdl);
		CloseHandle(_fileHdl);
		throw FileException(err, "Could not view into mapped file \"%s\": %08x", path.c_str(), err);
	}

	MEMORY_BASIC_INFORMATION info;
	size_t infoSize = VirtualQuery(_data, &info, sizeof(info));
	if(infoSize < offsetof(MEMORY_BASIC_INFORMATION, State))
	{
		DWORD err = GetLastError();
		UnmapViewOfFile(_data);
		CloseHandle(_mapHdl);
		CloseHandle(_fileHdl);
		throw FileException(err, "Could not get file view size\"%s\": %08x", path.c_str(), err);
	}
	_size = info.RegionSize;
}

MemoryMappedFile::~MemoryMappedFile()
{
	if(_data != nullptr)
	{
		UnmapViewOfFile(_data);
	}
	if(_mapHdl != INVALID_HANDLE_VALUE)
	{
		CloseHandle(_mapHdl);
	}
	if(_fileHdl != INVALID_HANDLE_VALUE)
	{
		CloseHandle(_fileHdl);
	}
}

void MemoryMappedFile::Flush() const
{
	FlushViewOfFile(_data, _size);
}

#else
static int FileModeToOpenFlags(enum MemoryMappedFile::FileMode mode)
{
	switch (mode)
	{
	case MemoryMappedFile::ReadOnly:
		return O_RDONLY;
	case MemoryMappedFile::WriteOnly:
		return O_WRONLY;
	case MemoryMappedFile::ReadWrite:
		return O_RDWR;
	case MemoryMappedFile::ReadExecute:
		return O_RDONLY;
	case MemoryMappedFile::ReadWriteExecute:
		return O_RDWR;
	default:
		assert(false);
		return 0;
	}
}

static int FileModeToProt(enum MemoryMappedFile::FileMode mode)
{
	switch (mode)
	{
	case MemoryMappedFile::ReadOnly:
		return PROT_READ;
	case MemoryMappedFile::WriteOnly:
		return PROT_WRITE;
	case MemoryMappedFile::ReadWrite:
		return PROT_READ | PROT_WRITE;
	case MemoryMappedFile::ReadExecute:
		return PROT_READ | PROT_EXEC;
	case MemoryMappedFile::ReadWriteExecute:
		return PROT_READ | PROT_WRITE | PROT_EXEC;
	default:
		assert(false);
		return 0;
	}
}

MemoryMappedFile::MemoryMappedFile(const std::string &path, enum FileMode mode)
{
	_fd = open(path.c_str(), FileModeToOpenFlags(mode));
	if (_fd < 0) {
		throw FileException(errno, "Could not open file %s", path.c_str());
	}

	_size = lseek(_fd, 0, SEEK_END);
	lseek(_fd, 0, SEEK_SET);

	_data = mmap(nullptr, _size, FileModeToProt(mode), MAP_SHARED, _fd, 0);
	if(_data == nullptr)
	{
		close(_fd);
		throw FileException(errno, "Could not mmap file %s", path.c_str());
	}
}

MemoryMappedFile::~MemoryMappedFile()
{
	if(_data != nullptr)
	{
		munmap(_data, _size);
	}
	if(_fd >= 0)
	{
		close(_fd);
	}
}

void MemoryMappedFile::Flush() const
{
	msync(_data, _size, MS_SYNC | MS_INVALIDATE);
}

#endif

void* MemoryMappedFile::Load(size_t offset, size_t size) const
{
	if(size + offset > _size)
	{
		throw std::out_of_range("Outside file");
	}
	return reinterpret_cast<uint8_t *>(_data) + offset;
}
