#include <emuall/util/log.h>

#include <cassert>
#include <vector>
#include <cstdarg>
#ifdef _WIN32
#include <Windows.h>
#endif

static std::vector<std::function<void(enum logger::loglevel, const char *)>> _loggers;
static std::vector<char>_LogBuffer;
static enum logger::loglevel _loglevel;

void logger::InitLog()
{
	_loglevel = logger::Message;
	_LogBuffer.resize(1024);
	_loggers.clear();
}

void logger::AddLogger(std::function<void(enum logger::loglevel level, const char *msg)> func)
{
	assert(func);
	_loggers.push_back(func);
}

void logger::SetLogLevel(enum logger::loglevel level)
{
	_loglevel = level;
}

void logger::Log(enum logger::loglevel level, const char *fmtstr, va_list args)
{
	if (level <= _loglevel)
	{
		va_list args2;
		va_copy(args2, args);
		size_t len = vsnprintf(_LogBuffer.data(), _LogBuffer.size(), fmtstr, args);
		if (len > _LogBuffer.size()) {
			_LogBuffer.resize(len+1);
			vsnprintf(_LogBuffer.data(), _LogBuffer.size(), fmtstr, args2);
		}
		va_end(args2);
		if (_loggers.size() == 0) {
			fprintf(stderr, "%s", _LogBuffer.data());
		}
		else {
			for (auto it : _loggers)
			{
				it(level, _LogBuffer.data());
			}
		}
	}
}

enum logger::loglevel logger::GetLogLevel() {
	return _loglevel;
}
