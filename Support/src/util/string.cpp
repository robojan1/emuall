
#include <emuall/util/string.h>
#include <sstream>
#include <iomanip>
#include <cstring>
#include <cctype>

static const char hexChars[16] = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' };

const char *HexToString(unsigned int data, int minPrint)
{
	static char buffer[9] = { 0 };
	int i = 7;
	do
	{
		buffer[i] = hexChars[data & 0xF];
		data >>= 4;
		--i;
		--minPrint;
	} while ((data || minPrint > 0) && i >= 0);
	return &buffer[i + 1];
}

void HexToString(char *dst, unsigned int data, int minPrint /*= 0*/)
{
	std::ostringstream oss;
	oss << std::hex << std::setw(minPrint) << std::setfill('0') << data;
	strcpy(dst, oss.str().c_str());
}

void DecToString(char *dst, unsigned int data, int minPrint /*= 0*/)
{
	std::ostringstream oss;
	oss << std::setw(minPrint) << std::setfill('0') << data;
	strcpy(dst, oss.str().c_str());
}

int StringEndsWith(const char *str, const char *end)
{
	size_t strLen = strlen(str);
	size_t endLen = strlen(end);
	if(endLen > strLen) return 0;
	for(int i = 0; i < endLen; i++) {
		if(str[strLen - endLen + i] != end[i]) return 0;
	}
	return 1;
}

int StringCaseEndsWith(const char *str, const char *end)
{
	size_t strLen = strlen(str);
	size_t endLen = strlen(end);
	if(endLen > strLen) return 0;
	for(int i = 0; i < endLen; i++) {
		if(std::tolower(str[strLen - endLen + i]) != std::tolower(end[i])) return 0;
	}
	return 1;
}

std::vector<std::string> Tokenize(const std::string &str, const std::string &seperators) {
	std::vector<std::string> result;

	size_t pos = 0;
	size_t start_pos = 0;
	while ((pos = str.find_first_of(seperators, pos)) != std::string::npos) {
		result.push_back(str.substr(start_pos, pos - start_pos));
		pos++;
		start_pos = pos;
	}
	result.push_back(str.substr(start_pos));

	return std::move(result);
}

std::string HexEncode(const void *data, size_t size) {
	std::stringstream ss;
	auto ptr = static_cast<const uint8_t *>(data);
	ss << std::hex << std::setfill('0') << std::setw(2);
	for (int i = 0; i < size; i++) {
		ss << static_cast<int>(ptr[i]);
	}
	return ss.str();
}
