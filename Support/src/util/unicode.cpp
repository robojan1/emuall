#include <emuall/util/unicode.h>

std::u32string utf8_to_utf32(const std::string &in)
{
    std::u32string result;

    for(int i = 0; i < in.length(); i++) {
        if((in[i] & 0x80) == 0) {
            result += (static_cast<char32_t>(in[i]));
        } else {
            int length = 1;
            char x = in[i];
            while((x & (1 << (7-length))) != 0) {
                length++;
                if(length >= 5) goto malformed_symbol;
            }
            if(in.length()-i < length) goto malformed_symbol;
            for(int j = 1; j < length; j++)  {
                if((static_cast<unsigned char>(in[i+j]) >> 6) != 2) goto malformed_symbol;
            }
            switch(length) {
            case 2:
                result += static_cast<char32_t>(((x & 0x1F) << 6) | ((in[i+1] & 0x3F)));
                i+=1;
                break;
            case 3:
                result += static_cast<char32_t>(((x & 0xF) << 12) | ((in[i+1] & 0x3F) << 6) | (in[i+2] & 0x3F));
                i+=2;
                break;
            case 4:
                result += static_cast<char32_t>(((x & 0x7) << 18) | ((in[i+1] & 0x3F) << 12) | ((in[i+2] & 0x3F) << 6) | (in[i+3] & 0x3F));
                i+=3;
                break;
            default: goto malformed_symbol;
            }
malformed_symbol:
            continue;
        }
    }

    return result;
}

std::u16string utf8_to_utf16(const std::string &in)
{
	std::u16string result;

	for (int i = 0; i < in.length(); i++) {
		if ((in[i] & 0x80) == 0) {
			result += (static_cast<char16_t>(in[i]));
		}
		else {
			int length = 1;
			char x = in[i];
			while ((x & (1 << (7 - length))) != 0) {
				length++;
				if (length >= 5) goto malformed_symbol;
			}
			if (in.length() - i < length) goto malformed_symbol;
			for (int j = 1; j < length; j++) {
				if ((static_cast<unsigned char>(in[i + j]) >> 6) != 2) goto malformed_symbol;
			}
			switch (length) {
			case 2:
				result += static_cast<char16_t>(((x & 0x1F) << 6) | ((in[i + 1] & 0x3F)));
				i += 1;
				break;
			case 3:
				result += static_cast<char16_t>(((x & 0xF) << 12) | ((in[i + 1] & 0x3F) << 6) | (in[i + 2] & 0x3F));
				i += 2;
				break;
			case 4: {
				char32_t c = static_cast<char32_t>(((x & 0x7) << 18) | ((in[i + 1] & 0x3F) << 12) | ((in[i + 2] & 0x3F) << 6) | (in[i + 3] & 0x3F));
				if(c >= 0x10000)
				{
					result += static_cast<char16_t>(0xD800 | ((c >> 10) & 0x3FF));
					result += static_cast<char16_t>(0xDC00 | ((c >> 0) & 0x3FF));
				} else
				{
					result += static_cast<char16_t>(c);
				}
				i += 3;
				break;
			}
			default: goto malformed_symbol;
			}
		malformed_symbol:
			continue;
		}
	}

	return result;
}