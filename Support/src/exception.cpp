
#include <emuall/exception.h>
#include <sstream>
#include <iomanip>
#include <cstdarg>

#ifdef _MSC_VER
#include <windows.h>
#include <DbgHelp.h>
#else
#include <execinfo.h>
#include <cstring>

#endif

BaseException::BaseException(const char *format, ...) :
	_msg(nullptr), _stacktrace(nullptr)
{
	va_list args;
	va_start(args, format);
	CreateMsg(format, args);
	va_end(args);
	CreateStacktrace();
}

BaseException::BaseException(const char *format, va_list args) :
	_msg(nullptr), _stacktrace(nullptr)
{
	CreateMsg(format, args);
	CreateStacktrace();
}

BaseException::BaseException(const BaseException &other)
{
	_msg = new std::string(*other._msg);
	char *stacktrace = new char[strlen(other._stacktrace) + 1];
	::strcpy(stacktrace, other._stacktrace);
	_stacktrace = stacktrace;
}

BaseException & BaseException::operator=(const BaseException &other)
{
	_msg = new std::string(*other._msg);
	char *stacktrace = new char[strlen(other._stacktrace) + 1];
	::strcpy(stacktrace, other._stacktrace);
	return *this;
}

BaseException::~BaseException()
{
	if (_msg != nullptr) delete _msg;
	if (_stacktrace != nullptr) delete[] _stacktrace;
}

const char * BaseException::GetMsg() const
{
	return _msg->c_str();
}

const char * BaseException::GetStacktrace() const
{
	return _stacktrace != nullptr ? _stacktrace : "(null)";
}

#define MAX_STACK_FRAMES 60
#define MAX_FUNCTION_NAME_LENGTH 256

void BaseException::CreateStacktrace()
{
	std::stringstream ss;
#ifdef _MSC_VER
	void *stack[60];
	USHORT frames = CaptureStackBackTrace(3, MAX_STACK_FRAMES, stack, NULL);
	HANDLE process = GetCurrentProcess();
	if (SymInitialize(process, NULL, TRUE) == FALSE) {
		_stacktrace = "Could not generate stack trace";
		return;
	}
	size_t symbolLength = sizeof(SYMBOL_INFO) + (MAX_FUNCTION_NAME_LENGTH - 1) * sizeof(TCHAR);
	SYMBOL_INFO *symbol = (SYMBOL_INFO *)malloc(symbolLength);
	ZeroMemory(symbol, symbolLength);
	symbol->MaxNameLen = MAX_FUNCTION_NAME_LENGTH;
	symbol->SizeOfStruct = sizeof(SYMBOL_INFO);
	DWORD displacement;
	IMAGEHLP_LINE64 line;
	ZeroMemory(&line, sizeof(IMAGEHLP_LINE64));
	line.SizeOfStruct = sizeof(IMAGEHLP_LINE64);
	for (int i = 0; i < frames; ++i) {
		DWORD64 address = (DWORD64)(stack[i]);
		SymFromAddr(process, address, NULL, symbol);
		if (SymGetLineFromAddr64(process, address, &displacement, &line)) {
			ss << "\tat " << symbol->Name;
			ss << " in " << line.FileName;
			ss << ":" << line.LineNumber; 
			ss << "(address: 0x";
			ss << std::setfill('0') << std::setw(sizeof(ULONG64) * 2) << std::hex;
			ss << symbol->Address << std::dec << ")" << std::endl;
		}
		else {
			ss << "\tat " << symbol->Name << "(address: 0x";
			ss << std::setfill('0') << std::setw(sizeof(ULONG64) * 2) << std::hex;
			ss << symbol->Address << ")" << std::endl;
		}
	}
	SymCleanup(process);
	free(symbol);

	std::string what_str = ss.str();
	auto *what = new char[what_str.length()+1];

	::strcpy(what, what_str.c_str());
	_stacktrace = what;
#else
	void *backtraceAddr[64];
	char **backtraceSymbols;
	int numBacktrace;
	va_list args_count;

	numBacktrace = backtrace(backtraceAddr, 64);
	backtraceSymbols = backtrace_symbols(backtraceAddr, numBacktrace);

	for(int i = 1; i < numBacktrace; i++) {
		ss << backtraceSymbols[i] << std::endl;
	}
	free(backtraceSymbols);

	std::string what_str = ss.str();
	auto *what = new char[what_str.length()+1];

	::strcpy(what, what_str.c_str());
	_stacktrace = what;
#endif
}

void BaseException::CreateMsg(const char *format, va_list args)
{
	va_list args2;
	va_copy(args2, args);
	int length = vsnprintf(nullptr, 0, format, args2);
	va_end(args2);

	char *buffer = new char[length+1];
	vsnprintf(buffer, length + 1, format, args);
	_msg = new std::string(buffer);
	delete[] buffer;
}

