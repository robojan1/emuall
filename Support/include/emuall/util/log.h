#ifndef EMUALL_UTIL_LOG_H
#define EMUALL_UTIL_LOG_H

#include <cstdarg>
#include <functional>
#include <emuall/common.h>

namespace logger {
	enum loglevel
	{
		Fatal,
		Error,
		Warn,
		Message,
		Debug
	};

	void DLLEXPORT InitLog();
	void DLLEXPORT AddLogger(std::function<void(enum loglevel, const char *msg)> func);
	void DLLEXPORT SetLogLevel(enum loglevel);
	enum loglevel DLLEXPORT GetLogLevel();
	void DLLEXPORT Log(enum loglevel level, const char *fmtstr, va_list args);
	inline void Log(enum loglevel level, const char *fmtstr, ...) {
		va_list args;
		va_start(args, fmtstr);
		logger::Log(level, fmtstr, args);
		va_end(args);
	}
};



#endif
