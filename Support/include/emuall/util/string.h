#pragma once

#include <emuall/common.h>
#include <ctype.h>
#include <cassert>
#include <string>
#include <vector>

DLLEXPORT const char * HexToString(unsigned int data, int minPrint = 0);
DLLEXPORT void HexToString(char *dst, unsigned int data, int minPrint = 0);
DLLEXPORT void DecToString(char *dst, unsigned int data, int minPrint = 0);
DLLEXPORT int StringEndsWith(const char *str, const char *end);
DLLEXPORT int StringCaseEndsWith(const char *str, const char *end);

inline int xdigitToNumber(int digit) {
	return isdigit(digit) ? digit - '0' : tolower(digit) - 'a' + 10;
}

inline char NumberToXDigit(int c) {
	assert(c >= 0 && c < 16);
	return "0123456789abcdef"[c];
}

std::vector<std::string> DLLEXPORT Tokenize(const std::string &str, const std::string &seperators);

constexpr unsigned long hashStr(const char *str)
{
	unsigned long result = 0x01234567;
	char c = 0;
	while ((c = *str) != '\0') {
		result = ((((result >> 13) | (result << 19)) + result) ^ 0xAAA31234) + c;
		str++;
	}
	return result;
}

std::string DLLEXPORT HexEncode(const void *data, size_t size);
