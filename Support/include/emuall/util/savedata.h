#ifndef EMUALL_UTIL_SAVEDATA_H
#define EMUALL_UTIL_SAVEDATA_H

#include <stdint.h>
#include <vector>
#include <string>
#include "emuall/common.h"

namespace emuall {

	class DLLEXPORT SaveData {
	public:
		explicit SaveData(int version = 0);
		explicit SaveData(const std::vector<uint8_t> &data);
		SaveData(const uint8_t *data, size_t dataLen);
		virtual ~SaveData();

		SaveData &operator<<(uint8_t x);
		SaveData &operator<<(uint16_t x);
		SaveData &operator<<(uint32_t x);
		SaveData &operator<<(uint64_t x);
		SaveData &operator<<(int8_t x);
		SaveData &operator<<(int16_t x);
		SaveData &operator<<(int32_t x);
		SaveData &operator<<(int64_t x);
		SaveData &operator<<(float x);
		SaveData &operator<<(double x);
		SaveData &operator<<(bool x);
		SaveData &operator<<(const std::string &x);
		SaveData &operator<<(SaveData &x);

		SaveData &StoreBuffer(const void *src, size_t size);

		SaveData &operator>>(uint8_t &x);
		SaveData &operator>>(uint16_t &x);
		SaveData &operator>>(uint32_t &x);
		SaveData &operator>>(uint64_t &x);
		SaveData &operator>>(int8_t &x);
		SaveData &operator>>(int16_t &x);
		SaveData &operator>>(int32_t &x);
		SaveData &operator>>(int64_t &x);
		SaveData &operator>>(float &x);
		SaveData &operator>>(double &x);
		SaveData &operator>>(bool &x);
		SaveData &operator>>(std::string &x);
		SaveData &operator>>(SaveData &x);

		std::vector<uint8_t> GetBuffer();
		SaveData GetChild();

		template<typename T>
		T Get(const T &defaultValue = T(), int minimumVersion = 0) {
			if (_version < minimumVersion) return defaultValue;
			T result; *this >> result;
			return result;
		}

		const std::vector<uint8_t> &GetData();
		int Version() const { return _version; }

	private:
		std::vector<uint8_t> _data;
		int _version;
		uint32_t _pos;

		void Parse();
	};
}

#endif