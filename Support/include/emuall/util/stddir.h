#ifndef UTIL_STDDIR_H
#define UTIL_STDDIR_H

#include <string>
#include <emuall/common.h>

namespace emuall
{
	std::string DLLEXPORT GetExecuatableDir();
	std::string DLLEXPORT GetConfigDir();
	std::string DLLEXPORT GetSavesDir();
	std::string DLLEXPORT GetPluginsDir();
	std::string DLLEXPORT GetSaveStatesDir();
}

#endif