#ifndef EMUALL_UTIL_UNICODE_H
#define EMUALL_UTIL_UNICODE_H

#include <string>

std::u32string utf8_to_utf32(const std::string &in);
std::u16string utf8_to_utf16(const std::string &in);

#endif