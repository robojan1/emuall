#include "file.h"
#ifdef _WIN32
#define NOMINMAX
#include <Windows.h>
#endif

class DLLEXPORT MemoryMappedFile
{
public:
	enum FileMode {
		ReadOnly,
		WriteOnly,
		ReadWrite,
		ReadExecute,
		ReadWriteExecute
	};
	MemoryMappedFile(const std::string &path, enum FileMode mode);
	~MemoryMappedFile();

	void *Load(size_t offset, size_t size) const;
	void Flush() const;

	size_t Size() const { return _size; }
	
private:
#ifdef _WIN32
	HANDLE _fileHdl;
	HANDLE _mapHdl;
#else
	int _fd;
#endif
	void *_data;
	size_t _size;

};