#ifndef EXCEPTION_H_
#define EXCEPTION_H_

#include <emuall/common.h>
#include <string>

class DLLEXPORT BaseException {
public:
	explicit BaseException(const char *format, ...);
	BaseException(const char *format, va_list args);
	BaseException(const BaseException &other);

	BaseException &operator=(const BaseException &other);

	virtual ~BaseException();

	virtual const char *GetMsg() const;
	virtual const char *GetStacktrace() const;

protected:
	void CreateStacktrace();
	void CreateMsg(const char *format, va_list args);

	std::string *_msg;
	const char *_stacktrace;
};

#endif
