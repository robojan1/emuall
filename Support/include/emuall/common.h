#ifndef EMUALL_COMMON_H_
#define EMUALL_COMMON_H_

#ifdef _WIN32
#ifdef EMULIBRARYDLL
#	define DLLEXPORT __declspec(dllexport)
#else
#	define DLLEXPORT __declspec(dllimport)
#endif
#else
#define DLLEXPORT
#define __stdcall
#endif

#endif
