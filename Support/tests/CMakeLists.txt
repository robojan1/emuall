find_package(GTest REQUIRED)

add_executable(SupportLibTests supportTests.cpp savedata.cpp ../src/util/unicode.cpp ../src/util/savedata.cpp)

target_link_libraries(SupportLibTests PRIVATE GTest::gtest GTest::gtest_main GTest::gmock support)