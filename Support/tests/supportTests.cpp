#include <gtest/gtest.h>
#include <emuall/util/endian.h>
#include <emuall/util/unicode.h>

TEST(EndianTest, testEndianSwap) {
	uint32_t f = 0x01234567u;
	uint64_t d = 0x0123456789abcdefu;
	uint32_t f_swap = 0x67452301u;
	uint64_t d_swap = 0xefcdab8967452301u;
	Endian endian(Endian::isHostLittleEndian());
	EXPECT_EQ(0x2301, endian.convi16(0x0123));
	EXPECT_EQ(0x2301u, endian.convu16(0x0123u));
	EXPECT_EQ(0x67452301, endian.convi32(0x01234567));
	EXPECT_EQ(0x67452301u, endian.convu32(0x01234567u));
	EXPECT_EQ(0xefcdab8967452301, endian.convi64(0x0123456789abcdef));
	EXPECT_EQ(0xefcdab8967452301u, endian.convu64(0x0123456789abcdefu));
	EXPECT_EQ(reinterpret_cast<float&>(f_swap), endian.convf(reinterpret_cast<float&>(f)));
	EXPECT_EQ(reinterpret_cast<double&>(d_swap), endian.convd(reinterpret_cast<double&>(d)));
}

TEST(EndianTest, testEndianNoSwap) {
	uint32_t f = 0x01234567;
	uint64_t d = 0x0123456789abcdef;
	Endian endian(!Endian::isHostLittleEndian());
	EXPECT_EQ(0x0123, endian.convi16(0x0123));
	EXPECT_EQ(0x0123u, endian.convu16(0x0123u));
	EXPECT_EQ(0x01234567, endian.convi32(0x01234567));
	EXPECT_EQ(0x01234567u, endian.convu32(0x01234567u));
	EXPECT_EQ(0x0123456789abcdef, endian.convi64(0x0123456789abcdef));
	EXPECT_EQ(0x0123456789abcdefu, endian.convu64(0x0123456789abcdefu));
	EXPECT_EQ(reinterpret_cast<float&>(f), endian.convf(reinterpret_cast<float&>(f)));
	EXPECT_EQ(reinterpret_cast<double&>(d), endian.convd(reinterpret_cast<double&>(d)));
}

struct UnicodeState
{
	std::string input;
	std::u32string output;
};

struct UnicodeTest : testing::Test, testing::WithParamInterface<UnicodeState>
{
};

TEST_P(UnicodeTest, utf8ToUtf32) {
	auto as = GetParam();
	auto result = utf8_to_utf32(as.input);
	EXPECT_EQ(result, as.output);
}

INSTANTIATE_TEST_CASE_P(Default, UnicodeTest,
	testing::Values(
	UnicodeState{u8"Hello world", U"Hello world"},
	UnicodeState{u8"ÑÔ÷ɞɿքפ", U"ÑÔ÷ɞɿքפ"},
	UnicodeState{u8"一些文字", U"一些文字"},
	UnicodeState{u8"😈😍😎😏😐😑😒😓😔", U"😈😍😎😏😐😑😒😓😔"},
	UnicodeState{u8"ÑÔ÷ɞɿքפ一些文字😈😍😎😏😐😑😒😓😔", U"ÑÔ÷ɞɿքפ一些文字😈😍😎😏😐😑😒😓😔"}
	));
