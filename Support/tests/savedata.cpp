#include <gtest/gtest.h>
#include <emuall/util/savedata.h>
#include <array>

using namespace emuall;

TEST(SaveData, EmptySize)
{
	SaveData data;
	ASSERT_TRUE(data.GetData().size() == 8);
}

TEST(SaveData, DefaultVersion)
{
	SaveData data;
	ASSERT_TRUE(data.Version() == 0);
}

template <typename T>
class SaveDataTypeTest : public ::testing::Test {
public:
};

using SaveDataBasicTypes = ::testing::Types<uint8_t, uint16_t, uint32_t, uint64_t, int8_t, int16_t, int32_t, int64_t, float, double>;
TYPED_TEST_CASE(SaveDataTypeTest, SaveDataBasicTypes);

TYPED_TEST(SaveDataTypeTest, SaveDataTypeSizes) {
	SaveData data;
	size_t startSize = data.GetData().size();
	data << static_cast<TypeParam>(123);
	size_t endSize = data.GetData().size();
	ASSERT_TRUE(endSize - startSize == sizeof(TypeParam));
}

TYPED_TEST(SaveDataTypeTest, CorrectParsing)
{
	std::array<TypeParam, 7> values = {
	    static_cast<TypeParam>(0),
	    static_cast<TypeParam>(32),
	    static_cast<TypeParam>(-32),
	    static_cast<TypeParam>(128),
	    static_cast<TypeParam>(-128),
	    static_cast<TypeParam>(1000),
	    static_cast<TypeParam>(-1000)
	};
	for (auto value : values) {
		SaveData generator;
		generator << static_cast<TypeParam>(value);
		auto data = generator.GetData();
		SaveData parser(data);
		auto result = parser.Get<TypeParam>();
		ASSERT_TRUE(result == value);
	}
}

TEST(SaveData, ComplexStructure) {
	SaveData generator;
	generator << static_cast<uint8_t>(0);
	generator << static_cast<uint16_t>(1);
	generator << static_cast<uint32_t>(2);
	generator << static_cast<uint64_t>(3);
	generator << static_cast<int8_t>(4);
	generator << static_cast<int16_t>(5);
	generator << static_cast<int32_t>(6);
	generator << static_cast<int64_t>(7);
	generator << static_cast<float>(8);
	generator << static_cast<double>(9);
	generator << "Hello world";
	SaveData gen2;
	gen2 << "Child" << static_cast<uint32_t>(124);
	generator << gen2;

	auto data = generator.GetData();
	SaveData parser(data);
	ASSERT_EQ(parser.Get<uint8_t>(), 0);
	ASSERT_EQ(parser.Get<uint16_t>(), 1);
	ASSERT_EQ(parser.Get<uint32_t>(), 2);
	ASSERT_EQ(parser.Get<uint64_t>(), 3);
	ASSERT_EQ(parser.Get<int8_t>(), 4);
	ASSERT_EQ(parser.Get<int16_t>(), 5);
	ASSERT_EQ(parser.Get<int32_t>(), 6);
	ASSERT_EQ(parser.Get<int64_t>(), 7);
	ASSERT_FLOAT_EQ(parser.Get<float>(), 8);
	ASSERT_DOUBLE_EQ(parser.Get<double>(), 9);
	ASSERT_EQ(parser.Get<std::string>(), "Hello world");
	SaveData child = parser.Get<SaveData>();
	ASSERT_EQ(child.Get<std::string>(), "Child");
	ASSERT_EQ(child.Get<uint32_t>(), 124);
}

TEST(SaveData, ComplexStructureStream) {
	SaveData generator;
	generator << static_cast<uint8_t>(0);
	generator << static_cast<uint16_t>(1);
	generator << static_cast<uint32_t>(2);
	generator << static_cast<uint64_t>(3);
	generator << static_cast<int8_t>(4);
	generator << static_cast<int16_t>(5);
	generator << static_cast<int32_t>(6);
	generator << static_cast<int64_t>(7);
	generator << static_cast<float>(8);
	generator << static_cast<double>(9);
	generator << "Hello world";
	SaveData gen2;
	gen2 << "Child" << static_cast<uint32_t>(124);
	generator << gen2;

	auto data = generator.GetData();
	SaveData parser(data);
	uint8_t u8; parser >> u8;
	uint16_t u16; parser >> u16;
	uint32_t u32; parser >> u32;
	uint64_t u64; parser >> u64;
	int8_t i8; parser >> i8;
	int16_t i16; parser >> i16;
	int32_t i32; parser >> i32;
	int64_t i64; parser >> i64;
	float f; parser >> f;
	double d; parser >> d;
	std::string s1; parser >> s1;
	SaveData child; parser >> child;
	std::string s2; child >> s2;
	uint32_t u32_2; child >> u32_2;
	ASSERT_EQ(u8, 0);
	ASSERT_EQ(u16, 1);
	ASSERT_EQ(u32, 2);
	ASSERT_EQ(u64, 3);
	ASSERT_EQ(i8, 4);
	ASSERT_EQ(i16, 5);
	ASSERT_EQ(i32, 6);
	ASSERT_EQ(i64, 7);
	ASSERT_FLOAT_EQ(f, 8);
	ASSERT_DOUBLE_EQ(d, 9);
	ASSERT_EQ(s1, "Hello world");
	ASSERT_EQ(s2, "Child");
	ASSERT_EQ(u32_2, 124);
}
