
#include <wx/wx.h>
#include <wx/evtloop.h>
#include <wx/scopedptr.h>
#include <wx/stdpaths.h>
#include <wx/cmdline.h>
#include "gui/MainFrame.h"
#include "emuAll.h"
#include "util/memDbg.h"

#include <emuall/emulator/EmulatorList.h>
#include <emuall/emulator/EmulatorInterface.h>
#include <emuall/util/log.h>

#include <emuall/exception.h>

IMPLEMENT_APP(EmuAll);

MainFrame *EmuAll::_mainFrame = nullptr;

bool EmuAll::OnInit()
{
	if (_mainFrame != nullptr) {
		delete _mainFrame;
		_mainFrame = nullptr;
	}
	// Do some general setup
	wxStandardPaths::Get().SetFileLayout(wxStandardPaths::FileLayout_XDG);

	// Initialize logging
	logger::InitLog();
	logger::SetLogLevel(logger::Message);
	logger::AddLogger([](enum logger::loglevel level, const char *str) {
#ifdef _WIN32
		OutputDebugStringA(str);
		OutputDebugStringA("\n");
#else
		switch(level) {
			case logger::Fatal:
				fprintf(stderr, "[Fatal]   %s\n", str);
				fflush(stderr);
				break;
			case logger::Error:
				fprintf(stderr, "[Error]   %s\n", str);
				fflush(stderr);
				break;
			case logger::Warn:
				fprintf(stderr, "[Warning] %s\n", str);
				fflush(stderr);
				break;
			case logger::Message:
				fprintf(stdout, "[Message] %s\n", str);
				fflush(stdout);
				break;
			case logger::Debug:
				fprintf(stdout, "[Debug]   %s\n", str);
				fflush(stdout);
				break;
		}
#endif
	});

	// Parse the command line options
	if(!wxApp::OnInit()) return false;

	// Start the program
	_mainFrame = new MainFrame(_("EmuAll"), wxDefaultPosition, wxSize(400,300));
	SetTopWindow(_mainFrame);
	SetAppDisplayName(_("EmuAll"));
	_mainFrame->Show(true);

	_mainFrame->Bind(wxEVT_SHOW, &EmuAll::OnTopShown, this);

	// Set timer resolution
#ifdef _WIN32
	timeBeginPeriod(1000);
#endif

	return true;
}

int EmuAll::OnExit()
{
	// Application wide destructor code
#ifdef _WIN32
	timeEndPeriod(1000);
#endif

	return 0;
}

MainFrame * EmuAll::GetMainFrame()
{
	return _mainFrame;
}

bool EmuAll::OnExceptionInMainLoop() {
	logger::Log(logger::Error, "Unhandled exception");
	return false;
}

bool EmuAll::OnCmdLineParsed(wxCmdLineParser &parser) {
	if(parser.FoundSwitch("h")) {
		parser.Usage();
		return false;
	}
	if(parser.FoundSwitch("v")) {
		logger::SetLogLevel(logger::Debug);
	}
	if(parser.FoundSwitch("L")) {
		ListEmulators();
		return false;
	}
	if(parser.GetParamCount() > 0) {
		_imageFile = parser.GetParam(0);
	}
	if(parser.Found("e", &_emulatorId)) {
		if(!EmulatorList::Get().Contains(_emulatorId.ToStdString())) {
			logger::Log(logger::Error, "Emulator \"%s\" is not an valid emulator. Use list-emulators for valid options.",
					_emulatorId.ToStdString().c_str());
			return false;
		}
	}
	return true;
}

void EmuAll::OnInitCmdLine(wxCmdLineParser &parser) {
	parser.SetSwitchChars("-");
	parser.AddSwitch("L", "list-emulators", wxT("List all the available emulators"));
	parser.AddSwitch("v", "verbose", wxT("Enable verbose logging"));
	parser.AddSwitch("h", "help", wxT("Show this help message"));
	parser.AddOption("e", "emulator", wxT("Specify which emulator to load"), wxCMD_LINE_VAL_STRING);
	parser.AddParam("image", wxCMD_LINE_VAL_STRING, wxCMD_LINE_PARAM_OPTIONAL);

	parser.SetLogo(wxT("Emuall is an emulator interface. It can emulator multiple systems."));
}

void EmuAll::ListEmulators() {
	auto &emulators = EmulatorList::Get();
	logger::Log(logger::Message, "Supported emulators:");
	for(auto emulator : emulators) {
		logger::Log(logger::Message, "%20s - %s", emulator->GetInfo().id.c_str(), emulator->GetName().c_str());
	}
}

void EmuAll::OnTopShown(wxShowEvent &evt) {
	if(evt.IsShown()) {
		if(!_imageFile.empty()) {
			_mainFrame->LoadEmulator(_imageFile.ToStdString(), _emulatorId.ToStdString());
		}
	}
	_mainFrame->Unbind(wxEVT_SHOW, &EmuAll::OnTopShown, this);
}

