#ifndef AUDIOEXCEPTION_H_
#define AUDIOEXCEPTION_H_

#include <emuall/exception.h>

class AudioException : public BaseException
{
public:
	AudioException(unsigned int error, bool al = true);
	AudioException(unsigned int error, bool al, const char *format, ...);
	AudioException(unsigned int error, bool al, const char *format, va_list args);
	AudioException(const AudioException &other);

	virtual ~AudioException();

	AudioException &operator=(const AudioException &other);

	unsigned int GetErrorCode() const;
	bool IsALError() const;

	static const char *GetALCErrorMsg(unsigned int error);
	static const char *GetALErrorMsg(unsigned int error);
private:
	unsigned int _error;
	bool _alError;
};

#endif
