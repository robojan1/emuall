#include "memDbg.h"

#include <string>
#include <cassert>
#include <wx/fileconf.h>
#include <wx/stdpaths.h>
#include <wx/app.h>
#include "Options.h"
#include <emuall/util/log.h>
#include <emuall/emulator/EmulatorList.h>


Options::Options()
{
	videoOptions.keepAspect = DEFAULT_KEEPASPECT;
	videoOptions.filter = DEFAULT_VIDEOFILTER;
	videoOptions.messageFontSize = 16;
	audioOptions.sampleRate = DEFAULT_SAMPLERATE;
	audioOptions.bufferSize = DEFAULT_BUFFERSIZE;
	audioOptions.numBuffers = DEFAULT_NUMAUDIOBUFFERS;

}

Options Options::_instance;

Options::~Options()
{

}

Options & Options::GetSingleton()
{
	return _instance;
}

void Options::LoadOptions()
{
	wxString appName = wxApp::GetInstance()->GetAppDisplayName();
	wxFileConfig mFileConfig(appName, wxEmptyString, GetConfigFileName(), wxEmptyString,
		wxCONFIG_USE_SUBDIR | wxCONFIG_USE_LOCAL_FILE);


	logger::Log(logger::Message, "Config file: '%s'", GetConfigFileName().c_str());

	// Read Audio
	audioOptions.sampleRate = mFileConfig.ReadLong("Audio/SampleRate", DEFAULT_SAMPLERATE);
	audioOptions.bufferSize = mFileConfig.ReadLong("Audio/BufferSize", DEFAULT_BUFFERSIZE);
	audioOptions.numBuffers = mFileConfig.ReadLong("Audio/NumBuffers", DEFAULT_NUMAUDIOBUFFERS);

	// Read Video
	videoOptions.keepAspect = mFileConfig.ReadBool("Video/KeepAspectRatio", DEFAULT_KEEPASPECT);
	videoOptions.filter = mFileConfig.ReadLong("Video/Filter", DEFAULT_VIDEOFILTER);

	wxString defaultMessageFont;
#ifdef _WIN32
	wxGetEnv("WINDIR", &defaultMessageFont);
	defaultMessageFont.append(wxFileName::GetPathSeparator());
	defaultMessageFont.append("Fonts");
	defaultMessageFont.append(wxFileName::GetPathSeparator());
	defaultMessageFont.append("georgia.ttf");
#elif defined(__APPLE__)
	defaultMessageFont = "/System/Library/Fonts/georgia.ttf";
#elif defined(__linux__) || defined(__unix__)
	defaultMessageFont = "/usr/share/fonts/truetype/dejavu/DejaVuSans.ttf";
#endif
	videoOptions.messageFont = mFileConfig.Read("Video/MessageFont", defaultMessageFont);
	videoOptions.messageFontIdx = mFileConfig.ReadLong("Video/MessageFontIdx", 0);
	videoOptions.messageFontSize = mFileConfig.ReadLong("Video/MessageFontSize", 16);

	// Recent files
	_recentFiles.clear();
	for(int idx = 0; idx < MAX_RECENT_FILES; idx++) {
		wxString fileKeyPath = wxString::Format("RecentFiles/file%d", idx);
		wxString emuKeyPath = wxString::Format("RecentFiles/emu%d", idx);
		wxString file = mFileConfig.Read(fileKeyPath, "");
		wxString emu = mFileConfig.Read(emuKeyPath, "");
		_recentFiles.emplace_back(std::make_pair(file.ToStdString(), emu.ToStdString()));
	}

	// Key bindings are loaded when the keys are added

	// DInput bindings
	dinputMap.clear();
	for (int i = 0; i < 255; i++) {
		wxString elementId = wxString::Format(_("DInput/device%d"), i);
		wxString guidString;
		if (mFileConfig.Read(elementId, &guidString)) {
			dinputMap[guidString.ToStdString()] = i;
		}
	}

	auto &emulators = EmulatorList::Get();
	_emulatorOptions.clear();
	for(auto emu : emulators) {
		auto emuOptions = emu->GetEmulatorOptions();
		auto emuId = emu->GetInfo().id;
		for(auto &emuOption : emuOptions) {
			wxString optionKey = wxString::Format("EmuOption.%s/%d", emuId, emuOption.id);
			EmulatorOptionValue value;
			value.type = emuOption.type;
			switch(emuOption.type) {
				case EmulatorOption_t::Boolean: {
					bool valBool;
					mFileConfig.Read(optionKey, &valBool, emuOption.defaultValueInt != 0);
					value.valInt = valBool ? 1 : 0;
					break;
				}
				case EmulatorOption_t::File:
				case EmulatorOption_t::Path: {
					wxString valStr;
					mFileConfig.Read(optionKey, &valStr, emuOption.defaultValueString);
					value.valStr = valStr;
					break;
				}
				case EmulatorOption_t::Integer:
				case EmulatorOption_t::Enumeration:
					mFileConfig.Read(optionKey, &value.valInt, emuOption.defaultValueInt);
					break;
				case EmulatorOption_t::Float:
					mFileConfig.Read(optionKey, &value.valFloat, emuOption.defaultValueFloat);
					break;
			}
			_emulatorOptions[emuId][emuOption.id] = std::move(value);
		}
	}
}

void Options::SaveOptions()
{
	wxString appName = wxApp::GetInstance()->GetAppDisplayName();
	wxFileConfig mFileConfig(appName, wxEmptyString, GetConfigFileName(), wxEmptyString,
		wxCONFIG_USE_SUBDIR | wxCONFIG_USE_LOCAL_FILE);

	// Read Audio
	mFileConfig.Write("Audio/SampleRate", audioOptions.sampleRate);
	mFileConfig.Write("Audio/BufferSize", audioOptions.bufferSize);
	mFileConfig.Write("Audio/NumBuffers", audioOptions.numBuffers);

	// Read Video
	mFileConfig.Write("Video/KeepAspectRatio", videoOptions.keepAspect);
	mFileConfig.Write("Video/Filter", videoOptions.filter);
	mFileConfig.Write("Video/MessageFont", wxString(videoOptions.messageFont));
	mFileConfig.Write("Video/MessageFontIdx", videoOptions.messageFontIdx);
	mFileConfig.Write("Video/MessageFontSize", videoOptions.messageFontSize);

	// Recent files
	int idx = 0;
	for(auto file : _recentFiles)
	{
		mFileConfig.Write(wxString::Format("RecentFiles/file%d", idx), wxString(file.first));
		mFileConfig.Write(wxString::Format("RecentFiles/emu%d", idx), wxString(file.second));
		idx++;
	}

	// Key bindings
	for (auto &emu : _keyBindings) {
		wxString groupBase = wxString::Format(_("KeyBindings.%s"), emu.first);
		for (auto &keybinding : emu.second) {
			wxString group = groupBase;
			mFileConfig.Write(wxString::Format("%s/%d.primary", groupBase, keybinding.id), keybinding.primaryKey);
			mFileConfig.Write(wxString::Format("%s/%d.secondary", groupBase, keybinding.id), keybinding.secondaryKey);
		}
	}

	// DInput bindings
	for (auto &map : dinputMap) {
		wxString elementId = wxString::Format(_("DInput/device%d"), map.second);
		mFileConfig.Write(elementId, wxString(map.first));
	}

	// Emulator options
	for(auto &emu : _emulatorOptions) {
		for(auto &emuOption : emu.second) {
			wxString optionKey = wxString::Format("EmuOption.%s/%d", emu.first, emuOption.first);
			switch(emuOption.second.type) {
				case EmulatorOption_t::Boolean:
					mFileConfig.Write(optionKey, emuOption.second.valInt != 0);
					break;
				case EmulatorOption_t::File:
				case EmulatorOption_t::Path:
					mFileConfig.Write(optionKey, wxString(emuOption.second.valStr));
					break;
				case EmulatorOption_t::Integer:
				case EmulatorOption_t::Enumeration:
					mFileConfig.Write(optionKey, emuOption.second.valInt);
					break;
				case EmulatorOption_t::Float:
					mFileConfig.Write(optionKey, emuOption.second.valFloat);
					break;
			}

		}
	}

	// Create directory
	wxFileName configFileName(GetConfigFileName());
	configFileName.Mkdir(wxS_DIR_DEFAULT, wxPATH_MKDIR_FULL);
	mFileConfig.Flush();
}


void Options::LoadKeyBindings(const std::string &name, const std::vector<EmulatorInput_t> &bindings)
{
	wxString appName = wxApp::GetInstance()->GetAppDisplayName();
	wxFileConfig mFileConfig(appName, wxEmptyString, GetConfigFileName(), wxEmptyString,
		wxCONFIG_USE_SUBDIR | wxCONFIG_USE_LOCAL_FILE);

	_keyBindings[name].clear();
	wxString groupBase = "KeyBindings.";
	groupBase.Append(name);
	for (auto &binding : bindings) {
		EmulatorInput_t input = binding;
		input.primaryKey = mFileConfig.ReadLong(wxString::Format("%s/%d.primary", groupBase, binding.id), binding.primaryKey);
		input.secondaryKey = mFileConfig.ReadLong(wxString::Format("%s/%d.secondary", groupBase, binding.id), binding.secondaryKey);
		_keyBindings[name].push_back(input);
	}
}

std::string Options::GetConfigDir() const {
	return (wxStandardPaths::Get().GetUserConfigDir() + "/" + wxApp::GetInstance()->GetAppName()).ToStdString();
}


std::string Options::GetConfigFileName() const
{
	auto appName = wxApp::GetInstance()->GetAppName().ToStdString();

	return GetConfigDir() + "/" + appName + ".conf";
}

void Options::SaveRecentFile(const std::string &file, const std::string &emulatorId)
{
	auto newPair = std::make_pair(file, emulatorId);
	_recentFiles.remove_if([newPair](auto &x) {return x == newPair; });
	_recentFiles.push_front(newPair);
	while(_recentFiles.size() > MAX_RECENT_FILES) {
		_recentFiles.pop_back();
	}
}

std::string Options::GetRecentFilePath(int idx) {
	assert(idx >= 0 && idx < MAX_RECENT_FILES);
	if(idx >= _recentFiles.size()) return "";
	auto it = _recentFiles.cbegin();
	std::advance(it, idx);
	return it->first;
}

std::string Options::GetRecentFileEmulator(int idx) {
	assert(idx >= 0 && idx < MAX_RECENT_FILES);
	if(idx >= _recentFiles.size()) return "";
	auto it = _recentFiles.cbegin();
	std::advance(it, idx);
	return it->second;
}

void Options::RebindKey(std::string name, int id, int keycode, int idx)
{
	auto bindings = _keyBindings.find(name);
	if (bindings == _keyBindings.end()) {
		return;
	}

	for (auto binding : bindings->second) {
		if (binding.id == id) {
			switch (idx) {
			case 0:
				binding.primaryKey = keycode;
				if (binding.secondaryKey == keycode) {
					binding.secondaryKey = -1;
				}
				break;
			case 1:
				binding.secondaryKey = keycode;
				if (binding.primaryKey == keycode) {
					binding.primaryKey = -1;
				}
				break;
			}
		}
		else {
			if (binding.primaryKey == keycode) {
				binding.primaryKey = -1;
			}
			if (binding.secondaryKey == keycode) {
				binding.secondaryKey = -1;
			}
		}
	}
}

const std::string &Options::GetEmulatorOptionString(const std::string &emuId, int optionId) const {
	auto &value = _emulatorOptions.at(emuId).at(optionId);
	assert(EmulatorOption_t::IsStringOption(value.type));
	return value.valStr;
}

int Options::GetEmulatorOptionInt(const std::string &emuId, int optionId) const {
	auto &value = _emulatorOptions.at(emuId).at(optionId);
	assert(EmulatorOption_t::IsIntOption(value.type));
	return value.valInt;
}

float Options::GetEmulatorOptionFloat(const std::string &emuId, int optionId) const {
	auto &value = _emulatorOptions.at(emuId).at(optionId);
	assert(EmulatorOption_t::IsFloatOption(value.type));
	return value.valFloat;
}

void Options::SetEmulatorOption(const std::string &emuId, int optionId, const std::string &value) {
	auto &option = _emulatorOptions.at(emuId).at(optionId);
	assert(EmulatorOption_t::IsStringOption(option.type));
	option.valStr = value;
}

void Options::SetEmulatorOption(const std::string &emuId, int optionId, int value) {
	auto &option = _emulatorOptions.at(emuId).at(optionId);
	assert(EmulatorOption_t::IsIntOption(option.type));
	option.valInt = value;
}

void Options::SetEmulatorOption(const std::string &emuId, int optionId, float value) {
	auto &option = _emulatorOptions.at(emuId).at(optionId);
	assert(EmulatorOption_t::IsStringOption(option.type));
	option.valFloat = value;
}
