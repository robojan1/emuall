#ifndef _LOGWX_H
#define _LOGWX_H

#include <wx/socket.h>
#include "log.h"

inline std::string wxSocketErrorToString(wxSocketError err)
{
	switch(err){
		case wxSOCKET_NOERROR: return "No error";
		case wxSOCKET_INVOP: return "Invalid operation";
		case wxSOCKET_IOERR: return "Input output error";
		case wxSOCKET_INVADDR: return "Invalid address";
		case wxSOCKET_INVSOCK: return "Invalid socket";
		case wxSOCKET_NOHOST: return "No corresponding host";
		case wxSOCKET_INVPORT: return "Invalid port";
		case wxSOCKET_WOULDBLOCK: return "Would block";
		case wxSOCKET_TIMEDOUT: return "Timed out";
		case wxSOCKET_MEMERR: return "Out of memory";
		case wxSOCKET_OPTERR: return "Opt error";
		default: return "Unknown error";
	}
}

#endif