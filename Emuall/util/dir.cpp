#include "memDbg.h"

#include "dir.h"

#ifdef _WIN32
#include <windows.h>

std::vector<std::string> GetDirFiles(std::string folder, std::string extension)
{
	std::vector<std::string> result;

	WIN32_FIND_DATAA ffd;
	HANDLE hfind = INVALID_HANDLE_VALUE;
	std::string searchPath = folder;
	if (!extension.empty())
	{
		searchPath += "\\*.";
		searchPath += extension;
	}

	hfind = FindFirstFileA(searchPath.c_str(), &ffd);
	if (INVALID_HANDLE_VALUE == hfind)
	{
		return result;
	}

	do
	{
		std::string file = folder;
		file.append("\\");
		file.append(ffd.cFileName);
		result.push_back(file);
	} while (FindNextFileA(hfind, &ffd) != 0);
	FindClose(hfind);
	return result;
}

#else
#include <sys/types.h>
#include <dirent.h>
#include <string>
#include <cstring>
#include <emuall/util/string.h>

std::vector<std::string> GetDirFiles(std::string folder, std::string extension)
{
	std::vector<std::string> result;
	DIR *dir;
	struct dirent *entry;

	std::string baseFileName = folder;
	baseFileName.append("/");

	dir = opendir(folder.c_str());
	if(dir == NULL) return result;
	
	while((entry = readdir(dir)) != NULL) {
		if(StringCaseEndsWith(entry->d_name, extension.c_str())) {
			std::string filename = baseFileName;
			filename.append(entry->d_name);
			result.push_back(filename);
		}
	}
	closedir(dir);
	return result;
}

#endif
