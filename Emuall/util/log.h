#ifndef UTIL_LOG_H
#define UTIL_LOG_H

#include <wx/log.h>

namespace logger {
	wxLogLevel GetWxLogLevel();
}

#endif
