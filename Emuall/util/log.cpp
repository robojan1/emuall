#include "log.h"
#include <emuall/util/log.h>

wxLogLevel logger::GetWxLogLevel() {
	switch (logger::GetLogLevel())
	{
	case logger::Fatal: return wxLOG_FatalError;
	case logger::Error: return wxLOG_Error;
	case logger::Warn: return wxLOG_Warning;
	default:
	case logger::Message: return wxLOG_Message;
	case logger::Debug: return wxLOG_Debug;
	}
}