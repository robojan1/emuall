#ifndef _OPTIONS_H
#define _OPTIONS_H

#define DEFAULT_SAMPLERATE 44100
#define DEFAULT_BUFFERSIZE 1024
#define DEFAULT_NUMAUDIOBUFFERS 2
#define DEFAULT_KEEPASPECT true
#define DEFAULT_VIDEOFILTER 0


#include <map>
#include <vector>
#include <list>
#include <emuall/emulator/Emulator.h>

constexpr int MAX_RECENT_FILES = 5;

typedef struct
{
	bool keepAspect;
	int filter;
	std::string messageFont;
	int messageFontIdx;
	int messageFontSize;
} OptionsVid_t;

typedef struct
{
	int sampleRate;
	int bufferSize;
	int numBuffers;
} OptionsAud_t;

class Options
{
	friend class InputOptionsFrame;
public:
	~Options();

	static Options &GetSingleton();

	void LoadOptions();
	void SaveOptions();
	void LoadKeyBindings(const std::string &name, const std::vector<EmulatorInput_t> &bindings);

	void SaveRecentFile(const std::string &file, const std::string &emulatorId);
	std::string GetRecentFilePath(int idx);
	std::string GetRecentFileEmulator(int idx);

	std::string GetConfigFileName() const;
	std::string GetConfigDir() const;

	const std::string &GetEmulatorOptionString(const std::string &emuId, int optionId) const;
	int GetEmulatorOptionInt(const std::string &emuId, int optionId) const;
	float GetEmulatorOptionFloat(const std::string &emuId, int optionId) const;

	void SetEmulatorOption(const std::string &emuId, int optionId, const std::string &value);
	void SetEmulatorOption(const std::string &emuId, int optionId, int value);
	void SetEmulatorOption(const std::string &emuId, int optionId, float value);

	void RebindKey(std::string name, int id, int keycode, int idx);

	OptionsVid_t videoOptions;
	OptionsAud_t audioOptions;
	std::map<std::string, int> dinputMap;
	std::map<std::string, std::vector<EmulatorInput_t>> _keyBindings;
private:
	Options();
	struct EmulatorOptionValue {
		EmulatorOption_t::Type type;
		std::string valStr;
		int valInt;
		float valFloat;
	};

	static Options _instance;

	std::list<std::pair<std::string, std::string>> _recentFiles;
	std::map<std::string, std::map<int, struct EmulatorOptionValue>> _emulatorOptions;
};

#endif