//
// Created by Robbert-Jan de Jager on 13-12-18.
//

#include <wx/tooltip.h>
#include <wx/textctrl.h>
#include <wx/checkbox.h>
#include <wx/spinctrl.h>
#include <wx/artprov.h>

#include <emuall/emulator/EmulatorList.h>
#include <emuall/emulator/Emulator.h>
#include <emuall/emulator/EmulatorInfo.h>
#include "../util/Options.h"
#include "EmulatorOptionsFrame.h"

IMPLEMENT_CLASS(EmulatorOptionsFrame, wxFrame)
BEGIN_EVENT_TABLE(EmulatorOptionsFrame, wxFrame)
	EVT_CLOSE(EmulatorOptionsFrame::OnClose)
	EVT_SHOW(EmulatorOptionsFrame::OnShow)
	EVT_BUTTON(wxID_CANCEL, EmulatorOptionsFrame::OnCancel)
	EVT_BUTTON(wxID_RESET, EmulatorOptionsFrame::OnReset)
	EVT_BUTTON(wxID_SAVE, EmulatorOptionsFrame::OnSave)
END_EVENT_TABLE()



EmulatorOptionsFrame::EmulatorOptionsFrame(wxFrame *parent) :
	wxFrame(parent, wxID_ANY, _("Emulator Options")), _notebook(nullptr), _initialized(false),
	_saveButton(nullptr), _resetButton(nullptr)
{
	wxBoxSizer *sizer = new wxBoxSizer(wxVERTICAL);
	_notebook = new wxNotebook(this, wxID_ANY);
	_notebook->SetMinSize(wxSize(300,200));
	wxBoxSizer *buttonSizer = new wxBoxSizer(wxHORIZONTAL);
	_saveButton = new wxButton(this, wxID_SAVE, _("Save"));
	wxButton *cancelButton = new wxButton(this, wxID_CANCEL, _("Cancel"));
	_resetButton = new wxButton(this, wxID_RESET, _("Reset"));
	buttonSizer->AddStretchSpacer(1);
	buttonSizer->Add(_saveButton, wxSizerFlags().Proportion(0).Border(wxALL, 2));
	buttonSizer->Add(cancelButton, wxSizerFlags().Proportion(0).Border(wxALL, 2));
	buttonSizer->Add(_resetButton, wxSizerFlags().Proportion(0).Border(wxALL, 2));

	sizer->Add(_notebook, wxSizerFlags().Expand().Proportion(1));
	sizer->Add(buttonSizer, wxSizerFlags().Border(wxALL, 5).Expand().Proportion(0));
	SetSizerAndFit(sizer);
	SetSize(wxSize(500, 600));
}

EmulatorOptionsFrame::~EmulatorOptionsFrame() {

}

void EmulatorOptionsFrame::Initialize() {
	auto &emulators = EmulatorList::Get();
	for(auto emu : emulators) {
		std::string emuId = emu->GetInfo().id;
		wxScrolledWindow *page = new wxScrolledWindow(_notebook);
		wxBoxSizer *pageSizer = new wxBoxSizer(wxVERTICAL);
		auto options = emu->GetEmulatorOptions();
		for(auto &option : options) {
			wxString toolTipText = option.description;
			switch(option.type) {
				case EmulatorOption_t::Boolean: {
					int id = wxIdManager::ReserveId(1);
					wxCheckBox * checkbox = new wxCheckBox(page, id, option.name);
					if(!toolTipText.empty()) {
						checkbox->SetToolTip(toolTipText);
					}
					pageSizer->Add(checkbox, wxSizerFlags().Expand().Border(wxALL, 4));

					Bind(wxEVT_CHECKBOX, std::bind(&EmulatorOptionsFrame::OnCheckboxChanged, this, std::placeholders::_1,
							emuId, option.id), id);
					_optionMap[emuId][option.id] = {option.type, checkbox};
					break;
				}
				case EmulatorOption_t::File: {
					int id = wxIdManager::ReserveId(2);
					wxBoxSizer *optionSizer = new wxBoxSizer(wxHORIZONTAL);
					wxStaticText *label = new wxStaticText(page, wxID_ANY, option.name);
					wxTextCtrl *filePathText = new wxTextCtrl(page, id);
					wxBitmapButton *filePathButton = new wxBitmapButton(page, id+1, wxArtProvider::GetBitmap(wxART_FILE_OPEN));
					optionSizer->Add(label, wxSizerFlags().Proportion(0).Border(wxRIGHT, 5));
					optionSizer->Add(filePathText, wxSizerFlags().Expand().Proportion(1));
					optionSizer->Add(filePathButton, wxSizerFlags().Proportion(0));
					if(!toolTipText.empty()) {
						label->SetToolTip(new wxToolTip(toolTipText));
						filePathText->SetToolTip(toolTipText);
						filePathButton->SetToolTip(toolTipText);
					}
					pageSizer->Add(optionSizer, wxSizerFlags().Expand().Border(wxALL, 4));

					Bind(wxEVT_TEXT, std::bind(&EmulatorOptionsFrame::OnStringChanged, this, std::placeholders::_1,
					                               emuId, option.id), id);
					Bind(wxEVT_BUTTON, std::bind(&EmulatorOptionsFrame::OnFileOpenButton, this, std::placeholders::_1,
							filePathText), id+1);
					_optionMap[emuId][option.id] = {option.type, filePathText};
					break;
				}
				case EmulatorOption_t::Path: {
					int id = wxIdManager::ReserveId(2);
					wxBoxSizer *optionSizer = new wxBoxSizer(wxHORIZONTAL);
					wxStaticText *label = new wxStaticText(page, wxID_ANY, option.name);
					wxTextCtrl *filePathText = new wxTextCtrl(page, id);
					wxBitmapButton *filePathButton = new wxBitmapButton(page, id+1, wxArtProvider::GetBitmap(wxART_FOLDER_OPEN));
					optionSizer->Add(label, wxSizerFlags().Proportion(0).Border(wxRIGHT, 5));
					optionSizer->Add(filePathText, wxSizerFlags().Expand().Proportion(1));
					optionSizer->Add(filePathButton, wxSizerFlags().Proportion(0));
					if(!toolTipText.empty()) {
						label->SetToolTip(toolTipText);
						filePathText->SetToolTip(toolTipText);
						filePathButton->SetToolTip(toolTipText);
					}
					pageSizer->Add(optionSizer, wxSizerFlags().Expand().Border(wxALL, 4));

					Bind(wxEVT_TEXT, std::bind(&EmulatorOptionsFrame::OnStringChanged, this, std::placeholders::_1,
					                               emuId, option.id), id);
					Bind(wxEVT_BUTTON, std::bind(&EmulatorOptionsFrame::OnFolderOpenButton, this, std::placeholders::_1,
					                             filePathText), id+1);
					_optionMap[emuId][option.id] = {option.type, filePathText};
					break;
				}
				case EmulatorOption_t::Integer: {
					int id = wxIdManager::ReserveId(1);
					wxBoxSizer *optionSizer = new wxBoxSizer(wxHORIZONTAL);
					wxStaticText *label = new wxStaticText(page, wxID_ANY, option.name);
					wxSpinCtrl *spinner = new wxSpinCtrl(page, id);
					if(option.hasMin && option.hasMax) spinner->SetRange(option.minInt, option.maxInt);
					else if(option.hasMin) spinner->SetRange(option.minInt, INT32_MAX);
					else if(option.hasMax) spinner->SetRange(INT32_MIN, option.maxInt);
					else spinner->SetRange(INT32_MIN, INT32_MAX);
					optionSizer->Add(label, wxSizerFlags().Proportion(0).Border(wxRIGHT, 5));
					optionSizer->Add(spinner,wxSizerFlags().Expand().Proportion(1));
					if(!toolTipText.empty()) {
						label->SetToolTip(toolTipText);
						spinner->SetToolTip(toolTipText);
					}
					pageSizer->Add(optionSizer, wxSizerFlags().Expand().Border(wxALL, 4));

					Bind(wxEVT_SPINCTRL, std::bind(&EmulatorOptionsFrame::OnIntChanged, this, std::placeholders::_1,
					                               emuId, option.id), id);
					_optionMap[emuId][option.id] = {option.type, spinner};
					break;
				}
				case EmulatorOption_t::Enumeration: {
					int id = wxIdManager::ReserveId(1);
					wxBoxSizer *optionSizer = new wxBoxSizer(wxHORIZONTAL);
					wxStaticText *label = new wxStaticText(page, wxID_ANY, option.name);
					wxChoice *choice = new wxChoice(page, id);
					for(auto optionValue : option.enumOptions) {
						choice->Append(optionValue.second, reinterpret_cast<void *>(static_cast<intptr_t>(optionValue.first)));
					}
					optionSizer->Add(label, wxSizerFlags().Proportion(0).Border(wxRIGHT, 5));
					optionSizer->Add(choice,wxSizerFlags().Expand().Proportion(1));
					if(!toolTipText.empty()) {
						label->SetToolTip(toolTipText);
						choice->SetToolTip(toolTipText);
					}
					pageSizer->Add(optionSizer, wxSizerFlags().Expand().Border(wxALL, 4));

					Bind(wxEVT_CHOICE, std::bind(&EmulatorOptionsFrame::OnEnumChanged, this, std::placeholders::_1,
					                               emuId, option.id), id);
					_optionMap[emuId][option.id] = {option.type, choice};
					break;
				}
				case EmulatorOption_t::Float: {
					int id = wxIdManager::ReserveId(1);
					wxBoxSizer *optionSizer = new wxBoxSizer(wxHORIZONTAL);
					wxStaticText *label = new wxStaticText(page, wxID_ANY, option.name);
					wxSpinCtrlDouble *spinner = new wxSpinCtrlDouble(page, id);
					if(option.hasMin && option.hasMax) spinner->SetRange(option.minFloat, option.maxFloat);
					else if(option.hasMin) spinner->SetRange(option.minFloat, INFINITY);
					else if(option.hasMax) spinner->SetRange(-INFINITY, option.maxFloat);
					else spinner->SetRange(-INFINITY, INFINITY);
					optionSizer->Add(label, wxSizerFlags().Proportion(0).Border(wxRIGHT, 5));
					optionSizer->Add(spinner,wxSizerFlags().Expand().Proportion(1));
					if(!toolTipText.empty()) {
						label->SetToolTip(toolTipText);
						spinner->SetToolTip(toolTipText);
					}
					pageSizer->Add(optionSizer, wxSizerFlags().Expand().Border(wxALL, 4));

					Bind(wxEVT_SPINCTRLDOUBLE, std::bind(&EmulatorOptionsFrame::OnFloatChanged, this, std::placeholders::_1,
					                               emuId, option.id), id);
					_optionMap[emuId][option.id] = {option.type, spinner};
					break;
				}
			}
		}
		page->SetSizerAndFit(pageSizer);

		_notebook->InsertPage(0, page, emu->GetName(), true);
	}
	_initialized = true;
}

void EmulatorOptionsFrame::ResetCurrentValues()
{
	auto &options = Options::GetSingleton();
	auto &emulators = EmulatorList::Get();
	for(auto emu : emulators) {
		auto emuId = emu->GetInfo().id;
		auto emuOptions = emu->GetEmulatorOptions();
		for(auto &option : emuOptions) {
			auto optionId = option.id;
			switch(option.type) {
				case EmulatorOption_t::Boolean: {
					bool value = options.GetEmulatorOptionInt(emuId, optionId) != 0;
					auto control = dynamic_cast<wxCheckBox *>(GetOptionInfo(emuId, optionId).control);
					control->SetValue(value);
					break;
				}
				case EmulatorOption_t::File: {
					const std::string &path = options.GetEmulatorOptionString(emuId, optionId);
					auto control = dynamic_cast<wxTextCtrl *>(GetOptionInfo(emuId, optionId).control);
					control->SetValue(path);
					break;
				}
				case EmulatorOption_t::Path: {
					const std::string &path = options.GetEmulatorOptionString(emuId, optionId);
					auto control = dynamic_cast<wxTextCtrl *>(GetOptionInfo(emuId, optionId).control);
					control->SetValue(path);
					break;
				}
				case EmulatorOption_t::Integer: {
					int value = options.GetEmulatorOptionInt(emuId, optionId);
					auto control = dynamic_cast<wxSpinCtrl *>(GetOptionInfo(emuId, optionId).control);
					control->SetValue(value);
					break;
				}
				case EmulatorOption_t::Enumeration: {
					int value = options.GetEmulatorOptionInt(emuId, optionId);
					auto control = dynamic_cast<wxChoice *>(GetOptionInfo(emuId, optionId).control);
					for(int i = 0; i < control->GetCount(); i++) {
						void *clientData = control->GetClientData(i);
						if(reinterpret_cast<std::intptr_t>(clientData) == value) {
							control->Select(i);
							break;
						}
					}
					break;
				}
				case EmulatorOption_t::Float: {
					float value = options.GetEmulatorOptionFloat(emuId, optionId);
					auto control = dynamic_cast<wxSpinCtrlDouble *>(GetOptionInfo(emuId, optionId).control);
					control->SetValue(value);
					break;
				}
			}
		}
	}
	_changedOptions.clear();
	_resetButton->Enable(false);
	_saveButton->Enable(false);
}

const struct EmulatorOptionsFrame::OptionInfo &EmulatorOptionsFrame::GetOptionInfo(const std::string &emuId, int optionId) const {
	return _optionMap.at(emuId).at(optionId);
}

void EmulatorOptionsFrame::SetChangedValue(const std::string &emuId, int optionId, bool isChanged)
{
	auto key = std::make_pair(emuId, optionId);
	if(isChanged) {
		_changedOptions.insert(key);
	} else {
		_changedOptions.erase(key);
	}

	bool hasChangedOptions = !_changedOptions.empty();
	_resetButton->Enable(hasChangedOptions);
	_saveButton->Enable(hasChangedOptions);
}

void EmulatorOptionsFrame::OnClose(wxCloseEvent &evt) {
	if(evt.CanVeto()) {
		evt.Veto(true);
		Show(false);
		return;
	}
	Destroy();
}

void EmulatorOptionsFrame::OnShow(wxShowEvent &evt) {
	if (!evt.IsShown())
		return;
	if(!_initialized) {
		Initialize();
		ResetCurrentValues();
	}
}

void EmulatorOptionsFrame::OnSave(wxCommandEvent &evt) {
	auto &options = Options::GetSingleton();
	for(auto &key : _changedOptions) {
		auto &emu = std::get<0>(key);
		auto option = std::get<1>(key);
		auto &info = GetOptionInfo(emu, option);
		switch(info.type) {
			case EmulatorOption_t::Boolean: {
				auto control = dynamic_cast<wxCheckBox *>(GetOptionInfo(emu, option).control);
				options.SetEmulatorOption(emu, option, control->IsChecked() ? 0 : 1);
				break;
			}
			case EmulatorOption_t::File: {
				auto control = dynamic_cast<wxTextCtrl *>(GetOptionInfo(emu, option).control);
				options.SetEmulatorOption(emu, option, std::string(control->GetValue()));
				break;
			}
			case EmulatorOption_t::Path: {
				auto control = dynamic_cast<wxTextCtrl *>(GetOptionInfo(emu, option).control);
				options.SetEmulatorOption(emu, option, std::string(control->GetValue()));
				break;
			}
			case EmulatorOption_t::Integer: {
				auto control = dynamic_cast<wxSpinCtrl *>(GetOptionInfo(emu, option).control);
				options.SetEmulatorOption(emu, option, control->GetValue());
				break;
			}
			case EmulatorOption_t::Enumeration: {
				auto control = dynamic_cast<wxChoice *>(GetOptionInfo(emu, option).control);
				int idx = control->GetSelection();
				if(idx != wxNOT_FOUND) {
					void *clientData = control->GetClientData(idx);
					options.SetEmulatorOption(emu, option, static_cast<int>(reinterpret_cast<std::intptr_t>(clientData)));
				}
				break;
			}
			case EmulatorOption_t::Float: {
				auto control = dynamic_cast<wxSpinCtrlDouble *>(GetOptionInfo(emu, option).control);
				options.SetEmulatorOption(emu, option, (float)control->GetValue());
				break;
			}
		}
		if(OnOptionChangedHandler) {
			OnOptionChangedHandler(emu, option);
		}
	}
	options.SaveOptions();
	ResetCurrentValues();
	Show(false);
}

void EmulatorOptionsFrame::OnCancel(wxCommandEvent &evt) {
	ResetCurrentValues();
	Show(false);
}

void EmulatorOptionsFrame::OnReset(wxCommandEvent &evt) {
	ResetCurrentValues();
}

void EmulatorOptionsFrame::OnCheckboxChanged(wxCommandEvent &evt, const std::string &emuId, int optionId)
{
	auto optionValue = Options::GetSingleton().GetEmulatorOptionInt(emuId, optionId) != 0;
	auto *control = dynamic_cast<wxCheckBox *>(evt.GetEventObject());
	bool value = control->IsChecked();

	SetChangedValue(emuId, optionId, value != optionValue);
}

void EmulatorOptionsFrame::OnStringChanged(wxCommandEvent &evt, const std::string &emuId, int optionId) {
	auto &optionValue = Options::GetSingleton().GetEmulatorOptionString(emuId, optionId);
	auto *control = dynamic_cast<wxTextCtrl *>(evt.GetEventObject());
	auto value = control->GetValue();

	SetChangedValue(emuId, optionId, value != optionValue);
}

void EmulatorOptionsFrame::OnFileOpenButton(wxCommandEvent &evt, wxTextCtrl *target) {
	wxFileDialog dialog(this, _("Select file"));
	auto result = dialog.ShowModal();
	if(result == wxID_CANCEL)
		return;
	target->SetValue(dialog.GetPath());
}

void EmulatorOptionsFrame::OnFolderOpenButton(wxCommandEvent &evt, wxTextCtrl *target) {
	wxDirDialog dialog(this, _("Select directory"));
	auto result = dialog.ShowModal();
	if(result == wxID_CANCEL)
		return;
	target->SetValue(dialog.GetPath());
}

void EmulatorOptionsFrame::OnIntChanged(wxSpinEvent &evt, const std::string &emuId, int optionId) {
	auto optionValue = Options::GetSingleton().GetEmulatorOptionInt(emuId, optionId);
	auto *control = dynamic_cast<wxSpinCtrl *>(evt.GetEventObject());
	auto value = control->GetValue();

	SetChangedValue(emuId, optionId, value != optionValue);
}

void EmulatorOptionsFrame::OnEnumChanged(wxCommandEvent &evt, const std::string &emuId, int optionId) {
	auto optionValue = Options::GetSingleton().GetEmulatorOptionInt(emuId, optionId);
	auto *control = dynamic_cast<wxChoice *>(evt.GetEventObject());
	int idx = control->GetSelection();
	assert(idx != wxNOT_FOUND);
	auto value = reinterpret_cast<std::intptr_t>(control->GetClientData(static_cast<unsigned int>(idx)));

	SetChangedValue(emuId, optionId, value != optionValue);
}

void EmulatorOptionsFrame::OnFloatChanged(wxSpinDoubleEvent &evt, const std::string &emuId, int optionId) {
	auto optionValue = Options::GetSingleton().GetEmulatorOptionFloat(emuId, optionId);
	auto *control = dynamic_cast<wxSpinCtrlDouble *>(evt.GetEventObject());
	auto value = control->GetValue();

	SetChangedValue(emuId, optionId, value != optionValue);
}
