//
// Created by Robbert-Jan de Jager on 13-12-18.
//

#ifndef EMUALL_EMULATOROPTIONSFRAME_H
#define EMUALL_EMULATOROPTIONSFRAME_H

#include <wx/wx.h>
#include <wx/notebook.h>
#include <wx/spinctrl.h>

#include <emuall/emulator/EmulatorInfo.h>
#include <set>

class EmulatorOptionsFrame : public wxFrame
{
DECLARE_CLASS(InputOptionsFrame);
DECLARE_EVENT_TABLE();

public:
	explicit EmulatorOptionsFrame(wxFrame *parent);
	~EmulatorOptionsFrame() override;

	std::function<void(const std::string &, int)> OnOptionChangedHandler;
private:
	struct OptionInfo {
		EmulatorOption_t::Type type;
		wxControl *control;
	};
	void Initialize();
	void ResetCurrentValues();

	const struct OptionInfo &GetOptionInfo(const std::string &emuId, int optionId) const;

	void OnClose(wxCloseEvent &evt);
	void OnShow(wxShowEvent &evt);
	void OnSave(wxCommandEvent &evt);
	void OnCancel(wxCommandEvent &evt);
	void OnReset(wxCommandEvent &evt);

	void OnCheckboxChanged(wxCommandEvent &evt, const std::string &emuId, int optionId);
	void OnStringChanged(wxCommandEvent &evt, const std::string &emuId, int optionId);
	void OnFileOpenButton(wxCommandEvent &evt, wxTextCtrl *target);
	void OnFolderOpenButton(wxCommandEvent &evt, wxTextCtrl *target);
	void OnIntChanged(wxSpinEvent &evt, const std::string &emuId, int optionId);
	void OnEnumChanged(wxCommandEvent &evt, const std::string &emuId, int optionId);
	void OnFloatChanged(wxSpinDoubleEvent &evt, const std::string &emuId, int optionId);

	wxNotebook *_notebook;
	bool _initialized;
	std::map<std::string, std::map<int, struct OptionInfo>> _optionMap;
	std::set<std::pair<std::string, int>> _changedOptions;
	wxButton *_saveButton;
	wxButton *_resetButton;

	void SetChangedValue(const std::string &emuId, int optionId, bool isChanged);
};

#endif //EMUALL_EMULATOROPTIONSFRAME_H
