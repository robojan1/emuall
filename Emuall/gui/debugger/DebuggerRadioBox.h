#ifndef _DEBUGGERRADIOBOX_H
#define _DEBUGGERRADIOBOX_H

#include "DebuggerElement.h"
#include <wx/wx.h>
#include <pugixml.hpp>
#include <emuall/emulator/Emulator.h>

namespace Debugger {

	class DebuggerRadioBox : public wxEvtHandler, public DebuggerElement
	{
	public:
		DebuggerRadioBox(const Emulator **emu, const pugi::xml_node &node);
		virtual ~DebuggerRadioBox();

		void UpdateInfo();
		wxRadioBox *GetWidget(wxWindow *parent, wxWindowID id);

	private:
		void OnClicked(wxCommandEvent &evt);

		struct Item {
			int id;
			std::string name;
			bool readOnly;
			std::map<std::string, int> options;
		};

		const Emulator **_emu;
		wxRadioBox *_widget;
		Item _item;
	};
}



#endif