#include "DebuggerRadioBox.h"
#include <emuall/util/log.h>
#include <pugixml.hpp>

using namespace Debugger;

DebuggerRadioBox::DebuggerRadioBox(const Emulator **emu, const pugi::xml_node &node) :
	_emu(emu), _widget(NULL)
{
	wxASSERT(strcmp(node.name(), "radio") == 0);
	wxASSERT(emu != NULL);
	_item.id = node.attribute("id").as_int(-1);
	_item.name = node.child("name").text().as_string("Error!");
	_item.readOnly = node.child("readonly").text().as_bool(false);
	pugi::xml_object_range<pugi::xml_named_node_iterator> options = node.children("option");
	for (pugi::xml_named_node_iterator iOption = options.begin(); iOption != options.end(); ++iOption)
	{
		int value = iOption->attribute("val").as_int(0);
		_item.options[iOption->text().as_string("Error!")] = value;
	}
}

DebuggerRadioBox::~DebuggerRadioBox()
{

}

wxRadioBox *DebuggerRadioBox::GetWidget(wxWindow *parent, wxWindowID id)
{
	if (_widget != NULL)
	{
		return _widget; // Widget already created
	}

	wxArrayString options;
	for (auto iOption : _item.options)
	{
		options.Add(iOption.first);
	}

	_widget = new wxRadioBox(parent, id, _item.name, wxDefaultPosition, wxDefaultSize, options, 0, wxRA_SPECIFY_ROWS);
	_widget->Bind(wxEVT_RADIOBOX, &DebuggerRadioBox::OnClicked, this);

	UpdateInfo();
	return _widget;
}

void DebuggerRadioBox::UpdateInfo()
{
	if (*_emu == nullptr || _widget == NULL)
	{
		return; // Nothing to do
	}

	if (_widget->IsShownOnScreen())
	{
		int selection = (*_emu)->GetValU(_item.id);
		for (auto iOption : _item.options)
		{
			if (iOption.second == selection)
			{
				_widget->SetSelection(_widget->FindString(iOption.first));
				break;
			}
		}
	}
}

void DebuggerRadioBox::OnClicked(wxCommandEvent &evt)
{
	if (*_emu == nullptr || _widget == NULL)
		return;

	wxString selection = _widget->GetString(evt.GetSelection());
	
	auto iVal = _item.options.find(selection.ToStdString());
	if (iVal == _item.options.end())
	{
		logger::Log(logger::Warn, "Could not find option '%s' in radiobox", selection.c_str().AsChar());
		return;
	}

	(*_emu)->SetValI(_item.id, iVal->second);
}