#include "DebuggerBoxSizer.h"
#include "DebuggerManager.h"
#include <emuall/util/log.h>
#include <pugixml.hpp>

using namespace Debugger;

DebuggerBoxSizer::DebuggerBoxSizer(const Emulator **emu, const pugi::xml_node &node, int orient) :
	_emu(emu), _widget(NULL), _orient(orient)
{
	wxASSERT(strcmp(node.name(), "hor") == 0 || strcmp(node.name(), "ver") == 0);
	wxASSERT(emu != NULL);

	pugi::xml_object_range<pugi::xml_node_iterator> children = node.children();
	for (pugi::xml_node_iterator iChild = children.begin(); iChild != children.end(); ++iChild)
	{
		Item item;
		item.element = GetElement(emu, *iChild);
		item.proportion = iChild->attribute("prop").as_int(0);
		if (item.element != NULL)
		{
			_items.push_back(item);
		}
	}
}

DebuggerBoxSizer::~DebuggerBoxSizer()
{
	for (auto iElement : _items)
	{
		delete iElement.element;
	}
	_items.clear();
}

wxPanel *DebuggerBoxSizer::GetWidget(wxWindow *parent, wxWindowID id)
{
	if (_widget != NULL)
	{
		return _widget; // Widget already created
	}

	_widget = new wxPanel(parent, id);
	wxBoxSizer *sizer = new wxBoxSizer(_orient);
	for (auto iItem : _items)
	{
		sizer->Add(iItem.element->GetWidget(_widget, wxID_ANY), iItem.proportion, wxEXPAND | wxALL, 5);
	}
	_widget->SetSizerAndFit(sizer);
		
	UpdateInfo();
	return _widget;
}

void DebuggerBoxSizer::UpdateInfo()
{
	if (*_emu == nullptr || _widget == NULL)
	{
		return; // Nothing to do
	}


	for (auto iItem = _items.begin(); iItem != _items.end(); ++iItem)
	{
		iItem->element->UpdateInfo();
	}
}