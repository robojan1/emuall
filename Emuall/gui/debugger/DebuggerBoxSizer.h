#ifndef _DEBUGGERBOXSIZER_H
#define _DEBUGGERBOXSIZER_H

#include "DebuggerElement.h"
#include <wx/wx.h>
#include <pugixml.hpp>
#include <emuall/emulator/Emulator.h>


namespace Debugger
{

	class DebuggerBoxSizer : public DebuggerElement
	{
	public:
		DebuggerBoxSizer(const Emulator **emu, const pugi::xml_node &node, int orient);
		virtual ~DebuggerBoxSizer();

		void UpdateInfo();
		wxPanel *GetWidget(wxWindow *parent, wxWindowID id);

	private:
		struct Item
		{
			DebuggerElement *element;
			int proportion;
		};
		const Emulator **_emu;
		wxPanel *_widget;
		int _orient;
		std::vector<Item> _items;
	};
}

#endif