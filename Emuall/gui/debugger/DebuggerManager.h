#ifndef _DEBUGGERMANAGER_H
#define _DEBUGGERMANAGER_H

#include <emuall/util/log.h>
#include <pugixml.hpp>
#include "DebuggerElement.h"
class Emulator;

namespace Debugger {
	DebuggerElement *GetElement(const Emulator **emu, pugi::xml_node &node);
}

#endif