#ifndef _DEBUGGERSCREEN_H
#define _DEBUGGERSCREEN_H

#include "DebuggerElement.h"
#include <wx/wx.h>
#include "../GLPane.h"
#include <pugixml.hpp>
#include <emuall/emulator/Emulator.h>

namespace Debugger {
	class DebuggerScreen : public DebuggerElement, public GLPaneI
	{
	public:
		DebuggerScreen(const Emulator **emu, const pugi::xml_node &node);
		virtual ~DebuggerScreen();

		void UpdateInfo();
		GLPane *GetWidget(wxWindow *parent, wxWindowID id);

	private:
		void DrawGL(int user);
		bool InitGL(int user);
		void DestroyGL(int user);
		void OnMotion(wxMouseEvent &evt);

		struct Item {
			int id;
			int shareId;
			int width;
			int height;
			int mouseX;
			int mouseY;
		};

		const Emulator **_emu;
		GLPane *_widget;
		Item _screen;
	};
}

#endif