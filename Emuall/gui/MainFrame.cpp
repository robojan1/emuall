#include <errno.h>
#include <wx/ffile.h>
#include <wx/filename.h>

#include <emuall/support.h>
#include <emuall/exception.h>
#include <emuall/util/log.h>
#include <emuall/emulator/Emulator.h>
#include <emuall/emulator/EmulatorList.h>
#include <emuall/emulator/SaveFile.h>

#include "../util/log.h"

#include "icon.h"
#include "MainFrame.h"
#include "../util/memDbg.h"
#include "IdList.h"
#include "GLPane.h"
#include "../emuAll.h"
#include "../input/xinput.h"
#include "EmulatorOptionsFrame.h"

BEGIN_EVENT_TABLE(MainFrame, wxFrame)
	EVT_MENU(ID_Main_File_quit, MainFrame::OnQuit)
	EVT_MENU(ID_Main_File_open, MainFrame::OnOpen)
	EVT_MENU(ID_Main_File_reset, MainFrame::OnReset)
	EVT_MENU(ID_Main_File_run, MainFrame::OnRun)
	EVT_MENU(ID_Main_Debug_window, MainFrame::OnLogWindow)
	// Debug levels
	EVT_MENU(ID_Main_Debug_level_fat, MainFrame::OnLogLevel)
	EVT_MENU(ID_Main_Debug_level_err, MainFrame::OnLogLevel)
	EVT_MENU(ID_Main_Debug_level_war, MainFrame::OnLogLevel)
	EVT_MENU(ID_Main_Debug_level_mes, MainFrame::OnLogLevel)
	EVT_MENU(ID_Main_Debug_level_deb, MainFrame::OnLogLevel)
	// Debug menu
	EVT_MENU(ID_Main_Debug_cpu, MainFrame::OnDebugCPU)
	EVT_MENU(ID_Main_Debug_mem, MainFrame::OnDebugMEM)
	EVT_MENU(ID_Main_Debug_gpu, MainFrame::OnDebugGPU)
	// Options
	EVT_MENU(ID_Main_Options_KeepAspect, MainFrame::OnOptions)
	EVT_MENU(ID_Main_Options_input, MainFrame::OnOptions)
	EVT_MENU_RANGE(ID_Main_Options_Filter, ID_Main_Options_Filter + 5, MainFrame::OnOptionVideoFilter)
	EVT_MENU(ID_Main_Options_Emulator, MainFrame::OnOptions)
	
	// General events
	EVT_TIMER(ID_Timer, MainFrame::OnTimer)
	EVT_IDLE(MainFrame::OnIdle)
	EVT_CLOSE(MainFrame::OnClose)
END_EVENT_TABLE()

void DrawFrameEmulatorCallback(Emulator *emu, int id) {
	(void)emu;
	MainFrame *mf = EmuAll::GetMainFrame();
	mf->DrawNow(id);
}

MainFrame::MainFrame(const wxString &title, const wxPoint &pos, const wxSize &size) :
	wxFrame(NULL, ID_MainFrame, title, pos, size), _screenAutoRefresh(true), _inputHandler(nullptr)
{	
	_bar = NULL;
	_menFile = NULL;
	_menOptions = NULL;
	_menDebug = NULL;
	_menDebugLevel = NULL;
	_menSaveState = NULL;
	_menLoadState = NULL;
	_display = NULL;
	_logger = NULL;
	_timer = NULL;
	_inputOptionsFrame = NULL;
	_emulatorOptionsFrame = nullptr;
	

	// Constructor
	_logger = new wxLogWindow(this, _("Debug Log"), false, false);
	_logger->SetVerbose(true);
	logger::AddLogger([](enum logger::loglevel level, const char *str) {
		switch(level)
		{
			case logger::Fatal:
				wxLogFatalError("%s", str);
				break;
			case logger::Error:
				wxLogError("%s", str);
				break;
			case logger::Warn:
				wxLogWarning("%s", str);
				break;
			case logger::Message:
				wxLogMessage("%s", str);
				break;
			case logger::Debug:
				wxLogDebug("%s", str);
				break;
		}
	});
	logger::AddLogger([this](enum logger::loglevel level, const char *cMsg) {
		switch(level)
		{
			case logger::Error: {
				wxString msg(cMsg);
				if(msg.Len() > 512) {
					msg = msg.SubString(0, 509) + "...";
				}
				bool isRunning = _emulator->IsRunning();
				_emulator->Run(false);
				wxMessageDialog dlg(nullptr, msg, wxT("Error"), wxOK | wxICON_ERROR | wxCENTRE);
				dlg.ShowModal();
				_emulator->Run(isRunning);
				break;
			}
			default:
				break;
		}
	});
	_logger->SetLogLevel(logger::GetWxLogLevel());

	// Load options
	Options::GetSingleton().LoadOptions();

	// Create layout
	CreateLayout();

	_inputHandler = new InputMaster(this);
	_display->Bind(wxEVT_KEY_UP, &InputMaster::OnKeyboard, _inputHandler);
	_display->Bind(wxEVT_KEY_DOWN, &InputMaster::OnKeyboard, _inputHandler);

	// Set the current context
	//_display->SetCurrentContext();

	// Initialize support library
	emuallSupportInit();

	// load plugins
	auto &emulators = EmulatorList::Get();
	for (auto it : emulators)
	{
		Options::GetSingleton().LoadKeyBindings(it->GetName(), it->GetEmulatorInputs());
	}

	// create Options frames
	_inputOptionsFrame = new InputOptionsFrame(this, _inputHandler);
	_emulatorOptionsFrame = new EmulatorOptionsFrame(this);
	_emulatorOptionsFrame->OnOptionChangedHandler = [this](const std::string &emu, int id) {
		if(_emulator && _emulator->GetInfo().id == emu) {
			_emulator->NotifyOptionChange(id);
		}
	};

	// Load debuggers
	_cpuDebugger = new CPUDebugger(this, wxID_ANY, _("CPU debugger"));
	_memDebugger = new MemDebugger(this, wxID_ANY, _("Memory debugger"), wxDefaultPosition, wxDefaultSize, wxDEFAULT_FRAME_STYLE & ~(wxRESIZE_BORDER|wxMAXIMIZE_BOX));
	_gpuDebugger = new GPUDebugger(this, wxID_ANY, _("GPU debugger"));

	_filePath.clear();
	//_timer = new wxTimer(this, ID_Timer);
	//_timer->Start(3000, false);

	_gdbServer = std::make_shared<gdb::Server>(55555);

	logger::Log(logger::Message,"EmuAll started");
}

MainFrame::~MainFrame()
{
	// Destructor
	if(_inputHandler != nullptr) {
		delete _inputHandler;
	}

	logger::Log(logger::Message, "EmuAll stopped");
}

void MainFrame::CreateLayout()
{
	// Set the window icon
	SetIcons(getIconBundle());

	// Create the layout
	CreateMenuBar();

	// Create the opengl screen
	wxGLAttributes glAttr;
	glAttr.PlatformDefaults();
	glAttr.DoubleBuffer();
	//glAttr.RGBA();
	glAttr.Depth(24);
	glAttr.Stencil(8);
	glAttr.EndList();
	wxGLContextAttrs ctxAttr;
	ctxAttr.CoreProfile();
	ctxAttr.OGLVersion(3, 2);
	//ctxAttr.ForwardCompatible();
	ctxAttr.EndList();
	_display = new EmulatorScreen(this, this, 0, ID_Main_display, wxDefaultPosition,
		wxDefaultSize, wxFULL_REPAINT_ON_RESIZE, glAttr, ctxAttr);

	_display->Connect(ID_Main_display, wxEVT_SIZE, wxSizeEventHandler(MainFrame::OnResize), (wxObject *) NULL, this);
}

void MainFrame::CreateMenuBar()
{	
	// Create the save state menu
	_menSaveState = new wxMenu;

	// Create the load state menu
	_menLoadState = new wxMenu;

	for (int i = 0; i < 10; i++) {
		_menSaveState->Append(ID_Main_File_SaveState + i, wxString::Format(_("Save State %d\tCtrl+Shift+F%d"), i + 1, i + 1));
		_menLoadState->Append(ID_Main_File_LoadState + i, wxString::Format(_("Load State %d\tCtrl+F%d"), i + 1, i + 1));
	}
	UpdateSaveStateLabels();
	Bind(wxEVT_MENU, &MainFrame::OnSaveState, this, ID_Main_File_SaveState, ID_Main_File_SaveState + 9);
	Bind(wxEVT_MENU, &MainFrame::OnLoadState, this, ID_Main_File_LoadState, ID_Main_File_LoadState + 9);

	// Create the File menu
	_menFile = new wxMenu;
	_menFile->Append(ID_Main_File_open, _("&Open\tCtrl+O"));
	_menFile->Append(ID_Main_File_run, _("&Run\tCtrl+R"));
	_menFile->Append(ID_Main_File_reset, _("Reset"));
	_menFile->AppendSeparator();
	_menFile->AppendSubMenu(_menSaveState, _("&Save state"));
	_menFile->AppendSubMenu(_menLoadState, _("&Load state"));
	_menFile->AppendSeparator();
	for (int i = 0; i < 5; i++) {
		_menFile->Append(ID_Main_File_RecentFile + i, wxString::Format(_("%d. ---"), i + 1));
	}
	Bind(wxEVT_MENU, &MainFrame::OnOpenRecentFile, this, ID_Main_File_RecentFile, ID_Main_File_RecentFile + 4);
	UpdateRecentFiles();
	_menFile->AppendSeparator();
	_menFile->Append(ID_Main_File_quit, _("&Quit"));

	// Create video filter menu
	wxMenu *videoFilterMenu = new wxMenu;
	videoFilterMenu->AppendRadioItem(ID_Main_Options_Filter + 0, _("Nearest"));
	videoFilterMenu->AppendRadioItem(ID_Main_Options_Filter + 1, _("Bi-Linear"));
	videoFilterMenu->AppendRadioItem(ID_Main_Options_Filter + 2, _("Bi-Cubic triangular"));
	videoFilterMenu->AppendRadioItem(ID_Main_Options_Filter + 3, _("Bi-Cubic Bell"));
	videoFilterMenu->AppendRadioItem(ID_Main_Options_Filter + 4, _("Bi-Cubic B-Spline"));
	videoFilterMenu->AppendRadioItem(ID_Main_Options_Filter + 5, _("Bi-Cubic CatMull-Rom"));
	int filterID = Options::GetSingleton().videoOptions.filter;
	if (filterID >= 5 || filterID < 0) {
		filterID = 0;
	}
	videoFilterMenu->Check(ID_Main_Options_Filter + filterID, true);

	// Create the options menu
	_menOptions = new wxMenu;
	_menOptions->AppendCheckItem(ID_Main_Options_KeepAspect, _("Keep aspect ratio"));
	_menOptions->Check(ID_Main_Options_KeepAspect, Options::GetSingleton().videoOptions.keepAspect);
	_menOptions->AppendSubMenu(videoFilterMenu, _("Video &filter"));
	_menOptions->Append(ID_Main_Options_input, _("&Input"));
	_menOptions->Append(ID_Main_Options_Emulator, _("&Emulator"));


	// Create the debug level menu
	_menDebugLevel = new wxMenu;
	_menDebugLevel->AppendRadioItem(ID_Main_Debug_level_fat, _("&Fatal"));
	_menDebugLevel->AppendRadioItem(ID_Main_Debug_level_err, _("&Error"));
	_menDebugLevel->AppendRadioItem(ID_Main_Debug_level_war, _("&Warning"));
	_menDebugLevel->AppendRadioItem(ID_Main_Debug_level_mes, _("&Message"))->Check(true);
	_menDebugLevel->AppendRadioItem(ID_Main_Debug_level_deb, _("&Debug"));

	// Create the debug menu
	_menDebug = new wxMenu;
	_menDebug->Append(ID_Main_Debug_cpu, _("&CPU"));
	_menDebug->Append(ID_Main_Debug_mem, _("&Memory"));
	_menDebug->Append(ID_Main_Debug_gpu, _("&GPU"));
	_menDebug->Append(ID_Main_Debug_window, _("&Debug"));
	_menDebug->AppendSubMenu(_menDebugLevel, _("Debug &Level"));

	// create the menubar
	_bar = new wxMenuBar;
	_bar->Append(_menFile, _("&File"));
	_bar->Append(_menOptions, _("&Options"));
	_bar->Append(_menDebug, _("&Debug"));

	SetMenuBar(_bar);
}

void MainFrame::LoadEmulator(const std::string &fileName, std::string emuId)
{
	Options &options = Options::GetSingleton();
	// Load the emulator
	auto &emulators = EmulatorList::Get();

	// Detect and create the emulator
	if(emuId.empty()) {
		emuId = emulators.GetMatchingEmulator(fileName);
	}
	if(emuId.empty()) {
		wxString msg =wxString::Format(_T("Could not find compatible Emulator for %s\n"), fileName.c_str());
		logger::Log(logger::Error, msg);
		return;
	}

	_emulator = emulators.CreateEmulator(emuId);

	if (!_emulator)
	{
		wxString msg =wxString::Format(_T("Could not find compatible Emulator for %s\n"), fileName.c_str());
		logger::Log(logger::Error, msg);
		return;
	}

	logger::Log(logger::Message, "Initializing the emulator");
	_filePath = fileName;

	// Loading the callback functions
	_emulator->SetDrawFrameHandler(DrawFrameEmulatorCallback);
	_emulator->GetOptionIntHandler = [options](const std::string &emuId, int optionId) -> int32_t {
		return options.GetEmulatorOptionInt(emuId, optionId);
	};
	_emulator->GetOptionStringHandler = [options](const std::string &emuId, int optionId) -> std::string {
		return options.GetEmulatorOptionString(emuId, optionId);
	};
	_emulator->GetOptionFloatHandler = [options](const std::string &emuId, int optionId) -> float {
		return options.GetEmulatorOptionFloat(emuId, optionId);
	};
	_emulator->ExitEmulatorHandler = [this](uint32_t exitCode) {
		_emulator->Run(false);
		logger::Log(logger::Error, "Emulator exited with exit code %u", exitCode);
	};

	// set current context
	_display->SetCurrentContext();

	// Initializing the emulator
	if (!_emulator->Init())
	{
		logger::Log(logger::Error, "Could not initialize the emulator");
		_emulator.reset();
		return;
	}

	EmulatorInfo_t info = _emulator->GetInfo();
	_screenAutoRefresh = info.screenAutoRefresh;

	try {
		// Init the display
		assert(info.screens.size() <= 1);
		if (info.screens.size() != 0 && info.screens[0].width > 0 && info.screens[0].height > 0) {
			_display->SetFrameBufferSize(info.screens[0].width, info.screens[0].height);
		}
		else {
			int w, h;
			_display->GetSize(&w, &h);
			_display->SetFrameBufferSize(w, h);
			_emulator->Reshape(_display->GetUserData(), w, h, options.videoOptions.keepAspect);
		}

		// Init the audio
		assert(info.numAudioStreams >= 0);
		_audioCBData.clear();
		if (info.numAudioStreams > 0) {
			_audioCBData.reserve(info.numAudioStreams);
			for (int i = 0; i < info.numAudioStreams; ++i) {
				_audioCBData.emplace_back(_emulator.get(), i);
				AudioStream stream(AudioBuffer::Stereo16, options.audioOptions.sampleRate,
					options.audioOptions.bufferSize, options.audioOptions.numBuffers,
					&MainFrame::AudioStreamCB, &_audioCBData.back());
				_audio.AddAudioStream(stream);
				_emulator->InitAudio(i, options.audioOptions.sampleRate, 2);
			}
			_audio.Play();
		}
	}
	catch (BaseException &e) {
		logger::Log(logger::Error, "%s\nStacktrace:\n%s", e.GetMsg(), e.GetStacktrace());
		_emulator.reset();
		return;
	}
	
	// Loading the rom in memory
	logger::Log(logger::Message, "Loading the ROM in memory");
	try {
		_emulator->LoadProgram(fileName);
	} catch(BaseException &ex) {
		wxString msg = wxString::Format("Could not load ROM: %s", ex.GetMsg());
		logger::Log(logger::Error, "%s", msg.ToStdString().c_str());
		_emulator.reset();
		return;
	}

	// Register for keybindings
	_inputHandler->RegisterClient(_emulator.get());

	// Load debuggers
	auto gdbInfo = _emulator->GetGdbInfo();
	_cpuDebugger->SetEmulator(_emulator.get());
	_memDebugger->SetEmulator(_emulator.get());
	_gpuDebugger->SetEmulator(_emulator.get());
	_gdbServer->SetExecutableFilename(fileName);
	_emulator->SetGdbServer(_gdbServer);

	_emulator->OnGdbAddBreakpointCB = std::bind(&MainFrame::OnGDBAction, this);
	_emulator->OnGdbRemoveBreakpointCB = std::bind(&MainFrame::OnGDBAction, this);
	_emulator->OnGdbStepCB = std::bind(&MainFrame::OnGDBAction, this);
	_emulator->OnGdbContinueCB = std::bind(&MainFrame::OnGDBAction, this);

	// Update save state labels
	Options::GetSingleton().SaveRecentFile(_filePath, _emulator->GetInfo().id);
	UpdateSaveStateLabels();
	UpdateRecentFiles();

	wxFileName fileNameInfo(_filePath);
	
	_display->ShowMessage(wxString::Format("Loaded: %s", fileNameInfo.GetFullName()));
	logger::Log(logger::Message, "Loaded: %s", fileNameInfo.GetFullName().ToStdString().c_str());

}

void MainFrame::CloseEmulator()
{
	// is a emulator running, yes then close it
	if (_emulator)
	{
		logger::Log(logger::Message, "Emulator closed");

		_cpuDebugger->SetEmulator(nullptr);
		_memDebugger->SetEmulator(nullptr);
		_gpuDebugger->SetEmulator(nullptr);
		_emulator->OnGdbAddBreakpointCB = nullptr;
		_emulator->OnGdbRemoveBreakpointCB = nullptr;
		_emulator->OnGdbStepCB = nullptr;
		_emulator->OnGdbContinueCB = nullptr;

		try {
			_audio.Pause();
			_audio.ClearAudioStreams();
			_audioCBData.clear();
			_display->DestroyGL();
			_inputHandler->RegisterClient(nullptr);
			_emulator->Save();
		}
		catch (BaseException &e) {
			wxString msg = wxString::Format("%s\nStacktrace:\n", e.GetMsg(), e.GetStacktrace());
			logger::Log(logger::Error, "%s", msg.ToStdString().c_str());
		}
		_emulator.reset();
	}
	Update();
}

void MainFrame::UpdateSaveStateLabels()
{
	for (int i = 0; i < 10; i++) {
		wxString filePath = _saveStateFilePath;
		filePath.Append(std::to_string(i));
		wxFileName file(filePath);
		wxString name;
		if (file.FileExists()) {
			wxDateTime date = file.GetModificationTime();
			name = wxString::Format(_("(%s)"),date.Format());
		}
		_menSaveState->SetLabel(ID_Main_File_SaveState + i, wxString::Format(_("Save State %d %s\tCtrl+Shift+F%d"), i + 1, name, i + 1));
		_menLoadState->SetLabel(ID_Main_File_LoadState + i, wxString::Format(_("Load State %d %s\tCtrl+F%d"), i + 1, name, i + 1));
	}
}

void MainFrame::UpdateRecentFiles()
{
	const int maxLength = 35;
	Options &options = Options::GetSingleton();
	for (int i = 0; i < MAX_RECENT_FILES; i++) {
		const std::string &file = options.GetRecentFilePath(i);
		wxString name("---");
		if (!file.empty()) {
			wxFileName fileName(file);
			wxString path = fileName.GetFullPath();
			if (path.length() > maxLength) {
				wxString base = fileName.GetVolume() + wxFileName::GetVolumeSeparator() + wxFileName::GetPathSeparator() + 
					".." + wxFileName::GetPathSeparator();
				const wxArrayString &dirs = fileName.GetDirs();
				unsigned numDirs = 0;
				int len = base.length() + fileName.GetFullName().length();
				while(len < maxLength && numDirs < dirs.size()) {
					len += 1 + dirs[dirs.size() - 1 - numDirs].length();
					numDirs++;
				}
				name = base;
				for (unsigned j = dirs.size() - numDirs + 1; j < dirs.size(); j++) {
					name += dirs[j] + wxFileName::GetPathSeparator();
				}
				name += fileName.GetFullName();
			}
		}
		_menFile->SetLabel(ID_Main_File_RecentFile + i, wxString::Format(_("%d. %s"), i+1, name));
	}
}

void MainFrame::RunEmulator(uint32_t deltaTime)
{
	static wxStopWatch sw;
	sw.Start(0);
	_inputHandler->Tick(deltaTime);
	if (_emulator)
	{
		if (_emulator->Tick(deltaTime)) // GUI update necessary
		{
			Update();
		}
	}
	try {
		_audio.Tick();
		if (_screenAutoRefresh) {
			_display->Refresh();
		}
		else if(_emulator && _emulator->IsInitialized() && !_emulator->IsRunning()){
			_display->DrawGUI();
		}
	}
	catch (BaseException &e) {
		logger::Log(logger::Error, "BaseException caught: %s\nStacktrace:\n%s", e.GetMsg(), e.GetStacktrace());
	}
}

void MainFrame::OnIdle(wxIdleEvent &evt)
{
	uint32_t deltaTime = _deltaTimeTimer.TimeInMicro().GetLo();
	// Limit the amount of time simulated
	if (deltaTime > 30000)
		deltaTime = 30000;

	_deltaTimeTimer.Start(0);
	RunEmulator(deltaTime);
	uint32_t emuTime = _deltaTimeTimer.TimeInMicro().GetLo();
	if (emuTime < 10000) {
		// Yield to reduce CPU usage
#ifdef _WIN32
		Sleep((10000-emuTime)/1000);
#else
		usleep((10000-emuTime));
#endif
	}
	evt.RequestMore();
}

void MainFrame::Update()
{
	if (_cpuDebugger != NULL)
		_cpuDebugger->Update();
	if (_memDebugger != NULL)
		_memDebugger->Update();
	if (_gpuDebugger != NULL)
		_gpuDebugger->Update();
	if (_emulator && _emulator->IsRunning() != 0) {
		_menFile->SetLabel(ID_Main_File_run, _("&Pause\tCtrl+R"));
	}
	else {
		_menFile->SetLabel(ID_Main_File_run, _("&Run\tCtrl+R"));
	}
}

void MainFrame::DrawNow(int id)
{
	_display->Refresh();
}

void MainFrame::OnClose(wxCloseEvent &evt)
{
	// Event when the frame closes
	CloseEmulator();
	if(_timer != NULL)
	{
		_timer->Stop();
		delete _timer;
		_timer = NULL;
	}

	Options::GetSingleton().SaveOptions();

	// Destroy the window
	Destroy();
}

void MainFrame::OnQuit(wxCommandEvent &evt)
{
	// Event when clicked on quit button
	Close(true);
}

void MainFrame::OnOpen(wxCommandEvent &evt)
{
	auto &emulators = EmulatorList::Get();

	// Filter
	std::vector<std::string> emuFilterStrs = emulators.GetEmulatorFileFilters();
	std::string combinedStrs = emulators.GetCombinedEmulatorFileFilter();

	std::string filterStr = combinedStrs;
	for(auto filter : emuFilterStrs) {
		filterStr.push_back('|');
		filterStr.append(filter);
	}
	filterStr.append("|All files(*.*)|*.*");
	// Open the file dialog
	wxFileDialog openFileDialog(this, _("Open rom file"), wxEmptyString,
								wxEmptyString, filterStr,
								wxFD_OPEN|wxFD_FILE_MUST_EXIST);

	// Check if a file was choosen
	if(openFileDialog.ShowModal() == wxID_CANCEL)
	{
		logger::Log(logger::Debug, "Loading canceled");
		return;
	}
	CloseEmulator();

	// Check if the file exists
	std::string file = openFileDialog.GetPath().ToStdString();
	std::string emuId;
	int idx = openFileDialog.GetFilterIndex() - 1;
	if(idx < 0 || idx >= emuFilterStrs.size()) {
		emuId = "";
	} else {
		emuId = emulators.GetEmulatorIdFromIdx(idx);
	}

	// Load the file
	LoadEmulator(file, emuId);
}

void MainFrame::OnOpenRecentFile(wxCommandEvent &evt)
{
	int idx = evt.GetId() - ID_Main_File_RecentFile;
	if (idx < 0 || idx >= MAX_RECENT_FILES) return;

	const std::string &file = Options::GetSingleton().GetRecentFilePath(idx);
	const std::string &emuId = Options::GetSingleton().GetRecentFileEmulator(idx);
	if(file.empty()) return;

	CloseEmulator();
	LoadEmulator(file, emuId);
}

void MainFrame::OnRun(wxCommandEvent &evt)
{
	if (_emulator)
	{
		if (_emulator->IsRunning()) {
			_display->ShowMessage(_("Emulation paused"));
			_emulator->Run(false);
		}
		else {
			_display->ShowMessage(_("Emulation started"));
			_emulator->Run(true);
		}
	}
	else {
		_display->ShowMessage(_("No ROM loaded"));
	}
	Update();
}

void MainFrame::OnSaveState(wxCommandEvent &evt)
{
	if (evt.GetEventType() == wxEVT_MENU) {
		int state = evt.GetId() - ID_Main_File_SaveState;
		if (state < 0 || state >= 10) {
			logger::Log(logger::Warn, "Invalid save state event received");
			return;
		}

		// Save the state
		SaveData_t saveData;
		saveData.romData = saveData.ramData = saveData.miscData = NULL;
		saveData.romDataLen = saveData.ramDataLen = saveData.miscDataLen = 0;
		_emulator->SaveState(&saveData);

		// Write it to an file
		wxString fileName = _saveStateFilePath;
		fileName.Append(std::to_string(state));
		SaveFile::WriteStateFile(fileName.ToStdString(), saveData);
		_display->ShowMessage(wxString::Format("Save state %d", state+1));
	}
	UpdateSaveStateLabels();
}

void MainFrame::OnLoadState(wxCommandEvent &evt)
{
	if (evt.GetEventType() == wxEVT_MENU) {
		int state = evt.GetId() - ID_Main_File_LoadState;
		if (state < 0 || state >= 10) {
			logger::Log(logger::Warn, "Invalid save state event received");
			return;
		}

		// Save the state
		SaveData_t saveData;

		// Read state from an file
		wxString fileName = _saveStateFilePath;
		fileName.Append(std::to_string(state));
		SaveFile::ReadStateFile(fileName.ToStdString(), saveData);

		_emulator->LoadState(&saveData);
		_display->ShowMessage(wxString::Format("load state %d", state+1));
		Update();
	}
}

void MainFrame::OnLogWindow(wxCommandEvent &evt)
{
	_logger->Show(true);
}

void MainFrame::OnLogLevel(wxCommandEvent &evt)
{
	switch(evt.GetId())
	{
	case ID_Main_Debug_level_fat:
		logger::SetLogLevel(logger::Fatal);
		_logger->SetLogLevel(wxLOG_FatalError);
		break;
	case ID_Main_Debug_level_err:
		logger::SetLogLevel(logger::Error);
		_logger->SetLogLevel(wxLOG_Error);
		break;
	case ID_Main_Debug_level_war:
		logger::SetLogLevel(logger::Warn);
		_logger->SetLogLevel(wxLOG_Warning);
		break;
	default:
	case ID_Main_Debug_level_mes:
		logger::SetLogLevel(logger::Message);
		_logger->SetLogLevel(wxLOG_Message);
		break;
	case ID_Main_Debug_level_deb:
		logger::SetLogLevel(logger::Debug);
		_logger->SetLogLevel(wxLOG_Debug);
		break;
	}
}

void MainFrame::OnTimer(wxTimerEvent &evt)
{
	uint32_t deltaTime = _deltaTimeTimer.TimeInMicro().GetLo();
	// Limit the amount of time simulated
	if (deltaTime > 30000)
		deltaTime = 30000;

	_deltaTimeTimer.Start(0);
	RunEmulator(deltaTime);
}

void MainFrame::OnReset(wxCommandEvent &evt)
{
	if(_filePath.empty() || !_emulator)
	{
		OnOpen(evt);
		return;
	}
	std::string emuId = _emulator->GetInfo().id;
	// Close the emulator
	CloseEmulator();

	// Load the file
	LoadEmulator(_filePath, emuId);
	_display->ShowMessage("Emulator reset");
}

void MainFrame::OnOptions(wxCommandEvent &evt)
{
	switch (evt.GetId())
	{
	case ID_Main_Options_KeepAspect:
		Options::GetSingleton().videoOptions.keepAspect = evt.IsChecked();
		if (_emulator)
		{
			int width, height;
			_display->GetSize(&width, &height);
			_emulator->Reshape(_display->GetUserData(), width, height, evt.IsChecked());
		}
		break;
	case ID_Main_Options_input:
		_inputOptionsFrame->Show();
		break;
	case ID_Main_Options_Emulator:
		_emulatorOptionsFrame->Show();
		break;
	}
}

void MainFrame::OnDebugCPU(wxCommandEvent &evt)
{
	_cpuDebugger->Show();
}

void MainFrame::OnDebugMEM(wxCommandEvent &evt)
{
	_memDebugger->Show();
}

void MainFrame::OnDebugGPU(wxCommandEvent &evt)
{
	_gpuDebugger->Show();
}

void MainFrame::OnOptionVideoFilter(wxCommandEvent &evt)
{
	EmulatorScreen::Filter filter;
	switch (evt.GetId()) {
	default:
	case ID_Main_Options_Filter + 0: // nearest
		filter = EmulatorScreen::Nearest;
		break;
	case ID_Main_Options_Filter + 1: // Bilinear
		filter = EmulatorScreen::BiLinear;
		break;
	case ID_Main_Options_Filter + 2: // bicubic triangular
		filter = EmulatorScreen::BicubicTriangular;
		break;
	case ID_Main_Options_Filter + 3: // bicubic bell
		filter = EmulatorScreen::BicubicBell;
		break;
	case ID_Main_Options_Filter + 4: // bicubic bspline
		filter = EmulatorScreen::BicubicBSpline;
		break;
	case ID_Main_Options_Filter + 5: // bicubic catmull-rom
		filter = EmulatorScreen::BicubicCatMullRom;
		break;
	}
	_display->SetPostProcessingFilter(filter);
}

void MainFrame::OnGDBAction()
{
	if (_cpuDebugger != NULL && _cpuDebugger->IsShownOnScreen())
		_cpuDebugger->Update();
	if (_memDebugger != NULL && _memDebugger->IsShownOnScreen())
		_memDebugger->Update();
	if (_gpuDebugger != NULL && _gpuDebugger->IsShownOnScreen())
		_gpuDebugger->Update();
}

void MainFrame::OnResize(wxSizeEvent &evt)
{
	if (_emulator && _display != NULL)
	{
		int width, height;
		_display->GetSize(&width, &height);
		_emulator->Reshape(_display->GetUserData(), width, height, Options::GetSingleton().videoOptions.keepAspect);
	}
	if (_display != NULL && !_emulator)
	{
		_display->Refresh();
	}
}

bool MainFrame::InitGL(int user)
{
	if (_emulator && _emulator->IsInitialized())
	{
		return _emulator->InitGL(user);
	}
	else {
		return false;
	}
}

void MainFrame::DestroyGL(int user)
{
	if (_emulator && _emulator->IsInitialized())
	{
		_emulator->DestroyGL(user);
	}
}

void MainFrame::DrawGL(int user)
{
	if (_emulator && _emulator->IsInitialized())
	{
		_emulator->Draw(user);
	}
	else {
		glClear(GL_COLOR_BUFFER_BIT);
	}
}

void MainFrame::AudioStreamCB(AudioBuffer::Format format, int freq, int elements, void *data, void *user)
{
	short *dataPtr = reinterpret_cast<short *>(data);
	MainFrame::AudioCallbackData *cbData = reinterpret_cast<MainFrame::AudioCallbackData *>(user);
	(void)freq;

	if (cbData != nullptr && cbData->emulator && cbData->emulator->IsRunning() != 0) {
		cbData->emulator->GetAudio(cbData->id, dataPtr, elements);
	}
	else {
		if (format == AudioBuffer::Stereo16 || AudioBuffer::Stereo8)
			elements *= 2;
		memset(dataPtr, 0, elements * sizeof(short));
	}
}

