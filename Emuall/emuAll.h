#ifndef _EMUALL_H
#define _EMUALL_H 0

#include <wx/wx.h>
#include "gui/MainFrame.h"

class EmuAll: public wxApp
{
public:
	bool OnInit() override;
	int OnExit() override;
	bool OnExceptionInMainLoop() override;
	void OnInitCmdLine(wxCmdLineParser &parser) override;
	bool OnCmdLineParsed(wxCmdLineParser &parser) override;

	static MainFrame *GetMainFrame();
private:
	static MainFrame *_mainFrame;

	wxString _imageFile;
	wxString _emulatorId;

	void ListEmulators();
	void OnTopShown(wxShowEvent &evt);
};

#endif