
#include <iostream>
#include <chrono>
#include <boost/program_options.hpp>

#include <emuall/emulator/EmulatorList.h>
#include <emuall/emulator/EmulatorInterface.h>
#include <emuall/emulator/Emulator.h>
#include <emuall/util/log.h>
#include <emuall/exception.h>

#ifdef _MSC_VER
#include <Windows.h>
#endif

namespace po = boost::program_options;
static int exitCode = 0;

static po::variables_map ParseOptions(int argc, const char **argv)
{
	po::options_description desc("Allowed options");
	desc.add_options()
		("help", "Show this help message")
		("list-emulators", "Show a list of available emulators")
		("emulator", po::value<std::string>(), "Selected emulator")
		("input", po::value<std::string>(), "Selected input file")
		("timeout", po::value<int>()->default_value(-1), "Timeout in seconds")
		("terminate-addr", po::value<std::vector<std::string>>(), "Address when hit will terminate the execution")
		("savestate-on-exit", po::value<std::string>(), "Save state on exit to file")
	;
	po::options_description options;
	options.add(desc);

	auto &list = EmulatorList::Get();
	for(auto emu : list) {
		auto emuOptions = emu->GetEmulatorOptions();
		if(emuOptions.empty()) continue;
		po::options_description emu_desc(emu->GetInfo().name + " options");
		std::string emuId = emu->GetInfo().id;
		for(auto &option : emuOptions) {
			if(option.shortname.empty()) continue;
			std::string optionKey = emuId + "." + option.shortname;
			switch(option.type) {
				case EmulatorOption_t::Boolean:
					emu_desc.add_options()(optionKey.c_str(), po::value<bool>(), option.description.c_str());
					break;
				case EmulatorOption_t::Integer:
				case EmulatorOption_t::Enumeration:
					emu_desc.add_options()(optionKey.c_str(), po::value<int>(), option.description.c_str());
					break;
				case EmulatorOption_t::Float:
					emu_desc.add_options()(optionKey.c_str(), po::value<float>(), option.description.c_str());
					break;
				case EmulatorOption_t::File:
				case EmulatorOption_t::Path:
					emu_desc.add_options()(optionKey.c_str(), po::value<std::string>(), option.description.c_str());
					break;
			}
		}
		options.add(emu_desc);
	}

	po::positional_options_description pd;
	pd.add("input", 1);

	po::variables_map vm;
	try {
		po::store(po::command_line_parser(argc, argv).options(options).positional(pd).run(), vm);
	}
	catch (po::error &e) {
		std::cerr << e.what() << std::endl;
		exit(1);
	}
	po::notify(vm);

	if (vm.count("help") || vm.size() <= 1) {
		std::cout << options << "\n";
		exit(0);
	}

	return std::move(vm);
}

static void ShowEmulatorsList()
{
	const EmulatorList &list = EmulatorList::Get();
	std::cout << "Available emulators:" << std::endl;
	for (auto emu : list)
	{
		EmulatorInfo_t info = emu->GetInfo();
		std::cout << info.id << ": " << emu->GetName() << std::endl;
		std::cout << "\tDescription: " << info.description << std::endl;
		std::cout << "\tAbout: " << info.aboutInfo << std::endl << std::endl;
	}
}

static void InitEmulator(Emulator *emu, const po::variables_map &options)
{
	logger::Log(logger::Message, "Initializing the emulator");
	std::string thisEmuId = emu->GetInfo().id;
	auto optionsList = emu->GetEmulatorOptions();
	emu->GetOptionIntHandler = [optionsList, thisEmuId, &options](const std::string &emuId, int optionId) -> int32_t {
		if(thisEmuId != emuId) return 0;
		auto optionIt = std::find_if(optionsList.begin(), optionsList.end(), [optionId](const EmulatorOption_t &option)->bool{
			return option.id == optionId;
			});
		if(optionIt == optionsList.end()) return 0;
		std::string key = thisEmuId + "." + optionIt->shortname;
		auto valueIt = options.find(key);
		if(valueIt == options.end()) return optionIt->defaultValueInt;
		return valueIt->second.as<int32_t>();
	};
	emu->GetOptionFloatHandler = [optionsList, thisEmuId, &options](const std::string &emuId, int optionId) -> float {
		if(thisEmuId != emuId) return 0;
		auto optionIt = std::find_if(optionsList.begin(), optionsList.end(), [optionId](const EmulatorOption_t &option)->bool{
			return option.id == optionId;
		});
		if(optionIt == optionsList.end()) return 0;
		std::string key = thisEmuId + "." + optionIt->shortname;
		auto valueIt = options.find(key);
		if(valueIt == options.end()) return optionIt->defaultValueFloat;
		return valueIt->second.as<float>();
	};
	emu->GetOptionStringHandler = [optionsList, thisEmuId, &options](const std::string &emuId, int optionId) -> std::string {
		if(thisEmuId != emuId) return "";
		auto optionIt = std::find_if(optionsList.begin(), optionsList.end(), [optionId](const EmulatorOption_t &option)->bool{
			return option.id == optionId;
		});
		if(optionIt == optionsList.end()) return "";
		std::string key = thisEmuId + "." + optionIt->shortname;
		auto valueIt = options.find(key);
		if(valueIt == options.end()) return optionIt->defaultValueString;
		return valueIt->second.as<std::string>();
	};
	emu->ExitEmulatorHandler = [emu](uint32_t exitCode) {
		emu->Run(0);
		::exitCode = exitCode;
	};
	if (!emu->Init())
		throw BaseException("Could not initialize the Emulator");
}

static void SetTerminationBreakpoints(Emulator *emu, const std::vector<uint32_t> &breakpoints)
{
	std::for_each(breakpoints.begin(), breakpoints.end(), 
		[emu](uint32_t addr) { emu->AddBreakpoint(addr); });
}

static void LoadROM(Emulator *emu, const std::string &romFile)
{
	logger::Log(logger::Message, "Loading the ROM");
	emu->LoadProgram(romFile);
}

static void RunEmulator(Emulator *emu, int timeout) 
{
	logger::Log(logger::Message, "Starting the emulator");
	using timer = std::chrono::high_resolution_clock;
	auto t1 = timer::now();
	auto endTime = t1 + std::chrono::seconds(timeout);
	emu->Run(1);
	while ((timeout < 0 || t1 < endTime) && emu->IsRunning()) {
		auto t2 = timer::now();
		auto delta = std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1);
		t1 = t2;
		emu->Tick(static_cast<uint32_t>(delta.count()));
	}
}

static void FreeEmulator(Emulator *emu)
{
	logger::Log(logger::Message, "Releasing the emulator");
	(void)emu;
}

int main(int argc, const char **argv)
{
	std::cout << "Emuall command line emulator" << std::endl;
	logger::InitLog();
	logger::AddLogger([](enum logger::loglevel level, const char *msg) {
		std::cout << msg << std::endl;
	});

#ifdef _WIN32
	CoInitializeEx(NULL, COINIT_MULTITHREADED);
#endif

	// Read the command line options
	auto options = ParseOptions(argc, argv);

	if (options.count("list-emulators")) {
		ShowEmulatorsList();
		exit(0);
	}

	auto inputIt = options.find("input");
	if (inputIt != options.end()) {
		auto inputFile = inputIt->second.as<std::string>();
		const EmulatorList &list = EmulatorList::Get();
		auto emuIt = options.find("emulator");
		std::string emuId = emuIt == options.end() ? 
			list.GetMatchingEmulator(inputFile) : emuIt->second.as<std::string>();
		if (emuId.empty()) {
			logger::Log(logger::Error, "Error could not find matching emulator");
			exit(1);
		}

		int timeoutSeconds = options.at("timeout").as<int>();

		auto emulator = list.CreateEmulator(emuId);
		if (!emulator) {
			logger::Log(logger::Error, "Error could not create an emulator with id %s", emuId.c_str());
			exit(1);
		}
		try {
			InitEmulator(emulator.get(), options);
			LoadROM(emulator.get(), inputFile);
			if (options.count("terminate-addr")) {
				auto &breakpointsStrings = options.at("terminate-addr").as<std::vector<std::string>>();
				std::vector<uint32_t> breakpoints;
				try {
					for (auto &str : breakpointsStrings)
					{
						auto addr = std::stoul(str, nullptr, 0);
						breakpoints.push_back(addr);
					}
					SetTerminationBreakpoints(emulator.get(), breakpoints);
				}
				catch (std::invalid_argument) {
					logger::Log(logger::Error, "Could not parse termination addr");
					exit(1);
				}
			}
			RunEmulator(emulator.get(), timeoutSeconds);
			if (options.count("savestate-on-exit")) {
				auto savestateFileName = options.at("savestate-on-exit").as<std::string>();
				logger::Log(logger::Message, "Saving state in \"%s\"", savestateFileName.c_str());
				if (!emulator->SaveState(savestateFileName)) {
					logger::Log(logger::Warn, "Saving state failed");
				}
			}
			FreeEmulator(emulator.get());
		}
		catch (BaseException &e) {
			logger::Log(logger::Error, "Uncaught exception: %s\n%s", e.GetMsg(), e.GetStacktrace());
			exit(1);
		}
	}

#ifdef _WIN32
	CoUninitialize();
	if (IsDebuggerPresent()) {
		system("pause");
	}
#endif
	return ::exitCode;
}