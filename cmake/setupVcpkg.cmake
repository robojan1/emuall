if(NO_VCPKG)
	# If we don't want to use VCPKG do an early return
	return()
endif()

set(_vcpkg_initialized OFF CACHE BOOL "")

if(_vcpkg_initialized)
	# We already have vcpkg we don't have to reconfigure it
	return()
endif()

# Find VCPKG location
if(DEFINED ENV{VCPKG_ROOT})
	message(STATUS "VCPKG should be at $ENV{VCPKG_ROOT}")
	set(vcpkg_root $ENV{VCPKG_ROOT})
else()
	message(FATAL "No system vcpkg found, installing a local instance")
	include(FetchContent)

	if(WIN32)
		set(vcpkg_bootstrap_script "call bootstrap-vcpkg.bat")
	else()
		set(vcpkg_bootstrap_script "./bootstrap-vcpkg.sh")
	endif()

	FetchContent_Declare(
		vcpkg
		GIT_REPOSITORY http://github.com/microsoft/vcpkg.git
		GIT_TAG master
		CONFIGURE_COMMAND ""
		BUILD_COMMAND "${vcpkg_bootstrap_script}"
		USES_TERMINAL_BUILD ON
	)
	FetchContent_MakeAvailable(vcpkg)

	set(vcpkg_root ${vcpkg_SOURCE_DIR})
endif()

if(NOT EXISTS ${vcpkg_root}/.vcpkg-root)
	message(FATAL "Could not find VCPKG root in ${vcpkg_root}")
endif()

set(ENV{VCPKG_ROOT} ${vcpkg_root})

set(vcpkg_toolchain_file "${vcpkg_root}/scripts/buildsystems/vcpkg.cmake")
cmake_path(NORMAL_PATH vcpkg_toolchain_file)

if(CMAKE_TOOLCHAIN_FILE)
	cmake_path(NORMAL_PATH CMAKE_TOOLCHAIN_FILE)
	if(NOT CMAKE_TOOLCHAIN_FILE STREQUAL vcpkg_toolchain_file)
		set(VCPKG_CHAINLOAD_TOOLCHAIN_FILE ${CMAKE_TOOLCHAIN_FILE} CACHE PATH "")
	endif()
endif()

set(CMAKE_TOOLCHAIN_FILE ${vcpkg_toolchain_file} CACHE PATH "")
set(_vcpkg_initialized ON CACHE BOOL "")