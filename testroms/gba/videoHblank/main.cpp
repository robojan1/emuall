
#include <gba/video.h>
#include <gba/gba.h>
#include <gba/bios.h>
#include <gba/palette.h>
#include <gba/irq.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

DEFINE_HEADER_TITLE = {'V','I','D','E','O',' ','H','L','A','N','K',' '};
DEFINE_HEADER_CODE = {'B','H','B','P'};
DEFINE_HEADER_MAKER = {'R','J'};
DEFINE_HEADER_CHECKSUM;
DEFINE_GBA_CARTID(GBA_CARTID_SRAM);

constexpr gba::Tile<true> tileset[] = {
	gba::Tile<true>({
		0, 0, 0, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 0, 0, 0, 0,
	}),
	gba::Tile<true>({
		1, 1, 1, 1, 1, 1, 1, 1,
		1, 1, 1, 1, 1, 1, 1, 1,
		1, 1, 1, 1, 1, 1, 1, 1,
		1, 1, 1, 1, 1, 1, 1, 1,
		1, 1, 1, 1, 1, 1, 1, 1,
		1, 1, 1, 1, 1, 1, 1, 1,
		1, 1, 1, 1, 1, 1, 1, 1,
		1, 1, 1, 1, 1, 1, 1, 1,
	}),
	gba::Tile<true>({
		2, 2, 2, 2, 2, 2, 2, 2,
		2, 2, 2, 2, 2, 2, 2, 2,
		2, 2, 2, 2, 2, 2, 2, 2,
		2, 2, 2, 2, 2, 2, 2, 2,
		2, 2, 2, 2, 2, 2, 2, 2,
		2, 2, 2, 2, 2, 2, 2, 2,
		2, 2, 2, 2, 2, 2, 2, 2,
		2, 2, 2, 2, 2, 2, 2, 2,
	}),
	gba::Tile<true>({
		3, 3, 3, 3, 3, 3, 3, 3,
		3, 3, 3, 3, 3, 3, 3, 3,
		3, 3, 3, 3, 3, 3, 3, 3,
		3, 3, 3, 3, 3, 3, 3, 3,
		3, 3, 3, 3, 3, 3, 3, 3,
		3, 3, 3, 3, 3, 3, 3, 3,
		3, 3, 3, 3, 3, 3, 3, 3,
		3, 3, 3, 3, 3, 3, 3, 3,
	}),
	gba::Tile<true>({
		4, 4, 4, 4, 4, 4, 4, 4,
		4, 4, 4, 4, 4, 4, 4, 4,
		4, 4, 4, 4, 4, 4, 4, 4,
		4, 4, 4, 4, 4, 4, 4, 4,
		4, 4, 4, 4, 4, 4, 4, 4,
		4, 4, 4, 4, 4, 4, 4, 4,
		4, 4, 4, 4, 4, 4, 4, 4,
		4, 4, 4, 4, 4, 4, 4, 4,
	}),
	gba::Tile<true>({
		5, 5, 5, 5, 5, 5, 5, 5,
		5, 5, 5, 5, 5, 5, 5, 5,
		5, 5, 5, 5, 5, 5, 5, 5,
		5, 5, 5, 5, 5, 5, 5, 5,
		5, 5, 5, 5, 5, 5, 5, 5,
		5, 5, 5, 5, 5, 5, 5, 5,
		5, 5, 5, 5, 5, 5, 5, 5,
		5, 5, 5, 5, 5, 5, 5, 5,
	}),
	gba::Tile<true>({
		6, 6, 6, 6, 6, 6, 6, 6,
		6, 6, 6, 6, 6, 6, 6, 6,
		6, 6, 6, 6, 6, 6, 6, 6,
		6, 6, 6, 6, 6, 6, 6, 6,
		6, 6, 6, 6, 6, 6, 6, 6,
		6, 6, 6, 6, 6, 6, 6, 6,
		6, 6, 6, 6, 6, 6, 6, 6,
		6, 6, 6, 6, 6, 6, 6, 6,
	}),
};

using bg0 = gba::Background0<gba::BackgroundVRAM<gba::VRAM_TILE_BASE_0000, gba::VRAM_MAP_BASE_4000, true, gba::BG_SIZE_512_512, true>>;
using bg1 = gba::Background1<gba::BackgroundVRAM<gba::VRAM_TILE_BASE_0000, gba::VRAM_MAP_BASE_6000, true, gba::BG_SIZE_256_256, true>>;
using bg2 = gba::Background2<gba::BackgroundVRAM<gba::VRAM_TILE_BASE_0000, gba::VRAM_MAP_BASE_6800, true, gba::BG_SIZE_256_256, true>>;
void videoInitMode0() {
	VBlankIntrWait();

	// Create palette
	auto &bgPalette = gba::BgPalette::Singleton();
	bgPalette.SetSubPalette(0, gba::BgPalette::SubPaletteData{
		gba::Color{0xFF, 0xFF, 0xFF},
		gba::Color{0x00, 0x00, 0x00},
		gba::Color{0xFF, 0x00, 0x00},
		gba::Color{0x00, 0xFF, 0x00},
		gba::Color{0x00, 0x00, 0xFF},
		gba::Color{0xFF, 0x00, 0xFF},
		gba::Color{0xFF, 0xFF, 0x00},
	});

	bg0::Tiles tiles;
	for(int i = 0; i < 7; i++) {
		tiles[i] = tileset[i];
	}

	// create maps
	bg0::Map map0;
	bg1::Map map1;
	bg2::Map map2;
	for(int y = 0; y < 32; y++) {
		for(int x = 0; x < 32; x++) {
			// Checkerboard
			map0(0,x,y) = gba::TextMapEntry(((x ^ y) & 1) != 0 ? 0 : 1, false, false, 0);
			map0(1,x,y) = gba::TextMapEntry((((x ^ y) & 1) != 0) ? 0 : 4, false, false, 0);
			map0(2,x,y) = gba::TextMapEntry((((x ^ y) & 1) != 0) ? 0 : 5, false, false, 0);
			map0(3,x,y) = gba::TextMapEntry((((x ^ y) & 1) != 0) ? 0 : 6, false, false, 0);
			map1(x,y) = gba::TextMapEntry((x < 4 && y < 4) ? 2 : 0);
			map2(x,y) = gba::TextMapEntry((x < 8 && y < 8) ? 3 : 0);
		}
	}

	bg0::Activate();
	bg1::Activate();
	bg2::Activate();

	bg2::SetOffset(128,192);
	bg1::SetOffset(-100, -50);
	bg0::SetOffset(-56, -50);
}

void hblankHandler() {
	int currentLine = gba::Video::CurrentLine();
	bg2::SetOffset(-currentLine, -currentLine);
	auto &palette = gba::BgPalette::Singleton();
	if((currentLine & 1) != 0) {
		palette[4] = gba::Color(0,0,255);
	} else {
		palette[4] = gba::Color(0,255, 0);
	}
}

//---------------------------------------------------------------------------------
// Program entry point
//---------------------------------------------------------------------------------
int main(void) {
//---------------------------------------------------------------------------------
	RegisterRamReset(gba::ResetFlag::Pallete | gba::ResetFlag::VRAM | gba::ResetFlag::OAM);

	// the vblank interrupt must be enabled for VBlankIntrWait() to work
	// since the default dispatcher handles the bios flags no vblank handler
	// is required
	gba::IRQ::Init();
	gba::IRQ::handlerHBlank = hblankHandler;
	gba::IRQ::EnableInterrupt(gba::IRQ_VBLANK | gba::IRQ_HBLANK);
	gba::Video::EnableInterrupt(gba::VIDEO_IRQ_VBLANK | gba::VIDEO_IRQ_HBLANK);

	videoInitMode0();

	gba::Video::Configure(gba::VIDEO_CTRL_MODE0 | gba::VIDEO_CTRL_BG0 | gba::VIDEO_CTRL_BG1 | gba::VIDEO_CTRL_BG2);

	while (1) {
		VBlankIntrWait();
	}
}


