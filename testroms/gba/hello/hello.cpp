
#include <stdio.h>
#include <stdlib.h>

#include <gba/gba.h>
#include <gba/emulator.h>

DEFINE_HEADER_TITLE = {'H','E','L','L','O',' ','W','O','R','L','D',' '};
DEFINE_HEADER_CODE = {'B','H','W','P'};
DEFINE_HEADER_MAKER = {'R','J'};
DEFINE_HEADER_CHECKSUM;
DEFINE_GBA_CARTID(GBA_CARTID_SRAM);


int main()
{
    fprintf(stderr, "Test error\n");
    fprintf(stdout, "Test output\n");
	return 0;
}
