#include <gba/video.h>
#include <gba/gba.h>
#include <gba/bios.h>
#include <gba/palette.h>
#include <gba/irq.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <gba/video.h>

DEFINE_HEADER_TITLE = {'V','I','D','E','O',' ','M','O','D','E',' ','3'};
DEFINE_HEADER_CODE = {'B','M','3','P'};
DEFINE_HEADER_MAKER = {'R','J'};
DEFINE_HEADER_CHECKSUM;
DEFINE_GBA_CARTID(GBA_CARTID_SRAM);

#include <grit/testImage.h>

void videoInitMode3() {
	VBlankIntrWait();

	gba::BackgroundMode3 bg;

	bg.SetFromBitmap(testImageBitmap, testImageBitmapLen);

	bg.SetRefPoint(-10 * 256, -10 * 256);
	bg.SetMatrix(128, 0, 0, 128);
}

//---------------------------------------------------------------------------------
// Program entry point
//---------------------------------------------------------------------------------
int main(void) {
//---------------------------------------------------------------------------------
    RegisterRamReset(gba::ResetFlag::Pallete | gba::ResetFlag::VRAM | gba::ResetFlag::OAM);

    gba::IRQ::Init();
    gba::IRQ::EnableInterrupt(gba::IRQ_VBLANK);
    gba::Video::EnableInterrupt(gba::VIDEO_IRQ_VBLANK);

    videoInitMode3();

    gba::Video::Configure(gba::VIDEO_CTRL_MODE3 | gba::VIDEO_CTRL_BG2);

    while (1) {
        VBlankIntrWait();
    }
}


