SET(TARGET_NAME TimerTest)

add_executable(${TARGET_NAME} main.cpp)
target_link_libraries(${TARGET_NAME} gba)
set_target_properties(${TARGET_NAME} PROPERTIES SUFFIX ".elf")
SET_TARGET_PROPERTIES(${TARGET_NAME} PROPERTIES LINK_FLAGS "-Wl,-Map=hello.map -Wl,--gc-sections" )

BUILD_ROM(${TARGET_NAME})