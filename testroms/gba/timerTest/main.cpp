
#include <stdio.h>
#include <stdlib.h>

#include <gba/gba.h>
#include <gba/emulator.h>
#include <gba/irq.h>
#include <gba/video.h>
#include <gba/timer.h>
#include <vector>

DEFINE_HEADER_TITLE = {'T','I','M','E','R',' ','T','E','S','T',' ',' '};
DEFINE_HEADER_CODE = {'B','T','T','P'};
DEFINE_HEADER_MAKER = {'R','J'};
DEFINE_HEADER_CHECKSUM;
DEFINE_GBA_CARTID(GBA_CARTID_SRAM);


constexpr gba::Tile<true> tileset[] = {
        gba::Tile<true>({
                0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0,
        }),
        gba::Tile<true>({
                0, 0, 0, 0, 0, 0, 0, 0,
                0, 1, 0, 0, 0, 0, 1, 0,
                0, 0, 1, 0, 0, 1, 0, 0,
                0, 0, 0, 1, 1, 0, 0, 0,
                0, 0, 0, 1, 1, 0, 0, 0,
                0, 0, 1, 0, 0, 1, 0, 0,
                0, 1, 0, 0, 0, 0, 1, 0,
                0, 0, 0, 0, 0, 0, 0, 0,
        }),
        gba::Tile<true>({
                0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 2,
                0, 0, 0, 0, 0, 0, 2, 0,
                0, 0, 0, 0, 0, 2, 0, 0,
                2, 0, 0, 0, 2, 0, 0, 0,
                0, 2, 0, 2, 0, 0, 0, 0,
                0, 0, 2, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0,
        }),
        gba::Tile<true>({
                0, 0, 3, 3, 3, 3, 0, 0,
                0, 3, 0, 0, 0, 0, 3, 0,
                3, 0, 0, 0, 0, 0, 0, 3,
                3, 0, 0, 0, 0, 0, 0, 3,
                3, 0, 0, 0, 0, 0, 0, 3,
                3, 0, 0, 0, 0, 0, 0, 3,
                0, 3, 0, 0, 0, 0, 3, 0,
                0, 0, 3, 3, 3, 3, 0, 0,
        }),
};

struct TestInfo {
    const char *name;
    std::function<bool()> test;
};

using bg0 = gba::Background0<gba::BackgroundVRAM<gba::VRAM_TILE_BASE_0000, gba::VRAM_MAP_BASE_4000, true, gba::BG_SIZE_256_256, true>>;


volatile bool timerOverflowOccured;
void TimerIRQHandler() {
    timerOverflowOccured = true;
}

// Test if the timer fires an interrupt
template <int TimerIdx>
bool TestTimerFunctional() {
    // Init
    gba::Timer<TimerIdx> timer;

    bg0::Map map;

    map(0,2) = {3};
    timer.EnableIRQ(true);
    timer.Stop();
    map(1,2) = {3};

    gba::IRQ::handlerTimer0 = TimerIRQHandler;
    gba::IRQ::handlerTimer1 = TimerIRQHandler;
    gba::IRQ::handlerTimer2 = TimerIRQHandler;
    gba::IRQ::handlerTimer3 = TimerIRQHandler;
    gba::IRQ::EnableInterrupt(gba::IRQ_TIMER0 | gba::IRQ_TIMER1 | gba::IRQ_TIMER2 | gba::IRQ_TIMER3);
    map(2,2) = {3};

    // Do the test
    // Wait until next vblank interrupt
    printf("Waiting until VBlank\n");
    map(2,3) = {3};
    VBlankIntrWait();
    map(3,2) = {3};
    timerOverflowOccured = false;
    timer.SetPeriodMs(1);
    timer.Start();

    map(4,2) = {3};
    printf("Waiting until VBlank or Timer\n");
    IntrWait(false, gba::IRQ_VBLANK | (gba::IRQ_TIMER0 << TimerIdx));
    printf("Cleanup\n");
    map(5,2) = {3};

    // Cleanup
    timer.EnableIRQ(false);
    timer.Stop();
    map(6,2) = {3};

    gba::IRQ::DisableInterrupt(gba::IRQ_TIMER0 | gba::IRQ_TIMER1 | gba::IRQ_TIMER2 | gba::IRQ_TIMER3);
    gba::IRQ::handlerTimer0 = nullptr;
    gba::IRQ::handlerTimer1 = nullptr;
    gba::IRQ::handlerTimer2 = nullptr;
    gba::IRQ::handlerTimer3 = nullptr;
    map(7,2) = {3};

    return timerOverflowOccured;
}

int main()
{
    RegisterRamReset(gba::ResetFlag::Pallete | gba::ResetFlag::VRAM | gba::ResetFlag::OAM);
    gba::IRQ::Init();
    gba::Video::EnableInterrupt(gba::VIDEO_IRQ_VBLANK);
    gba::IRQ::EnableInterrupt(gba::IRQ_VBLANK);

    printf("Timer test\n");
    printf("Current handler %p\n", gba_irq_handler);
    VBlankIntrWait();

    std::vector<TestInfo> tests;

    tests.push_back({"TestTimerFunctional 0", std::bind(TestTimerFunctional<0>)});
    tests.push_back({"TestTimerFunctional 1", std::bind(TestTimerFunctional<1>)});
    tests.push_back({"TestTimerFunctional 2", std::bind(TestTimerFunctional<2>)});
    tests.push_back({"TestTimerFunctional 3", std::bind(TestTimerFunctional<3>)});

    bg0::Tiles tiles;
    tiles[0] = tileset[0]; // Background
    tiles[1] = tileset[1]; // Error
    tiles[2] = tileset[2]; // Success
    tiles[3] = tileset[3]; // Pending
    auto &bgPalette = gba::BgPalette::Singleton();
    bgPalette[0] = gba::Color{0x00, 0x00, 0x00};
    bgPalette[1] = gba::Color{0xFF, 0x00, 0x00};
    bgPalette[2] = gba::Color{0x00, 0xFF, 0x00};
    bgPalette[3] = gba::Color{0x00, 0x00, 0xFF};

    bg0::Map map;
    for(int i = 0; i < tests.size(); i++) {
        map(i % 32, i / 32) = {3};
    }
    for(int i = tests.size(); i < 32*32; i++) {
        map(i % 32, i / 32) = {0};
    }
    bg0::SetOffset(0,0);
    bg0::Activate();
    gba::Video::Configure(gba::VIDEO_CTRL_BG0 | gba::VIDEO_CTRL_MODE0);

    int success = 0;
    int failed = 0;
    for(int i = 0; i < tests.size(); i++) {
        printf("Running test %d - %s\n", i, tests[i].name);
        bool result = tests[i].test();
        if(result) {
            success++;
            printf("\tPassed\n");
            map(i % 32, i / 32) = {2};
        } else {
            failed++;
            printf("\tFailed\n");
            map(i % 32, i / 32) = {1};
        }
    }

    printf("Test summary:\nSucceeded: %d\nFailed: %d\n", success, failed);
    if(failed == 0) {
        printf("All passed! :)\n");
    }

	return failed;
}
