
#include <stdio.h>
#include <stdlib.h>

#include <gba/gba.h>
#include <gba/flash.h>

DEFINE_HEADER_TITLE = {'D','U','M','P',' ','I','N','I','T',' ',' ',' '};
DEFINE_HEADER_CODE = {'B','D','I','P'};
DEFINE_HEADER_MAKER = {'R','J'};
DEFINE_HEADER_CHECKSUM;
DEFINE_GBA_CARTID(GBA_CARTID_FLASH_1M);

using namespace gba;

int main()
{
	flash::DeviceID id = flash::GetID();

	printf("Flash id: %02x %02x\n", id.manufacturer, id.device);

}
