

    .section .text
    .arm
    .global __do_global_ctors_aux
__do_global_ctors_aux:
    stmdb   sp!, {lr}
    ldr     r0, .L__CTOR_END
2:
    ldr     r1, [r0, #-0x04]!
    cmp     r1, r1
    beq     1f
    mov     lr, pc
    mov     pc, r1
    b       2b
1:  ldmia   sp!, {lr}
    bx      lr

.L__CTOR_END:
    .word   __CTOR_END__

    .section .init,"ax"
    .arm
    bl      __do_global_ctors_aux
    ldmia   sp!, {lr}
    bx      lr

    .section .fini,"ax"
    .global __fini_return__
    .arm
__fini_return__:
    ldmia   sp!, {lr}
    bx      lr


    .section .ctors,"a"
    //.align  4
__CTOR_END__:
    .word   0

    .section .dtors,"a"
    //.align  4
__DTOR_END__:
    .word   0
