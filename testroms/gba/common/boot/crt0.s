    .arch armv4t

    .text
    .syntax unified

    .macro FUNC_START name
    .arm
    .global \name
\name:
    .endm

/* The entry function */
    .align 0
    FUNC_START _start

    // Setup the FIQ stack
    msr     CPSR_c, 0xD1
    ldr     sp, .Lfiq_stack
    // Setup the abort stack
    msr     CPSR_c, 0xD7
    ldr     sp, .Labort_stack
    // Setup the undefined stack
    msr     CPSR_c, 0xDB
    ldr     sp, .Lund_stack
    // Setup the IRQ stack
    msr     CPSR_c, 0xD2
    ldr     sp, .Lirq_stack
    // Setup the supervisor stack
    msr     CPSR_c, 0xD3
    ldr     sp, .Lsvc_stack
    // Setup the system stack
    msr     CPSR_c, 0xDF
    ldr     sp, .Luser_stack

    // Setup the user stack without switching to user mode
    ldr     r3, .Luser_stack
    str     r3, [sp, -4]
    ldmdb   sp, {sp}^

    // Zero the memory and reset the system
    mov     r0, 0x00
    swi     0x10000

    // Call the constructors
    bl      __libc_init_array
    // Call main
    mov     r0, 0x00
    mov     r1, 0x00
    mov     r2, 0x00
    mov     r3, 0x00
    bl      main
    mov     r4, r0

    // Call the destructors
    bl      __libc_fini_array

    // Call the exit function
    mov     r0, r4
    bl      exit    // Should not return
    // Hard reset
    swi     0x260000

.Lsvc_stack:
    .word   __svc_stack
.Lfiq_stack:
    .word   __fiq_stack
.Labort_stack:
    .word   __abort_stack
.Lund_stack:
    .word   __und_stack
.Lirq_stack:
    .word   __irq_stack
.Luser_stack:
    .word   __user_stack
.Lfini:
    .word   __libc_fini_array

    .section .ctors,"a"
    .align  2
__CTOR_LIST__:
    .word   0

    .section .dtors,"a"
    .align  2
__DTOR_LIST__:
    .word   0

    .section .text
    .arm
    .global __do_global_dtors_aux
__do_global_dtors_aux:
    stmdb   sp!, {lr}
    ldr     r0, .L__DTOR_LIST
2:
    ldr     r1, [r0, #+0x04]!
    cmp     r1, r1
    beq     1f
    mov     lr, pc
    mov     pc, r1
    b       2b
1:  ldmia   sp!, {lr}
    bx      lr

.L__DTOR_LIST:
    .word   __DTOR_LIST__

__do_setup_data:
    ldr     r0, .L__data_start
    ldr     r1, .L__data_end
    ldr     r2, .L__data_load
1:  cmp     r0, r1
    beq     2f
    ldr     r3, [r2], #+0x04
    str     r3, [r0], #+0x04
    b       1b
2:
    bx      lr

.L__data_start:
    .word   __data_start
.L__data_end:
    .word   __data_end
.L__data_load:
    .word   __data_load


    .global _init
    .section .init,"ax"
_init:
    stmdb    sp!, {lr}
    bl      __do_setup_data

    .global _fini
    .section .fini,"ax"
_fini:
    stmdb    sp!, {lr}
    bl      __do_global_dtors_aux


    .section .data
    .global __dso_handle
__dso_handle:
    .word   0


