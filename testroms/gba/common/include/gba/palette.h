//
// Created by Robbert-Jan de Jager on 4-12-18.
//

#ifndef GBA_PALETTE_H
#define GBA_PALETTE_H

#include <stdint.h>
#include <assert.h>
#include <gba/memory.h>
#include <gba/bios.h>

namespace gba {

	class Color {
	public:
		Color() : _rgb(0) {}
		Color(uint8_t r, uint8_t g, uint8_t b) : _rgb(FromRGB(r,g,b)) { }
		explicit Color(uint32_t rgb32) : _rgb(FromRGB32(rgb32)) {}

	protected:
		static constexpr uint16_t FromRGB(uint8_t r, uint8_t g, uint8_t b) {
			return static_cast<uint16_t>(((r & 0xF8) >> 3) | ((g & 0xF8) << 2) | ((b & 0xF8) << 7));
		}
		static constexpr uint16_t FromRGB32(uint32_t rgb32)  {
			return static_cast<uint16_t>(((rgb32 & 0xF8) >> 3) | ((rgb32 & 0xf800) >> 6) | ((rgb32 & 0xF80000) >> 9));
		}
	private:
		uint16_t _rgb;
	};
	static_assert(sizeof(Color) == 2, "Unmatched color data size");

	template<bool Obj>
	class Palette {
	public:
		struct SubPaletteData {
			Color c[16];
		};
		union PaletteData {
			struct SubPaletteData lowDepth[16];
			Color highDepth[256];
		};
		static_assert(sizeof(union PaletteData) == 512, "Unmatched palette data size");

		static Palette &Singleton() {
			static Palette g_palette;
			return g_palette;
		}

		void SetSubPalette(int idx, const struct SubPaletteData &palette){
			assert(idx >= 0 && idx < 16);
			CpuFastSet(&palette, &_palette->lowDepth[idx], sizeof(SubPaletteData)/4);
		}

		Color &operator[](int idx) {
			assert(idx >= 0 && idx < 256);
			return _palette->highDepth[idx];
		}

	private:
		Register<PALETTE_BASE_ADDR + (Obj ? 0x200 : 0), union PaletteData> _palette;
	};

	using BgPalette = Palette<false>;
	using ObjPalette = Palette<true>;
}

#endif //GBA_PALETTE_H
