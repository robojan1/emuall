//
// Created by Robbert-Jan de Jager on 9-12-18.
//

#ifndef GBA_VIDEO_MAP_H
#define GBA_VIDEO_MAP_H

#include <stdint.h>
#include <type_traits>
#include <assert.h>
#include <tuple>

namespace gba {

	enum VramTileBase {
		VRAM_TILE_BASE_0000 = 0,
		VRAM_TILE_BASE_4000 = 1,
		VRAM_TILE_BASE_8000 = 2,
		VRAM_TILE_BASE_C000 = 3
	};
	enum VramMapBase {
		VRAM_MAP_BASE_0000 = 0,
		VRAM_MAP_BASE_0800 = 1,
		VRAM_MAP_BASE_1000 = 2,
		VRAM_MAP_BASE_1800 = 3,
		VRAM_MAP_BASE_2000 = 4,
		VRAM_MAP_BASE_2800 = 5,
		VRAM_MAP_BASE_3000 = 6,
		VRAM_MAP_BASE_3800 = 7,
		VRAM_MAP_BASE_4000 = 8,
		VRAM_MAP_BASE_4800 = 9,
		VRAM_MAP_BASE_5000 = 10,
		VRAM_MAP_BASE_5800 = 11,
		VRAM_MAP_BASE_6000 = 12,
		VRAM_MAP_BASE_6800 = 13,
		VRAM_MAP_BASE_7000 = 14,
		VRAM_MAP_BASE_7800 = 15,
		VRAM_MAP_BASE_8000 = 16,
		VRAM_MAP_BASE_8800 = 17,
		VRAM_MAP_BASE_9000 = 18,
		VRAM_MAP_BASE_9800 = 19,
		VRAM_MAP_BASE_A000 = 20,
		VRAM_MAP_BASE_A800 = 21,
		VRAM_MAP_BASE_B000 = 22,
		VRAM_MAP_BASE_B800 = 23,
		VRAM_MAP_BASE_C000 = 24,
		VRAM_MAP_BASE_C800 = 25,
		VRAM_MAP_BASE_D000 = 26,
		VRAM_MAP_BASE_D800 = 27,
		VRAM_MAP_BASE_E000 = 28,
		VRAM_MAP_BASE_E800 = 29,
		VRAM_MAP_BASE_F000 = 30,
		VRAM_MAP_BASE_F800 = 31
	};
	enum BackgroundSize {
		BG_SIZE_128_128,
		BG_SIZE_256_256,
		BG_SIZE_512_256,
		BG_SIZE_256_512,
		BG_SIZE_512_512,
		BG_SIZE_1024_1024
	};

	class TextMapEntry {
	public:
		TextMapEntry(uint16_t tile, bool hflip = false, bool yflip = false, uint8_t palette = 0)
		{
			_val = tile | (hflip ? 0x400 : 0) | (yflip ? 0x800 : 0) | palette << 12;
		}

		inline void Set(uint16_t tile, bool hflip = false, bool yflip = false, uint8_t palette = 0) {
			_val = tile | (hflip ? 0x400 : 0) | (yflip ? 0x800 : 0) | palette << 12;
		}

		inline uint16_t GetTile() const {
			return _val & 0x3FF;
		}
		inline bool IsHFlip() const {
			return (_val & 0x400) != 0;
		}
		inline bool IsVFlip() const {
			return (_val & 0x800) != 0;
		}
		inline int GetPalette() const {
			return (_val & 0xF000) >> 12;
		}
		inline void SetTile(uint16_t tile) {
			_val = (_val & 0xfc00) | tile;
		}
		inline void SetHFlip(bool flip) {
			if(flip) {
				_val |= 0x400;
			} else {
				_val &= ~0x400;
			}
		}
		inline void SetVFlip(bool flip) {
			if(flip) {
				_val |= 0x400;
			} else {
				_val &= ~0x400;
			}
		}
		inline void SetPalette(int palette) {
			_val = (_val & 0x3FF) | (palette << 12);
		}
	private:
		uint16_t _val;
	};

	template <uint32_t BaseAddress, int N>
	class BGTextMap {
	public:

		static_assert(sizeof(TextMapEntry) == 2, "Entry must be 2 bytes in size");

		TextMapEntry &operator[](std::tuple<std::size_t, std::size_t, std::size_t> indices) {
			return *this(std::get<0>(indices), std::get<1>(indices), std::get<2>(indices));
		}
		const TextMapEntry &operator[](std::tuple<std::size_t, std::size_t, std::size_t> indices) const {
			return *this(std::get<0>(indices), std::get<1>(indices), std::get<2>(indices));
		}
		TextMapEntry &operator[](std::tuple<std::size_t, std::size_t> indices) {
			return *this(std::get<0>(indices), std::get<1>(indices));
		}
		const TextMapEntry &operator[](std::tuple<std::size_t, std::size_t> indices) const {
			return *this(std::get<0>(indices), std::get<1>(indices));
		}
		TextMapEntry &operator()(std::size_t map, std::size_t x, std::size_t y) {
			assert(map >= 0 && map < N);
			assert(x >= 0 && x < 32);
			assert(y >= 0 && y < 32);
			return _map->map[map][y * 32 + x];
		}
		const TextMapEntry &operator()(std::size_t map, std::size_t x, std::size_t y) const {
			assert(map >= 0 && map < N);
			assert(x >= 0 && x < 32);
			assert(y >= 0 && y < 32);
			return _map->map[map][y * 32 + x];
		}
		TextMapEntry &operator()(std::size_t x, std::size_t y) {
			assert(x >= 0 && x < 32);
			assert(y >= 0 && y < 32);
			return _map->map[0][y * 32 + x];
		}
		const TextMapEntry &operator()(std::size_t x, std::size_t y) const {
			assert(x >= 0 && x < 32);
			assert(y >= 0 && y < 32);
			return _map->map[0][y * 32 + x];
		}

	private:
		struct map {
			TextMapEntry map[N][32*32];
		};
		static_assert(sizeof(struct map) == N * 0x800, "Map must be 0x800 bytes");
		Register<BaseAddress, struct map> _map;
	};

	template <uint32_t BaseAddress, int W, int H>
	class BGRotScaleMap {
	public:
		uint8_t &operator[](std::tuple<std::size_t, std::size_t> indices) {
			return *this(std::get<0>(indices), std::get<1>(indices));
		}
		uint8_t operator[](std::tuple<std::size_t, std::size_t> indices) const {
			return *this(std::get<0>(indices), std::get<1>(indices));
		}
		uint8_t &operator()(std::size_t x, std::size_t y) {
			assert(x >= 0 && x < 32);
			assert(y >= 0 && y < 32);
			return _map->map[y * W + x];
		}
		uint8_t operator()(std::size_t x, std::size_t y) const {
			assert(x >= 0 && x < W);
			assert(y >= 0 && y < H);
			return _map->map[y * W + x];
		}

	private:
		struct map {
			uint8_t map[W*H];
		};
		Register<BaseAddress, struct map> _map;
	};
};

#endif //GBA_VIDEO_MAP_H
