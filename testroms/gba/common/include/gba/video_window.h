//
// Created by Robbert-Jan on 2018-12-29.
//

#ifndef GBA_VIDEO_WINDOW_H
#define GBA_VIDEO_WINDOW_H

#include <gba/memory.h>

namespace gba {
    enum WindowSource {
        WINDOW_BG0 = 0x01,
        WINDOW_BG1 = 0x02,
        WINDOW_BG2 = 0x04,
        WINDOW_BG3 = 0x08,
        WINDOW_OBJ = 0x10,
        WINDOW_EFFECT = 0x20,
        WINDOW_NONE = 0
    };

    inline constexpr enum WindowSource operator|(enum WindowSource a, enum WindowSource b)
    {
        return static_cast<enum WindowSource>(static_cast<uint8_t>(a) | static_cast<uint8_t>(b));
    }

    template <int WindowID>
    class Window {
        using WINH_REG = Register<IO_BASE_ADDR + 0x040 + WindowID * 2, uint16_t>;
        using WINV_REG = Register<IO_BASE_ADDR + 0x044 + WindowID * 2, uint16_t>;
        using WININ_REG = Register<IO_BASE_ADDR + 0x048 + WindowID, uint8_t>;
    public:
        static inline void SetWindowRect(uint8_t left, uint8_t top, uint8_t right, uint8_t bottom) {
            WINH_REG hreg;
            WINV_REG vreg;
            hreg = (left << 8) | right;
            vreg = (top << 8) | bottom;
        }
        static inline void SetWindowInside(enum WindowSource src) {
            WININ_REG reg;
            reg = static_cast<uint8_t>(src);
        }



    protected:
    };
}

#endif //GBA_VIDEO_WINDOW_H
