//
// Created by Robbert-Jan de Jager on 4-12-18.
//

#ifndef GBA_TILE_H
#define GBA_TILE_H

#include <stdint.h>
#include <assert.h>
#include <array>

namespace gba {
	template <bool HighColor>
	class Tile {
	public:
		constexpr explicit Tile(const std::array<uint8_t, 64> data);
		Tile(const Tile<HighColor> &other) : _data(other._data) {

		}

		Tile &operator=(const Tile &other) {
			_data = other._data;
			return *this;
		}

	private:
		std::array<uint8_t, HighColor ? 64 : 32> _data;
	};

	static_assert(sizeof(Tile<true>) == 64, "8-bit depth tiles must be 64 bytes in size");
	static_assert(sizeof(Tile<false>) == 32, "4-bit depth tiles must be 32 bytes in size");

#define _(x) static_cast<uint8_t>(x)

	template<>
	constexpr Tile<false>::Tile(const std::array<uint8_t, 64> data) : _data{
		_(data[0]  | (data[1] << 4)) , _(data[2]  | (data[3] << 4)) , _(data[4]  | (data[5] << 4)) , _(data[6]  | (data[7] << 4)),
		_(data[8]  | (data[9] << 4)) , _(data[10] | (data[11] << 4)), _(data[12] | (data[13] << 4)), _(data[14] | (data[15] << 4)),
		_(data[16] | (data[17] << 4)), _(data[18] | (data[19] << 4)), _(data[20] | (data[21] << 4)), _(data[22] | (data[23] << 4)),
		_(data[24] | (data[25] << 4)), _(data[26] | (data[27] << 4)), _(data[28] | (data[29] << 4)), _(data[30] | (data[31] << 4)),
		_(data[32] | (data[33] << 4)), _(data[34] | (data[35] << 4)), _(data[36] | (data[37] << 4)), _(data[38] | (data[39] << 4)),
		_(data[40] | (data[41] << 4)), _(data[42] | (data[43] << 4)), _(data[44] | (data[45] << 4)), _(data[46] | (data[47] << 4)),
		_(data[48] | (data[49] << 4)), _(data[50] | (data[51] << 4)), _(data[52] | (data[53] << 4)), _(data[54] | (data[55] << 4)),
		_(data[56] | (data[57] << 4)), _(data[58] | (data[59] << 4)), _(data[60] | (data[61] << 4)), _(data[62] | (data[63] << 4))
	} {	}
#undef _
	template<>
	constexpr Tile<true>::Tile(const std::array<uint8_t, 64>  data) : _data{
		data[0], data[1], data[2], data[3], data[4], data[5], data[6], data[7],
		data[8], data[9], data[10], data[11], data[12], data[13], data[14], data[15],
		data[16], data[17], data[18], data[19], data[20], data[21], data[22], data[23],
		data[24], data[25], data[26], data[27], data[28], data[29], data[30], data[31],
		data[32], data[33], data[34], data[35], data[36], data[37], data[38], data[39],
		data[40], data[41], data[42], data[43], data[44], data[45], data[46], data[47],
		data[48], data[49], data[50], data[51], data[52], data[53], data[54], data[55],
		data[56], data[57], data[58], data[59], data[60], data[61], data[62], data[63],
	} { }
}

#endif //GBA_TILE_H
