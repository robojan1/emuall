//
// Created by Robbert-Jan de Jager on 4-12-18.
//

#ifndef GBA_VIDEO_H
#define GBA_VIDEO_H

#include <gba/memory.h>
#include <type_traits>
#include <assert.h>
#include <tuple>
#include <gba/video_map.h>
#include <gba/video_tilemap.h>
#include <gba/video_oam.h>
#include <gba/video_window.h>
#include <gba/palette.h>

namespace gba {


	template<enum VramTileBase TileBaseOffset, enum VramMapBase MapBaseOffset,
			bool TextMode, enum BackgroundSize BGSize, bool HighDepth>
	class BackgroundVRAM {
	public:
		static constexpr bool IsValidBGSize() {
			return (TextMode && (BGSize == BG_SIZE_256_256 || BGSize == BG_SIZE_512_512 ||
					BGSize == BG_SIZE_256_512 || BGSize == BG_SIZE_512_256)) ||
					(!TextMode && (BGSize == BG_SIZE_128_128 || BGSize == BG_SIZE_256_256 ||
							BGSize == BG_SIZE_512_512 || BGSize == BG_SIZE_1024_1024));
		}
		static constexpr int GetNumMaps() {
			if(!TextMode) return 1;
			switch(BGSize) {
				case BG_SIZE_256_256: return 1;
				case BG_SIZE_256_512: return 2;
				case BG_SIZE_512_256: return 2;
				case BG_SIZE_512_512: return 4;
				default: return 1;
			}
		}
		static constexpr int GetMapWidth() {
			switch(BGSize) {
				case BG_SIZE_128_128: return 128;
				case BG_SIZE_256_256: return 256;
				case BG_SIZE_512_256: return 512;
				case BG_SIZE_256_512: return 256;
				case BG_SIZE_512_512: return 512;
				case BG_SIZE_1024_1024: return 1024;
			}
		}
		static constexpr int GetMapHeight() {
			switch(BGSize) {
				case BG_SIZE_128_128: return 128;
				case BG_SIZE_256_256: return 256;
				case BG_SIZE_512_256: return 256;
				case BG_SIZE_256_512: return 512;
				case BG_SIZE_512_512: return 512;
				case BG_SIZE_1024_1024: return 1024;
			}
		}

		static constexpr int GetSizeIdx() {
			if(TextMode) {
				switch(BGSize) {
					case BG_SIZE_256_256: return 0;
					case BG_SIZE_512_256: return 1;
					case BG_SIZE_256_512: return 2;
					case BG_SIZE_512_512: return 3;
				}
			} else {
				switch(BGSize) {
					case BG_SIZE_128_128: return 0;
					case BG_SIZE_256_256: return 1;
					case BG_SIZE_512_512: return 2;
					case BG_SIZE_1024_1024: return 3;
				}
			}
		}

		static_assert(IsValidBGSize(), "A valid background configuration must be selected");

		static constexpr uint16_t GetControlConfig() {
			return (TileBaseOffset << 2) | (HighDepth ? 0x80 : 0) | (MapBaseOffset << 8) | (GetSizeIdx() << 14);

		}

		using Tiles = TileMap<VRAM_BASE_ADDR + 0x4000 * TileBaseOffset, 0x10000 - 0x4000 * TileBaseOffset, HighDepth>;
		using Map = std::conditional_t<TextMode, BGTextMap<VRAM_BASE_ADDR + 0x0800 * MapBaseOffset, GetNumMaps()>,
				BGRotScaleMap<VRAM_BASE_ADDR + 0x800 * MapBaseOffset, GetMapWidth(), GetMapHeight()>>;
	};

	template<int BG, class VRAM>
	class BackgroundBase {
		using CTRL_REG = Register<IO_BASE_ADDR + 0x008 + BG * 2, uint16_t>;
		using OFFSET_X_REG = Register<IO_BASE_ADDR + 0x010 + BG * 4, uint16_t>;
		using OFFSET_Y_REG = Register<IO_BASE_ADDR + 0x012 + BG * 4, uint16_t>;
	public:
		using Tiles = typename VRAM::Tiles;
		using Map = typename VRAM::Map;

		static void Activate() {
			CTRL_REG a;
			a = VRAM::GetControlConfig();
		}

		static void SetOffset(uint16_t x, uint16_t y) {
			OFFSET_X_REG rx;
			OFFSET_Y_REG ry;
			rx = x;
			ry = y;
		}
	protected:
	};

    template<int Offset>
    class BackgroundAffineBase {
    public:
        inline void SetMatrix(int16_t a, int16_t b, int16_t c, int16_t d) {
            _matA = a;
            _matB = b;
            _matC = c;
            _matD = d;
        }
        inline void SetRefPoint(int32_t x, int32_t y) {
            _refX = x;
            _refY = y;
        }
    protected:
        Register<IO_BASE_ADDR + 0x020 + Offset, int16_t> _matA;
        Register<IO_BASE_ADDR + 0x022 + Offset, int16_t> _matB;
        Register<IO_BASE_ADDR + 0x024 + Offset, int16_t> _matC;
        Register<IO_BASE_ADDR + 0x026 + Offset, int16_t> _matD;
        Register<IO_BASE_ADDR + 0x028 + Offset, int32_t> _refX;
        Register<IO_BASE_ADDR + 0x02C + Offset, int32_t> _refY;
    };

    template<int BG, int Offset, class VRAM>
	class BackgroundAffineTextBase : public BackgroundBase<BG, VRAM>, public BackgroundAffineBase<Offset>
    {
    };


	template<class VRAM>
	using Background0 = BackgroundBase<0, VRAM>;
	template<class VRAM>
	using Background1 = BackgroundBase<1, VRAM>;
	template<class VRAM>
	using Background2 = BackgroundAffineTextBase<2, 0x00, VRAM>;
	template<class VRAM>
	using Background3 = BackgroundAffineTextBase<3, 0x10, VRAM>;

	class BackgroundMode3 : public BackgroundAffineBase<0x00>
    {
	    struct Mode3Data {
	        Color col[240*160];
	    };
	    using VRAM = Register<VRAM_BASE_ADDR, struct Mode3Data>;
	    static_assert(sizeof(struct Mode3Data) == 160*240*2, "Invalid Mode3Data size");

    public:
        static inline Color &At(int idx) {
            VRAM ram;
            assert(idx >= 0 && idx < 240 * 160);
            return ram->col[idx];
        }

        static inline Color &At(int x, int y) {
            VRAM ram;
            assert(x >= 0 && x < 240);
            assert(y >= 0 && y < 160);
            return ram->col[y * 240 + x];
        }

        static inline void SetFromBitmap(const void *src, size_t size) {
            assert(size <= 160*240*2);
            VRAM ram;
            CpuSet(src, &ram->col[0], size/2);
        }

        inline Color &operator()(int x, int y) {
            return At(x,y);
        }

    };

	enum VideoMode {
		VIDEO_MODE0 = 0,
		VIDEO_MODE1 = 1,
		VIDEO_MODE2 = 2,
		VIDEO_MODE3 = 3,
		VIDEO_MODE4 = 4,
		VIDEO_MODE5 = 5
	};

	enum VideoInterrupt {
		VIDEO_IRQ_VBLANK = 0x1,
		VIDEO_IRQ_HBLANK = 0x2,
		VIDEO_IRQ_VCOUNT = 0x4
	};

	enum VideoConfigurations {
		VIDEO_CTRL_MODE0 = 0,
		VIDEO_CTRL_MODE1 = 1,
		VIDEO_CTRL_MODE2 = 2,
		VIDEO_CTRL_MODE3 = 3,
		VIDEO_CTRL_MODE4 = 4,
		VIDEO_CTRL_MODE5 = 5,
		VIDEO_CTRL_SECOND_FRAME = 0x10,
		VIDEO_CTRL_HBLANK_OAM = 0x20,
		VIDEO_CTRL_1D_OBJ_MAP = 0x40,
		VIDEO_CTRL_FORCE_BLANK = 0x80,
		VIDEO_CTRL_BG0 = 0x100,
		VIDEO_CTRL_BG1 = 0x200,
		VIDEO_CTRL_BG2 = 0x400,
		VIDEO_CTRL_BG3 = 0x800,
		VIDEO_CTRL_OBJ = 0x1000,
		VIDEO_CTRL_WIN0 = 0x2000,
		VIDEO_CTRL_WIN1 = 0x4000,
		VIDEO_CTRL_OBJWIN = 0x8000
	};

	enum BlendTarget {
		BLEND_TARGET_BG0 = 0x01,
		BLEND_TARGET_BG1 = 0x02,
		BLEND_TARGET_BG2 = 0x04,
		BLEND_TARGET_BG3 = 0x08,
		BLEND_TARGET_OBJ = 0x10,
		BLEND_TARGET_BD = 0x20,
	};

	enum BlendEffect {
		BLEND_NONE = 0,
		BLEND_ALPHA = 1,
		BLEND_INCR = 2,
		BLEND_DECR = 3
	};

	inline constexpr enum VideoInterrupt operator|(enum VideoInterrupt a, enum VideoInterrupt b)
	{
		return static_cast<VideoInterrupt>(static_cast<uint16_t>(a) | static_cast<uint16_t>(b));
	}

	inline constexpr enum VideoConfigurations operator|(enum VideoConfigurations a, enum VideoConfigurations b)
	{
		return static_cast<enum VideoConfigurations>(static_cast<uint16_t>(a) | static_cast<uint16_t>(b));
	}
	inline constexpr enum BlendTarget operator|(enum BlendTarget a, enum BlendTarget b)
	{
		return static_cast<enum BlendTarget>(static_cast<uint16_t>(a) | static_cast<uint16_t>(b));
	}

	class Video {
		using DISPCNT_REG = Register<IO_BASE_ADDR + 0x000, uint16_t>;
		using GRNSWP_REG = Register<IO_BASE_ADDR + 0x002, uint16_t>;
		using DISPSTAT_REG = Register<IO_BASE_ADDR + 0x004, uint16_t>;
		using VCOUNT_REG = Register<IO_BASE_ADDR + 0x006, const uint16_t>;
		using MOSAIC_REG = Register<IO_BASE_ADDR + 0x04C, uint32_t>;
		using BLEND_CTRL_REG = Register<IO_BASE_ADDR + 0x050, uint16_t>;
		using BLEND_ALPHA_REG = Register<IO_BASE_ADDR + 0x052, uint16_t>;
		using BLEND_BRIGHT_REG = Register<IO_BASE_ADDR + 0x054, uint32_t>;
		using WINOUT_REG = Register<IO_BASE_ADDR + 0x04A, uint8_t>;

	public:

		inline static void Configure(enum VideoConfigurations config) {
			DISPCNT_REG ctrl;
			ctrl = static_cast<uint16_t>(config);
		}

		inline static void SetMode(enum VideoMode mode) {
			DISPCNT_REG ctrl;
			ctrl = (ctrl & ~7) | static_cast<uint16_t>(mode);
		}

		inline static void FrameSelect(bool second) {
			DISPCNT_REG  ctrl;
			if(second)
				ctrl |= 1<<4;
			else
				ctrl &= 1<<4;
		}

		inline static void OAMduringHBlank(bool enabled) {
			DISPCNT_REG  ctrl;
			if(enabled)
				ctrl |= 1<<5;
			else
				ctrl &= 1<<5;
		}

		inline static void OBJmapping2D(bool enable) {
			DISPCNT_REG  ctrl;
			if(enable)
				ctrl |= 1<<6;
			else
				ctrl &= 1<<6;
		}

		inline static void ForceBlank(bool blank) {
			DISPCNT_REG  ctrl;
			if(blank)
				ctrl |= 1<<7;
			else
				ctrl &= 1<<7;
		}

		inline static void EnableBG0(bool enabled) {
			DISPCNT_REG ctrl;
			if(enabled)
				ctrl |= 1<<8;
			else
				ctrl &= 1<<8;
		}

		inline static void EnableBG1(bool enabled) {
			DISPCNT_REG ctrl;
			if(enabled)
				ctrl |= 1<<9;
			else
				ctrl &= 1<<9;
		}

		inline static void EnableBG2(bool enabled) {
			DISPCNT_REG ctrl;
			if(enabled)
				ctrl |= 1<<10;
			else
				ctrl &= 1<<10;
		}

		inline static void EnableBG3(bool enabled) {
			DISPCNT_REG ctrl;
			if(enabled)
				ctrl |= 1<<11;
			else
				ctrl &= 1<<11;
		}

		inline static void EnableOBJ(bool enabled) {
			DISPCNT_REG ctrl;
			if(enabled)
				ctrl |= 1<<12;
			else
				ctrl &= 1<<12;
		}

		inline static void EnableWin0(bool enabled) {
			DISPCNT_REG ctrl;
			if(enabled)
				ctrl |= 1<<13;
			else
				ctrl &= 1<<13;
		}

		inline static void EnableWin1(bool enabled) {
			DISPCNT_REG ctrl;
			if(enabled)
				ctrl |= 1<<14;
			else
				ctrl &= 1<<14;
		}

		inline static void EnableOBJWin(bool enabled) {
			DISPCNT_REG ctrl;
			if(enabled)
				ctrl |= 1<<15;
			else
				ctrl &= 1<<15;
		}

		inline static void EnableInterrupt(enum VideoInterrupt irq) {
			DISPSTAT_REG stat;
			stat |= static_cast<uint16_t>(irq) << 3;
		}

		inline static void DisableInterrupt(enum VideoInterrupt irq) {
			DISPSTAT_REG stat;
			stat &= ~(static_cast<uint16_t>(irq) << 3);
		}

		inline static bool IsInterruptPending(enum VideoInterrupt irq) {
			DISPSTAT_REG stat;
			return (stat & static_cast<uint16_t>(irq)) != 0;
		}

		inline static void SetBlend(enum BlendTarget first, enum BlendTarget second, enum BlendEffect effect)
		{
			BLEND_CTRL_REG reg;
			reg = static_cast<uint16_t>(first) | (static_cast<uint16_t>(second) << 8) |
					(static_cast<uint16_t>(effect) << 6);
		}

		inline static void SetBlendAlpha(int first, int second)
		{
			BLEND_ALPHA_REG  reg;
			reg = (first << 0) | (second << 8);
		}

		inline static uint16_t CurrentLine() {
			VCOUNT_REG reg;
			return reg;
		}

		static inline void SetWindowOutside(enum WindowSource src) {
			WINOUT_REG reg;
			reg = static_cast<uint8_t>(src);
		}
	private:
	};
}

#endif //GBA_VIDEO_H
