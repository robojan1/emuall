//
// Created by Robbert-Jan de Jager on 1-12-18.
//

#ifndef GBA_MEMORY_H
#define GBA_MEMORY_H

#include <stdint.h>

namespace gba {

	constexpr uint32_t IO_BASE_ADDR         = 0x4000000;
	constexpr uint32_t PALETTE_BASE_ADDR    = 0x5000000;
	constexpr uint32_t VRAM_BASE_ADDR       = 0x6000000;
	constexpr uint32_t OAM_BASE_ADDR        = 0x7000000;

	template<uint32_t BaseAddr, typename AccessType>
	class Register {
	public:
		inline AccessType operator=(AccessType val) {
			return *((volatile AccessType *) BaseAddr) = val;
		}

		inline AccessType operator+=(AccessType val) {
			return *((volatile AccessType *) BaseAddr) += val;
		}

		inline AccessType operator-=(AccessType val) {
			return *((volatile AccessType *) BaseAddr) -= val;
		}

		inline operator AccessType() const {
			return *((volatile AccessType *) BaseAddr);
		}

		inline AccessType operator|=(AccessType val) {
			return *((volatile AccessType *) BaseAddr) |= val;
		}

		inline AccessType operator&=(AccessType val) {
			return *((volatile AccessType *) BaseAddr) &= val;
		}

		inline AccessType operator^=(AccessType val) {
			return *((volatile AccessType *) BaseAddr) ^= val;
		}

		inline AccessType operator>>=(AccessType val) {
			return *((volatile AccessType *) BaseAddr) >>= val;
		}

		inline AccessType operator<<=(AccessType val) {
			return *((volatile AccessType *) BaseAddr) <<= val;
		}

		inline AccessType *operator->() {
			return (( AccessType *) BaseAddr);
		}

		inline volatile AccessType *operator&() {
			return (volatile AccessType *)BaseAddr;
		}
	};
}

#endif //GBA_MEMORY_H
