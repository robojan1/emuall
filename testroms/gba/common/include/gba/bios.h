//
// Created by Robbert-Jan de Jager on 3-12-18.
//

#ifndef GBA_BIOS_H
#define GBA_BIOS_H

#include <stdint.h>

namespace gba {
	using fix14_16 = int16_t;
	using fix8_32 = int32_t;
	using fix8_16 = int16_t;

	struct DivResult {
		int32_t div;
		int32_t rem;
		int32_t abs_div;
	};
	struct BgAffineParam {
		fix8_32 src_center_x;
		fix8_32 src_center_y;
		int16_t display_center_x;
		int16_t display_center_y;
		fix8_16 scale_x;
		fix8_16 scale_y;
		uint16_t rotation;
	};
	struct BgAffineResult {
		int16_t same_diff_x;
		int16_t next_diff_x;
		int16_t same_diff_y;
		int16_t next_diff_y;
		int32_t start_x;
		int32_t start_y;
	};
	struct ObjAffineParam {
		fix8_16 scale_x;
		fix8_16 scale_y;
		uint16_t rotation;
	};
	struct ObjAffineResult {
		int16_t same_diff_x;
		int16_t next_diff_x;
		int16_t same_diff_y;
		int16_t next_diff_y;
	};
	struct UnpackInfo {
		uint16_t srcLen;
		uint8_t srcWidth;
		uint8_t dstWidth;
		uint32_t dataOffset;
	};

	enum class ResetFlag : uint8_t {
		None = 0,
		OBWRAM = 0x01,
		OCWRAM = 0x02,
		Pallete = 0x04,
		VRAM = 0x08,
		OAM = 0x10,
		SIO = 0x20,
		Sound = 0x40,
		Other = 0x80,
	};

	inline constexpr enum ResetFlag operator|(enum ResetFlag a, enum ResetFlag b)
	{
		return static_cast<ResetFlag>(static_cast<uint8_t>(a) | static_cast<uint8_t>(b));
	}

	struct MultiBootParam {
		uint32_t res0[5];
		uint8_t handshake_data;
		uint8_t res1[4];
		uint8_t client_data[3];
		uint8_t palette_data;
		uint8_t res2;
		uint8_t client_detected;
		const void *boot_start;
		const void *boot_end;
		uint32_t res3[9];
	};

	enum class TransferMode {
		Slow = 0,
		Multiplay = 1,
		Fast = 2
	};

	struct WaveData {
		uint16_t type;
		uint16_t stat;
		uint32_t freq;
		uint32_t loop;
		uint32_t size;
		int8_t data[0];
	};

	struct SoundChannel {
		uint8_t flag;
		uint8_t _r1;
		uint8_t right_volume;
		uint8_t left_volume;
		uint8_t attack;
		uint8_t decay;
		uint8_t sustain;
		uint8_t release;
		uint8_t _r2[4];
		uint32_t frequency;
		struct WaveData *wp;
		uint32_t _r3[6];
		uint8_t _r4[4];
	};

	struct SoundArea {
		uint32_t id;
		uint8_t _dma_count;
		uint8_t reverb;
		uint16_t _d1;
		void (*_func)();
		int _intp;
		void *_noUse;
		struct SoundChannel vchn[8];
		int8_t pcmBuf[32];
	};

}
extern "C" {
void SoftReset(void) __attribute__((noreturn));
void RegisterRamReset(enum gba::ResetFlag flags);
void Halt(void);
void Stop(void);
void IntrWait(int returnImm, uint32_t flag);
void VBlankIntrWait(void);
int32_t Div(int32_t num, int32_t den);
int32_t DivRem(int32_t num, int32_t den);
gba::DivResult DivRes(int32_t num, int32_t den);
int32_t DivArm(int32_t den, int32_t num);
int32_t DivRemArm(int32_t den, int32_t num);
gba::DivResult DivResArm(int32_t den, int32_t num);
uint16_t Sqrt(uint32_t x);
gba::fix14_16 ArcTan(gba::fix14_16 x);
uint16_t ArcTan2(gba::fix14_16 x, gba::fix14_16 y);
void CpuSet(const void *src, void *dst, uint32_t len);
void CpuFastSet(const void *src, void *dst, uint32_t len);
uint32_t GetBiosChecksum(void);
void BgAffineSet(const struct gba::BgAffineParam *param, struct gba::BgAffineResult *result, int num);
void ObjAffineSet(const struct gba::ObjAffineParam *param, struct gba::ObjAffineResult *result, int num, int stride);
void BitUnPack(const void *src, void *dst, const struct gba::UnpackInfo *info);
void LZ77UnCompReadNormalWrite8bit(const void *src, void *dst);
void LZ77UnCompReadNormalWrite16bit(const void *src, void *dst);
void HuffUnCompReadNormal(const void *src, void *dst);
void RLUnCompReadNormalWrite8bit(const void *src, void *dst);
void RLUnCompReadNormalWrite16bit(const void *src, void *dst);
void Diff8bitUnFilterWrite8bit(const void *src, void *dst);
void Diff8bitUnFilterWrite16bit(const void *src, void *dst);
void Diff16bitUnfilter(const void *src, void *dst);
void SoundBias(int level);
void SoundDriverInit(struct gba::SoundArea *sa);
void SoundDriverMode(uint32_t mode);
void SoundDriverMain(void);
void SoundDriverVSync(void);
void SoundChannelClear(void);
uint32_t MidiKey2Freq(struct gba::WaveData *wa, uint8_t key, uint8_t fp);
void SoundUnk0(void);
void SoundUnk1(void);
void SoundUnk2(void);
void SoundUnk3(void);
void SoundUnk4(void);
int MultiBoot(struct gba::MultiBootParam *param, enum gba::TransferMode mode);
void HardReset(void) __attribute__((noreturn));
void CustomHalt(int res0, int res1, uint8_t val);
void SoundDriverVSyncOff(void);
void SoundDriverVSyncOn(void);
void SoundGetJumpList(uint32_t *funcs);

// Emulator extensions
void EmulatorWrite(const void *src, uint32_t size);
void EmulatorExit(uint32_t exitCode);
}

#endif //GBA_BIOS_H
