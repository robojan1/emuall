//
// Created by Robbert-Jan de Jager on 1-12-18.
//

#ifndef TESTROMS_FLASH_H
#define TESTROMS_FLASH_H

#include <stdint.h>
#include <gba/memory.h>
namespace gba {
	namespace flash {
#define FLASH_BASE_ADDR 0xE000000
#define FLASH_CMD1_ADDR (FLASH_BASE_ADDR + 0x5555)
#define FLASH_CMD2_ADDR (FLASH_BASE_ADDR + 0x2AAA)

		struct DeviceID {
			uint8_t manufacturer;
			uint8_t device;
		};

		enum class CommandID : uint8_t {
			EraseAll = 0x10,
			EraseSector = 0x30,
			Erase = 0x80,
			EnterIDMode = 0x90,
			EraseAndWrite = 0xA0,
			WriteSingleByte = 0xA0,
			SwitchBank = 0xB0,
			ExitCommand = 0xF0,
		};

		inline void WriteCommand(enum CommandID cmd) {
			Register<FLASH_CMD1_ADDR, uint8_t> cmd1;
			Register<FLASH_CMD2_ADDR, uint8_t> cmd2;
			cmd1 = 0xAA;
			cmd2 = 0x55;
			cmd1 = static_cast<uint8_t>(cmd);
		}

		inline uint8_t GetByte(uint16_t addr) {
			return *((volatile uint8_t *) FLASH_BASE_ADDR + addr);
		}

		struct DeviceID GetID();

		void EraseAll();

		void EraseSector(uint16_t addr);

	}
}

#endif //TESTROMS_FLASH_H
