//
// Created by Robbert-Jan de Jager on 27-12-18.
//

#ifndef GBA_EMULATOR_H
#define GBA_EMULATOR_H

#include <gba/memory.h>
#include <gba/bios.h>

namespace gba {

	namespace emulator {

		inline bool IsInEmulator() {
			Register<IO_BASE_ADDR + 0x280, uint32_t> _id;
			return _id == 0x456d7541;
		}

		inline void Exit(uint32_t exitCode) {
			if(IsInEmulator()) {
				EmulatorExit(exitCode);
			}
			while(true) {
				Stop();
			}
		}

		inline void Write(const void *src, uint32_t size) {
			if(IsInEmulator()) {
				EmulatorWrite(src,size);
			}
		}

	}

}

#endif // GBA_EMULATOR_H
