//
// Created by Robbert-Jan de Jager on 9-12-18.
//

#ifndef GBA_IRQ_H
#define GBA_IRQ_H

#include <gba/memory.h>
#include <functional>

namespace gba {

	enum InterruptSrc {
		IRQ_VBLANK = 0x0001,
		IRQ_HBLANK = 0x0002,
		IRQ_VCOUNT = 0x0004,
		IRQ_TIMER0 = 0x0008,
		IRQ_TIMER1 = 0x0010,
		IRQ_TIMER2 = 0x0020,
		IRQ_TIMER3 = 0x0040,
		IRQ_SERIAL = 0x0080,
		IRQ_DMA0   = 0x0100,
		IRQ_DMA1   = 0x0200,
		IRQ_DMA2   = 0x0400,
		IRQ_DMA3   = 0x0800,
		IRQ_KEYPAD = 0x1000,
		IRQ_EXT    = 0x2000,
	};

	inline constexpr enum InterruptSrc operator|(enum InterruptSrc a, enum InterruptSrc b)
	{
		return static_cast<InterruptSrc>(static_cast<uint16_t>(a) | static_cast<uint16_t>(b));
	}

	class IRQ {
		using IME_REG = Register<IO_BASE_ADDR + 0x208, uint32_t>;
		using IE_REG = Register<IO_BASE_ADDR + 0x200, uint16_t>;
		using IF_REG = Register<IO_BASE_ADDR + 0x202, uint16_t>;
	public:
		inline static void DisableInterrupts(bool disabled) {
			IME_REG r;
			r = disabled ? 1 : 0;
		}
		inline static bool AreInterruptDisabled() {
			IME_REG r;
			return r != 0;
		}

		inline static void EnableInterrupt(enum InterruptSrc sources)
		{
			IE_REG r;
			r |= static_cast<uint16_t>(sources);
		}

		inline static void DisableInterrupt(enum InterruptSrc sources)
		{
			IE_REG r;
			r &= ~static_cast<uint16_t>(sources);
		}

		inline static void AcknowledgeInterrupt(enum InterruptSrc sources)
		{
			IF_REG r;
			r = static_cast<uint16_t>(sources);
		}

		inline static bool IsInterruptPending(enum InterruptSrc sources)
		{
			IF_REG r;
			return (r & static_cast<uint16_t>(sources)) != 0;
		}

		inline static enum InterruptSrc GetPendingInterrupts() {
			IF_REG r;
			return static_cast<enum InterruptSrc>(static_cast<uint16_t>(r));
		}

		static void Init();
		using IRQHandler = void (*)(void);
		static IRQHandler handlerVBlank;
		static IRQHandler handlerHBlank;
		static IRQHandler  handlerVCount;
		static IRQHandler handlerTimer0;
		static IRQHandler handlerTimer1;
		static IRQHandler handlerTimer2;
		static IRQHandler handlerTimer3;
		static IRQHandler handlerSerial;
		static IRQHandler handlerDMA0;
		static IRQHandler handlerDMA1;
		static IRQHandler handlerDMA2;
		static IRQHandler handlerDMA3;
		static IRQHandler handlerKeypad;
		static IRQHandler handlerExternal;
	};

}

#endif //GBA_IRQ_H
