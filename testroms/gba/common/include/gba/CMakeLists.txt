
TARGET_SOURCES(${TARGET_NAME}
        PUBLIC
            ${CMAKE_CURRENT_LIST_DIR}/flash.h
            ${CMAKE_CURRENT_LIST_DIR}/gba.h
            ${CMAKE_CURRENT_LIST_DIR}/memory.h
            ${CMAKE_CURRENT_LIST_DIR}/bios.h
            ${CMAKE_CURRENT_LIST_DIR}/video.h
            ${CMAKE_CURRENT_LIST_DIR}/palette.h
            ${CMAKE_CURRENT_LIST_DIR}/tile.h
            ${CMAKE_CURRENT_LIST_DIR}/video_map.h
            ${CMAKE_CURRENT_LIST_DIR}/video_tilemap.h
            ${CMAKE_CURRENT_LIST_DIR}/video_oam.h
            ${CMAKE_CURRENT_LIST_DIR}/video_window.h
            ${CMAKE_CURRENT_LIST_DIR}/irq.h
            ${CMAKE_CURRENT_LIST_DIR}/emulator.h
            ${CMAKE_CURRENT_LIST_DIR}/timer.h
        )