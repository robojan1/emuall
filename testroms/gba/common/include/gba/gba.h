//
// Created by Robbert-Jan de Jager on 7-10-18.
//

#ifndef GBA_GBA_H
#define GBA_GBA_H

#include <stdint.h>

extern "C" {
extern unsigned char _gbaheader_logo[156];
extern unsigned char _gbaheader_jmp[4];
extern void *gba_snd_buf;
extern uint16_t gba_irq_flags;
extern void (*gba_irq_handler)();
};

#define GBA_CARTID_EEPROM      "EEPROM_Vnnn"
#define GBA_CARTID_SRAM        "SRAM_Vnnn"
#define GBA_CARTID_FLASH_512k  "FLASH512_Vnnn"
#define GBA_CARTID_FLASH_1M    "FLASH1M_Vnnn"

#define DEFINE_HEADER_TITLE constexpr char _gbaheader_title[12] __attribute__((section(".gbaheader.title"),used))
#define DEFINE_HEADER_CODE constexpr char _gbaheader_code[4] __attribute__((section(".gbaheader.code"),used))
#define DEFINE_HEADER_MAKER constexpr char _gbaheader_maker[2] __attribute__((section(".gbaheader.maker"),used))
#define DEFINE_HEADER_CHECKSUM constexpr unsigned char _gbaheader_checksum __attribute__((section(".gbaheader.checksum"),used)) = gba::_CalculateHeaderChecksum(_gbaheader_title, _gbaheader_code, _gbaheader_maker)
#define DEFINE_GBA_CARTID(x) constexpr char _gba_cartid[] __attribute__((section(".gbaheader.cartid"),used)) = x

namespace gba {
	constexpr unsigned char _CalculateHeaderChecksum(const char title[12], const char code[4], const char maker[2]) {
		unsigned int sum = 0;
		for(int i=0; i < 12; i++) sum -= title[i];
		for(int i=0; i < 4; i++) sum -= code[i];
		for(int i=0; i < 2; i++) sum -= maker[i];
		return static_cast<unsigned char>((sum - 0x96 - 0x19) & 0xFF);
	}
}

#endif //GBA_GBA_H
