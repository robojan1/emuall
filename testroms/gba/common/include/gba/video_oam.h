//
// Created by Robbert-Jan on 2018-12-29.
//

#ifndef GBA_VIDEO_OAM_H
#define GBA_VIDEO_OAM_H

#include <gba/memory.h>
#include <gba/video_tilemap.h>
#include <stdint.h>

namespace gba{

    class OAMSprite {
    public:
        enum Flags {
            RotScale = 0x01,
            DoubleSize = 0x02,
            Disable = 0x02,
            Mosaic = 0x10,
            Color8bpp = 0x20,
            HFlip = 0x1000,
            VFlip = 0x2000
        };
        enum Mode {
            Normal = 0,
            SemiTransparent = 1,
            OBJWindow = 2,
        };
        enum Shape {
            Square = 0,
            Horizontal = 1,
            Vertical = 2
        };
        enum Size {
            Size8x8,
            Size16x8,
            Size8x16,
            Size16x16,
            Size32x8,
            Size8x32,
            Size32x32,
            Size32x16,
            Size16x32,
            Size64x64,
            Size64x32,
            Size32x64
        };
        static constexpr enum Shape GetShapeFromSize(enum Size size) {
            switch(size) {
                default:
                case Size8x8:
                case Size16x16:
                case Size32x32:
                case Size64x64:
                    return Square;
                case Size16x8:
                case Size32x8:
                case Size32x16:
                case Size64x32:
                    return Horizontal;
                case Size8x16:
                case Size8x32:
                case Size16x32:
                case Size32x64:
                    return Vertical;
            }
        }
        static constexpr int GetSizeIdFromSize(enum Size size) {
            switch(size) {
                default:
                case Size8x8:
                case Size16x8:
                case Size8x16:
                    return 0;
                case Size16x16:
                case Size32x8:
                case Size8x32:
                    return 1;
                case Size32x32:
                case Size32x16:
                case Size16x32:
                    return 2;
                case Size64x64:
                case Size64x32:
                case Size32x64:
                    return 3;
            }
        }

        inline void Configure(uint16_t x, uint16_t y, enum Flags flags, enum Mode mode, enum Size size,
                              int tile, int priority = 0, int palette = 0, int rotMatrix = 0)
        {
            assert(palette >= 0 && palette <= ((flags & Color8bpp) != 0 ? 255 : 15));
            assert(priority >= 0 && priority < 4);
            assert(rotMatrix >= 0 && rotMatrix < 32);
            assert(tile >= 0 && tile < 1024);
            assert(((flags & Color8bpp) == 0) || ((tile & 0x1) == 0));
            _attr0 = (y & 0xFF) | ((static_cast<uint16_t>(flags) & 0xFF) << 8) |
                     (static_cast<uint16_t>(mode) << 10) | (static_cast<uint16_t>(GetShapeFromSize(size)) << 14);
            _attr1 = (x & 0x1FF) | ((static_cast<uint16_t>(flags) & 0xFF00)) |
                     (rotMatrix << 9) | (GetSizeIdFromSize(size) << 14);
            _attr2 = tile | (priority << 10) | (palette << 12);
        }

        inline void SetPos(uint16_t x, uint16_t y) {
            _attr0 = (_attr0 & ~0xFF) | (y & 0xFF);
            _attr1 = (_attr1 & ~0x1FF) | (x & 0x1FF);
        }

        inline void SetTile(uint16_t tile) {
            assert((_attr0 & (1<<13)) == 0 || (tile & 1) == 0);
            assert(tile >= 0 && tile < 1024);
            _attr2 = (_attr2 & 0x3FF) | tile;
        }
    private:
        uint16_t _attr0;
        uint16_t _attr1;
        uint16_t _attr2;
    };
    static_assert(sizeof(OAMSprite) == 6, "OAM sprite must have size of 6");

    inline constexpr enum OAMSprite::Flags operator|(enum OAMSprite::Flags a, enum OAMSprite::Flags b)
    {
        return static_cast<enum OAMSprite::Flags>(static_cast<uint32_t>(a) | static_cast<uint32_t>(b));
    }

    class OAM {
    public:
        using Tiles4bpp = TileMap<VRAM_BASE_ADDR + 0x10000, 0x18000 - 0x10000, false>;
        using Tiles8bpp = TileMap<VRAM_BASE_ADDR + 0x10000, 0x18000 - 0x10000, true>;

        OAMSprite &operator[](int idx) {
            assert(idx >= 0 && idx < 128);
            return _mem->mem[idx].attributes;
        }
        void SetMatrix(int idx, uint16_t a, uint16_t b, uint16_t c, uint16_t d) {
            assert(idx >= 0 && idx < 32);
            _mem->mem[idx*4+0].matrix.param = a;
            _mem->mem[idx*4+1].matrix.param = b;
            _mem->mem[idx*4+2].matrix.param = c;
            _mem->mem[idx*4+3].matrix.param = d;
        }
    private:
        struct OAMMatrix {
            uint16_t res0;
            uint16_t res1;
            uint16_t res2;
            uint16_t param;
        };
        union OAMStruct {
            OAMSprite attributes;
            struct OAMMatrix matrix;
        };

        struct OAMMemory {
            union OAMStruct mem[128];
        };

        static_assert(sizeof(union OAMStruct) == 8, "OAMStruct must be 8 bytes");
        static_assert(offsetof(union OAMStruct, attributes) == 0, "OAM attributes must have no offset");
        static_assert(offsetof(union OAMStruct, matrix) == 0, "OAM matrix must have no offset");
        static_assert(sizeof(struct OAMMemory) == 1024, "OAM memory size must be 128 bytes");

        Register<OAM_BASE_ADDR, struct OAMMemory> _mem;
    };
}

#endif //GBA_VIDEO_OAM_H
