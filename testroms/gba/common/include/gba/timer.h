//
// Created by Robbert-Jan on 2018-12-30.
//

#ifndef GBA_TIMER_H
#define GBA_TIMER_H

#include <gba/memory.h>
#include <stdlib.h>
#include <cassert>

namespace gba {
    template<int TimerIdx>
    class Timer {
        static_assert(TimerIdx >= 0 && TimerIdx < 4, "TimerIDX must be between 0 and 4");
        using RELOAD_REG = Register<IO_BASE_ADDR + 0x100 + 0x4 * TimerIdx, uint16_t>;
        using CTRL_REG = Register<IO_BASE_ADDR + 0x102 + 0x4 * TimerIdx, uint16_t>;
    public:
        static constexpr uint32_t Frequency = 16777216;

        enum Prescaler {
            PRE_1 = 0,
            PRE_64 = 1,
            PRE_256 = 2,
            PRE_1024 = 3
        };

        inline static void SetReloadValue(uint16_t val) {
            RELOAD_REG reg;
            reg = val;
        }
        inline static uint16_t GetCurrentValue() {
            RELOAD_REG reg;
            return reg;
        }
        inline static void ConfigureTimer(enum Prescaler pre, bool countUp, bool irqEn, bool start = false) {
            CTRL_REG reg;
            reg = pre | (countUp ? 0x4 : 0) | (irqEn ? 0x40 : 0) | (start ? 0x80 : 0);
        }

        inline static void SetPrescaler(enum Prescaler pre)
        {
            CTRL_REG reg;
            reg = (reg & 0xFFFC) | static_cast<uint16_t>(pre);
        }

        inline static void SetFrequency(int freq) {
            enum Prescaler bestPrescalerOption = PRE_1;
            int bestError = INT32_MAX;
            uint32_t bestDivider = 0;

            auto checkPrescaler = [&bestPrescalerOption, &bestError, &bestDivider](uint32_t prescaler, enum Prescaler prescalerOption, int freq) {
                uint32_t divider = (Frequency + (freq * prescaler) / 2 ) / (freq * prescaler);
                int error = std::abs(freq * 256 - static_cast<int>((Frequency/prescaler)*256 / divider));
                if(error < bestError && divider > 0 && divider <= UINT16_MAX + 1) {
                    bestPrescalerOption = prescalerOption;
                    bestError = error;
                    bestDivider = divider;
                }
            };

            checkPrescaler(1024, PRE_1024, freq);
            checkPrescaler(256, PRE_256, freq);
            checkPrescaler(64, PRE_64, freq);
            checkPrescaler(1, PRE_1, freq);

            SetReloadValue(bestDivider - 1);
            SetPrescaler(bestPrescalerOption);
        }

        inline void SetPeriodMs(uint32_t period) {
            SetPeriodUs(period * 1000);
        }

        inline static void SetPeriodUs(uint32_t period) {
            assert(period < 4000000u); // Limit of hardware
            enum Prescaler bestPrescalerOption = PRE_1;
            int bestError = INT32_MAX;
            uint32_t bestDivider = 0;

            auto checkPrescaler = [&bestPrescalerOption, &bestError, &bestDivider](uint32_t prescaler, enum Prescaler prescalerOption, uint32_t period) {
                uint32_t divider = static_cast<uint32_t>(static_cast<uint64_t>(Frequency) * period / prescaler / 1000000u);
                int error = std::abs(static_cast<int>(period) - static_cast<int>(1000000ull * prescaler * divider / Frequency));
                if(error < bestError && divider > 0 && divider <= UINT16_MAX + 1) {
                    bestPrescalerOption = prescalerOption;
                    bestError = error;
                    bestDivider = divider;
                }
            };

            checkPrescaler(1024, PRE_1024, period);
            checkPrescaler(256, PRE_256, period);
            checkPrescaler(64, PRE_64, period);
            checkPrescaler(1, PRE_1, period);

            SetReloadValue(bestDivider - 1);
            SetPrescaler(bestPrescalerOption);
        }

        inline static void Start(bool start = true) {
            CTRL_REG  reg;
            if(start) {
                reg |= 0x80;
            } else {
                reg &= ~0x80;
            }
        }
        inline static void Stop() {
            Start(false);
        }
        inline static void EnableIRQ(bool enabled) {
            CTRL_REG  reg;
            if(enabled) {
                reg |= 0x40;
            } else {
                reg &= ~0x40;
            }
        }

    };
};

#endif //GBA_TIMER_H
