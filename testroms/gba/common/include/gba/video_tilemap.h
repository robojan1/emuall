//
// Created by Robbert-Jan de Jager on 9-12-18.
//

#ifndef GBA_VIDEO_TILEMAP_H
#define GBA_VIDEO_TILEMAP_H

#include <stdint.h>
#include <gba/memory.h>
#include <assert.h>
#include <gba/tile.h>

namespace gba
{
	template <uint32_t BaseAddr, std::size_t Size, bool HighDepth>
	struct TileMap {

	public:

		Tile<HighDepth> &operator[](std::size_t idx) {
			assert(idx >= 0 && idx < (Size / sizeof(Tile<HighDepth>)));
			return _tiles->map[idx];
		}

	private:
		struct map {
			Tile<HighDepth> map[Size / sizeof(Tile<HighDepth>)];
		};
		Register<BaseAddr, struct map> _tiles;
	};
}

#endif //GBA_VIDEO_TILEMAP_H
