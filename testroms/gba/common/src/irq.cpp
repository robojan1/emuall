//
// Created by Robbert-Jan de Jager on 9-12-18.
//

#include <gba/irq.h>
#include <gba/gba.h>

gba::IRQ::IRQHandler gba::IRQ::handlerVBlank;
gba::IRQ::IRQHandler gba::IRQ::handlerHBlank;
gba::IRQ::IRQHandler gba::IRQ::handlerVCount;
gba::IRQ::IRQHandler gba::IRQ::handlerTimer0;
gba::IRQ::IRQHandler gba::IRQ::handlerTimer1;
gba::IRQ::IRQHandler gba::IRQ::handlerTimer2;
gba::IRQ::IRQHandler gba::IRQ::handlerTimer3;
gba::IRQ::IRQHandler gba::IRQ::handlerSerial;
gba::IRQ::IRQHandler gba::IRQ::handlerDMA0;
gba::IRQ::IRQHandler gba::IRQ::handlerDMA1;
gba::IRQ::IRQHandler gba::IRQ::handlerDMA2;
gba::IRQ::IRQHandler gba::IRQ::handlerDMA3;
gba::IRQ::IRQHandler gba::IRQ::handlerKeypad;
gba::IRQ::IRQHandler gba::IRQ::handlerExternal;

static void userIRQHandler()
{
	enum gba::InterruptSrc pending = gba::IRQ::GetPendingInterrupts();
	if(pending & gba::IRQ_VBLANK) {
		if(gba::IRQ::handlerVBlank != nullptr) gba::IRQ::handlerVBlank();
	}
	if(pending & gba::IRQ_HBLANK) {
		if(gba::IRQ::handlerHBlank != nullptr) gba::IRQ::handlerHBlank();
	}
	if(pending & gba::IRQ_VCOUNT) {
		if(gba::IRQ::handlerVCount != nullptr) gba::IRQ::handlerVCount();
	}
	if(pending & gba::IRQ_TIMER0) {
		if(gba::IRQ::handlerTimer0 != nullptr) gba::IRQ::handlerTimer0();
	}
	if(pending & gba::IRQ_TIMER1) {
		if(gba::IRQ::handlerTimer1 != nullptr) gba::IRQ::handlerTimer1();
	}
	if(pending & gba::IRQ_TIMER2) {
		if(gba::IRQ::handlerTimer2 != nullptr) gba::IRQ::handlerTimer2();
	}
	if(pending & gba::IRQ_TIMER3) {
		if(gba::IRQ::handlerTimer3 != nullptr) gba::IRQ::handlerTimer3();
	}
	if(pending & gba::IRQ_SERIAL) {
		if(gba::IRQ::handlerSerial != nullptr) gba::IRQ::handlerSerial();
	}
	if(pending & gba::IRQ_DMA0) {
		if(gba::IRQ::handlerDMA0 != nullptr) gba::IRQ::handlerDMA0();
	}
	if(pending & gba::IRQ_DMA1) {
		if(gba::IRQ::handlerDMA1 != nullptr) gba::IRQ::handlerDMA1();
	}
	if(pending & gba::IRQ_DMA2) {
		if(gba::IRQ::handlerDMA2 != nullptr) gba::IRQ::handlerDMA2();
	}
	if(pending & gba::IRQ_DMA3) {
		if(gba::IRQ::handlerDMA3 != nullptr) gba::IRQ::handlerDMA3();
	}
	if(pending & gba::IRQ_KEYPAD) {
		if(gba::IRQ::handlerKeypad != nullptr) gba::IRQ::handlerKeypad();
	}
	if(pending & gba::IRQ_EXT) {
		if(gba::IRQ::handlerExternal != nullptr) gba::IRQ::handlerExternal();
	}

	printf("IRQ %04x\n", pending);

	gba_irq_flags |= pending;
	gba::IRQ::AcknowledgeInterrupt(pending);
}

void gba::IRQ::Init() {
	gba_irq_handler = userIRQHandler;
	AcknowledgeInterrupt(GetPendingInterrupts());
	// Enable the interrupts
	asm("mrs r0, CPSR\n"
	    "bic r0, r0, #0x80\n"
	    "msr CPSR, r0"
		: : : "r0");

}
