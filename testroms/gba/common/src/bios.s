
// Software interrupts
    .section .text
    .thumb

    .macro syscall name id
    .global \name
    .type \name, %function
\name:
    .fnstart
    swi \id
    bx lr
    .fnend
    .endm

    syscall SoftReset           0x00
    syscall RegisterRamReset    0x01
    syscall Halt                0x02
    syscall Stop                0x03
    syscall IntrWait            0x04
    syscall VBlankIntrWait      0x05
    syscall Sqrt                0x08
    syscall ArcTan              0x09
    syscall ArcTan2             0x0A
    syscall CpuSet              0x0B
    syscall CpuFastSet          0x0C
    syscall GetBiosChecksum     0x0D
    syscall BgAffineSet         0x0E
    syscall ObjAffineSet        0x0F
    syscall BitUnPack           0x10
    syscall LZ77UnCompReadNormalWrite8bit   0x11
    syscall LZ77UnCompReadNormalWrite16bit  0x12
    syscall HuffUnCompReadNormal            0x13
    syscall RLUnCompReadNormalWrite8bit     0x14
    syscall RLUnCompReadNormalWrite16bit    0x15
    syscall Diff8bitUnFilterWrite8bit       0x16
    syscall Diff8bitUnFilterWrite16bit      0x17
    syscall Diff16bitUnfilter   0x18
    syscall SoundBias           0x19
    syscall SoundDriverInit     0x1A
    syscall SoundDriverMode     0x1B
    syscall SoundDriverMain     0x1C
    syscall SoundDriverVSync    0x1D
    syscall SoundChannelClear   0x1E
    syscall MidiKey2Freq        0x1F
    syscall SoundUnk0           0x20
    syscall SoundUnk1           0x21
    syscall SoundUnk2           0x22
    syscall SoundUnk3           0x23
    syscall SoundUnk4           0x24
    syscall MultiBoot           0x25
    syscall HardReset           0x26
    syscall CustomHalt          0x27
    syscall SoundDriverVSyncOff 0x28
    syscall SoundDriverVSyncOn  0x29
    syscall SoundGetJumpList    0x2A
    syscall EmulatorWrite       0x30
    syscall EmulatorExit        0x31


    .global Div
    .type Div, %function
Div:
    .fnstart
    swi 0x06
    bx lr
    .fnend

    .global DivRem
    .type DivRem, %function
DivRem:
    .fnstart
    swi 0x06
    mov r0,r1
    bx lr
    .fnend

    .global DivRes
    .type DivRes, %function
DivRes:
    .fnstart
    push {r4}
    mov r4,r0
    mov r0,r1
    mov r1,r2
    swi 0x06
    str r2,[r4,#8]
    str r1,[r4,#4]
    str r0,[r4]
    pop {r4}
    bx lr
    .fnend

    .global DivArm
    .type DivArm, %function
DivArm:
    .fnstart
    swi 0x07
    bx lr
    .fnend

    .global DivRemArm
    .type DivRemArm, %function
DivRemArm:
    .fnstart
    swi 0x07
    mov r0,r1
    bx lr
    .fnend

    .global DivResArm
    .type DivResArm, %function
DivResArm:
    .fnstart
    push {r4}
    mov r4,r0
    mov r0,r1
    mov r1,r2
    swi 0x07
    str r2,[r4,#8]
    str r1,[r4,#4]
    str r0,[r4]
    bx lr
    .fnend
