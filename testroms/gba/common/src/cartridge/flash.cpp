//
// Created by Robbert-Jan de Jager on 1-12-18.
//

#include <gba/flash.h>
namespace gba {
	namespace flash {
		struct DeviceID GetID() {
			WriteCommand(CommandID::EnterIDMode);
			struct DeviceID result{GetByte(0), GetByte(1)};
			WriteCommand(CommandID::ExitCommand);
			return result;
		}

		void EraseAll() {
			Register <FLASH_CMD1_ADDR, uint8_t> cmd1;
			Register <FLASH_CMD2_ADDR, uint8_t> cmd2;

			Register <FLASH_BASE_ADDR, uint8_t> reg;
			cmd1 = 0xAA;
			cmd2 = 0x55;
			reg = static_cast<uint8_t>(CommandID::EraseAll);
			while (reg != 0xFF);
		}

		void EraseSector(uint16_t addr) {
			Register <FLASH_CMD1_ADDR, uint8_t> cmd1;
			Register <FLASH_CMD2_ADDR, uint8_t> cmd2;
			uint32_t sectorAddr = FLASH_BASE_ADDR + (addr & ~0xFFF);
			WriteCommand(CommandID::Erase);
			cmd1 = 0xAA;
			cmd2 = 0x55;
			*((volatile uint8_t *) FLASH_BASE_ADDR + sectorAddr) = static_cast<uint8_t>(CommandID::EraseSector);

			while (GetByte(sectorAddr) != 0xFF);
		}

	}
}
