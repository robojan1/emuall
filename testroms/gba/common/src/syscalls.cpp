//
// Created by Robbert-Jan on 2018-12-28.
//

#include <errno.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/times.h>
#include <sys/types.h>
#include <reent.h>

#include <gba/emulator.h>
#include <gba/bios.h>

extern "C" {
char **environ = {nullptr};

void _ATTRIBUTE ((__noreturn__)) _exit(int rc)
{
    if(gba::emulator::IsInEmulator()){
        gba::emulator::Exit(rc);
    }

    Stop();

    while(true);
}

int _close(int file)
{
    errno = EBADF;
    return -1;
}

int _execve(const char *name, char *const argv[], char * const env[])
{
    errno = ENOSYS;
    return -1;
}

pid_t _fork()
{
    errno = ENOSYS;
    return -1;
}

int _fstat(int file, struct stat *st)
{
    if(STDOUT_FILENO == file || STDERR_FILENO == file) {
        st->st_mode = S_IFCHR;
        return 0;
    } else {
        errno = EBADF;
        return -1;
    }
}

pid_t _getpid()
{
    return 1;
}

int _isatty(int file)
{
    if(STDOUT_FILENO == file || STDERR_FILENO == file) {
        return 1;
    } else {
        errno = EBADF;
        return -1;
    }
}

int _kill(pid_t pid, int sig)
{
    errno = ENOSYS;
    return -1;
}

int _link(const char *oldpath, const char *newpath)
{
    errno = EMLINK;
    return -1;
}

off_t _lseek(int file, off_t offset, int whence)
{
    if(STDOUT_FILENO == file || STDERR_FILENO == file) {
        return 0;
    } else {
        errno = EBADF;
        return -1;
    }
}

int _open(const char *name, int flags, mode_t mode)
{
    errno = ENOSYS;
    return -1;
}

ssize_t _read(int file, void *ptr, size_t len)
{
    if(STDOUT_FILENO == file || STDERR_FILENO == file) {
        return 0;
    } else {
        errno = EBADF;
        return -1;
    }
}

void *_sbrk(int nbytes)
{
    extern char _end;
    static char *heap_end = nullptr;
    char *prev_heap_end;

    if(heap_end == nullptr) {
        heap_end = &_end;
    }
    prev_heap_end = heap_end;
    if(heap_end + nbytes > (char *)0x02040000)
    {
        errno = ENOMEM;
        return (void *)-1;
    }

    heap_end += nbytes;
    return prev_heap_end;
}

int _stat(const char *file, struct stat *st)
{
    errno = ENOSYS;
    return -1;
}

clock_t _times(struct tms *buf) {
    errno = ENOSYS;
    return static_cast<clock_t>(-1);
}

int _unlink(const char *name) {
    errno = ENOENT;
    return -1;
}

pid_t _wait(int *status) {
    errno = ECHILD;
    return -1;
}

ssize_t _write(int file, const void *buf, size_t len) {
    if(STDOUT_FILENO == file || STDERR_FILENO == file) {
        if(gba::emulator::IsInEmulator()) {
            gba::emulator::Write(buf, len);
        }
        return len;
    } else {
        errno = EBADF;
        return -1;
    }
}

}

