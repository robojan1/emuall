$version = "8-2018-q4-major"
$url = "https://developer.arm.com/-/media/Files/downloads/gnu-rm/8-2018q4/gcc-arm-none-eabi-8-2018-q4-major-win32.zip?revision=ab9cb8f8-6a9d-4a6e-818a-295f5d1ca982?product=GNU%20Arm%20Embedded%20Toolchain,ZIP,,Windows,8-2018-q4-major"
$fullname = "gcc-arm-none-eabi-${version}"
$archive = "${fullname}-windows.zip"


Add-Type -AssemblyName System.IO.Compression.FileSystem
function Unzip
{
    param([string]$zipfile, [string]$outpath)
    [Environment]::CurrentDirectory = (Get-Location -PSProvider FileSystem).ProviderPath
    [System.IO.Compression.ZipFile]::ExtractToDirectory($zipfile, $outpath)
}
function DownloadFile
{
    param([string]$uri, [string]$outpath)
    #[Environment]::CurrentDirectory = (Get-Location -PSProvider FileSystem).ProviderPath
    
    #$wc = New-Object System.Net.WebClient
    #$wc.DownloadFile($uri, $outpath)
    
    Invoke-WebRequest -Uri $uri -UseBasicParsing -OutFile $outpath
}

if(Test-Path "toolchain/") {
    Write-Output "Toolchain already installed, If this is not correct remove the Toolchain directory"
    exit
}

if(Test-Path $archive) {
    Remove-Item -Path $archive -Force
}

DownloadFile $url $archive

Unzip $archive "toolchain/"

Remove-Item -Path $archive -Force

