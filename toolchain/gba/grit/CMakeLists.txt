cmake_minimum_required(VERSION 3.12)
project(grit)

set(CMAKE_MODULE_PATH ${CMAKE_SOURCE_DIR}/../../../cmake)

SET(GRIT_VERSION 0.8.6)
SET(GRIT_BUILD 20100317)

ADD_DEFINITIONS(-DGRIT_VERSION=\"${GRIT_VERSION}\" -DGRIT_BUILD=\"${GRIT_VERSION}\")

ADD_SUBDIRECTORY(srcgrit)
ADD_SUBDIRECTORY(cldib)
ADD_SUBDIRECTORY(libgrit)
IF(WIN32)
	# NOt working
	#ADD_SUBDIRECTORY(srcwingrit)
ENDIF()
