#!/bin/sh

VERSION=8-2018-q4-major
FULLNAME=gcc-arm-none-eabi-${VERSION}
ARCHIVE=${FULLNAME}-linux.tar.bz2
URL=https://developer.arm.com/-/media/Files/downloads/gnu-rm/8-2018q4/gcc-arm-none-eabi-8-2018-q4-major-linux.tar.bz2?revision=d830f9dd-cd4f-406d-8672-cca9210dd220?product=GNU%20Arm%20Embedded%20Toolchain,64-bit,,Linux,8-2018-q4-major

if [ -e ${FULLNAME} ]; then
	echo "Toolchain already installed. If this is not correct delete ${FULLNAME}"
	exit 0
fi

rm -f ${ARCHIVE}

wget --content-disposition ${URL}

echo Extracting archive

tar -xjf ${ARCHIVE}

echo Creating link
ln -s ${FULLNAME} toolchain

echo Cleaning up
rm -f ${ARCHIVE}
