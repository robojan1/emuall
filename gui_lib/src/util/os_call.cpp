//
// Created by Robbert-Jan de Jager on 17-10-18.
//

#include <emuall/util/os_call.h>

#include <cstring>
#include <string>

#include <stdlib.h>
#if defined(_MSC_VER) // Microsoft compiler
#include <windows.h>
#elif defined(__GNUC__) // GNU compiler
#include <dlfcn.h>
#include <errno.h>
#else
#error define your compiler
#endif

#ifdef _MSC_VER
static std::string GetLastErrorAsString()
{
    //Get the error message, if any.
    DWORD errorMessageID = ::GetLastError();
    if(errorMessageID == 0)
        return std::string(); //No error message has been recorded

    LPSTR messageBuffer = nullptr;
    size_t size = FormatMessageA(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
                                 NULL, errorMessageID, MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), (LPSTR)&messageBuffer, 0, NULL);

    std::string message(messageBuffer, size);

    //Free the buffer.
    LocalFree(messageBuffer);

    return message;
}

#define RTLD_LAZY   1
#define RTLD_NOW    2
#define RTLD_GLOBAL 4
#endif

void* LoadSharedLibrary(const char *pcDllname, int iMode)
{
	void *handle = NULL;
#if defined(_MSC_VER) // Microsoft compiler
	handle = (void*) LoadLibraryA(pcDllname);
#elif defined(__GNUC__) // GNU compiler
	handle = dlopen(pcDllname, iMode);
#endif
	return handle;
}

std::string GetLastLibraryError()
{
#if defined(_MSC_VER)
	return GetLastErrorAsString();
#elif defined(__GNUC__)
	return dlerror();
#endif
}

void *GetFunction(void *Lib, const char *Fnname)
{
#if defined(_MSC_VER) // Microsoft compiler
	return (void*) GetProcAddress((HINSTANCE) Lib, Fnname);
#elif defined(__GNUC__) // GNU compiler
	return dlsym(Lib, Fnname);
#endif
}

void *_GetStdcallFunc(void *lib, const char *name, int argSize)
{
#ifdef _WIN32
	#ifndef _WIN64
	static char buffer[255];
	sprintf(buffer, "_%s@%d", name, argSize);
	printf("Function %s\n", buffer);
	return GetFunction(lib, buffer);
#endif
	(void)argSize;
	return GetFunction(lib, name);
#else
	(void)argSize;
	return GetFunction(lib, name);
#endif
}

bool FreeSharedLibrary(void *hDLL)
{
#if defined(_MSC_VER) // Microsoft compiler
	return FreeLibrary((HINSTANCE) hDLL) != 0;
#elif defined(__GNUC__) // GNU compiler
	return dlclose(hDLL);
#endif
}
