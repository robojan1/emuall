//
// Created by Robbert-Jan de Jager on 11-10-18.
//

#include "Server_p.h"
#include "ServerConnection.h"
#include <emuall/exception.h>
#include <iostream>

#include <boost/asio.hpp>
#include <boost/bind.hpp>
#include <emuall/util/log.h>
#include <emuall/gdb/Server.h>


gdb::Server::Server(int port) :
	_priv(std::make_unique<gdb::Server_p>(port, *this))
{
}

gdb::Server::~Server() {
}

gdb::Server_p::Server_p(int port, Server &server) :
	appAttached(false), numRegisters(0), appStatus(Exited), appSignalNr(0), textOffset(0), dataOffset(0),
	_server(server),
	_acceptor(boost::asio::ip::tcp::acceptor(io_context, boost::asio::ip::tcp::endpoint(boost::asio::ip::tcp::v4(), port), true)),
	_clientSocket(std::make_shared<boost::asio::ip::tcp::socket>(io_context))
{
	StartAccept();

	_thread = std::make_unique<std::thread>(&gdb::Server_p::run_asio, this);
}

gdb::Server_p::~Server_p() {
	if (!io_context.stopped()) {
		io_context.stop();
	}
	if (_thread->joinable()) {
		_thread->join();
	}
}

void gdb::Server_p::run_asio()
{
	try {
		io_context.run();
	}
	catch (std::exception &e) {
		std::cerr << e.what() << std::endl;
	}
}
void gdb::Server_p::handle_accept(const boost::system::error_code & error)
{
	if (!error) {
		if (gdbConnection || !_clientSocket->is_open()) {
			return;
		}

		try {
			gdbConnection = std::make_unique<ServerConnection>(_server, _clientSocket);
			logger::Log(logger::Message, "GDB client connected");
		}
		catch (BaseException &) {
			gdbConnection.reset();
			boost::system::error_code ec;
			_clientSocket->shutdown(boost::asio::socket_base::shutdown_both, ec);
			if (ec) {
				_clientSocket->close(ec);
			}
		}
	} else {
		logger::Log(logger::Warn, "GDB accept error: %s", error.message().c_str());
	}
}

void gdb::Server_p::StartAccept()
{
	_acceptor.async_accept(*_clientSocket, boost::bind(&gdb::Server_p::handle_accept, this, boost::asio::placeholders::error));
}

void gdb::Server_p::DisconnectClient() {
	if(gdbConnection) {
		gdbConnection.reset();
	}
}

void gdb::Server::OnGdbClientDisconnected()
{
	if (_priv->gdbClientDisconnected) {
		_priv->gdbClientDisconnected();
	}
	logger::Log(logger::Message, "GDB client disconnected");
	_priv->DisconnectClient();
	_priv->StartAccept();
}

void gdb::Server::OnGdbClientContinue(bool hasAddr, uint32_t addr)
{
	if (_priv->gdbClientContinue)
		_priv->gdbClientContinue(hasAddr, addr);
}

void gdb::Server::OnGdbClientStep(bool hasAddr, uint32_t addr)
{
	if (_priv->gdbClientStep)
		_priv->gdbClientStep(hasAddr, addr);
}

void gdb::Server::OnGdbClientAddBreakpoint(uint32_t addr, int kind)
{
	if (_priv->gdbClientAddBreakpoint)
		_priv->gdbClientAddBreakpoint(addr, kind);
}

void gdb::Server::OnGdbClientRemoveBreakpoint(uint32_t addr, int kind)
{
	if (_priv->gdbClientRemoveBreakpoint)
		_priv->gdbClientRemoveBreakpoint(addr, kind);
}

void gdb::Server::OnGdbClientInterrupt()
{
	if (_priv->gdbClientInterrupt)
		_priv->gdbClientInterrupt();
}

bool gdb::Server::IsAttached() const {
	return _priv->appAttached;
}

void gdb::Server::SetAttached(bool attached) {
	_priv->appAttached = attached;
}

int gdb::Server::GetNumberOfRegisters() const {
	return _priv->numRegisters;
}

void gdb::Server::SetNumRegisters(int num) {
	assert(num >= 0);
	_priv->numRegisters = num;
}

void gdb::Server::SetReadRegisterFunc(const std::function<RegisterValue(int)> &func) {
	_priv->readRegistersFunc = func;
}

void gdb::Server::SetWriteRegisterFunc(const std::function<void(int,const RegisterValue &)> &func) {
	_priv->writeRegistersFunc = func;
}

gdb::RegisterValue gdb::Server::ReadRegister(int i) {
	if(_priv->readRegistersFunc == nullptr) {
		return {Reg8, false};
	}
	return _priv->readRegistersFunc(i);
}

void gdb::Server::WriteRegister(int i, const RegisterValue &value)
{
	if(_priv->writeRegistersFunc != nullptr) {
		_priv->writeRegistersFunc(i, value);
	}
}

const std::string &gdb::Server::GetExecutableFilename() const {
	return _priv->filename;
}

void gdb::Server::SetExecutableFilename(const std::string &filename) {
	_priv->filename = filename;
}

void gdb::Server::SetTargetDescription(const std::string &description) {
	_priv->targetDescription = description;
}

void gdb::Server::SetTargetMemoryMap(const std::string &memoryMap) {
	_priv->targetMemoryMap = memoryMap;
}

const std::string &gdb::Server::GetTargetDescription() const {
	return _priv->targetDescription;
}

const std::string &gdb::Server::GetTargetMemoryMap() const {
	return _priv->targetMemoryMap;
}

void *gdb::Server::GetMemoryPointer(uint32_t addr, int32_t *size) {
	if(_priv->getMemoryFunc == nullptr) {
		*size = -ENOTSUP;
		return nullptr;
	}
	return _priv->getMemoryFunc(addr, size);
}

void gdb::Server::SetMemory(uint32_t addr, uint8_t val)
{
	if(_priv->setMemoryFunc != nullptr)
	{
		_priv->setMemoryFunc(addr, val);
	}
}

void gdb::Server::SetGetMemoryFunc(const std::function<void *(uint32_t, int32_t *)> &func) {
	_priv->getMemoryFunc = func;
}

void gdb::Server::SetSetMemoryFunc(const std::function<void(uint32_t, uint8_t)>& func)
{
	_priv->setMemoryFunc = func;
}

void gdb::Server::SetAppStatus(gdb::ApplicationStatus status, int signalNr) {
	_priv->appStatus = status;
	_priv->appSignalNr = signalNr;
}

void gdb::Server::SetOffsets(uint32_t text, uint32_t data)
{
	_priv->textOffset = text;
	_priv->dataOffset = data;
}

enum gdb::ApplicationStatus gdb::Server::GetAppStatus() const
{
	return _priv->appStatus;
}

int gdb::Server::GetAppLastSignal() const
{
	return _priv->appSignalNr;
}

uint32_t gdb::Server::GetTextOffset() const
{
	return _priv->textOffset;
}

uint32_t gdb::Server::GetDataOffset() const
{
	return _priv->dataOffset;
}

void gdb::Server::OnAppSignalReceived() {
	if(_priv->gdbConnection) {
		_priv->gdbConnection->OnAppSignalReceived();
	}
}

void gdb::Server::RegisterDisconnectedCallback(std::function<void(void)> &&func)
{
	_priv->gdbClientDisconnected = std::move(func);
}

void gdb::Server::RegisterContinueCallback(std::function<void(bool, uint32_t)> &&func)
{
	_priv->gdbClientContinue = std::move(func);
}

void gdb::Server::RegisterStepCallback(std::function<void(bool, uint32_t)> &&func)
{
	_priv->gdbClientStep = std::move(func);
}

void gdb::Server::RegisterAddBreakpointCallback(std::function<void(uint32_t, int)> &&func)
{
	_priv->gdbClientAddBreakpoint = std::move(func);
}

void gdb::Server::RegisterRemoveBreakpointCallback(std::function<void(uint32_t, int)> &&func)
{
	_priv->gdbClientRemoveBreakpoint = std::move(func);
}

void gdb::Server::RegisterInterruptCallback(std::function<void(void)> &&func)
{
	_priv->gdbClientInterrupt = std::move(func);
}

void gdb::Server::DeregisterDisconnectedCallback(std::function<void(void)> &&func) 
{
	_priv->gdbClientDisconnected = nullptr;
}

void gdb::Server::DeregisterContinueCallback(std::function<void(bool, uint32_t)> &&func)
{
	_priv->gdbClientContinue = nullptr;
}

void gdb::Server::DeregisterStepCallback(std::function<void(bool, uint32_t)> &&func)
{
	_priv->gdbClientStep = nullptr;
}

void gdb::Server::DeregisterAddBreakpointCallback(std::function<void(uint32_t, int)> &&func)
{
	_priv->gdbClientAddBreakpoint = nullptr;
}

void gdb::Server::DeregisterRemoveBreakpointCallback(std::function<void(uint32_t, int)> &&func)
{
	_priv->gdbClientRemoveBreakpoint = nullptr;
}

void gdb::Server::DeregisterInterruptCallback(std::function<void(void)> &&func)
{
	_priv->gdbClientInterrupt = nullptr;
}

bool gdb::Server::HasTargetMemoryMap() const {
	return !_priv->targetMemoryMap.empty();
}
