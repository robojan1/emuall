//
// Created by Robbert-Jan de Jager on 13-10-18.
//

/*#include "ClientMessageEvent.h"

wxDEFINE_EVENT(GDB_CLIENT_MESSAGE, gdb::ClientMessageEvent);
wxDEFINE_EVENT(GDB_CONTINUE, gdb::ClientContinueEvent);
wxDEFINE_EVENT(GDB_STEP, gdb::ClientContinueEvent);
wxDEFINE_EVENT(GDB_ADDBREAKPOINT, gdb::ClientBreakpointEvent);
wxDEFINE_EVENT(GDB_REMOVEBREAKPOINT, gdb::ClientBreakpointEvent);
wxDEFINE_EVENT(GDB_INTERRUPT, gdb::ClientSignalEvent);

gdb::ClientMessageEvent::ClientMessageEvent(wxEventType eventType, int winid, const std::string &msg) :
	wxEvent(winid, eventType), _msg(msg)
{

}

gdb::ClientContinueEvent::ClientContinueEvent(wxEventType eventType, int winid) :
	wxEvent(winid, eventType), _hasAddr(false), _addr(0)
{

}

gdb::ClientContinueEvent::ClientContinueEvent(wxEventType eventType, int winid, uint32_t addr) :
	wxEvent(winid, eventType), _hasAddr(true), _addr(addr)
{

}

gdb::ClientBreakpointEvent::ClientBreakpointEvent(wxEventType eventType, int winid, uint32_t addr, int kind) :
	wxEvent(winid, eventType), _addr(addr), _kind(kind)
{

}

gdb::ClientSignalEvent::ClientSignalEvent(wxEventType eventType, int winid) :
	wxEvent(winid, eventType)
{

}
*/