//
// Created by Robbert-Jan de Jager on 11-10-18.
//

#ifndef EMUALL_SERVERCONNECTION_H
#define EMUALL_SERVERCONNECTION_H

#include <memory>
#include <array>
#include <boost/asio.hpp>

namespace gdb {

class Server;

class ServerConnection {
public:
	explicit ServerConnection(gdb::Server &server, std::shared_ptr<boost::asio::ip::tcp::socket> socket);
	virtual ~ServerConnection();

	void OnAppSignalReceived();

private:
	//void OnSocketEvent(wxSocketEvent &evt);
	void OnPacketSend(const boost::system::error_code &ec, size_t bytes_transferred);
	void OnPacketReceived(const boost::system::error_code &ec, size_t bytes_transferred);
	void ParseStream();

	void CloseConnection(); // WARNING!!!!: This is deleted after this function

	Server &_server;
	std::shared_ptr<boost::asio::ip::tcp::socket> _socket{};
	std::array<uint8_t, 16*1024> _buffer;
	std::string _replyBuffer;
	bool _replyInProgress;
	size_t _buffer_size;
	bool _ackEnabled;
	bool _extendedMode;

	void ParseEscapedMessage(std::array<unsigned char, 16384>::const_iterator first,
	                         std::array<unsigned char, 16384>::const_iterator end);
	void ParseMessage(std::string basic_string);
	void SendAck();
	void SendNak();
	void SendReply(const std::string &msg);
	void SendReplyPacket(const std::string &packet);
	void SendError(int error);
	void SendStatus();
	void EncodeMessage(const std::string &input, std::string &output);
	void ProcessQuery(const std::string &msg);
	void ProcessMultiletter(const std::string &msg);
	void ProcessSetThreadOp(const std::string &msg);
	void ReplyQueryCRC(std::vector<std::string>& args);
	void ReplyQueryOffsets();
	void ReplyQuerySupported(std::vector<std::string> &args);
	void ReplyQuerySymbol(std::vector<std::string> &args);
	void ReplyQueryThreadExtraInfo(std::vector<std::string> &args);
	void ReplyQueryThreadID();
	void ReplyQueryAttached(const std::string &msg);
	void ProcessReadGeneralRegister(const std::string &msg);
	void ProcessWriteGeneralRegisters(const std::string &msg);
	void ProcessReadRegisterValue(const std::string &msg);
	void ProcessWriteRegisterValue(const std::string &msg);
	void ReplyQueryFirstThreadInfo();
	void ReplyQueryNextThreadInfo();
	void ProcessXfer(std::vector<std::string> &args);
	void ProcessXferWrite(std::vector<std::string> &args);
	void ProcessXferRead(std::vector<std::string> &args);
	void ReplyQueryXferFeatures(const std::string &annex, int offset, int length);
	void ReplyQueryXferExecFile(int annex, int offset, int length);
	void ReplyQueryXferMemoryMap(int offset, int length);
	void ProcessOldContinue(const std::string &msg);
	void ProcessContinue(std::vector<std::string> &args);
	void ProcessOldContinueWithSignal(const std::string &msg);
	void ProcessOldStep(const std::string &msg);
	void ProcessOldStepWithSignal(const std::string &msg);
	void ProcessRangeStepping(const std::string &start, const std::string &end);
	void ProcessReadMemory(const std::string &msg);
	void ProcessWriteMemory(const std::string &msg);
	void ProcessWriteMemoryBinary(const std::string &msg);
	void ProcessAddBreakpoint(const std::string &msg);
	void ProcessAddSoftwareBreakpoint(const std::vector<std::string> &args);
	void ProcessRemoveBreakpoint(const std::string &msg);
	void ProcessRemoveSoftwareBreakpoint(const std::vector<std::string> &args);
	void ProcessCtrlC();
	void ProcessDetach();
};

}


#endif //EMUALL_SERVERCONNECTION_H
