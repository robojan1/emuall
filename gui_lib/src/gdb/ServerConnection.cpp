//
// Created by Robbert-Jan de Jager on 11-10-18.
//

#include <numeric>
#include <iomanip>
#include <sstream>

#include "ServerConnection.h"
#include <emuall/util/string.h>
#include <emuall/exception.h>
#include <emuall/gdb/Server.h>
#include <emuall/util/log.h>
#include <emuall/util/string.h>

#include <boost/asio.hpp>
#include <boost/bind.hpp>
#include <boost/crc.hpp>

struct ConnectionClosedException
{
	
};

gdb::ServerConnection::ServerConnection(gdb::Server &server, std::shared_ptr<boost::asio::ip::tcp::socket> socket) :
	_server(server), _socket(std::move(socket)),
	_ackEnabled(true), _extendedMode(false), _replyInProgress(false)
{
	_buffer_size = 0;

	_socket->async_read_some(boost::asio::buffer(_buffer.data() + _buffer_size, _buffer.size() - _buffer_size),
		boost::bind(&gdb::ServerConnection::OnPacketReceived, this, 
			boost::asio::placeholders::error, boost::asio::placeholders::bytes_transferred));
}

gdb::ServerConnection::~ServerConnection() {
	_socket->shutdown(boost::asio::socket_base::shutdown_both);
	_socket->close();
}

void gdb::ServerConnection::OnPacketSend(const boost::system::error_code & ec, size_t bytes_transferred)
{
	try {
		if (ec) {
			logger::Log(ec == boost::asio::error::eof ? logger::Debug : logger::Warn, "GDB connection error: %s", ec.message().c_str());
			CloseConnection();
			return;
		}
		_replyInProgress = false;
	}
	catch (ConnectionClosedException)
	{}
}

void gdb::ServerConnection::OnPacketReceived(const boost::system::error_code & ec, size_t bytes_transferred)
{
	if (!_socket->is_open()) {

		return;
	}
	try {
		if (!ec) {
			_buffer_size += bytes_transferred;
			ParseStream();
			_socket->async_read_some(boost::asio::buffer(_buffer.data() + _buffer_size, _buffer.size() - _buffer_size),
				boost::bind(&gdb::ServerConnection::OnPacketReceived, this,
					boost::asio::placeholders::error, boost::asio::placeholders::bytes_transferred));
		}
		else {
			logger::Log(ec == boost::asio::error::eof ? logger::Debug : logger::Warn, "GDB connection error: %s", ec.message().c_str());
			CloseConnection();
			return;// This is deleted after this function
		}
	}
	catch (ConnectionClosedException)
	{}
}

void gdb::ServerConnection::ParseStream() {
	auto end = _buffer.cbegin() + _buffer_size;
	auto it = end;
	for(it = _buffer.cbegin(); it < end; it++) {
		if(*it == 0x03) {
			// Ctrl-C
			ProcessCtrlC();
			continue;
		}
		if(*it != '$') {
			continue;
		}
		auto eom = std::find(it, end, '#');
		if(eom == end || eom + 1 == end || eom + 2 == end) {
			std::copy(it, end, _buffer.begin());
			break;
		}
		if(!isxdigit(eom[1]) || !isxdigit(eom[2])) {
			SendNak();
			continue;
		}
		uint8_t msgChecksum = xdigitToNumber(eom[1]) * 16 + xdigitToNumber(eom[2]);
		uint8_t checksum = std::accumulate(it+1, eom, 0);
		if(msgChecksum != checksum) {
			SendNak();
			continue;
		}
		if(_ackEnabled) {
			SendAck();
		}
		ParseEscapedMessage(it + 1, eom);
		it = eom + 2;
	}
	_buffer_size = end - it;
}

void gdb::ServerConnection::CloseConnection() {
	_server.OnGdbClientDisconnected();
	throw ConnectionClosedException();
}

void gdb::ServerConnection::ParseEscapedMessage(std::array<unsigned char, 16384>::const_iterator first,
                                                std::array<unsigned char, 16384>::const_iterator end) {
	std::vector<uint8_t> unescapedMessage;
	unescapedMessage.reserve(std::distance(first, end));

	for(auto it = first; it != end; it++) {
		if(*it == '}') {
			it++;
			if(it == end) {
				throw BaseException("Corrupt GDB message");
			}
			unescapedMessage.push_back(*it ^ 0x20);
		}  else {
			unescapedMessage.push_back(*it);
		}
	}

	std::string message(unescapedMessage.begin(),unescapedMessage.end());
	ParseMessage(message);
}

void gdb::ServerConnection::ParseMessage(std::string msg) {
	logger::Log(logger::Debug, "GDB message: %s", msg.c_str());
	bool hasSequenceId = msg.length() >= 3 && msg[2] == ':';

	if(hasSequenceId) {
		// Ignore the sequence ID
		msg = msg.substr(3);
	}
	if(msg.length() == 0) return;
	switch(msg[0]) {
		case '!':
			_extendedMode = true;
			SendReply("OK");
			break;
		case '?':
			SendStatus();
			break;
		case 'A': // Set argv
		case 'b': // Set baudrate
		case 'B': // Set or clear a breakpoint
			SendError(ENOTSUP);
			break;
		case 'c': // Continue
			ProcessOldContinue(msg);
			break;
		case 'C': // Continue with signal
			ProcessOldContinueWithSignal(msg);
			break;
		case 'D': // Detach
			ProcessDetach();
			return; // This is deleted after this function
		case 'g': // Read general register
			ProcessReadGeneralRegister(msg);
			break;
		case 'G': // Write general registers
			ProcessWriteGeneralRegisters(msg);
			break;
		case 'H': // Set thread operation
			ProcessSetThreadOp(msg);
			break;
		case 'm': // Read memory
			ProcessReadMemory(msg);
			break;
		case 'M': // Write memory
			ProcessWriteMemory(msg);
			break;
		case 'p': // Read register value
			ProcessReadRegisterValue(msg);
			break;
		case 'P': // Write register value
			ProcessWriteRegisterValue(msg);
			break;
		case 'q': // Query
			ProcessQuery(msg);
			break;
		case 's': // Step
			ProcessOldStep(msg);
			break;
		case 'S': // Step with Signal
			ProcessOldStepWithSignal(msg);
			break;
		case 'v': // Multiletter
			ProcessMultiletter(msg);
			break;
		case 'X': // Write data to memory
			ProcessWriteMemoryBinary(msg);
			break;
		case 'Z': // Add breakpoint
			ProcessAddBreakpoint(msg);
			break;
		case 'z': // remove breakpoint
			ProcessRemoveBreakpoint(msg);
			break;
		default: // Unknown
			logger::Log(logger::Warn, "Unkown GDB message: %s", msg.c_str());
			break;
	}
}

void gdb::ServerConnection::SendAck() {
	boost::asio::async_write(*_socket, boost::asio::buffer("+", 1),
		boost::bind(&gdb::ServerConnection::OnPacketSend, this, boost::asio::placeholders::error,
			boost::asio::placeholders::bytes_transferred));
	logger::Log(logger::Debug, "GDB reply Ack");
}

void gdb::ServerConnection::SendNak() {
	boost::asio::async_write(*_socket, boost::asio::buffer("-", 1),
		boost::bind(&gdb::ServerConnection::OnPacketSend, this, boost::asio::placeholders::error,
			boost::asio::placeholders::bytes_transferred));
	logger::Log(logger::Debug, "GDB reply Nak");
}

void gdb::ServerConnection::SendReply(const std::string &msg) {
	std::string encodedMessage("$");
	EncodeMessage(msg, encodedMessage);
	uint8_t checksum = std::accumulate(encodedMessage.cbegin() + 1, encodedMessage.cend(), 0);
	encodedMessage.push_back('#');
	encodedMessage.push_back(NumberToXDigit((checksum >> 4) & 0xF));
	encodedMessage.push_back(NumberToXDigit(checksum & 0xF));
	SendReplyPacket(encodedMessage);
}

void gdb::ServerConnection::SendReplyPacket(const std::string &packet) {
	assert(!_replyInProgress);
	_replyInProgress = true;
	_replyBuffer = packet;
	boost::asio::async_write(*_socket, boost::asio::buffer(_replyBuffer),
		boost::bind(&gdb::ServerConnection::OnPacketSend, this, boost::asio::placeholders::error,
			boost::asio::placeholders::bytes_transferred));
	logger::Log(logger::Debug, "GDB reply: %s", packet.c_str());
}

void gdb::ServerConnection::EncodeMessage(const std::string &input, std::string &output) {
	output.reserve(output.length() + input.length());
	for(char c : input) {
		if(c == '#' || c == '$' || c == '}') {
			output.push_back('}');
			c ^= 0x20;
		}
		output.push_back(c);
	}
}

void gdb::ServerConnection::SendStatus() {
	std::string signalNr = HexToString(_server.GetAppLastSignal(), 2);

	switch(_server.GetAppStatus()) {
		case SignalReceived:
			SendReply("S" + signalNr);
			break;
		case Exited:
			SendReply("W" + signalNr);
			break;
		case Terminated:
			SendReply("X" + signalNr);
			break;
		case Running:
			SendReply("OK");
			break;
	}
}

void gdb::ServerConnection::SendError(int error) {
	std::string errStr = HexToString(error, 2);
	SendReply("E" + errStr);
}

#ifdef _MSC_VER
#pragma warning( push )
#pragma warning( disable: 4307 )
#endif

void gdb::ServerConnection::ProcessQuery(const std::string &msg) {
	size_t typeSize = msg.find(':');
	std::string type = msg.substr(0, typeSize);
	std::vector<std::string> arguments;
	if(typeSize != std::string::npos) {
		arguments = Tokenize(msg.substr(typeSize + 1), ",;:");
	}
	switch(hashStr(type.c_str())) {

		case hashStr("qC"):
			ReplyQueryThreadID();
			break;
		case hashStr("qCRC"):
			ReplyQueryCRC(arguments);
			break;
		case hashStr("qfThreadInfo"):
			ReplyQueryFirstThreadInfo();
			break;
		case hashStr("qsThreadInfo"):
			ReplyQueryNextThreadInfo();
			break;
		case hashStr("qOffsets"):
			ReplyQueryOffsets();
			break;
		case hashStr("qSupported"):
			ReplyQuerySupported(arguments);
			break;
		case hashStr("qSymbol"):
			ReplyQuerySymbol(arguments);
			break;
		case hashStr("qThreadExtraInfo"):
			ReplyQueryThreadExtraInfo(arguments);
			break;
		case hashStr("qXfer"):
			ProcessXfer(arguments);
			break;
		case hashStr("qAttached"):
			ReplyQueryAttached(msg);
			break;
		default:
			logger::Log(logger::Warn, "Unkown GDB query: %s", msg.c_str());
			SendReply("");
			break;
	}
}

void gdb::ServerConnection::ReplyQuerySupported(std::vector<std::string> &args) {
	std::ostringstream ss;
	ss << std::hex;
	//ss << "qSupported:";
	ss << "PacketSize=" << _buffer.size();
	ss << ";qXfer:features:read+";
	ss << ";qXfer:exec-file:read+";
	if(_server.HasTargetMemoryMap()) {
		ss << ";qXfer:memory-map:read+";
	}
	SendReply(ss.str());
}

void gdb::ServerConnection::ReplyQuerySymbol(std::vector<std::string> &args) {
	SendReply("OK");
}

void gdb::ServerConnection::ReplyQueryThreadExtraInfo(std::vector<std::string> &args)
{
	SendReply("");
}

void gdb::ServerConnection::ProcessMultiletter(const std::string &msg) {
	size_t typeSize = msg.find(':');
	std::string type = msg.substr(0, typeSize);
	std::vector<std::string> arguments;
	if(typeSize == std::string::npos) {
		arguments = Tokenize(msg.substr(), ",;");
	}
	switch(hashStr(type.c_str())) {
		case hashStr("vCont"):
			ProcessContinue(arguments);
			break;
		case hashStr("vCont?"):
			SendReply("vCont;c;s");
			break;
		case hashStr("vMustReplyEmpty"): // Must be handled as a unknown command, but is handled to suppress a warning 
			SendReply("");
			break;
		default:
			SendReply("");
			logger::Log(logger::Warn, "Unkown GDB multiletter command: %s", msg.c_str());
			break;
	}
}

void gdb::ServerConnection::ProcessXferRead(std::vector<std::string> &args) {
	try {
		const std::string &type = args[0];
		switch (hashStr(type.c_str())) {
		case hashStr("features"): {
			if (args.size() < 5) {
				SendError(EINVAL);
				break;
			}
			const std::string &annex = args[2];
			int offset = std::stoi(args[3], nullptr, 16);
			int length = std::stoi(args[4], nullptr, 16);
			ReplyQueryXferFeatures(annex, offset, length);
			break;
		}
		case hashStr("exec-file"): {
			int annex = args[2].size() == 0 ? -1 : std::stoi(args[2]);
			int offset = std::stoi(args[3], nullptr, 16);
			int length = std::stoi(args[4], nullptr, 16);
			ReplyQueryXferExecFile(annex, offset, length);
			break;
		}
		case hashStr("memory-map"): {
			int offset = std::stoi(args[3], nullptr, 16);
			int length = std::stoi(args[4], nullptr, 16);
			ReplyQueryXferMemoryMap(offset, length);
			break;
		}
		default:
			logger::Log(logger::Warn, "Unkown GDB transfer read: %s", type.c_str());
			SendReply("");
			break;
		}
	}
	catch (std::invalid_argument &) {
		SendError(EINVAL);
	}
}

#ifdef _MSC_VER
#pragma warning( pop )
#endif

void gdb::ServerConnection::ProcessSetThreadOp(const std::string &msg) {
	if(msg.length() < 3) {
		SendError(EINVAL);
		return;
	}
	try {
		int thread = std::stoi(msg.substr(2));

		if(thread > 1 || thread < -1) {
			SendError(ERANGE);
			return;
		}

		SendReply("OK");
	} catch(std::invalid_argument &) {
		SendError(EINVAL);
		return;
	}
}

void gdb::ServerConnection::ReplyQueryCRC(std::vector<std::string>& args)
{
	if(args.size() < 3)
	{
		SendError(EINVAL);
		return;
	}
	try {
		uint32_t address = std::stoul(args[1], nullptr, 16);
		int32_t length = std::stoi(args[2], nullptr, 16);

		int32_t dataLength;
		uint8_t *data = static_cast<uint8_t *>(_server.GetMemoryPointer(address, &dataLength));
		if(dataLength < 0)
		{
			SendError(-length);
			return;
		}
		if (data == nullptr || dataLength != length)
		{
			SendError(ERANGE);
			return;
		}
		boost::crc_32_type crc;
		crc.process_bytes(data, dataLength);
		
		std::stringstream ss;
		ss << "C" << std::setfill('0') << std::hex << std::setw(8) << crc.checksum();
		SendReply(ss.str());
	} catch(std::out_of_range &)
	{
		SendError(ERANGE);
		return;
	}
	catch(std::invalid_argument &)
	{
		SendError(EINVAL);
		return;
	}
}

void gdb::ServerConnection::ReplyQueryOffsets() {
	//SendReply("TextSeg=0");
	std::stringstream ss;
	ss << std::setfill('0') << std::hex;
	ss << "TextSeg=" << std::setw(8) << _server.GetTextOffset()
		<< ";DataSeg=" << std::setw(8) << _server.GetDataOffset();
	SendReply(ss.str());
}

void gdb::ServerConnection::ReplyQueryThreadID() {
	SendReply("QC1");
}

void gdb::ServerConnection::ReplyQueryAttached(const std::string &msg) {
	SendReply(_server.IsAttached() ? "1" : "0");
}

void gdb::ServerConnection::ProcessReadGeneralRegister(const std::string &msg) {
	int numRegisters = _server.GetNumberOfRegisters();
	std::stringstream ss;
	ss << std::setfill('0') << std::hex;
	for(int i = 0; i < numRegisters; i++) {
		RegisterValue value = _server.ReadRegister(i);
		switch(value.type) {
			default:
			case Reg8:
				if (value.defined)
					ss << std::setw(2) << value.u8;
				else
					ss << "XX";
				break;
			case Reg16:
				if (value.defined) {
					ss << std::setw(2) << (value.u16 & 0xFF);
					ss << std::setw(2) << ((value.u16 >> 8) & 0xFF);
				} else
					ss << "XXXX";
				break;
			case Reg32:
				if (value.defined) {
					ss << std::setw(2) << ((value.u32 >> 0) & 0xFF);
					ss << std::setw(2) << ((value.u32 >> 8) & 0xFF);
					ss << std::setw(2) << ((value.u32 >> 16) & 0xFF);
					ss << std::setw(2) << ((value.u32 >> 24) & 0xFF);
				}else
					ss << "XXXXXXXX";
				break;
			case Reg64:
				if(value.defined) {
					ss << std::setw(2) << ((value.u64 >> 0) & 0xFF);
					ss << std::setw(2) << ((value.u64 >> 8) & 0xFF);
					ss << std::setw(2) << ((value.u64 >> 16) & 0xFF);
					ss << std::setw(2) << ((value.u64 >> 24) & 0xFF);
					ss << std::setw(2) << ((value.u64 >> 32) & 0xFF);
					ss << std::setw(2) << ((value.u64 >> 40) & 0xFF);
					ss << std::setw(2) << ((value.u64 >> 48) & 0xFF);
					ss << std::setw(2) << ((value.u64 >> 56) & 0xFF);
				} else
					ss << "XXXXXXXXXXXXXXXX";
				break;
		}
	}
	SendReply(ss.str());
}

void gdb::ServerConnection::ProcessWriteGeneralRegisters(const std::string &msg)
{
	int numRegisters = _server.GetNumberOfRegisters();
	int pos = 1;
	std::vector<RegisterValue> values;
	try {
		for (int i = 0; i < numRegisters; i++) {
			RegisterValue value = _server.ReadRegister(i);
			switch (value.type) {
				default:
				case Reg8: {
					if (msg.size() < pos + 2) {
						SendError(EINVAL);
						return;
					}
					std::string valueStr = msg.substr(pos, 2);
					value.defined = true;
					value.u8 = std::stoul(valueStr, nullptr, 16);
					break;
				}
				case Reg16: {
					if (msg.size() < pos + 4) {
						SendError(EINVAL);
						return;
					}
					std::string valueStr = msg.substr(pos + 2, 2) + msg.substr(pos, 2);
					value.defined = true;
					value.u16 = std::stoul(valueStr, nullptr, 16);
					break;
				}
				case Reg32: {
					if (msg.size() < pos + 8) {
						SendError(EINVAL);
						return;
					}
					std::string valueStr = msg.substr(pos + 6, 2) + msg.substr(pos + 4, 2) +
					                       msg.substr(pos + 2, 2) + msg.substr(pos, 2);
					value.defined = true;
					value.u32 = std::stoul(valueStr, nullptr, 16);
					break;
				}
				case Reg64: {
					if (msg.size() < pos + 16) {
						SendError(EINVAL);
						return;
					}
					std::string valueStr = msg.substr(pos + 14, 2) + msg.substr(pos + 12, 2) +
					                       msg.substr(pos + 10, 2) + msg.substr(pos + 8, 2) +
					                       msg.substr(pos + 6, 2) + msg.substr(pos + 4, 2) +
					                       msg.substr(pos + 2, 2) + msg.substr(pos, 2);
					value.defined = true;
					value.u64 = std::stoull(valueStr, nullptr, 16);
					break;
				}
			}
			_server.WriteRegister(i, value);
		}
	} catch(std::out_of_range &) {
		SendError(ERANGE);
		return;
	} catch(std::invalid_argument &) {
		SendError(EINVAL);
		return;
	}
}

void gdb::ServerConnection::ProcessReadRegisterValue(const std::string& msg)
{
	int numRegisters = _server.GetNumberOfRegisters();
	try
	{
		int i = std::stoul(msg.substr(1), nullptr, 16);
		if(i >= numRegisters)
		{
			SendError(ERANGE);
			return;
		}
		auto value = _server.ReadRegister(i);
		if(!value.defined)
		{
			SendError(EINVAL);
			return;
		}
		std::stringstream ss;
		ss << std::setfill('0') << std::hex;
		switch (value.type) {
		default:
		case Reg8:
			if (value.defined)
				ss << std::setw(2) << value.u8;
			else
				ss << "XX";
			break;
		case Reg16:
			if (value.defined) {
				ss << std::setw(2) << (value.u16 & 0xFF);
				ss << std::setw(2) << ((value.u16 >> 8) & 0xFF);
			}
			else
				ss << "XXXX";
			break;
		case Reg32:
			if (value.defined) {
				ss << std::setw(2) << ((value.u32 >> 0) & 0xFF);
				ss << std::setw(2) << ((value.u32 >> 8) & 0xFF);
				ss << std::setw(2) << ((value.u32 >> 16) & 0xFF);
				ss << std::setw(2) << ((value.u32 >> 24) & 0xFF);
			}
			else
				ss << "XXXXXXXX";
			break;
		case Reg64:
			if (value.defined) {
				ss << std::setw(2) << ((value.u64 >> 0) & 0xFF);
				ss << std::setw(2) << ((value.u64 >> 8) & 0xFF);
				ss << std::setw(2) << ((value.u64 >> 16) & 0xFF);
				ss << std::setw(2) << ((value.u64 >> 24) & 0xFF);
				ss << std::setw(2) << ((value.u64 >> 32) & 0xFF);
				ss << std::setw(2) << ((value.u64 >> 40) & 0xFF);
				ss << std::setw(2) << ((value.u64 >> 48) & 0xFF);
				ss << std::setw(2) << ((value.u64 >> 56) & 0xFF);
			}
			else
				ss << "XXXXXXXXXXXXXXXX";
			break;
		}
		SendReply(ss.str());
	}
	catch (std::out_of_range &) {
		SendError(ERANGE);
		return;
	}
	catch (std::invalid_argument &) {
		SendError(EINVAL);
		return;
	}
}

void gdb::ServerConnection::ProcessWriteRegisterValue(const std::string& msg)
{
	auto arguments = Tokenize(msg.substr(1), "=");
	if(arguments.size() != 2)
	{
		SendError(EINVAL);
		return;
	}

	int numRegisters = _server.GetNumberOfRegisters();
	try
	{
		int i = std::stoul(arguments[0], nullptr, 16);
		if (i >= numRegisters)
		{
			SendError(ERANGE);
			return;
		}
		auto value = _server.ReadRegister(i);
		if (!value.defined)
		{
			SendError(EINVAL);
			return;
		}
		auto &valueStr = arguments[1];
		switch (value.type) {
		default:
		case Reg8: {
			if (valueStr.size() < 2) {
				SendError(EINVAL);
				return;
			}
			std::string valuePart = valueStr.substr(0, 2);
			value.defined = true;
			value.u8 = std::stoul(valuePart, nullptr, 16);
			break;
		}
		case Reg16: {
			if (valueStr.size() < 4) {
				SendError(EINVAL);
				return;
			}
			std::string valuePart = valueStr.substr(2, 2) + valueStr.substr(0, 2);
			value.defined = true;
			value.u16 = std::stoul(valuePart, nullptr, 16);
			break;
		}
		case Reg32: {
			if (valueStr.size() < 8) {
				SendError(EINVAL);
				return;
			}
			std::string valuePart = valueStr.substr(6, 2) + valueStr.substr(4, 2) +
				valueStr.substr(2, 2) + valueStr.substr(0, 2);
			value.defined = true;
			value.u32 = std::stoul(valuePart, nullptr, 16);
			break;
		}
		case Reg64: {
			if (valueStr.size() < 16) {
				SendError(EINVAL);
				return;
			}
			std::string valuePart = valueStr.substr(14, 2) + valueStr.substr(12, 2) +
				valueStr.substr( 10, 2) + valueStr.substr(8, 2) +
				valueStr.substr(6, 2) + valueStr.substr(4, 2) +
				valueStr.substr(2, 2) + valueStr.substr(0, 2);
			value.defined = true;
			value.u64 = std::stoull(valuePart, nullptr, 16);
			break;
		}
		}
		_server.WriteRegister(i, value);
		SendReply("OK");
	}
	catch (std::out_of_range &) {
		SendError(ERANGE);
		return;
	}
	catch (std::invalid_argument &) {
		SendError(EINVAL);
		return;
	}
}

void gdb::ServerConnection::ReplyQueryFirstThreadInfo() {
	SendReply("m1,l");
}

void gdb::ServerConnection::ReplyQueryNextThreadInfo() {
	SendReply("l");
}

void gdb::ServerConnection::ProcessXfer(std::vector<std::string> &args) {
	if(args.size() < 2) {
		SendError(EINVAL);
		return;
	}
	std::string method = args[1];
	if(method == "read") {
		ProcessXferRead(args);
	} else if(method == "write") {
		ProcessXferWrite(args);
	} else {
		logger::Log(logger::Warn, "Unkown GDB transfer method: %s", method.c_str());
		SendReply("");
		return;
	}
}

void gdb::ServerConnection::ProcessXferWrite(std::vector<std::string> &args) {
	logger::Log(logger::Warn, "Unkown GDB transfer write: %s", args[0].c_str());
	SendReply("");
}

void gdb::ServerConnection::ReplyQueryXferFeatures(const std::string &annex, int offset, int length) {
	if(annex != "target.xml") {
		SendError(0);
	}
	const std::string &description = _server.GetTargetDescription();
	if(offset < 0 || length < 0) {
		SendError(EINVAL);
		return;
	}
	if(offset >= description.size()) {
		SendReply("l");
		return;
	}
	length = static_cast<int>(description.size()) - offset;
	SendReply("l" + std::string(description.c_str() + offset, length));
}

void gdb::ServerConnection::ReplyQueryXferExecFile(int annex, int offset, int length) {
	if(annex != -1 && annex != 0) {
		SendError(0);
		return;
	}
	const std::string &filename = _server.GetExecutableFilename();
	if(offset < 0 || length < 0) {
		SendError(EINVAL);
		return;
	}
	if(offset >= filename.size()) {
		SendReply("l");
		return;
	}
	length = static_cast<int>(filename.size()) - offset;
	SendReply("l" + std::string(filename.c_str() + offset, length));
}

void gdb::ServerConnection::ReplyQueryXferMemoryMap(int offset, int length){
	const std::string &memoryMap = _server.GetTargetMemoryMap();
	if(offset < 0 || length < 0) {
		SendError(EINVAL);
		return;
	}
	if(offset >= memoryMap.size()) {
		SendReply("l");
		return;
	}
	length = static_cast<int>(memoryMap.size()) - offset;
	SendReply("l" + std::string(memoryMap.c_str() + offset, length));
}

void gdb::ServerConnection::ProcessOldContinue(const std::string &msg) {
	if(msg.size() > 1) {
		try {
			uint32_t addr = std::stoul(msg.substr(1), nullptr, 16);
			_server.OnGdbClientContinue(true, addr);
		} catch(std::out_of_range &) {
			SendError(EINVAL);
		}
	} else {
		_server.OnGdbClientContinue(false, 0);
	}
}

void gdb::ServerConnection::ProcessContinue(std::vector<std::string> &args) {
	char action = 'c';
	int threadId = 1;
	if(args.size() > 0) {
		action = args[0][0];
	}
	if((args.size() > 1 && action != 'r') || args.size() > 2) {
		try {
			threadId = std::stoi(args[action == 'r' ? 2 : 1]);
		} catch(std::out_of_range &) {
			SendError(EINVAL);
			return;
		}
		if(threadId <= 0) threadId = 1;
	}
	if(threadId > 1) {
		SendError(EINVAL);
		return;
	}
	switch(action) {
		case 'c': // Continue
			ProcessOldContinue(args[0]);
			break;
		case 'C': // Continue with signal
			ProcessOldContinueWithSignal(args[0]);
			break;
		case 's': // Step
			ProcessOldStep(args[0]);
			break;
		case 'S': // Step with signal
			ProcessOldStepWithSignal(args[0]);
			break;
		case 't': // Stop
			SendStatus();
			break;
		case 'r': // Range stepping
			ProcessRangeStepping(args[0].substr(1), args[1]);
			break;
	}
}

void gdb::ServerConnection::ProcessOldContinueWithSignal(const std::string &msg) {
	// ignore the signal for now
	ProcessOldContinue(msg);
}

void gdb::ServerConnection::ProcessOldStep(const std::string &msg) {
	if(msg.size() > 1) {
		try {
			uint32_t addr = std::stoul(msg.substr(1), nullptr, 16);
			_server.OnGdbClientStep(true, addr);
			SendStatus();
		} catch(std::out_of_range &) {
			SendError(EINVAL);
		}
	} else {
		_server.OnGdbClientStep(false, 0);
		SendStatus();
	}
}

void gdb::ServerConnection::ProcessOldStepWithSignal(const std::string &msg) {
	ProcessOldStep(msg);
}

void gdb::ServerConnection::ProcessRangeStepping(const std::string &start, const std::string &end) {
	ProcessOldStep("s");
}

void gdb::ServerConnection::ProcessReadMemory(const std::string &msg) {
	std::vector<std::string> arguments = Tokenize(msg.substr(1), ",");
	if(arguments.size() != 2) {
		SendError(EINVAL);
		return;
	}
	try {
		uint32_t addr = std::stoul(arguments[0], nullptr, 16);
		int32_t size = std::stoi(arguments[1], nullptr, 16);
		uint8_t *data = static_cast<uint8_t *>(_server.GetMemoryPointer(addr, &size));
		if(size < 0) {
			SendError(-size);
			return;
		}
		std::stringstream ss;
		ss << std::hex << std::setfill('0');
		for(int i = 0; i < size; i++) {
			ss << std::setw(2) << +data[i];
		}
		SendReply(ss.str());
	} catch(std::out_of_range &) {
		SendError(EINVAL);
	}
}

void gdb::ServerConnection::ProcessWriteMemory(const std::string& msg)
{
	std::vector<std::string> arguments = Tokenize(msg.substr(1), ",:");
	if(arguments.size() != 3)
	{
		SendError(EINVAL);
		return;
	}
	try
	{
		uint32_t addr = std::stoul(arguments[0], nullptr, 16);
		int32_t size = std::stoi(arguments[1], nullptr, 16);

		auto &dataStr = arguments[2];
		if(dataStr.size() != 2 * size)
		{
			SendError(EINVAL);
			return;
		}

		for(int i = 0; i < size; i++)
		{
			std::string valStr = dataStr.substr(i, 2);
			int val = std::stoul(valStr, nullptr, 16);
			_server.SetMemory(addr + i, val);
		}
		SendReply("OK");
	} catch(std::out_of_range &)
	{
		SendError(EINVAL);
	}
}

void gdb::ServerConnection::ProcessWriteMemoryBinary(const std::string& msg)
{
	std::vector<std::string> arguments = Tokenize(msg.substr(1), ",:");
	if (arguments.size() < 3)
	{
		SendError(EINVAL);
		return;
	}
	try
	{
		uint32_t addr = std::stoul(arguments[0], nullptr, 16);
		int32_t size = std::stoi(arguments[1], nullptr, 16);

		auto dataStr = msg.substr(msg.find_first_of(":") + 1);
		if (dataStr.size() != size)
		{
			SendError(EINVAL);
			return;
		}

		for (int i = 0; i < size; i++)
		{
			_server.SetMemory(addr + i, dataStr[i]);
		}
		SendReply("OK");
	}
	catch (std::out_of_range &)
	{
		SendError(EINVAL);
	}
}

void gdb::ServerConnection::ProcessAddBreakpoint(const std::string &msg)
{
	std::vector<std::string> arguments = Tokenize(msg.substr(1), ",");
	if(arguments.size() < 3) {
		SendError(EINVAL);
		return;
	}
	switch(hashStr(arguments[0].c_str()))
	{
	case hashStr("0"): // Software breakpoint
		ProcessAddSoftwareBreakpoint(arguments);
		break;
	case hashStr("1"): // Hardware breakpoint
		ProcessAddSoftwareBreakpoint(arguments);
		break;
	default:
		SendReply("");
		break;
	}
}

void gdb::ServerConnection::ProcessAddSoftwareBreakpoint(const std::vector<std::string> &args) {
	if(args.size() > 3) {
		SendReply("");
		return;
	}
	try {
		uint32_t addr = std::stoul(args[1], nullptr, 16);
		int kind = std::stoi(args[2], nullptr, 16);

		_server.OnGdbClientAddBreakpoint(addr, kind);
		SendReply("OK");
	} catch(std::out_of_range &) {
		SendError(EINVAL);
	}
}

void gdb::ServerConnection::ProcessRemoveBreakpoint(const std::string &msg) {
	std::vector<std::string> arguments = Tokenize(msg.substr(1), ",");
	if(arguments.size() < 3) {
		SendError(EINVAL);
		return;
	}
	switch(hashStr(arguments[0].c_str()))
	{
		case hashStr("0"): // Software breakpoint
			ProcessRemoveSoftwareBreakpoint(arguments);
			break;
		case hashStr("1"): // Hardware breakpoint
			ProcessRemoveSoftwareBreakpoint(arguments);
			break;
		default:
			SendReply("");
			break;
	}
}

void gdb::ServerConnection::ProcessRemoveSoftwareBreakpoint(const std::vector<std::string> &args) {
	if(args.size() > 3) {
		SendReply("");
		return;
	}
	try {
		uint32_t addr = std::stoul(args[1], nullptr, 16);
		int kind = std::stoi(args[2], nullptr, 16);

		_server.OnGdbClientRemoveBreakpoint(addr, kind);
		SendReply("OK");
	} catch(std::out_of_range &) {
		SendError(EINVAL);
	}
}

void gdb::ServerConnection::OnAppSignalReceived()
{
	SendStatus();
}

void gdb::ServerConnection::ProcessCtrlC() {
	_server.OnGdbClientInterrupt();
	SendStatus();
}

void gdb::ServerConnection::ProcessDetach()
{
	CloseConnection();
}
