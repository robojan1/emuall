#ifndef GDB_SERVER_P_H
#define GDB_SERVER_P_H

#include <boost/asio.hpp>
#include <string>
#include <functional>
#include <thread>
#include <emuall/gdb/Server.h>

namespace gdb {
	class Server_p
	{
	public:
		Server_p(int port, Server &server);
		~Server_p();

		void StartAccept();
		void DisconnectClient();


		boost::asio::io_context io_context;

		//void OnServerEvent(wxSocketEvent &evt);
		//void OnConnectionClosed(wxCommandEvent &evt);

		std::unique_ptr<ServerConnection> gdbConnection;
		
		bool appAttached;
		int numRegisters;
		std::function<RegisterValue(int)>readRegistersFunc;
		std::function<void(int, const RegisterValue&)>writeRegistersFunc;
		std::function<void *(uint32_t, int32_t *)>getMemoryFunc;
		std::function<void(uint32_t, uint8_t)>setMemoryFunc;
		std::string filename;
		std::string targetDescription;
		std::string targetMemoryMap;
		enum ApplicationStatus appStatus;
		int appSignalNr;
		uint32_t textOffset;
		uint32_t dataOffset;
		
		std::function<void(void)>gdbClientDisconnected;
		std::function<void(bool, uint32_t)>gdbClientContinue;
		std::function<void(bool, uint32_t)>gdbClientStep;
		std::function<void(uint32_t, int)>gdbClientAddBreakpoint;
		std::function<void(uint32_t, int)>gdbClientRemoveBreakpoint;
		std::function<void(void)>gdbClientInterrupt;

	private:
		void run_asio();
		void handle_accept(const boost::system::error_code &error);

		Server &_server;

		std::unique_ptr<std::thread> _thread;
		boost::asio::ip::tcp::acceptor _acceptor;
		std::shared_ptr<boost::asio::ip::tcp::socket> _clientSocket;

	};
}

#endif