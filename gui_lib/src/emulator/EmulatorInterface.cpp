//
// Created by Robbert-Jan de Jager on 15-10-18.
//

#include <emuall/emulator/EmulatorInterface.h>
#include <emuall/util/os_call.h>
#include <emuall/util/log.h>
#include <cassert>
#include <cstring>
#include <cmath>
#include <set>


EmulatorInterface::EmulatorInterface(const std::string &dllName) :
		_hDLL(NULL), _valid(false), _getInterfaceVersion(NULL), _createEmulator(NULL), _releaseEmulator(NULL),
		_init(NULL), _load(NULL), _getDescription(NULL), _isCompatible(NULL), _initGL(NULL), _destroyGL(NULL),
		_run(NULL), _isRunning(NULL), _tick(NULL), _input(NULL), _initPlugin(NULL),
		_draw(NULL), _reshape(NULL), _save(NULL), _step(NULL), _saveState(NULL), _loadState(NULL),
		_disassemble(NULL), _addBreakpoint(NULL), _removeBreakpoint(NULL), _isBreakpoint(NULL),
		_getMemoryData(NULL), _getValI(NULL), _getValU(NULL), _getString(NULL), _getFloat(NULL), _initAudio(nullptr), _getAudio(nullptr),
		_getGdbTargetDescription(nullptr), _getMemoryPointer(nullptr),
		_notifyOptionChange(nullptr), _setMemory(nullptr), _getGdbMemoryMap(nullptr)
{
	_hDLL = LoadSharedLibrary(dllName.c_str());
	if (_hDLL == NULL)
	{
		logger::Log(logger::Error, "Could not load emulator %s: %s", dllName.c_str(), GetLastLibraryError().c_str());
		return;
	}
	_getInterfaceVersion = (int32_t(__stdcall*)())GetStdcallFunc(_hDLL, "GetInterfaceVersion");
	_getDescription = (const uint8_t *(__stdcall*)(uint32_t *))GetStdcallFunc<uint32_t *>(_hDLL, "GetDescription");
	_initPlugin = (void(__stdcall*)())GetStdcallFunc(_hDLL, "InitPlugin");
	if (_initPlugin) {
		_initPlugin();
	}
	if (!_getInterfaceVersion)
	{
		// Not valid emulator
		logger::Log(logger::Error, "Could not load emulator %s: Could not get the interface version", dllName.c_str());
		return;
	}
	if (!GetDescription(NULL))
	{
		logger::Log(logger::Error, "Could not load emulator %s: Could not get the description", dllName.c_str());
		return;
	}
	unsigned int xmlSize;
	const char *xml = GetDescription(&xmlSize);
	pugi::xml_parse_result result = _description.load_buffer(xml, xmlSize);
	if (!result)
	{
		logger::Log(logger::Error, "Could not load emulator %s: Parsing error \"%s\" in description at position %d ", dllName.c_str(), result.description(), result.offset);
		return;
	}
	_root = _description.child("emulator");
	if (!_root)
	{
		logger::Log(logger::Error, "Could not load emulator %s: description does not contain an 'emulator' root", dllName.c_str());
		return;
	}

	int version = _getInterfaceVersion();
	if (version >= 100)
	{
		_getValI				= (int32_t(__stdcall*)(EMUHANDLE, int32_t))					GetStdcallFunc<EMUHANDLE, int32_t>				(_hDLL, "GetValI");
		_getValU				= (uint32_t(__stdcall*)(EMUHANDLE, int32_t))				GetStdcallFunc<EMUHANDLE, int32_t>				(_hDLL, "GetValU");
		_getString				= (const uint8_t*(__stdcall*)(EMUHANDLE, int32_t))			GetStdcallFunc<EMUHANDLE, int32_t>				(_hDLL, "GetString");
		_getFloat				= (float (__stdcall*)(EMUHANDLE, int32_t))					GetStdcallFunc<EMUHANDLE, int32_t>				(_hDLL, "GetFloat");
		_setValI				= (void(__stdcall *)(EMUHANDLE, int32_t, int32_t))			GetStdcallFunc<EMUHANDLE, int32_t, int32_t>		(_hDLL, "SetValI");
		_setValU				= (void(__stdcall *)(EMUHANDLE, int32_t, uint32_t))			GetStdcallFunc<EMUHANDLE, int32_t, uint32_t>	(_hDLL, "SetValU");
		_createEmulator			= (EMUHANDLE(__stdcall*)())									GetStdcallFunc									(_hDLL, "CreateEmulator");
		_releaseEmulator		= (void(__stdcall*)(EMUHANDLE))								GetStdcallFunc<EMUHANDLE>						(_hDLL, "ReleaseEmulator");
		_init					= (int32_t(__stdcall*)(EMUHANDLE, callBackfunctions_t))		GetStdcallFunc<EMUHANDLE, callBackfunctions_t>	(_hDLL, "Init");
		_load					= (int32_t(__stdcall*)(EMUHANDLE, const SaveData_t *))		GetStdcallFunc<EMUHANDLE, const SaveData_t *>	(_hDLL, "Load");
		_loadState				= (int32_t(__stdcall*)(EMUHANDLE, const SaveData_t *))		GetStdcallFunc<EMUHANDLE, const SaveData_t *>	(_hDLL, "LoadState");
		_initGL					= (uint32_t(__stdcall*)(EMUHANDLE, int32_t))				GetStdcallFunc<EMUHANDLE, int32_t>				(_hDLL, "InitGL");
		_destroyGL				= (void(__stdcall*)(EMUHANDLE, int32_t))		 			GetStdcallFunc<EMUHANDLE, int32_t>				(_hDLL, "DestroyGL");
		_isCompatible			= (int32_t(__stdcall*)(const uint8_t *))					GetStdcallFunc<const uint8_t *>					(_hDLL, "IsCompatible");
		_releaseSaveData		= (void(__stdcall*)(EMUHANDLE, SaveData_t *))				GetStdcallFunc<EMUHANDLE, SaveData_t *>			(_hDLL, "ReleaseSaveData");
		_run					= (void(__stdcall*)(EMUHANDLE, int32_t))					GetStdcallFunc<EMUHANDLE, int32_t>				(_hDLL, "Run");
		_step					= (void(__stdcall*)(EMUHANDLE))								GetStdcallFunc<EMUHANDLE>						(_hDLL, "Step");
		_isRunning				= (int32_t(__stdcall*)(EMUHANDLE))							GetStdcallFunc<EMUHANDLE>						(_hDLL, "IsRunning");
		_tick					= (int32_t(__stdcall*)(EMUHANDLE, uint32_t))				GetStdcallFunc<EMUHANDLE, uint32_t>				(_hDLL, "Tick");
		_input					= (void(__stdcall*)(EMUHANDLE, int32_t, int32_t))			GetStdcallFunc<EMUHANDLE, int32_t, int32_t>		(_hDLL, "Input");
		_draw					= (void(__stdcall*)(EMUHANDLE, int32_t))					GetStdcallFunc<EMUHANDLE, int32_t>				(_hDLL, "Draw");
		_reshape				= (void(__stdcall*)(EMUHANDLE, int32_t, int32_t, int32_t, int32_t))	GetStdcallFunc<EMUHANDLE, int32_t, int32_t, int32_t, int32_t>(_hDLL, "Reshape");
		_save					= (int32_t(__stdcall*)(EMUHANDLE, SaveData_t *))			GetStdcallFunc<EMUHANDLE, SaveData_t *>			(_hDLL, "Save");
		_saveState				= (int32_t(__stdcall*)(EMUHANDLE, SaveData_t *))			GetStdcallFunc<EMUHANDLE, SaveData_t *>			(_hDLL, "SaveState");
		_disassemble			= (uint8_t(__stdcall*)(EMUHANDLE, uint32_t, const uint8_t **, const uint8_t **))GetStdcallFunc<EMUHANDLE, uint32_t, const uint8_t **, const uint8_t **>(_hDLL, "Disassemble");
		_addBreakpoint			= (void(__stdcall*)(EMUHANDLE, uint32_t))					GetStdcallFunc<EMUHANDLE, uint32_t>				(_hDLL, "AddBreakpoint");
		_removeBreakpoint		= (void(__stdcall*)(EMUHANDLE, uint32_t))					GetStdcallFunc<EMUHANDLE, uint32_t>				(_hDLL, "RemoveBreakpoint");
		_isBreakpoint			= (int32_t(__stdcall*)(EMUHANDLE, uint32_t))				GetStdcallFunc<EMUHANDLE, uint32_t>				(_hDLL, "IsBreakpoint");
		_getMemoryData			= (uint8_t(__stdcall*)(EMUHANDLE, int32_t, uint32_t))		GetStdcallFunc<EMUHANDLE, int32_t, uint32_t>	(_hDLL, "GetMemoryData");
		_initAudio				= (void (__stdcall *)(EMUHANDLE, int32_t, uint32_t, int32_t)) GetStdcallFunc<EMUHANDLE, int32_t, uint32_t, int32_t> (_hDLL, "InitAudio");
		_getAudio				= (void (__stdcall *)(EMUHANDLE, int32_t, int16_t *, uint32_t)) GetStdcallFunc<EMUHANDLE, int32_t, int16_t *, uint32_t> (_hDLL, "GetAudio");
		_getGdbTargetDescription =(const uint8_t *(__stdcall *)(uint32_t *))                GetStdcallFunc<uint32_t *>                      (_hDLL, "GetGdbTargetDescription");
		_getGdbMemoryMap        = (const uint8_t *(__stdcall *)(uint32_t *))                GetStdcallFunc<uint32_t *>                      (_hDLL, "GetGdbMemoryMap");
		_getMemoryPointer       = (int (__stdcall *)(EMUHANDLE, uint32_t, void **, uint32_t *)) GetStdcallFunc<EMUHANDLE, uint32_t, void **, uint32_t *>  (_hDLL, "GetMemoryPointer");
		_notifyOptionChange     = (void (__stdcall *)(EMUHANDLE, int32_t))                  GetStdcallFunc<EMUHANDLE, int32_t>              (_hDLL, "NotifyOptionChange");
		_setMemory              = (void(__stdcall *)(EMUHANDLE, uint32_t, uint8_t))         GetStdcallFunc<EMUHANDLE, uint32_t, uint8_t>    (_hDLL, "SetMemory");
	}
	_valid = true;
	logger::Log(logger::Debug, "Loaded functions from emulator");
}

EmulatorInterface::~EmulatorInterface()
{
	if (_hDLL)
	{
		FreeSharedLibrary(_hDLL);
	}

}

EmulatorInfo_t EmulatorInterface::GetInfo() const
{
	EmulatorInfo_t info;
	info.name = _root.child_value("name");
	info.id = _root.child_value("id");
	info.description = _root.child_value("description");
	info.fileFilterString = _root.child_value("fileFilter");
	info.aboutInfo = _root.child_value("about");
	info.screenAutoRefresh = _root.child("automaticScreenRefresh").text().as_bool(true);
	for (pugi::xml_node screen = _root.child("screen"); screen; screen = screen.next_sibling("screen")) {
		EmulatorScreen_t screenInfo;
		screenInfo.id = screen.attribute("id").as_int(-1);
		screenInfo.width = screen.child("width").text().as_int(-1);
		screenInfo.height = screen.child("height").text().as_int(-1);
		screenInfo.mouseXvarID = screen.child("mousex").text().as_int(-1);
		screenInfo.mouseYvarID = screen.child("mousey").text().as_int(-1);
		info.screens.push_back(screenInfo);
	}
	info.numAudioStreams = _root.child("audio").child("sources").text().as_int(0);
	return info;
}

std::string EmulatorInterface::GetFileFilter() const
{
	return _root.child_value("fileFilter");
}

std::string EmulatorInterface::GetName() const
{
	std::string name = _root.child_value("name");
	if (name.empty())
	{
		name = "Error!";
	}
	return name;
}

std::vector<EmulatorInput_t> EmulatorInterface::GetEmulatorInputs() const
{
	std::vector<EmulatorInput_t> ret;
	pugi::xml_node inputs = _root.child("input");
	if (!inputs)
	{
		logger::Log(logger::Warn, "Emulator \"%s\" has no input", GetName().c_str());
		return ret;
	}
	for (pugi::xml_node_iterator it = inputs.begin(); it != inputs.end(); ++it)
	{
		if (strcmp(it->name(), "key") == 0)
		{
			EmulatorInput_t input;
			input.id = it->attribute("id").as_int(-1);
			input.primaryKey = it->child("defaultKey").text().as_int(-1);
			input.secondaryKey = it->child("defaultSecondaryKey").text().as_int(-1);
			input.name = it->child_value("name");
			input.flags = 0;
			if (input.id >= 0) {
				ret.push_back(input);
			}
		}
	}
	return ret;
}


CpuDebuggerInfo_t EmulatorInterface::GetCpuDebuggerInfo() const
{
	CpuDebuggerInfo_t ret;
	ret.breakpointSupport = false;
	ret.disassembler = false;
	ret.disassemblerSize = 0;
	ret.step = false;
	ret.stepOut = false;
	ret.stepOver = false;
	pugi::xml_node cpu = _root.child("debugging").child("cpu");
	if (!cpu)
	{
		logger::Log(logger::Warn, "Emulator \"%s\" has no \"debugging/cpu\" section", GetName().c_str());
		return ret;
	}
	if (strcmp(cpu.child("disassembler").child_value("enabled"), "true") == 0)
	{
		ret.disassembler = true;
		ret.breakpointSupport = strcmp(cpu.child("disassembler").child_value("breakpoint"), "true") == 0;
		ret.disassemblerSize = strtol(cpu.child("disassembler").child_value("size"), NULL, 0);
		ret.curLineId = strtol(cpu.child("disassembler").child_value("curLineId"), NULL, 0);
	}
	ret.step = strcmp(cpu.child_value("step"), "true") == 0;
	ret.stepOver = strcmp(cpu.child_value("stepOver"), "true") == 0;
	ret.stepOut = strcmp(cpu.child_value("stepOut"), "true") == 0;
	pugi::xml_node node = cpu.child("registers");
	if (node)
	{
		for (pugi::xml_node_iterator it = node.begin(); it != node.end(); ++it)
		{
			if (strcmp(it->name(), "register") == 0)
			{
				EmulatorRegister_t reg;
				reg.id = it->attribute("id").as_int();
				reg.name = it->child_value("name");
				reg.size = strtol(it->child_value("size"), NULL, 0);
				reg.readonly = it->child("readOnly") ? true : false; // stupid ms warnings
				ret.registers.push_back(reg);
			}
			else if (strcmp(it->name(), "flag") == 0)
			{
				EmulatorRegister_t reg;
				reg.id = it->attribute("id").as_int();
				reg.name = it->child_value("name");
				reg.size = 1;
				reg.readonly = it->child("readOnly") ? true : false; // stupid ms warnings
				ret.flags.push_back(reg);
			}
		}
	}
	return ret;
}

MemDebuggerInfo_t EmulatorInterface::GetMemDebuggerInfo(EMUHANDLE handle) const
{
	MemDebuggerInfo_t ret;
	pugi::xml_node mem = _root.child("debugging").child("mem");
	if (!mem)
	{
		logger::Log(logger::Warn, "Emulator \"%s\" has no \"debugging/mem\" section", GetName().c_str());
		return ret;
	}
	else
	{
		for (pugi::xml_node_iterator it = mem.begin(); it != mem.end(); ++it)
		{
			if (strcmp(it->name(), "memory") == 0)
			{
				EmulatorMemory_t mem;
				mem.id = it->attribute("id").as_int();
				mem.name = it->child_value("name");
				mem.size = GetXMLVal(handle, *it, "size");
				ret.memories.push_back(mem);
			}
			else if (strcmp(it->name(), "info") == 0)
			{
				EmulatorValue_t val;
				val.id = it->attribute("id").as_int();
				val.name = it->child("name").text().as_string("Error!");
				val.size = it->child("size").text().as_int();
				const char *mode = it->child("mode").text().as_string("dec");
				if (strcmp(mode, "dec") == 0)
				{
					val.mode = EmulatorValue_t::dec;
				}
				else if (strcmp(mode, "hex") == 0)
				{
					val.mode = EmulatorValue_t::hex;
				}
				else if (strcmp(mode, "oct") == 0)
				{
					val.mode = EmulatorValue_t::oct;
				}
				else if (strcmp(mode, "string") == 0)
				{
					val.mode = EmulatorValue_t::string;
				}
				else {
					logger::Log(logger::Warn, "Unknown info mode \"%s\" found", mode);
					val.mode = EmulatorValue_t::dec;
				}
				ret.infos.push_back(val);
			}
			else if (strcmp(it->name(), "readOnly") == 0)
			{
				ret.readOnly = strcmp(it->text().as_string(), "true") == 0;
			}
		}
	}
	return ret;
}

pugi::xml_node EmulatorInterface::GetGpuDebuggerInfo(const Emulator *emu) const
{
	return _root.child("debugging").child("gpu");
}

std::vector<EmulatorOption_t> EmulatorInterface::GetEmulatorOptions() const {
	std::vector<EmulatorOption_t> options;
	std::set<int> seenIds;
	std::set<std::string> shortNames;

	auto root = _root.child("options");
	if(root) {
		for(auto optionDef : root.children()) {
			EmulatorOption_t option;
			if(strcmp(optionDef.name(), "option") != 0) {
				logger::Log(logger::Warn, "Invalid option \"%s\" in the options description", optionDef.name());
				continue;
			}
			auto idAttr = optionDef.attribute("id");
			if(!idAttr) {
				logger::Log(logger::Warn, "Option is missing the required id");
				continue;
			}
			option.id = idAttr.as_int();
			if(seenIds.count(option.id) > 0) {
				logger::Log(logger::Warn, "Duplicate option id %d", option.id);
				continue;
			}
			auto nameChild = optionDef.child("name");
			if(!nameChild) {
				logger::Log(logger::Warn, "Option %d is missing the required name field", option.id);
				continue;
			}
			option.name = nameChild.text().as_string();
			option.description = optionDef.child("description").text().as_string();
			option.shortname = optionDef.child("shortname").text().as_string();

			if(!option.shortname.empty() && shortNames.count(option.shortname) > 0) {
				logger::Log(logger::Warn, "duplicate short name %s detected", option.shortname.c_str());
				continue;
			}

			option.required = optionDef.child("required");
			auto typeChild = optionDef.child("type");
			if(!typeChild) {
				logger::Log(logger::Warn, "Option \"%s\" is missing the required type field", option.name.c_str());
				continue;
			}
			auto typeStr = typeChild.text().as_string();
			auto defaultChild = optionDef.child("default");

			if(strcmp(typeStr, "boolean") == 0) {
				option.type = EmulatorOption_t::Boolean;
				option.defaultValueInt = defaultChild.text().as_bool(false);
			} else if(strcmp(typeStr, "file") == 0) {
				option.type = EmulatorOption_t::File;
				option.defaultValueString = defaultChild.text().as_string();
			} else if(strcmp(typeStr, "path") == 0) {
				option.type = EmulatorOption_t::Path;
				option.defaultValueString = defaultChild.text().as_string();
			} else if(strcmp(typeStr, "integer") == 0) {
				option.type = EmulatorOption_t::Integer;
				option.defaultValueInt = defaultChild.text().as_int(0);
				auto minAttr = typeChild.attribute("min");
				option.hasMin = minAttr;
				option.minInt = minAttr.as_int();
				auto maxAttr = typeChild.attribute("max");
				option.hasMax = maxAttr;
				option.maxInt = maxAttr.as_int();
				if(option.hasMax && option.hasMin && option.minInt > option.maxInt) {
					logger::Log(logger::Warn, "Inverted range limits for option \"%s\"", option.name.c_str());
					std::swap(option.maxInt, option.minInt);
				}
				if(option.hasMax && option.defaultValueInt > option.maxInt) {
					logger::Log(logger::Warn, "Default value too large for option \"%s\"", option.name.c_str());
					option.defaultValueInt = option.maxInt;
				}
				if(option.hasMin && option.defaultValueInt < option.minInt) {
					logger::Log(logger::Warn, "Default value too small for option \"%s\"", option.name.c_str());
					option.defaultValueInt = option.minInt;
				}
			} else if(strcmp(typeStr, "enum") == 0) {
				option.type = EmulatorOption_t::Enumeration;
				auto enumOptionsChild = optionDef.child("options");
				std::set<int> enumOptionIds;
				option.defaultValueInt = 0;
				for(auto enumOptionChild : enumOptionsChild.children()) {
					if(strcmp(enumOptionChild.name(), "option") != 0) {
						logger::Log(logger::Warn, "Invalid enumeration option for option \"%s\"", option.name.c_str());
						continue;
					}
					auto enumOptionIdAttr = enumOptionChild.attribute("id");
					if(!enumOptionIdAttr) {
						logger::Log(logger::Warn, "missing enumeration id for option \"%s\"", option.name.c_str());
						continue;
					}
					int enumOptionId = enumOptionIdAttr.as_int();
					if(enumOptionIds.count(enumOptionId) > 0) {
						logger::Log(logger::Warn, "Duplicate enumeration option %d for option \"%s\"", enumOptionId, option.name.c_str());
						continue;
					}
					if(enumOptionIds.empty()) {
						option.defaultValueInt = defaultChild.text().as_int(enumOptionId);
					}
					std::string enumOptionValue = enumOptionChild.text().as_string();
					option.enumOptions[enumOptionId] = enumOptionValue;
					enumOptionIds.insert(enumOptionId);
				}
			} else if(strcmp(typeStr, "float") == 0) {
				option.type = EmulatorOption_t::Float;
				option.defaultValueFloat = defaultChild.text().as_float();
				auto minAttr = typeChild.attribute("min");
				option.hasMin = minAttr;
				option.minFloat = minAttr.as_float();
				auto maxAttr = typeChild.attribute("max");
				option.hasMax = maxAttr;
				option.maxFloat = maxAttr.as_float();
				if(option.hasMax && option.hasMin && option.minFloat > option.maxFloat) {
					logger::Log(logger::Warn, "Inverted range limits for option \"%s\"", option.name.c_str());
					std::swap(option.maxFloat, option.minFloat);
				}
				if(option.hasMax && option.defaultValueFloat > option.maxFloat) {
					logger::Log(logger::Warn, "Default value too large for option \"%s\"", option.name.c_str());
					option.defaultValueFloat = option.maxFloat;
				}
				if(option.hasMin && option.defaultValueFloat < option.minFloat) {
					logger::Log(logger::Warn, "Default value too small for option \"%s\"", option.name.c_str());
					option.defaultValueFloat = option.minFloat;
				}
			} else {
				logger::Log(logger::Warn, "Option \"%s\" type \"%s\" is unknown", option.name.c_str(), typeStr);
				continue;
			}
			options.push_back(std::move(option));
			seenIds.insert(option.id);
		}
	}

	return std::move(options);
}

uint32_t EmulatorInterface::GetValU(EMUHANDLE handle, int id) const
{
	if (_getValU == NULL)
	{
		return 0;
	}
	return _getValU(handle, id);
}

int32_t EmulatorInterface::GetValI(EMUHANDLE handle, int id) const
{
	if (_getValI == NULL)
	{
		return 0;
	}
	return _getValI(handle, id);
}

const char *EmulatorInterface::GetString(EMUHANDLE handle, int id) const
{
	if (_getString == NULL)
	{
		return NULL;
	}
	return (const char *)_getString(handle, id);
}

void EmulatorInterface::SetValI(EMUHANDLE handle, int id, int32_t val) const
{
	if (_setValI == NULL)
	{
		return;
	}
	return _setValI(handle, id, val);
}

void EmulatorInterface::SetValU(EMUHANDLE handle, int id, uint32_t val) const
{
	if (_setValU == NULL)
	{
		return;
	}
	return _setValU(handle, id, val);
}

EMUHANDLE EmulatorInterface::CreateEmulator() const
{
	if (_createEmulator == NULL)
		return NULL;
	return _createEmulator();
}

void EmulatorInterface::ReleaseEmulator(EMUHANDLE handle) const
{
	if (_releaseEmulator == NULL)
		return;
	_releaseEmulator(handle);
}

int EmulatorInterface::Init(EMUHANDLE handle, callBackfunctions_t funcs) const
{
	if (_init == NULL)
		return false;
	return _init(handle, funcs);
}

int EmulatorInterface::Load(EMUHANDLE handle, const SaveData_t *data) const
{
	if (_load == NULL)
		return false;
	return _load(handle, data);
}

int EmulatorInterface::LoadState(EMUHANDLE handle, const SaveData_t *data) const
{
	if (_loadState == NULL)
		return false;
	return _loadState(handle, data);
}

bool EmulatorInterface::InitGL(EMUHANDLE handle, int id) const
{
	if (_initGL == NULL)
		return false;
	logger::Log(logger::Debug, "InitGL %d", id);
	return _initGL(handle, id) != 0;
}

void EmulatorInterface::DestroyGL(EMUHANDLE handle, int id) const
{
	if (_destroyGL == NULL)
		return;
	logger::Log(logger::Debug, "DestoyGL %d", id);
	_destroyGL(handle, id);
}

void EmulatorInterface::ReleaseSaveData(EMUHANDLE handle, SaveData_t *data) const
{
	if (_releaseSaveData == NULL)
		return;
	_releaseSaveData(handle, data);
}

const char *EmulatorInterface::GetDescription(unsigned int *size) const
{
	if (_getDescription == NULL)
		return NULL;
	return (const char *) _getDescription(size);
}

int EmulatorInterface::IsCompatible(const char *filename) const
{
	if (_isCompatible == NULL)
		return false;
	return _isCompatible((const uint8_t *) filename);
}

void EmulatorInterface::Run(EMUHANDLE handle, int run) const
{
	if (_run == NULL)
		return;
	_run(handle, run);
}

void EmulatorInterface::Step(EMUHANDLE handle) const
{
	if (_step == NULL)
		return;
	_step(handle);
}

int EmulatorInterface::IsRunning(EMUHANDLE handle) const
{
	if (_isRunning == NULL)
		return -1;
	return _isRunning(handle);
}

int EmulatorInterface::Tick(EMUHANDLE handle, uint32_t time) const
{
	if (_tick == NULL)
		return false;
	return _tick(handle, time);
}

void EmulatorInterface::Input(EMUHANDLE handle, int key, int pressed) const
{
	if (_input == NULL)
		return;
	return _input(handle, key, pressed);
}

void EmulatorInterface::Draw(EMUHANDLE handle, int id) const
{
	if (_draw == NULL)
		return;
	_draw(handle, id);
}

void EmulatorInterface::Reshape(EMUHANDLE handle,int id, int width, int height, bool keepAspect) const
{
	if (_reshape == NULL)
		return;
	_reshape(handle, id, width, height, keepAspect);
}

int EmulatorInterface::Save(EMUHANDLE handle, SaveData_t *data) const
{
	if (_save == NULL) {
		logger::Log(logger::Warn, "Save not supported");
		return false;
	}
	return _save(handle, data);
}

int EmulatorInterface::SaveState(EMUHANDLE handle, SaveData_t *data) const
{
	if (_saveState == NULL) {
		logger::Log(logger::Warn, "SaveState not supported");
		return false;
	}
	return _saveState(handle, data);
}

void EmulatorInterface::InitAudio(EMUHANDLE handle, int source, unsigned int sampleRate, int channels) const
{
	if (_initAudio == nullptr) {
		return;
	}
	_initAudio(handle, source, sampleRate, channels);
}

void EmulatorInterface::GetAudio(EMUHANDLE handle, int source, short *buffer, unsigned int samples) const
{
	if (_getAudio == nullptr) {
		return;
	}
	_getAudio(handle, source, buffer, samples);
}

char EmulatorInterface::Disassemble(EMUHANDLE handle, unsigned int pos, const char **raw, const char **instr) const
{
	if (_disassemble == NULL)
		return -1;
	return _disassemble(handle, pos, (const uint8_t **) raw, (const uint8_t **) instr);
}

void EmulatorInterface::AddBreakpoint(EMUHANDLE handle, unsigned int pos) const
{
	if (_addBreakpoint == NULL)
		return;
	return _addBreakpoint(handle, pos);
}

void EmulatorInterface::RemoveBreakpoint(EMUHANDLE handle, unsigned int pos) const
{
	if (_removeBreakpoint == NULL)
		return;
	return _removeBreakpoint(handle, pos);
}

int EmulatorInterface::IsBreakpoint(EMUHANDLE handle, unsigned int pos) const
{
	if (_isBreakpoint == NULL)
		return 1 == 0;
	return _isBreakpoint(handle, pos);
}

char EmulatorInterface::GetMemoryData(EMUHANDLE handle, int memory, unsigned int address) const
{
	if (_getMemoryData == NULL)
		return 0;
	return _getMemoryData(handle, memory, address);
}

unsigned int EmulatorInterface::GetXMLVal(EMUHANDLE handle, pugi::xml_node &node, const char *element) const
{
	if (!node.child(element))
	{
		logger::Log(logger::Warn, "node %s doesn't contain child %s", node.name(), element);
		return 0;
	}
	if (!node.child(element).attribute("var").as_bool(false))
	{
		return strtol(node.child_value(element), NULL, 0);
	}
	else {
		return GetValU(handle, strtol(node.child_value(element), NULL, 0));
	}
}

float EmulatorInterface::GetFloat(EMUHANDLE handle, int id) const
{
	if (_getFloat == NULL)
	{
		return NAN;
	}
	return _getFloat(handle, id);
}

std::string EmulatorInterface::GetGdbTargetDescription() const {
	if(_getGdbTargetDescription == nullptr) {
		return std::string();
	}
	uint32_t size;
	const uint8_t *data = _getGdbTargetDescription(&size);
	return std::string(reinterpret_cast<const char *>(data), size);
}

std::string EmulatorInterface::GetGdbMemoryMap() const {
	if(_getGdbMemoryMap == nullptr) {
		return std::string();
	}
	uint32_t size;
	const uint8_t *data = _getGdbMemoryMap(&size);
	return std::string(reinterpret_cast<const char *>(data), size);
}

GdbInfo_t EmulatorInterface::GetGdbInfo() const {
	GdbInfo_t ret;
	ret.supported = false;
	pugi::xml_node gdb = _root.child("gdb");
	if (!gdb)
	{
		logger::Log(logger::Message, "Emulator \"%s\" has no \"gdb\" support", GetName().c_str());
		return ret;
	}
	pugi::xml_node registers = gdb.child("registers");
	if(!registers) {
		logger::Log(logger::Warn, "Emulator \"%s\" has no \"gdb/registers\" section", GetName().c_str());
		return ret;
	}
	ret.supported = true;
	for(pugi::xml_node_iterator it = registers.begin(); it != registers.end(); it++)
	{
		GdbRegister_t reg;
		reg.size = it->attribute("size").as_int(32);
		reg.id = it->text().as_int(-1);
		ret.registers.push_back(reg);
	}
	auto textOffset = gdb.child("textOffset");
	ret.textOffset = textOffset.text().as_uint(0);
	auto dataOffset = gdb.child("dataOffset");
	ret.dataOffset = dataOffset.text().as_uint(0);
	return ret;
}

int EmulatorInterface::GetMemoryPointer(EMUHANDLE handle, uint32_t addr, void **data, unsigned int *size) const {
	if(_getMemoryPointer == nullptr) {
		return 0;
	}
	return _getMemoryPointer(handle, addr, data, size);
}

void EmulatorInterface::SetMemory(EMUHANDLE handle, uint32_t addr, uint8_t val) const
{
	if(_setMemory != nullptr)
	{
		_setMemory(handle, addr, val);
	}
}

EmulatorInterface::EmulatorInterface(const EmulatorInterface &other)
{
	// Recreate the XML document
	uint32_t xmlSize;
	const uint8_t *xml = other._getDescription(&xmlSize);
	pugi::xml_parse_result result = _description.load_buffer(xml, xmlSize);
	assert(result);
	_root = _description.child("emulator");
	assert(_root);

	_hDLL = other._hDLL;
	_valid = other._valid;
	_initPlugin = other._initPlugin;
	_createEmulator = other._createEmulator;
	_releaseEmulator = other._releaseEmulator;
	_init = other._init;
	_load = other._load;
	_loadState = other._loadState;
	_initGL = other._initGL;
	_destroyGL = other._destroyGL;
	_getValI = other._getValI;
	_getValU = other._getValU;
	_getString = other._getString;
	_getFloat = other._getFloat;
	_setValI = other._setValI;
	_setValU = other._setValU;
	_getDescription = other._getDescription;
	_isCompatible = other._isCompatible;
	_getInterfaceVersion = other._getInterfaceVersion;
	_run = other._run;
	_step = other._step;
	_isRunning = other._isRunning;
	_releaseSaveData = other._releaseSaveData;
	_tick = other._tick;
	_input = other._input;
	_draw = other._draw;
	_reshape = other._reshape;
	_save = other._save;
	_saveState = other._saveState;
	_initAudio = other._initAudio;
	_getAudio = other._getAudio;
	_disassemble = other._disassemble;
	_addBreakpoint = other._addBreakpoint;
	_removeBreakpoint = other._removeBreakpoint;
	_isBreakpoint = other._isBreakpoint;
	_getMemoryData = other._getMemoryData;
	_getGdbTargetDescription = other._getGdbTargetDescription;
	_getGdbMemoryMap = other._getGdbMemoryMap;
	_getMemoryPointer = other._getMemoryPointer;
	_notifyOptionChange = other._notifyOptionChange;
	_setMemory = other._setMemory;
}

EmulatorInterface &EmulatorInterface::operator=(const EmulatorInterface &other) {
	// Recreate the XML document
	uint32_t xmlSize;
	const uint8_t *xml = other._getDescription(&xmlSize);
	pugi::xml_parse_result result = _description.load_buffer(xml, xmlSize);
	assert(result);
	_root = _description.child("emulator");
	assert(_root);

	_hDLL = other._hDLL;
	_valid = other._valid;
	_initPlugin = other._initPlugin;
	_createEmulator = other._createEmulator;
	_releaseEmulator = other._releaseEmulator;
	_init = other._init;
	_load = other._load;
	_loadState = other._loadState;
	_initGL = other._initGL;
	_destroyGL = other._destroyGL;
	_getValI = other._getValI;
	_getValU = other._getValU;
	_getString = other._getString;
	_getFloat = other._getFloat;
	_setValI = other._setValI;
	_setValU = other._setValU;
	_getDescription = other._getDescription;
	_isCompatible = other._isCompatible;
	_getInterfaceVersion = other._getInterfaceVersion;
	_run = other._run;
	_step = other._step;
	_isRunning = other._isRunning;
	_releaseSaveData = other._releaseSaveData;
	_tick = other._tick;
	_input = other._input;
	_draw = other._draw;
	_reshape = other._reshape;
	_save = other._save;
	_saveState = other._saveState;
	_initAudio = other._initAudio;
	_getAudio = other._getAudio;
	_disassemble = other._disassemble;
	_addBreakpoint = other._addBreakpoint;
	_removeBreakpoint = other._removeBreakpoint;
	_isBreakpoint = other._isBreakpoint;
	_getMemoryData = other._getMemoryData;
	_getGdbTargetDescription = other._getGdbTargetDescription;
	_getGdbMemoryMap = other._getGdbMemoryMap;
	_getMemoryPointer = other._getMemoryPointer;
	_notifyOptionChange = other._notifyOptionChange;
	_setMemory = other._setMemory;
	return *this;
}

void EmulatorInterface::NotifyOptionChange(EMUHANDLE handle, int optionId) const {
	if(_notifyOptionChange) {
		_notifyOptionChange(handle, optionId);
	}
}
