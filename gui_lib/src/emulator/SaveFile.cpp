
#include <emuall/emulator/SaveFile.h>

#include <emuall/util/log.h>
#include <emuall/util/endian.h>
#include <fstream>
#include <emuall/exception.h>
#include <boost/filesystem.hpp>

void SaveFile::ReadSaveFile(const std::string &fileName, SaveData_t &data)
{
	data.ramData = NULL;
	data.ramDataLen = 0;
	data.miscData = NULL;
	data.miscDataLen = 0;
	
	if (!boost::filesystem::exists(fileName))
		return;

	const char *cfilename = fileName.c_str();
	std::ifstream file;
	file.exceptions(std::ios::failbit);
	try {
		file.open(cfilename, std::ios::in | std::ios::binary);
	} catch(std::ios_base::failure &e) {
		logger::Log(logger::Error, "Could not open save data file '%s': %s", cfilename, e.code().message().c_str());
		return;
	}
	try {
		auto endian = Endian::getSingleton(false);
		uint32_t id;
		file.read((char *) &id, sizeof(id));
		if(endian->convu32(id) != SaveFile::saveID) {
			logger::Log(logger::Error, "Save state corrupted '%s'", cfilename);
			return;
		}
		ReadSection(file, &data.ramDataLen, &data.ramData);
		ReadSection(file, &data.miscDataLen, &data.miscData);
	} catch(std::ios_base::failure &e) {
		boost::filesystem::remove(fileName);
		logger::Log(logger::Error, "Could not read save file '%s': %s", cfilename, e.code().message().c_str());
	}
}

void SaveFile::WriteSaveFile(const std::string &fileName, const SaveData_t &data)
{
	if (data.miscDataLen == 0 && data.ramDataLen == 0) {
		// Don't create save file when there is nothing to save
		return;
	}
	boost::filesystem::path filePath(fileName);

	auto parentPath = filePath.parent_path();
	if (!parentPath.empty()) {
		boost::filesystem::create_directories(parentPath);
	}

	std::ofstream file;
	file.exceptions(std::ios::failbit);
	try {
		file.open(filePath.generic_string(), std::ios::out | std::ios::binary);
	} catch(std::ios_base::failure &e) {
		logger::Log(logger::Error, "Could not open save data file '%s': %s", filePath.c_str(), e.code().message().c_str());
		return;
	}
	try {
		auto endian = Endian::getSingleton(false);
		uint32_t ramDataLen = endian->convu32(static_cast<uint32_t>(data.ramDataLen));
		uint32_t miscDataLen = endian->convu32(static_cast<uint32_t>(data.miscDataLen));
		uint32_t id = endian->convu32(SaveFile::saveID);
		file.write(reinterpret_cast<const char *>(&id), sizeof(id));
		file.write(reinterpret_cast<const char *>(&ramDataLen), sizeof(ramDataLen));
		if(data.ramDataLen > 0) file.write(reinterpret_cast<const char *>(data.ramData), data.ramDataLen);
		file.write(reinterpret_cast<const char *>(&miscDataLen), sizeof(miscDataLen));
		if(data.miscDataLen > 0) file.write(reinterpret_cast<const char *>(data.miscData), data.miscDataLen);
	} catch(std::ios_base::failure &e) {
		boost::filesystem::remove(filePath);
		logger::Log(logger::Error, "Could write save file '%s': %s", filePath.c_str(), e.code().message().c_str());
	}
}

void SaveFile::ReadStateFile(const std::string &fileName, SaveData_t &data)
{
	data.romData = NULL;
	data.romDataLen = 0;
	data.ramData = NULL;
	data.ramDataLen = 0;
	data.miscData = NULL;
	data.miscDataLen = 0;

	boost::filesystem::path filePath(fileName);

	if (!boost::filesystem::exists(filePath))
		return;

	std::ifstream file;
	file.exceptions(std::ios::failbit);
	try {
		file.open(filePath.c_str(), std::ios::in | std::ios::binary);
	} catch(std::ios_base::failure &e) {
		logger::Log(logger::Error, "Could not open save state data file '%s': %s", filePath.c_str(), e.code().message().c_str());
		return;
	}
	try {
		auto endian = Endian::getSingleton(false);
		uint32_t id;
		file.read((char *) &id, sizeof(id));
		if(endian->convu32(id) != SaveFile::stateID) {
			logger::Log(logger::Error, "Save state corrupted '%s'", filePath.c_str());
			return;
		}
		ReadSection(file, &data.ramDataLen, &data.ramData);
		ReadSection(file, &data.miscDataLen, &data.miscData);
	} catch(std::ios_base::failure &e) {
		boost::filesystem::remove(filePath);
		logger::Log(logger::Error, "Could not read save state file '%s': %s", filePath.c_str(), e.code().message().c_str());
	}
}

void SaveFile::WriteStateFile(const std::string &fileName, const SaveData_t &data)
{
	if (data.miscDataLen == 0 && data.ramDataLen == 0) {
		// Don't create save file when there is nothing to save
		return;
	}
	boost::filesystem::path filePath(fileName);

	auto parentPath = filePath.parent_path();
	if (!parentPath.empty()) {
		boost::filesystem::create_directories(parentPath);
	}

	std::ofstream file;
	file.exceptions(std::ios::failbit);
	try {
		file.open(filePath.generic_string(), std::ios::out | std::ios::binary);
	} catch(std::ios_base::failure &e) {
		logger::Log(logger::Error, "Could not open save state data file '%s': %s", filePath.c_str(), e.code().message().c_str());
		return;
	}
	try {
		auto endian = Endian::getSingleton(false);
		uint32_t ramDataLen = endian->convu32(static_cast<uint32_t>(data.ramDataLen));
		uint32_t miscDataLen = endian->convu32(static_cast<uint32_t>(data.miscDataLen));
		uint32_t id = endian->convu32(SaveFile::saveID);
		file.write(reinterpret_cast<const char *>(&id), sizeof(id));
		file.write(reinterpret_cast<const char *>(&ramDataLen), sizeof(ramDataLen));
		if(data.ramDataLen > 0) file.write(reinterpret_cast<const char *>(data.ramData), data.ramDataLen);
		file.write(reinterpret_cast<const char *>(&miscDataLen), sizeof(miscDataLen));
		if(data.miscDataLen > 0) file.write(reinterpret_cast<const char *>(data.miscData), data.miscDataLen);
	} catch(std::ios_base::failure &e) {
		boost::filesystem::remove(filePath);
		logger::Log(logger::Error, "Could write save state file '%s': %s", filePath.c_str(), e.code().message().c_str());
		return;
	}
}

void SaveFile::ReadSection(std::ifstream &file, size_t *len, uint8_t **data)
{
	uint32_t sectionLen;
	file.read(reinterpret_cast<char *>(&sectionLen), sizeof(uint32_t));
	sectionLen = Endian::getSingleton(false)->convu32(sectionLen);
	if (sectionLen > 0) {
		*data = new uint8_t[sectionLen];
		file.read(reinterpret_cast<char *>(*data), sectionLen);
		*len = sectionLen;
	}
}
