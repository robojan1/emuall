//
// Created by Robbert-Jan de Jager on 15-10-18.
//

#include <emuall/emulator/EmulatorList.h>
#include <emuall/emulator/Emulator.h>
#include <emuall/emulator/EmulatorInterface.h>
#include <emuall/util/log.h>
#include <emuall/util/stddir.h>
#include <emuall/util/string.h>
#include <vector>
#include <algorithm>
#include <boost/filesystem.hpp>

#ifdef _WIN32
#define EMULATOR_EXTENSION "emu.dll"
#else
#define EMULATOR_EXTENSION "emu.so"
#endif


EmulatorList::EmulatorList()
{
	std::vector<std::string> searchDirs;
	searchDirs.push_back(emuall::GetPluginsDir());
	searchDirs.push_back(emuall::GetExecuatableDir() + "/plugins");

	const char *envDir = std::getenv("EMUALL_EXT_DIR");
	if(envDir) {
		searchDirs.push_back(envDir);
	}

	std::transform(searchDirs.begin(), searchDirs.end(), searchDirs.begin(), 
		[](std::string &p) -> std::string {
			return boost::filesystem::path(p).lexically_normal().generic_string();
		});
	std::sort(searchDirs.begin(), searchDirs.end());
	auto newEnd = std::unique(searchDirs.begin(), searchDirs.end(), 
		[](std::string &a, std::string &b) -> bool {
			return boost::filesystem::equivalent(a, b);
		});

	searchDirs.resize(std::distance(searchDirs.begin(), newEnd));

	LoadEmulators(searchDirs);
}

std::vector<std::string> EmulatorList::GetEmulatorFileFilters() const {
	std::vector<std::string>result;
	result.reserve(_emulators.size());
	for(auto &emu : _emulators)
	{
		std::string filterStr = emu->GetFileFilter();
		filterStr.append(";*.elf");
		std::string filter = emu->GetName() + "(" + filterStr + ")|" + filterStr;
		result.push_back(filter);
	}
	return std::move(result);
}

std::string EmulatorList::GetCombinedEmulatorFileFilter() const {
	std::string filterStr;
	for(auto &emu : _emulators)
	{
		filterStr.append(emu->GetFileFilter());
		filterStr.push_back(';');
	}
	filterStr.pop_back();

	return "Compatible emulators(" + filterStr + ")|" + filterStr;
}

std::string EmulatorList::GetEmulatorIdFromIdx(int i) const {
	if(i < 0 || i >= _emulators.size()) return "";
	return _emulators.at(i)->GetInfo().id;
}

std::string EmulatorList::GetMatchingEmulator(const std::string &fileName) const {
	for (auto it = _emulators.cbegin(); it != _emulators.cend(); ++it)
	{
		if ((*it)->IsCompatible(fileName.c_str()))
		{
			return (*it)->GetInfo().id;
		}
	}
	return "";
}

std::unique_ptr<Emulator> EmulatorList::CreateEmulator(const std::string &emuId) const {
	for (auto &it : _emulators)
	{
		if (emuId == it->GetInfo().id)
		{
			return std::make_unique<Emulator>(it);
		}
	}
	return nullptr;
}

const EmulatorList &EmulatorList::Get() {
	static EmulatorList singleton;
	return singleton;
}

bool EmulatorList::Contains(const std::string &id) const {
	for(auto &it : _emulators)
	{
		if(it->GetInfo().id == id) return true;
	}
	return false;
}

void EmulatorList::LoadEmulators(const std::vector<std::string> &searchDirs) {
	// Load all emulators
	std::vector<std::string> foundEmulators;
	for(auto &dir : searchDirs)
	{
		using namespace boost::filesystem;
		path dirPath(dir);
		if (!exists(dirPath) || !is_directory(dirPath)) continue;
		for (directory_entry &entry : directory_iterator(dirPath))
		{
			if (!StringEndsWith(entry.path().filename().generic_string().c_str(), EMULATOR_EXTENSION)) continue;
			auto emu = std::make_unique<EmulatorInterface>(entry.path().generic_string());
			if (emu->IsValid())
			{
				std::string id = emu->GetInfo().id;
				if(std::find(foundEmulators.begin(), foundEmulators.end(), id) != foundEmulators.end()) {
					logger::Log(logger::Warn, "Emulator collision found for emulator %s", id.c_str());
					continue;
				}
				_emulators.emplace_back(std::move(emu));
				foundEmulators.emplace_back(std::move(id));
			}
		}
	}
}
