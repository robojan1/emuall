#include <emuall/exception.h>

#include <emuall/emulator/Emulator.h>
#include <emuall/emulator/SaveFile.h>
#include <emuall/emulator/EmulatorInterface.h>

#include <emuall/util/log.h>
#include <emuall/util/stddir.h>
#include <emuall/util/picosha2.h>
#include <emuall/util/mmapfile.h>

#include <emuall/gdb/Server.h>

#include <elfio/elfio.hpp>

#include <unordered_map>
#include <csignal>

#include <boost/filesystem.hpp>
#include <iomanip>

#ifdef _MSC_VER
#define SIGTRAP 5
#endif

Emulator::Emulator(std::shared_ptr<EmulatorInterface> emuif) : _handle(nullptr), _emu(std::move(emuif)),
	_initialized(false)
{
	_handle = _emu->CreateEmulator();
	if(_handle == nullptr) {
		throw BaseException("Could not create the emulator");
	}

	_infoEmulator = _emu->GetInfo();
	_infoCpuDebugger = _emu->GetCpuDebuggerInfo();
	_infoMemDebugger = _emu->GetMemDebuggerInfo(_handle);
	_infoGdb = _emu->GetGdbInfo();
}

Emulator::~Emulator() {
	SetGdbServer(nullptr);
	if(_handle != nullptr) {
		_emu->ReleaseEmulator(_handle);
		_handle = nullptr;
	}
}

gdb::RegisterValue Emulator::ReadGdbRegister(int i) const
{
	try {
		gdb::RegisterValue value;
		value.defined = true;
		auto &reg = _infoGdb.registers.at(i);
		switch(reg.size) {
			case 8:
				value.type = gdb::Reg8;
				value.u8 = GetValU(reg.id);
				break;
			case 16:
				value.type = gdb::Reg16;
				value.u16 = GetValU(reg.id);
				break;
			default:
			case 32:
				value.type = gdb::Reg32;
				value.u32 = GetValU(reg.id);
				break;
			case 64:
				logger::Log(logger::Warn, "64 bit types not yet supported");
				value.type = gdb::Reg64;
				value.u64 = GetValU(reg.id);
				break;
		}
		return value;
	} catch(std::out_of_range &) {
		return {gdb::Reg8, false};
	}
}

void Emulator::WriteGdbRegister(int i, const gdb::RegisterValue &value) {
	try {
		auto &reg = _infoGdb.registers.at(i);
		switch(reg.size) {
			case 8:
				if(value.type != gdb::Reg8) {
					logger::Log(logger::Warn, "Register type mismatch for register %d type %d", i, value.type);
				}
				SetValU(reg.id, value.u8);
				break;
			case 16:
				if(value.type != gdb::Reg16) {
					logger::Log(logger::Warn, "Register type mismatch for register %d type %d", i, value.type);
				}
				SetValU(reg.id, value.u16);
				break;
			default:
			case 32:
				if(value.type != gdb::Reg32) {
					logger::Log(logger::Warn, "Register type mismatch for register %d type %d", i, value.type);
				}
				SetValU(reg.id, value.u32);
				break;
			case 64:
				if(value.type != gdb::Reg64) {
					logger::Log(logger::Warn, "Register type mismatch for register %d type %d", i, value.type);
				}
				logger::Log(logger::Warn, "64 bit types not yet supported");
				SetValU(reg.id, value.u64);
				break;
		}
	} catch(std::out_of_range &) {
		logger::Log(logger::Warn, "GDB tried setting unknown register %d", i);
	}
}

void *Emulator::GetGdbMemoryPointer(uint32_t addr, int *size) const {
	void *data;
	int result = GetMemoryPointer(addr, &data, reinterpret_cast<uint32_t *>(size));
	if(!result) {
		*size = -ENOMEM;
		return nullptr;
	}
	return data;
}

void Emulator::Run(int run) const {
	_emu->Run(_handle, run);
	if(_gdbServer) {
		if(IsRunning()) {
			_gdbServer->SetAppStatus(gdb::Running);
		} else {
			_gdbServer->SetAppStatus(gdb::SignalReceived, SIGTRAP);
		}
	}
}

void Emulator::SetGdbServer(const std::shared_ptr<gdb::Server> &server) {
	if(!server || !_infoGdb.supported) {
		if(_gdbServer) {
			_gdbServer->SetAttached(false);
			_gdbServer->SetNumRegisters(0);
			_gdbServer->SetReadRegisterFunc(nullptr);
			_gdbServer->SetWriteRegisterFunc(nullptr);
			_gdbServer->SetTargetDescription("");
			_gdbServer->SetTargetMemoryMap("");
			_gdbServer->SetGetMemoryFunc(nullptr);
			_gdbServer->SetSetMemoryFunc(nullptr);
			_gdbServer->SetOffsets(0,0);
			_gdbServer->SetAppStatus(gdb::Terminated, SIGTERM);
			_gdbServer->DeregisterAddBreakpointCallback(std::bind(&Emulator::OnGdbAddBreakpoint, this, std::placeholders::_1, std::placeholders::_2));
			_gdbServer->DeregisterRemoveBreakpointCallback(std::bind(&Emulator::OnGdbRemoveBreakpoint, this, std::placeholders::_1, std::placeholders::_2));
			_gdbServer->DeregisterContinueCallback(std::bind(&Emulator::OnGdbContinue, this, std::placeholders::_1, std::placeholders::_2));
			_gdbServer->DeregisterStepCallback(std::bind(&Emulator::OnGdbStep, this, std::placeholders::_1, std::placeholders::_2));
			_gdbServer->DeregisterInterruptCallback(std::bind(&Emulator::OnGdbInterrupt, this));
			_gdbServer = nullptr;
		}
	} else {
		_gdbServer = server;
		_gdbServer->SetAttached(true);
		_gdbServer->SetNumRegisters(static_cast<int>(_infoGdb.registers.size()));
		_gdbServer->SetReadRegisterFunc(std::bind(&Emulator::ReadGdbRegister, this, std::placeholders::_1));
		_gdbServer->SetWriteRegisterFunc(std::bind(&Emulator::WriteGdbRegister, this, std::placeholders::_1, std::placeholders::_2));
		_gdbServer->SetTargetDescription(GetGdbTargetDescription());
		_gdbServer->SetTargetMemoryMap(GetGdbMemoryMap());
		_gdbServer->SetOffsets(GetGdbInfo().textOffset, GetGdbInfo().dataOffset);
		_gdbServer->SetGetMemoryFunc(std::bind(&Emulator::GetGdbMemoryPointer, this, std::placeholders::_1, std::placeholders::_2));
		_gdbServer->SetSetMemoryFunc(std::bind(&Emulator::SetMemory, this, std::placeholders::_1, std::placeholders::_2));
		if(IsRunning()){
			_gdbServer->SetAppStatus(gdb::Running);
		} else {
			_gdbServer->SetAppStatus(gdb::SignalReceived, SIGTRAP);
		}
		_gdbServer->RegisterAddBreakpointCallback(std::bind(&Emulator::OnGdbAddBreakpoint, this, std::placeholders::_1, std::placeholders::_2));
		_gdbServer->RegisterRemoveBreakpointCallback(std::bind(&Emulator::OnGdbRemoveBreakpoint, this, std::placeholders::_1, std::placeholders::_2));
		_gdbServer->RegisterContinueCallback(std::bind(&Emulator::OnGdbContinue, this, std::placeholders::_1, std::placeholders::_2));
		_gdbServer->RegisterStepCallback(std::bind(&Emulator::OnGdbStep, this, std::placeholders::_1, std::placeholders::_2));
		_gdbServer->RegisterInterruptCallback(std::bind(&Emulator::OnGdbInterrupt, this));
	}
}

void Emulator::OnGdbAddBreakpoint(uint32_t addr, int kind) {
	if(!IsBreakpoint(addr)) {
		AddBreakpoint(addr);
	}
	if(OnGdbAddBreakpointCB)
	{
		OnGdbAddBreakpointCB(addr);
	}
}

void Emulator::OnGdbRemoveBreakpoint(uint32_t addr, int kind) {
	if(IsBreakpoint(addr)) {
		RemoveBreakpoint(addr);
	}
	if (OnGdbRemoveBreakpointCB)
	{
		OnGdbRemoveBreakpointCB(addr);
	}
}

void Emulator::OnGdbContinue(bool hasAddr, uint32_t kind) {
	if(hasAddr) {
		logger::Log(logger::Warn, "Cannot continue from address\n");
		return;
	}
	Run(true);
	if(OnGdbContinueCB)
	{
		OnGdbContinueCB();
	}
}

void Emulator::OnGdbStep(bool hasAddr, uint32_t kind) {
	if(hasAddr) {
		logger::Log(logger::Warn, "Cannot continue from address\n");
		return;
	}
	Step();
	if (OnGdbStepCB)
	{
		OnGdbStepCB();
	}
}

void Emulator::OnGdbInterrupt() {
	if(IsRunning()) {
		Run(false);
		_gdbServer->SetAppStatus(gdb::SignalReceived, SIGINT);
	}
}

int Emulator::Init() {
	callBackfunctions_t funcs;
	funcs.user = this;
	funcs.OnBreakpointHit = [](void *user, uint32_t addr) {
		Emulator *emu = reinterpret_cast<Emulator *>(user);
		emu->OnBreakpointHit(addr);
	};
	funcs.DrawFrame = [](void *user, int id) {
		Emulator *emu = reinterpret_cast<Emulator *>(user);
		emu->OnDrawFrame(id);
	};
	funcs.GetOptionInt = [](void *user, int32_t id) -> int32_t {
		Emulator *emu = reinterpret_cast<Emulator *>(user);
		if(emu->GetOptionIntHandler) {
			return emu->GetOptionIntHandler(emu->_infoEmulator.id, id);
		}
		logger::Log(logger::Warn, "Get option int not implemented");
		return 0;
	};
	funcs.GetOptionFloat =  [](void *user, int32_t id) -> float {
		Emulator *emu = reinterpret_cast<Emulator *>(user);
		if(emu->GetOptionFloatHandler) {
			return emu->GetOptionFloatHandler(emu->_infoEmulator.id, id);
		}
		logger::Log(logger::Warn, "Get option float not implemented");
		return 0;
	};
	funcs.GetOptionString =  [](void *user, int32_t id) -> const char * {
		Emulator *emu = reinterpret_cast<Emulator *>(user);
		if(emu->GetOptionStringHandler) {
			auto result = emu->GetOptionStringHandler(emu->_infoEmulator.id, id);
			auto result_c_str = new char[result.size()+1];
			strcpy(result_c_str, result.c_str());
			return result_c_str;
		}
		logger::Log(logger::Warn, "Get option string not implemented");
		return nullptr;
	};
	funcs.ReleaseString = [](void *user, const char *str) {
		delete[] str;
	};
	funcs.ExitEmulator = [](void *user, uint32_t exitCode) {
		Emulator *emu = reinterpret_cast<Emulator *>(user);
		if(emu->ExitEmulatorHandler) {
			emu->ExitEmulatorHandler(exitCode);
			return;
		}
		logger::Log(logger::Warn, "Exit emulator not implemented");
	};
	_initialized = _emu->Init(_handle, funcs) != 0;
	return _initialized;
}

void Emulator::OnBreakpointHit(uint32_t addr) {
	(void)addr;
	if (_gdbServer) {
		_gdbServer->SetAppStatus(gdb::SignalReceived, SIGTRAP);
		_gdbServer->OnAppSignalReceived();
	}
}

void Emulator::OnDrawFrame(int id) {
	if(_drawFrameHandler) {
		_drawFrameHandler(this, id);
	}
}

void Emulator::LoadProgram(const std::string &path) {
	try {
		MemoryMappedFile file(path, MemoryMappedFile::ReadOnly);
		CreateRomHash(static_cast<const uint8_t *>(file.Load(0, file.Size())), file.Size());
		try {
			LoadProgramElf(path);
		} catch(BaseException &ex) {
			logger::Log(logger::Debug, "%s is not an elf file: %s", path.c_str(), ex.GetMsg());
			LoadProgramRaw(&file, path);
		}
		_romFilePath  = path;
	} catch(FileException &ex) {
		throw BaseException("Could not open file \"%s\": %s", path.c_str(), ex.what());
	}
}

void Emulator::LoadProgramElf(const std::string &path) {
	using namespace ELFIO;

	elfio reader;
	if(!reader.load(path))
	{
		throw BaseException("Could not load file \"%s\" as elf", path.c_str());
	}
	uint32_t min_addr = UINT32_MAX;
	uint32_t max_addr = 0;
	for(auto segment : reader.segments) {
		bool skipSegment = segment->get_type() != PT_LOAD || segment->get_memory_size() == 0;
		logger::Log(logger::Debug, "Segment type: %08x flags: %08x size: %10llu filesize: %10llu paddr: 0x%08x vaddr 0x%08x Load: %s",
		    segment->get_type(),
		    segment->get_flags(),
		    segment->get_memory_size(),
		    segment->get_file_size(),
		    segment->get_physical_address(),
		    segment->get_virtual_address(),
		    skipSegment ? "no" : "yes");
		if(skipSegment) continue;
		Elf64_Addr phy_addr = segment->get_physical_address();
		size_t phy_last_addr = phy_addr + segment->get_memory_size() - 1;
		if(phy_addr < min_addr) min_addr = static_cast<uint32_t>(phy_addr);
		if(phy_last_addr > max_addr) max_addr = static_cast<uint32_t>(phy_last_addr);
	}
	if(min_addr >= max_addr) {
		throw BaseException("Elf file doesn't contain loadable sections");
	}
	size_t romSize = max_addr - min_addr + 1;
	try {
		auto romData = new uint8_t[romSize];

		// Copy the data to a ROM file
		for(auto segment : reader.segments) {
			if(segment->get_type() != PT_LOAD || segment->get_memory_size() == 0) continue;

			// FIXME segment.data() has a bug and returns the wrong address
			//const void *elfData = segment.data();
			const void *elfData = segment->get_data();
			size_t fileSize = segment->get_file_size();
			size_t memSize = segment->get_memory_size();
			size_t offset = segment->get_physical_address() - min_addr;
			size_t initializedSize = std::min(fileSize, memSize);
			size_t zeroSize = memSize <= fileSize ? 0 : memSize - fileSize;
			if(initializedSize > 0) {
				memcpy(romData + offset, elfData, initializedSize);
			}
			if(zeroSize > 0) {
				memset(romData + offset + initializedSize, 0, zeroSize);
			}
		}

		SaveData_t data = {0,};
		data.romData = romData;
		data.romDataLen = romSize;
		SaveFile::ReadSaveFile(GetSaveFilePath(path), data);
		if(!Load(&data)) {
			throw BaseException("Could not load the ROM");
		}
	} catch(std::bad_alloc &) {
		throw BaseException("Could not allocate enough memory for ROM size %llu", romSize);
	}
}

void Emulator::LoadProgramRaw(const MemoryMappedFile *loader, const std::string &path) {
	SaveData_t data = {0,};
	if(!IsCompatible(path.c_str())){
		throw BaseException("Could not load file \"%s\": Unknown format", path.c_str());
	}
	data.romData = static_cast<const uint8_t *>(loader->Load(0, loader->Size()));
	data.romDataLen = loader->Size();
	SaveFile::ReadSaveFile(GetSaveFilePath(path), data);
	if(!Load(&data)) {
		throw BaseException("Could not load the ROM");
	}
}

std::string Emulator::GetSaveFilePath(const std::string &rom_path) const {
	boost::filesystem::path romPath(rom_path);
	boost::filesystem::path saveFilePath(emuall::GetSavesDir());
	std::string filename = romPath.stem().generic_string() + "-" + _romHash;
	filename += romPath.extension().generic_string() + ".sav";
	saveFilePath /= filename;
	return saveFilePath.generic_string();
}

void Emulator::CreateRomHash(const uint8_t *data, size_t size) {
	uint32_t sum = 0x9BA009BA;
	while (size >= 4)
	{
		sum += *reinterpret_cast<const uint32_t *>(data);
		data += sizeof(uint32_t);
		size -= sizeof(uint32_t);
	}
	uint8_t remainder[4];
	size_t i = 0;
	for (i = 0; i < size; i++)
	{
		remainder[i] = *data++;
	}
	for (; i < 4; i++)
	{
		remainder[i] = 0x00;
	}
	sum += *reinterpret_cast<const uint32_t *>(remainder);

	std::stringstream ss;
	ss << std::hex << std::setfill('0') << std::setw(8) << sum;
	_romHash = (ss.str());
}

int Emulator::Save() const {
	SaveData_t saveData;
	saveData.miscData = saveData.ramData = nullptr;
	saveData.romData = nullptr;
	saveData.miscDataLen = saveData.ramDataLen = saveData.romDataLen = 0;
	int result = Save(&saveData);
	SaveFile::WriteSaveFile(GetSaveFilePath(_romFilePath), saveData);
	ReleaseSaveData(&saveData);
	return result;
}

int Emulator::SaveState(const std::string & filename) const
{
	SaveData_t saveData{ nullptr };
	if (!SaveState(&saveData))
		return false;

	SaveFile::WriteStateFile(filename, saveData);

	ReleaseSaveData(&saveData);

	return true;
}
