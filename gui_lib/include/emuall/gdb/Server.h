//
// Created by Robbert-Jan de Jager on 11-10-18.
//

#ifndef EMUALL_SERVER_H
#define EMUALL_SERVER_H

#include <memory>
#include <string>
#include <functional>

namespace gdb {

class ServerConnection;

enum RegisterType {
	Reg8,
	Reg16,
	Reg32,
	Reg64
};
struct RegisterValue {
	enum RegisterType type;
	bool defined;
	union {
		uint8_t u8;
		uint16_t u16;
		uint32_t u32;
		uint64_t u64;
	};
};

enum ApplicationStatus {
	SignalReceived,
	Exited,
	Terminated,
	Running,
};

class Server_p;

class Server {
public:
	explicit Server(int port);
	virtual ~Server();

	bool IsAttached() const;
	int GetNumberOfRegisters() const;
	const std::string &GetExecutableFilename() const;
	const std::string &GetTargetDescription() const;
	const std::string &GetTargetMemoryMap() const;
	bool HasTargetMemoryMap() const;

	void OnAppSignalReceived();

	void SetNumRegisters(int num);
	void SetAttached(bool _appAttached);
	void SetReadRegisterFunc(const std::function<RegisterValue(int)> &func);
	void SetWriteRegisterFunc(const std::function<void(int,const RegisterValue &)> &func);
	void SetGetMemoryFunc(const std::function<void *(uint32_t, int32_t *)> &func);
	void SetSetMemoryFunc(const std::function<void(uint32_t, uint8_t)> &func);
	void SetExecutableFilename(const std::string &filename);
	void SetTargetDescription(const std::string &description);
	void SetTargetMemoryMap(const std::string &map);
	void SetAppStatus(enum ApplicationStatus status, int signalNr = 0);
	void SetOffsets(uint32_t text, uint32_t data);

	RegisterValue ReadRegister(int i);
	void WriteRegister(int i, const RegisterValue &value);
	void *GetMemoryPointer(uint32_t addr, int32_t *size);
	void SetMemory(uint32_t addr, uint8_t val);
	enum ApplicationStatus GetAppStatus() const;
	int GetAppLastSignal() const;
	uint32_t GetTextOffset() const;
	uint32_t GetDataOffset() const;

	void RegisterDisconnectedCallback(std::function<void(void)> &&func);
	void RegisterContinueCallback(std::function<void(bool, uint32_t)> &&func);
	void RegisterStepCallback(std::function<void(bool, uint32_t)> &&func);
	void RegisterAddBreakpointCallback(std::function<void(uint32_t,int)> &&func);
	void RegisterRemoveBreakpointCallback(std::function<void(uint32_t, int)> &&func);
	void RegisterInterruptCallback(std::function<void(void)> &&func);
	void DeregisterDisconnectedCallback(std::function<void(void)> &&func);
	void DeregisterContinueCallback(std::function<void(bool, uint32_t)> &&func);
	void DeregisterStepCallback(std::function<void(bool, uint32_t)> &&func);
	void DeregisterAddBreakpointCallback(std::function<void(uint32_t, int)> &&func);
	void DeregisterRemoveBreakpointCallback(std::function<void(uint32_t, int)> &&func);
	void DeregisterInterruptCallback(std::function<void(void)> &&func);
	

	void OnGdbClientDisconnected();
	void OnGdbClientContinue(bool hasAddr, uint32_t addr);
	void OnGdbClientStep(bool hasAddr, uint32_t addr);
	void OnGdbClientAddBreakpoint(uint32_t addr, int kind);
	void OnGdbClientRemoveBreakpoint(uint32_t addr, int kind);
	void OnGdbClientInterrupt();

private:
	std::unique_ptr<Server_p> _priv;
};
}

#endif //EMUALL_SERVER_H
