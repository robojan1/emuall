//
// Created by Robbert-Jan de Jager on 13-10-18.
//

#ifndef EMUALL_CLIENTMESSAGEEVENT_H
#define EMUALL_CLIENTMESSAGEEVENT_H
/*

#include <wx/event.h>
#include <string>

namespace gdb {

class ClientMessageEvent : public wxEvent {
public:
	ClientMessageEvent(wxEventType eventType, int winid, const std::string &msg);

	const std::string &getMessage() const { return _msg; }
	void setMessage(const std::string &msg) { _msg = msg; }

	wxEvent *Clone() const override { return new ClientMessageEvent(*this); }

private:
	std::string _msg;
};

class ClientContinueEvent : public wxEvent {
public:
	ClientContinueEvent(wxEventType eventType, int winid);
	ClientContinueEvent(wxEventType eventType, int winid, uint32_t addr);

	bool hasAddress() const { return _hasAddr; }
	uint32_t getAddr() const { return _addr; }

	wxEvent *Clone() const override { return new ClientContinueEvent(*this); }

private:
	uint32_t _addr;
	bool _hasAddr;
};

class ClientBreakpointEvent : public wxEvent {
public:
	ClientBreakpointEvent(wxEventType eventType, int winid, uint32_t addr, int kind);

	uint32_t getAddr() const { return _addr; }
	int getKind() const { return _kind; }

	wxEvent *Clone() const override { return new ClientBreakpointEvent(*this); }

private:
	uint32_t _addr;
	int _kind;
};

class ClientSignalEvent : public wxEvent {
public:
	ClientSignalEvent(wxEventType eventType, int winid);

	wxEvent *Clone() const override { return new ClientSignalEvent(*this); }
};

};

wxDECLARE_EVENT(GDB_CLIENT_MESSAGE, gdb::ClientMessageEvent);
wxDECLARE_EVENT(GDB_CONTINUE, gdb::ClientContinueEvent);
wxDECLARE_EVENT(GDB_STEP, gdb::ClientContinueEvent);
wxDECLARE_EVENT(GDB_ADDBREAKPOINT, gdb::ClientBreakpointEvent);
wxDECLARE_EVENT(GDB_REMOVEBREAKPOINT, gdb::ClientBreakpointEvent);
wxDECLARE_EVENT(GDB_INTERRUPT, gdb::ClientSignalEvent);
*/
#endif //EMUALL_CLIENTMESSAGEEVENT_H
