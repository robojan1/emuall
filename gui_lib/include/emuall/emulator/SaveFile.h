#ifndef SAVEFILE_H_
#define SAVEFILE_H_

#include <emu.h>
#include <string>

namespace SaveFile {
	static const uint32_t saveID = 0x454D5541;
	static const uint32_t stateID = 0x454D5553;

	void ReadSaveFile(const std::string &fileName, SaveData_t &data);
	void WriteSaveFile(const std::string &fileName, const SaveData_t &data);
	void ReadStateFile(const std::string &fileName, SaveData_t &data);
	void WriteStateFile(const std::string &fileName, const SaveData_t &data);

	void ReadSection(std::ifstream &file, size_t *len, uint8_t **data);
}

#endif
