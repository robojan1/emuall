#ifndef _EMULATOR_H
#define _EMULATOR_H

#include <list>
#include <vector>
#include <string>
#include <functional>
#include <cstdint>
#include <memory>
#include <emuall/gdb/Server.h>
#include <emuall/emulator/EmulatorInfo.h>
#include <emuall/emulator/EmulatorInterface.h>

class MemoryMappedFile;

namespace Debugger {
	class DebuggerRoot;
}

class Emulator
{
public:
	explicit Emulator(std::shared_ptr<EmulatorInterface> emuif);
	virtual ~Emulator();

	const EmulatorInfo_t &GetInfo() const { return _infoEmulator; }
	const CpuDebuggerInfo_t &GetCpuDebuggerInfo() const { return  _infoCpuDebugger; }
	const MemDebuggerInfo_t &GetMemDebuggerInfo() const { return _infoMemDebugger; }
	const GdbInfo_t &GetGdbInfo() const { return _infoGdb; }
	std::string GetName() const { return std::move(_emu->GetName()); }
	std::vector<EmulatorInput_t> GetEmulatorInputs() const { return std::move(_emu->GetEmulatorInputs()); }
	pugi::xml_node GetGpuDebuggerInfo() const { return _emu->GetGpuDebuggerInfo(this); }
	std::vector<EmulatorOption_t> GetEmulatorOptions() const { return std::move(_emu->GetEmulatorOptions()); }

	uint32_t GetValU(int id) const { return _emu->GetValU(_handle, id); }
	int32_t GetValI(int id) const { return _emu->GetValI(_handle, id); }
	const char *GetString(int id) const { return _emu->GetString(_handle, id); }
	void SetValU(int id, uint32_t val) const { _emu->SetValU(_handle, id, val); }
	void SetValI(int id, int32_t val) const { _emu->SetValI(_handle, id, val); }
	float GetFloat(int id) const { return _emu->GetFloat(_handle, id); }

	void LoadProgram(const std::string &path);

	int Init();
	bool IsInitialized() const {return _initialized; };
	int Load(const SaveData_t *data) const { return _emu->Load(_handle, data); }
	int LoadState(const SaveData_t *state) const { return _emu->LoadState(_handle, state); }
	bool InitGL(int id) const { return _emu->InitGL(_handle, id); }
	void DestroyGL(int id) const { _emu->DestroyGL(_handle, id); }

	const char *GetDescription(unsigned int *size) const { return _emu->GetDescription(size); }
	int IsCompatible(const char *filename) const { return _emu->IsCompatible(filename); }

	void Run(int run) const;
	void Step() const { _emu->Step(_handle); }
	int IsRunning() const { return _emu->IsRunning(_handle); }

	int Tick(uint32_t time) const { return _emu->Tick(_handle, time); }
	void Input(int key, int pressed) const { _emu->Input(_handle, key, pressed); }
	void Draw(int id) const { _emu->Draw(_handle, id); }
	void Reshape(int id, int width, int height, bool keepAspect) const { _emu->Reshape(_handle, id, width, height, keepAspect); }

	int Save() const;
	int SaveState(SaveData_t *state) const { return _emu->SaveState(_handle, state); }
	int SaveState(const std::string &filename) const;

	void InitAudio(int source, unsigned int sampleRate, int channels) const { _emu->InitAudio(_handle, source, sampleRate, channels); }
	void GetAudio(int source, short *buffer, unsigned int samples) const { _emu->GetAudio(_handle, source, buffer, samples); }

	// Debugging
	char Disassemble(unsigned int pos, const char **raw, const char **instr) const { return _emu->Disassemble(_handle, pos, raw, instr); }
	void AddBreakpoint(unsigned int pos) const { return _emu->AddBreakpoint(_handle, pos); }
	void RemoveBreakpoint(unsigned int pos) const { _emu->RemoveBreakpoint(_handle, pos); }
	int IsBreakpoint(unsigned int pos) const { return _emu->IsBreakpoint(_handle, pos); }
	char GetMemoryData(int memory, unsigned int address) const { return _emu->GetMemoryData(_handle, memory, address); }
	std::string GetGdbTargetDescription() const { return std::move(_emu->GetGdbTargetDescription()); }
	std::string GetGdbMemoryMap() const { return std::move(_emu->GetGdbMemoryMap()); }
	int GetMemoryPointer(unsigned int addr, void **data, unsigned int *size) const { return _emu->GetMemoryPointer(_handle, addr, data, size); }
	void SetMemory(uint32_t addr, uint8_t val) { return _emu->SetMemory(_handle, addr, val); }

	void SetGdbServer(const std::shared_ptr<gdb::Server> &server);
	void SetDrawFrameHandler(std::function<void(Emulator*, int)> func) { _drawFrameHandler = func; }

	void NotifyOptionChange(int id) { _emu->NotifyOptionChange(_handle, id); }

	std::function<int32_t(const std::string &,int)> GetOptionIntHandler;
	std::function<float(const std::string &,int)> GetOptionFloatHandler;
	std::function<std::string(const std::string &,int)> GetOptionStringHandler;
	std::function<void(uint32_t)> ExitEmulatorHandler;
	std::function<void()> OnGdbStepCB;
	std::function<void()> OnGdbContinueCB;
	std::function<void(uint32_t)> OnGdbAddBreakpointCB;
	std::function<void(uint32_t)> OnGdbRemoveBreakpointCB;

private:
	std::shared_ptr<EmulatorInterface> _emu;
	EMUHANDLE _handle;

	GdbInfo_t _infoGdb;
	CpuDebuggerInfo_t _infoCpuDebugger;
	MemDebuggerInfo_t _infoMemDebugger;
	EmulatorInfo_t _infoEmulator;

	std::shared_ptr<gdb::Server> _gdbServer;
	std::function<void(Emulator *, int)> _drawFrameHandler;

	std::string _romHash;
	std::string _romFilePath;

	bool _initialized;

	int Save(SaveData_t* data) const { return _emu->Save(_handle, data); }
	void ReleaseSaveData(SaveData_t *data) const { _emu->ReleaseSaveData(_handle, data); }

	gdb::RegisterValue ReadGdbRegister(int i) const;
	void WriteGdbRegister(int i, const gdb::RegisterValue &value);
	void *GetGdbMemoryPointer(uint32_t addr, int *size) const;
	void OnGdbAddBreakpoint(uint32_t addr, int kind);
	void OnGdbRemoveBreakpoint(uint32_t addr, int kind);
	void OnGdbContinue(bool hasAddr, uint32_t addr);
	void OnGdbStep(bool hasAddr, uint32_t addr);
	void OnGdbInterrupt();

	void OnBreakpointHit(uint32_t addr);
	void OnDrawFrame(int id);

	void LoadProgramElf(const std::string &path);
	void LoadProgramRaw(const MemoryMappedFile *file, const std::string &path);

	std::string GetSaveFilePath(const std::string &rom_path) const;

	void CreateRomHash(const uint8_t *data, size_t size);
};

#endif
