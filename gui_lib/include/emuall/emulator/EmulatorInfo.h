//
// Created by Robbert-Jan de Jager on 15-10-18.
//

#ifndef EMUALL_EMULATORINFO_H
#define EMUALL_EMULATORINFO_H

#include <string>
#include <vector>
#include <map>

#define EMU_INPUT_ANALOG 1
#define EMU_INPUT_PASS 2

typedef struct {
	std::string name;
	int id;
	int primaryKey;
	int secondaryKey;
	int flags;
} EmulatorInput_t;

typedef struct {
	std::string name;
	int id;
	int size;
	bool readonly;
} EmulatorRegister_t;

typedef struct {
	enum Mode {
		dec,
		hex,
		oct,
		flag,
		string
	};
	std::string name;
	int id;
	int size;
	Mode mode;
} EmulatorValue_t;

typedef struct {
	std::string name;
	unsigned int size;
	int id;
} EmulatorMemory_t;

struct EmulatorOption_t {
	enum Type {
		Boolean,
		File,
		Path,
		Integer,
		Enumeration,
		Float,
	};
	std::string name;
	std::string description;
	std::string shortname;
	int id;
	Type type;
	std::map<int, std::string> enumOptions;
	bool hasMin;
	int minInt;
	float minFloat;
	bool hasMax;
	int maxInt;
	float maxFloat;
	std::string defaultValueString;
	int defaultValueInt;
	float defaultValueFloat;
	bool required;

	static bool IsStringOption(enum Type type) { return type == File || type == Path; }
	static bool IsIntOption(enum Type type) { return type == Boolean || type == Integer || type == Enumeration; }
	static bool IsFloatOption(enum Type type) { return type == Float; }
};

typedef struct {
	bool disassembler;
	bool breakpointSupport;
	unsigned int disassemblerSize;
	int curLineId;
	bool step;
	bool stepOut;
	bool stepOver;
	std::vector<EmulatorRegister_t> registers;
	std::vector<EmulatorRegister_t> flags;
} CpuDebuggerInfo_t;

typedef struct {
	std::vector<EmulatorMemory_t> memories;
	std::vector<EmulatorValue_t> infos;
	bool readOnly;
} MemDebuggerInfo_t;

typedef struct {
	int size;
	int id;
} GdbRegister_t;

typedef struct {
	std::vector<GdbRegister_t> registers;
	uint32_t textOffset;
	uint32_t dataOffset;
	bool supported;
} GdbInfo_t;

typedef struct {
	int id;
	int width;
	int height;
	int mouseXvarID;
	int mouseYvarID;
} EmulatorScreen_t;

typedef struct {
	std::string id;
	std::string name;
	std::string fileFilterString;
	std::string description;
	std::string aboutInfo;
	std::vector <EmulatorScreen_t> screens;
	int numAudioStreams;
	bool screenAutoRefresh;
} EmulatorInfo_t;

#endif //EMUALL_EMULATORINFO_H
