//
// Created by Robbert-Jan de Jager on 15-10-18.
//

#ifndef EMUALL_EMULATORLIST_H
#define EMUALL_EMULATORLIST_H

#include <list>
#include <string>
#include <memory>
#include <vector>

class Emulator;
class EmulatorInterface;

class EmulatorList
{
public:

	class iterator {
		using container_type = std::vector<std::shared_ptr<EmulatorInterface>>::const_iterator;
	public:
		using iterator_category = std::bidirectional_iterator_tag;
		using value_type = const EmulatorInterface *;
		using difference_type = int;
		using pointer = const EmulatorInterface **;
		using reference = const EmulatorInterface *&;

		explicit iterator(container_type it) : _it(it) {}
		iterator(const iterator &other) = default;
		~iterator() = default;

		iterator &operator=(const iterator &other) = default;
		bool operator==(const iterator &other) { return _it == other._it; }
		bool operator!=(const iterator &other) { return _it != other._it; }
		value_type operator*() {return _it->get(); }
		value_type operator->() {return _it->get(); }

		iterator &operator++() { ++_it; return *this; }
		iterator operator++(int) { auto it = *this; ++_it; return it; }
		iterator &operator--() { --_it; return *this; }
		iterator operator--(int) {auto it = *this; --_it; return it; }

	private:
		container_type _it;
	};

	EmulatorList(const EmulatorList &) = delete;
	~EmulatorList() = default;
	static const EmulatorList &Get();

	EmulatorList &operator=(const EmulatorList &) = delete;

	iterator begin() const { return iterator(_emulators.begin()); }
	iterator end() const { return iterator(_emulators.end()); }

	bool Contains(const std::string &id) const;

	std::unique_ptr<Emulator> CreateEmulator(const std::string &emuId) const;
	std::string GetFileFilters() const;
	std::vector<std::string> GetEmulatorFileFilters() const;
	std::string GetCombinedEmulatorFileFilter() const;
	std::string GetEmulatorIdFromIdx(int i) const;
	std::string GetMatchingEmulator(const std::string &fileName) const;

private:
	EmulatorList();
	void LoadEmulators(const std::vector<std::string> &searchDirs);

	std::vector<std::shared_ptr<EmulatorInterface>> _emulators;
};


#endif //EMUALL_EMULATORLIST_H
