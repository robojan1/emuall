//
// Created by Robbert-Jan de Jager on 15-10-18.
//

#ifndef EMUALL_EMULATORINTERFACE_H
#define EMUALL_EMULATORINTERFACE_H

#include <cstdint>
#include <emuall/emulator/EmulatorInfo.h>
#include <emu.h>
#include <string>
#include <vector>
#include <pugixml.hpp>

namespace Debugger {
	class DebuggerRoot;
}
class Emulator;

class EmulatorInterface
{
public:
	EmulatorInterface(const std::string &dllName);
	~EmulatorInterface();
	EmulatorInterface(const EmulatorInterface &other);

	EmulatorInterface &operator=(const EmulatorInterface &other);
	bool IsValid() { return _valid; }

	EmulatorInfo_t GetInfo() const;
	std::string GetFileFilter() const;
	std::string GetName() const;
	std::vector<EmulatorInput_t> GetEmulatorInputs() const;
	CpuDebuggerInfo_t GetCpuDebuggerInfo() const;
	MemDebuggerInfo_t GetMemDebuggerInfo(EMUHANDLE handle) const;
	GdbInfo_t GetGdbInfo() const;
	pugi::xml_node GetGpuDebuggerInfo(const Emulator *emu) const;
	std::vector<EmulatorOption_t> GetEmulatorOptions() const;


	uint32_t GetValU(EMUHANDLE handle, int id) const;
	int32_t GetValI(EMUHANDLE handle, int id) const;
	const char *GetString(EMUHANDLE handle, int id) const;
	void SetValU(EMUHANDLE handle, int id, uint32_t val) const;
	void SetValI(EMUHANDLE handle, int id, int32_t val) const;
	float GetFloat(EMUHANDLE handle, int id) const;

	EMUHANDLE CreateEmulator() const;
	void ReleaseEmulator(EMUHANDLE handle) const;
	int Init(EMUHANDLE handle, callBackfunctions_t funcs) const;
	int Load(EMUHANDLE handle, const SaveData_t *data) const;
	int LoadState(EMUHANDLE handle, const SaveData_t *state) const;
	bool InitGL(EMUHANDLE handle, int id) const;
	void DestroyGL(EMUHANDLE handle, int id) const;
	void ReleaseSaveData(EMUHANDLE handle, SaveData_t *data) const;

	const char *GetDescription(unsigned int *size) const;
	int IsCompatible(const char *filename) const;
	void Run(EMUHANDLE handle, int run) const;
	void Step(EMUHANDLE handle) const;
	int IsRunning(EMUHANDLE handle) const;

	int Tick(EMUHANDLE handle, uint32_t time) const;
	void Input(EMUHANDLE handle, int key, int pressed) const;
	void Draw(EMUHANDLE handle, int id) const;
	void Reshape(EMUHANDLE handle, int id, int width, int height, bool keepAspect) const;

	int Save(EMUHANDLE handle, SaveData_t* data) const;
	int SaveState(EMUHANDLE handle, SaveData_t *state) const;

	void InitAudio(EMUHANDLE handle, int source, unsigned int sampleRate, int channels)  const;
	void GetAudio(EMUHANDLE handle, int source, short *buffer, unsigned int samples)  const;

	// Debugging
	char Disassemble(EMUHANDLE handle, unsigned int pos, const char **raw, const char **instr)  const;
	void AddBreakpoint(EMUHANDLE handle, unsigned int pos)  const;
	void RemoveBreakpoint(EMUHANDLE handle, unsigned int pos)  const;
	int IsBreakpoint(EMUHANDLE handle, unsigned int pos)  const;
	char GetMemoryData(EMUHANDLE handle, int memory, unsigned int address)  const;
	std::string GetGdbTargetDescription()  const;
	std::string GetGdbMemoryMap() const;
	int GetMemoryPointer(EMUHANDLE handle, unsigned int addr, void **data, unsigned int *size)  const;
	void SetMemory(EMUHANDLE handle, uint32_t addr, uint8_t val) const;


	void NotifyOptionChange(EMUHANDLE handle, int optionId) const;

private:
	unsigned int GetXMLVal(EMUHANDLE handle, pugi::xml_node &node, const char *element) const;

	void *_hDLL;
	bool _valid;
	pugi::xml_document _description;
	pugi::xml_node _root;

	// DLL functions
	void(__stdcall *_initPlugin)();

	// Initialization functions
	EMUHANDLE( __stdcall *_createEmulator)();
	void(__stdcall *_releaseEmulator)(EMUHANDLE);
	int32_t(__stdcall *_init)(EMUHANDLE, callBackfunctions_t);
	int32_t(__stdcall *_load)(EMUHANDLE, const SaveData_t *);
	int32_t(__stdcall *_loadState)(EMUHANDLE, const SaveData_t *);
	uint32_t(__stdcall *_initGL)(EMUHANDLE, int32_t);
	void(__stdcall *_destroyGL)(EMUHANDLE, int32_t);


	// Misc functions
	int32_t(__stdcall *_getValI)(EMUHANDLE, int32_t);
	uint32_t(__stdcall *_getValU)(EMUHANDLE, int32_t);
	const uint8_t *(__stdcall *_getString)(EMUHANDLE, int32_t);
	float (__stdcall *_getFloat)(EMUHANDLE handle, int id);
	void(__stdcall *_setValI)(EMUHANDLE, int32_t, int32_t);
	void(__stdcall *_setValU)(EMUHANDLE, int32_t, uint32_t);
	const uint8_t *(__stdcall *_getDescription)(uint32_t *);
	int32_t(__stdcall *_isCompatible)(const uint8_t *);
	int32_t(__stdcall *_getInterfaceVersion)();
	void(__stdcall *_run)(EMUHANDLE, int32_t);
	void(__stdcall *_step)(EMUHANDLE);
	int32_t(__stdcall *_isRunning)(EMUHANDLE);
	void(__stdcall *_releaseSaveData)(EMUHANDLE, SaveData_t *);

	// Running functions
	int32_t(__stdcall *_tick)(EMUHANDLE, uint32_t);
	void(__stdcall *_input)(EMUHANDLE, int32_t, int32_t);
	void(__stdcall *_draw)(EMUHANDLE, int32_t);
	void(__stdcall *_reshape)(EMUHANDLE, int32_t, int32_t, int32_t, int32_t);

	// stopping functions
	int32_t(__stdcall *_save)(EMUHANDLE, SaveData_t *);
	int32_t(__stdcall *_saveState)(EMUHANDLE, SaveData_t *);

	// Audio functions
	void(__stdcall *_initAudio)(EMUHANDLE, int32_t, uint32_t, int32_t);
	void(__stdcall *_getAudio)(EMUHANDLE, int32_t, int16_t *, uint32_t);

	// Debugging functions
	uint8_t(__stdcall *_disassemble)(EMUHANDLE, uint32_t, const uint8_t **, const uint8_t **);
	void(__stdcall *_addBreakpoint)(EMUHANDLE, uint32_t);
	void(__stdcall *_removeBreakpoint)(EMUHANDLE, uint32_t);
	int32_t(__stdcall *_isBreakpoint)(EMUHANDLE, uint32_t);
	uint8_t(__stdcall *_getMemoryData)(EMUHANDLE, int32_t, uint32_t);
	const uint8_t * (__stdcall *_getGdbTargetDescription)(uint32_t *);
	const uint8_t * (__stdcall *_getGdbMemoryMap)(uint32_t *);
	int32_t(__stdcall *_getMemoryPointer)(EMUHANDLE, uint32_t, void **, uint32_t *);
	void (__stdcall *_setMemory)(EMUHANDLE, uint32_t, uint8_t);

	void(__stdcall *_notifyOptionChange)(EMUHANDLE, int32_t);
};

#endif //EMUALL_EMULATORINTERFACE_H
