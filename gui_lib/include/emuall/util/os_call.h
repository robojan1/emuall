//Boby Thomas pazheparampil - march 2006
#ifndef os_call_h
#define os_call_h
#include <string>

void *_GetStdcallFunc(void *lib, const char *name, int argSize);
std::string GetLastLibraryError();
void* LoadSharedLibrary(const char *pcDllname, int iMode = 2);
bool FreeSharedLibrary(void *hDLL);

inline void *GetStdcallFunc(void *lib, const char *name)
{
	return _GetStdcallFunc(lib, name, 0);
}

template<typename T1>
void *GetStdcallFunc(void *lib, const char *name)
{
	return _GetStdcallFunc(lib, name, sizeof(T1));
}

template<typename T1, typename T2>
void *GetStdcallFunc(void *lib, const char *name)
{
	return _GetStdcallFunc(lib, name, sizeof(T1) + sizeof(T2));
}

template<typename T1, typename T2, typename T3>
void *GetStdcallFunc(void *lib, const char *name)
{
	return _GetStdcallFunc(lib, name, sizeof(T1) + sizeof(T2) + sizeof(T3));
}

template<typename T1, typename T2, typename T3, typename T4>
void *GetStdcallFunc(void *lib, const char *name)
{
	return _GetStdcallFunc(lib, name, sizeof(T1) + sizeof(T2) + sizeof(T3) + sizeof(T4));
}

template<typename T1, typename T2, typename T3, typename T4, typename T5>
void *GetStdcallFunc(void *lib, const char *name)
{
	return _GetStdcallFunc(lib, name, sizeof(T1) + sizeof(T2) + sizeof(T3) + sizeof(T4) + sizeof(T5));
}

template<typename T1, typename T2, typename T3, typename T4, typename T5, typename T6>
void *GetStdcallFunc(void *lib, const char *name)
{
	return _GetStdcallFunc(lib, name, sizeof(T1) + sizeof(T2) + sizeof(T3) + sizeof(T4) + sizeof(T5) + sizeof(T6));
}

template<typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7>
void *GetStdcallFunc(void *lib, const char *name)
{
	return _GetStdcallFunc(lib, name, sizeof(T1) + sizeof(T2) + sizeof(T3) + sizeof(T4) + sizeof(T5) + sizeof(T6) + sizeof(T7));
}

template<typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8>
void *GetStdcallFunc(void *lib, const char *name)
{
	return _GetStdcallFunc(lib, name, sizeof(T1) + sizeof(T2) + sizeof(T3) + sizeof(T4) + sizeof(T5) + sizeof(T6) + sizeof(T7) + sizeof(T8));
}

template<typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9>
void *GetStdcallFunc(void *lib, const char *name)
{
	return _GetStdcallFunc(lib, name, sizeof(T1) + sizeof(T2) + sizeof(T3) + sizeof(T4) + sizeof(T5) + sizeof(T6) + sizeof(T7) + sizeof(T8) + sizeof(T9));
}

template<typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9, typename T10>
void *GetStdcallFunc(void *lib, const char *name)
{
	return _GetStdcallFunc(lib, name, sizeof(T1) + sizeof(T2) + sizeof(T3) + sizeof(T4) + sizeof(T5) + sizeof(T6) + sizeof(T7) + sizeof(T8) + sizeof(T9) + sizeof(T10));
}



#endif //os_call_h

